package com.nitrox.mo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nitrox.mo.model.Awards;
import com.nitrox.mo.model.CompanyInvolved;
import com.nitrox.mo.model.ConnectedCompany;
import com.nitrox.mo.model.ConnectedCountries;
import com.nitrox.mo.model.ConnectedIndividual;
import com.nitrox.mo.model.IndividualPhones;
import com.nitrox.mo.model.Education;
import com.nitrox.mo.model.Language;
import com.nitrox.mo.model.MarketSignal;
import com.nitrox.mo.model.Meetings;
import com.nitrox.mo.model.MeetingsIndividual;
import com.nitrox.mo.model.Nationalities;
import com.nitrox.mo.model.Individual;
import com.nitrox.mo.model.jsonobject.Id;
import com.nitrox.mo.model.jsonobject.ResponseObject;
import com.nitrox.mo.service.AwardsService;
import com.nitrox.mo.service.CompanyInvolvedService;
import com.nitrox.mo.service.ConnectedCompanyService;
import com.nitrox.mo.service.ConnectedCountriesService;
import com.nitrox.mo.service.ConnectedIndividualService;
import com.nitrox.mo.service.IndividualPhoneService;
import com.nitrox.mo.service.EducationService;
import com.nitrox.mo.service.IndividualService;
import com.nitrox.mo.service.LanguageService;
import com.nitrox.mo.service.MarketSignalService;
import com.nitrox.mo.service.MeetingsIndividualService;
import com.nitrox.mo.service.MeetingsService;
import com.nitrox.mo.service.NationalitiesService;
import com.nitrox.mo.service.UserService;
import com.nitrox.mo.service.helper.Download;

@Controller
@RequestMapping("/view")
public class ViewsController {

	@Autowired
	private IndividualService indService;
	
	@Autowired
	private LanguageService langService;
	
	@Autowired
	private NationalitiesService nationService;
	
	@Autowired
	private AwardsService awardsService;
	
	@Autowired
	private CompanyInvolvedService compService;
	
	@Autowired
	private IndividualPhoneService contactService;
	
	@Autowired
	private EducationService educService;
	
	@Autowired
	private MeetingsService meetService;
	
	@Autowired
	private MeetingsIndividualService meetIndService;
	
	@Autowired
	private MarketSignalService signalService;
	
	@Autowired
	private ConnectedCompanyService connCompService;
	
	@Autowired
	private ConnectedCountriesService connCountService;
	
	@Autowired
	private ConnectedIndividualService connIndService;
	
	@Autowired
	private UserService userService;
	
	@Resource(name="downloadService")
	private Download downloadService;
	
	/*views for individual*/
	
	@RequestMapping(value = {"/individual/all" }, method = RequestMethod.GET)
	public String page(HttpSession session, HttpServletRequest request, ModelMap model) {
		 
		String sessName = userService.hasSession(session, request);
		String sessType = userService.getType(session, request);
		if(!sessName.equals("invalid")){
			model.addAttribute("isAdmin", sessType);
			return "individual_list";
		}
		 return "index_logIn"; 
	}
	
	@RequestMapping(value = { "/individual","" }, method = RequestMethod.GET)
	public String ind(HttpSession session, HttpServletRequest request, ModelMap model) {
	    	
		String sessName = userService.hasSession(session, request);
		String sessType = userService.getType(session, request);
	    if(!sessName.equals("invalid")){
	    	model.addAttribute("user", sessName);
	    	model.addAttribute("page", "view/individual/all");
	    	model.addAttribute("isAdmin", sessType);
	      	return "index";
	    }
	    return "redirect:/";
	 }
	
	@RequestMapping(value = { "/individual/list" }, method = RequestMethod.GET)
    @ResponseBody
    public List<Individual> getIndList(HttpSession session, HttpServletRequest request) {
		
		String sessName = userService.hasSession(session, request);
		
		if(sessName.equals("invalid")){
			List<Individual> myList = new ArrayList<Individual>();
			return myList;
		}
		return indService.list();
	}
    
    @RequestMapping(value={"/ind_languages"}, method=RequestMethod.POST)
    @ResponseBody
    public List<Language> getLanguages(@ModelAttribute("langID") Id indID ){
    	return langService.list(indID.getId());
    }
    
    @RequestMapping(value={"/ind_nationalities"}, method=RequestMethod.POST)
    @ResponseBody
    public List<Nationalities> getNationalities(@ModelAttribute("nationID") Id nationID ){
    	return nationService.list(nationID.getId());
    }
    
    @RequestMapping(value={"/ind_awards"}, method=RequestMethod.POST)
    @ResponseBody
    public List<Awards> getAwards(@ModelAttribute("awardsID") Id awardsID ){
    	return awardsService.list(awardsID.getId());
    }
    
    @RequestMapping(value={"/ind_companies"}, method=RequestMethod.POST)
    @ResponseBody
    public List<CompanyInvolved> getCompanies(@ModelAttribute("awardsID") Id companiesID ){
    	return compService.listCompanies(companiesID.getId());
    }
    
    @RequestMapping(value={"/ind_phones"}, method=RequestMethod.POST)
    @ResponseBody
    public List<IndividualPhones> getPhones(@ModelAttribute("phoneID") Id phoneID ){
    	return contactService.list(phoneID.getId());
    }
    
    @RequestMapping(value={"/ind_contacts"}, method=RequestMethod.POST)
    @ResponseBody
    public Individual getContacts(@ModelAttribute("contactID") Id contactID ){
    	return indService.getIndividualById(contactID.getId());
    }
    
    @RequestMapping(value={"/ind_education"}, method=RequestMethod.POST)
    @ResponseBody
    public List<Education> getEduc(@ModelAttribute("educID") Id educID ){
    	return educService.list(educID.getId());
    }
    
    /*views for signal*/
    @RequestMapping(value = {"/signal/all" }, method = RequestMethod.GET)
	public String signalPage(HttpSession session, HttpServletRequest request, ModelMap model) {
		 
    	String sessName = userService.hasSession(session, request);
    	String sessType = userService.getType(session, request);
    	if(!sessName.equals("invalid")){
    		model.addAttribute("isAdmin", sessType);
    		return "marketSignal_list";
    	}
		 return "index_logIn";
	}
	
	@RequestMapping(value = { "/signal","" }, method = RequestMethod.GET)
	public String mSignal(HttpSession session, HttpServletRequest request, ModelMap model) {
	    	
		String sessName = userService.hasSession(session, request);
		String sessType = userService.getType(session, request);
		if(!sessName.equals("invalid")){
			model.addAttribute("user",sessName);
		 	model.addAttribute("page", "view/signal/all");
		 	model.addAttribute("isAdmin", sessType);
		 	return "index";
		}
		return "redirect:/";
	  }
	
	@RequestMapping(value = { "/signal/list" }, method = RequestMethod.GET)
    @ResponseBody
    public List<MarketSignal> getSignalList(HttpSession session, HttpServletRequest request) {
		
		String sessName = userService.hasSession(session, request);
		if(sessName.equals("invalid")){
			List<MarketSignal> myList = new ArrayList<MarketSignal>();
			return myList;
		}
		return signalService.list();
	}
	
	@RequestMapping(value={"/signal_companies"}, method=RequestMethod.POST)
	@ResponseBody
	public List<ConnectedCompany>  getConnComp(@ModelAttribute("conCompID") Id conCompID ){
	    return connCompService.list(conCompID.getId());
	}
	
	@RequestMapping(value={"/signal_countries"}, method=RequestMethod.POST)
	@ResponseBody
	public List<ConnectedCountries>  getConnCountries(@ModelAttribute("conCountID") Id conCountID ){
	    return connCountService.list(conCountID.getId());
	}
	
	@RequestMapping(value={"/signal_individual"}, method=RequestMethod.POST)
	@ResponseBody
	public List<ConnectedIndividual>  getConnIndividual(@ModelAttribute("conIndID") Id conIndID ){
	    return connIndService.list(conIndID.getId());
	}
	
	@RequestMapping(value={"/signal_Details"}, method=RequestMethod.POST)
	@ResponseBody
	public MarketSignal getDetails(@ModelAttribute("signalID") Id signalID ){
	    return signalService.getSignalId(signalID.getId());
	}
	
	 /*views for meeting*/
	 @RequestMapping(value = {"/meetings/all" }, method = RequestMethod.GET)
		public String meetingsPage(HttpSession session, HttpServletRequest request, ModelMap model) {
			 
	    	String sessName = userService.hasSession(session, request);
	    	String sessType = userService.getType(session, request);
	    	if(!sessName.equals("invalid")){
	    		model.addAttribute("isAdmin", sessType);
	    		return "meeting_list";
	    }
			 return "index_logIn";
	}
		
	@RequestMapping(value = { "/meetings","" }, method = RequestMethod.GET)
	public String meetSignal(HttpSession session, HttpServletRequest request, ModelMap model) {
	    	
		String sessName = userService.hasSession(session, request);
		String sessType = userService.getType(session, request);
		if(!sessName.equals("invalid")){
			model.addAttribute("user",sessName);
		 	model.addAttribute("page", "view/meetings/all");
		 	model.addAttribute("isAdmin", sessType);
		 	return "index";
		}
		return "redirect:/";
	}
//		
//	@RequestMapping(value = { "/exportIndividuals","" }, method = RequestMethod.GET)
//	public String exportIndividuals(HttpSession session, HttpServletRequest request, ModelMap model) {
//	    	
//		String sessName = userService.hasSession(session, request);
//		String sessType = userService.getType(session, request);
//		if(!sessName.equals("invalid")){
//			model.addAttribute("user",sessName);
//		 	model.addAttribute("page", "view/exportIndividuals/all");
//		 	model.addAttribute("isAdmin", sessType);
//		 	return "index";
//		}
//		return "redirect:/";
//	}
//	
//	@RequestMapping(value = {"/exportIndividuals/all" }, method = RequestMethod.GET)
//	public String exportIndividualPage(HttpSession session, HttpServletRequest request,ModelMap model) {
//		
//		String sessType = userService.getType(session, request);
//		String sessName = userService.hasSession(session, request);
//		
//		model.addAttribute("user", sessName);
//	   	model.addAttribute("isAdmin", sessType);
//		 return "exportIndividuals";
//	}
	@RequestMapping(value = { "/meetings/list" }, method = RequestMethod.GET)
    @ResponseBody
    public List<Meetings> getMeetingsList(HttpSession session, HttpServletRequest request) {
		
		String sessName = userService.hasSession(session, request);
		if(sessName.equals("invalid")){
			List<Meetings> myList = new ArrayList<Meetings>();
			return myList;
		}
		return meetService.list();
	}
	
	@RequestMapping(value={"/meeting_Details"}, method=RequestMethod.POST)
	@ResponseBody
	public Meetings getMeetingDetails(@ModelAttribute("signalID") Id meetingID ){
	    return meetService.getMeetingsById(meetingID.getId());
	}
	
	@RequestMapping(value={"/meeting_Individuals"}, method=RequestMethod.POST)
    @ResponseBody
    public List<MeetingsIndividual> getMeetInd(@ModelAttribute("meetIndID") Id meetingID ){
		
    	return meetIndService.list(meetingID.getId());
    }
	
	@RequestMapping(value= "/IndividualXLS", method = RequestMethod.GET)
	public void getIndXLS(HttpServletResponse response, Model model) throws ClassNotFoundException {
	 downloadService.downloadIndividualXLS(response);
	}
	
//	@RequestMapping(value= "/IndByCountryXLS", method = RequestMethod.GET)
//	public void getIndByCountryXLS(HttpServletResponse response, Model model) throws ClassNotFoundException {
//	 downloadService.downloadIndividualXLS(response);
//	}
//	
//	@RequestMapping(value= "/IndByCompanyXLS", method = RequestMethod.GET)
//	public void getIndByCompanyXLS(HttpServletResponse response, Model model) throws ClassNotFoundException {
//	 downloadService.downloadIndividualXLS(response);
//	}
	
	@RequestMapping(value= "/MarketSignalXLS", method = RequestMethod.GET)
	public void getMSXLS(HttpServletResponse response, Model model) throws ClassNotFoundException {
	 downloadService.downloadMarketSignalXLS(response);
	}
	
	@RequestMapping(value= "/MeetingXLS", method = RequestMethod.GET)
	public void getMeetingsXLS(HttpServletResponse response, Model model) throws ClassNotFoundException {
	 downloadService.downloadMeetingXLS(response);
	}
	
	@RequestMapping(value= "/CompanyXLS", method = RequestMethod.GET)
	public void getCompanyXLS(HttpServletResponse response, Model model) throws ClassNotFoundException {
	 downloadService.downloadCompanyXLS(response);
	}
	
	@RequestMapping(value= "/ConnIndXLS/{id}", method = RequestMethod.GET)
	public void getConnectedIndividualXLS(HttpServletResponse response, Id id) throws ClassNotFoundException {
	 downloadService.downloadIndividualConnectionsXLS(response, id.getId());
	}
	
	@RequestMapping(value= "/ConnCompXLS/{id}", method = RequestMethod.GET)
	public void getConnectedCompaniesXLS(HttpServletResponse response, Id id) throws ClassNotFoundException {
	 downloadService.downloadCompanyConnectionsXLS(response, id.getId());
	}
	
//	@RequestMapping(value= "/SignalDatesXLS/{dates}", method = RequestMethod.GET)
//	public void getSignalXLS(HttpServletResponse response, ResponseObject dates) throws ClassNotFoundException {
//		System.out.println(" -" + dates.message );
//	 //downloadService.downloadIndividualConnectionsXLS(response, id.getId());
//	}
	
//	@RequestMapping(value ="/SignalDatesXLS", method = RequestMethod.GET)
//    public void getSignalXLS(HttpServletResponse response, ResponseObject id) throws ClassNotFoundException {
//	    	
//		System.out.println(" ** " + id.getMessage());
//		 downloadService.downloadMeetingXLS(response);
//		
//	}
}
