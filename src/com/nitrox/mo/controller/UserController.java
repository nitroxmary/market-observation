package com.nitrox.mo.controller;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sun.misc.BASE64Decoder;

import com.nitrox.mo.model.User;
import com.nitrox.mo.model.jsonobject.ResponseObject;
import com.nitrox.mo.service.UserService;
import com.nitrox.mo.service.helper.Validation;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private Validation validator;
	
	@Autowired
	private MessageSource messageSource;
	

	@RequestMapping(value = { "/","" }, method = RequestMethod.GET)
	public String index(HttpSession session, HttpServletRequest request, ModelMap model) {
		
		model.addAttribute("page", "homepage");
		model.addAttribute("modal", "logIn");
	    	
	   	return "index_logIn";
	 }
	
	@RequestMapping(value = { "/logOut","" }, method = RequestMethod.GET)
	public String out(HttpSession session,HttpServletRequest request) {
		
		session.removeAttribute("userName");
		session.removeAttribute("userPass");
		request.getSession(false);
    	request.getSession().invalidate();
    		
	   	return "redirect:/";
	 }
	
	
	 @RequestMapping(value = {"/authUser","" }, method = RequestMethod.GET)
	 public String authUser(HttpSession session, HttpServletRequest request, ModelMap model) {

		    String sessName = userService.hasSession(session, request);
		    String sessType = userService.getType(session, request);
		  
		    if(!sessName.equals("invalid")){
		    	model.addAttribute("page", "homepage");
		    	model.addAttribute("user", sessName);
		    	model.addAttribute("isAdmin",sessType);
		    	return "index";
		    }
		    
		return "redirect:/";
	 }
	 
	@RequestMapping(value = {"/logIn" }, method = RequestMethod.GET)
	public String page(ModelMap model) {
		 
		 return "logIn";
	}
	
	@RequestMapping(value = { "/register","" }, method = RequestMethod.GET)
	public String register(HttpSession session, HttpServletRequest request, ModelMap model) {
		
		model.addAttribute("page", "homepage");
		model.addAttribute("modal", "registerUser");
	    	
	   	return "index_logIn";
	}
	
	@RequestMapping(value = {"/registerUser" }, method = RequestMethod.GET)
	public String register(ModelMap model) {
		 
		 return "register";
	}
	
	@RequestMapping(value="/sendUser", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject sendUser(User user){
		
		ResponseObject res = new ResponseObject();
		BASE64Decoder decoder = new BASE64Decoder();
		
		
		try{
	        User u = userService.getUser(user.getUname());
	        user.setPsword(new String(decoder.decodeBuffer(user.getPsword())));

			if(StringUtils.isEmpty(u)){
				
				if(StringUtils.isEmpty(user.getPsword())){
					res.setMessage(messageSource.getMessage("user.notSave", null, Locale.US));
		    		res.setOk(false);
				}else{
					userService.addUser(user);
		    		res.setMessage(messageSource.getMessage("user.save", null, Locale.US));
		    		res.setOk(true);
				}
			}else {
				
				res.setMessage(messageSource.getMessage("user.exist", null, Locale.US));
	    		res.setOk(false);
			}
						
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
    		e.printStackTrace();
    	}
		
		return res;
	}
	
	@RequestMapping(value="/admin", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject adminUser(@ModelAttribute(value="user")User user, HttpSession session, ModelMap model) throws IOException{
		
		ResponseObject res= new ResponseObject();
		BASE64Decoder decoder = new BASE64Decoder();
		String message= validator.validateUser(user.getUname(), new String(decoder.decodeBuffer(user.getPsword())));
		
		try{
		   if(message.equals("admin")){
			   removeAttr(session);
			   User usr = userService.getUser(user.getUname());
			   session.setAttribute("userName", usr.getUname());
			   session.setAttribute("userPass", usr.getPsword());
			   session.setAttribute("userType", message);
			   res.setOk(true);
		  }else if(message.equals("user")){
			   removeAttr(session);
			   User usr = userService.getUser(user.getUname());
			   session.setAttribute("userName", usr.getUname());
			   session.setAttribute("userPass", usr.getPsword());
			   session.setAttribute("userType", message);
			   res.setOk(true);
		  }else{
			res.setMessage(messageSource.getMessage(message, null, Locale.US));
 	    }
		}catch(Exception e){
			res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
	     	e.printStackTrace();
    	}
		
	return res;
	}
	
//	private String createSessionCookie(User user){
//    	//cleanup cookies
//    	cookieService.deleteSessionCookies(client);
//    	
//    	SessionCookie cookie= new SessionCookie();
//    	cookie.setClient(client);
//		   while(true){
//			   cookie.setSessionString(UUID.randomUUID().toString());
//			   if(validator.validateSessionCookie(cookie).equals("valid")){
//				   break;
//			   }
// 		   }
//		
//		cookieService.saveSessionCookie(cookie);
//		return cookie.getSessionString();
//    }
	
	private void removeAttr(HttpSession session){
		
    	if(!StringUtils.isEmpty(session.getAttribute("user")))
    		session.removeAttribute("user");
    }
	
}
