package com.nitrox.mo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeController {


	 @RequestMapping(value = {"/homepage" }, method = RequestMethod.GET)
	 public String homepage() {
	    return "home";
	 }
	 
	 @RequestMapping(value = {"/","" }, method = RequestMethod.GET)
	 public String index(HttpSession session, HttpServletRequest request, ModelMap model) {

	    	model.addAttribute("page", "homepage");
	    	
	    	return "index_logIn";
	 }
	 
	
	 
}
