package com.nitrox.mo.controller;

import java.io.BufferedOutputStream;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nitrox.mo.model.jsonobject.Id;
import com.nitrox.mo.model.jsonobject.ResponseObject;
import com.nitrox.mo.service.*;
import com.nitrox.mo.model.*;

@Controller
@RequestMapping("/")
public class MarketDataController {
	
		@Autowired
		private CountryService countryService;
		
		@Autowired
		private CompanyService companyService;
		
		@Autowired
		private ContactDetailsService contactService;
		
		@Autowired
		private CompanyPhonesService companyPhonesService;
		
		@Autowired
		private CompanyFilesService companyFilesService;
		
		@Autowired
		private CompanyALMSellerService companyALMSellerService;
		
		@Autowired
		private CompanyALMProviderService companyALMProviderService;
		
		@Autowired
		private CompanyShareholdersService companyShareHolderService;
		
		@Autowired
		private PhoneService phoneService;
		
		@Autowired
		private UserService userService;
		
		@Autowired
		private MessageSource messageSource;
		
		/*------------------------------------------------------------------------------------------------------------------------------------ */
		/*COUNTRY */
		
		@RequestMapping(value = { "/country/countryCode" }, method = RequestMethod.GET)
	    @ResponseBody
	    public List<Countries> getCountryCodes() {
			return countryService.list();
		}
		
		
		@RequestMapping(value = "/country/list", method = RequestMethod.GET)
		@ResponseBody
		public List<Countries> listCountries() {
			return countryService.nameList();
		}
		
		/*------------------------------------------------------------------------------------------------------------------------------------ */
		/*COMPANY */
		
		@RequestMapping(value="/company/viewFile/{fileID}", method = RequestMethod.GET)
		@ResponseBody
	    public String viewFile(@PathVariable("fileID") Integer fileID,  HttpServletResponse response, HttpSession session, ModelMap model, HttpServletRequest request) {
	    	CompanyFiles compFiles= companyFilesService.getCompanyFilesId(fileID);
	    	try{
	    		ServletOutputStream out = response.getOutputStream();
	    		BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(out);
       	        response.setContentLength(compFiles.getFileData().length);

       	        response.setContentType("application/"+compFiles.getFileExt());
       	        response.setHeader("Content-Disposition", "inline; filename=" + compFiles.getFileName());
       	        response.setHeader("Cache-Control", "cache, must-revalidate");
       	        response.setHeader("Pragma", "public");
       	        
    	        bufferedOutputStream.write(compFiles.getFileData());

	//	    	} else{
	//      		  model.addAttribute("errorMessage", "access.noauth");
	//      		  return "error";
	//      	  }
	        } catch (Exception e) {
	      	  model.addAttribute("errorMessage", "login.nologin");
	            e.printStackTrace();
	  		  return "error";
	        }
        
         return null;
	    }
		
		
		
		@RequestMapping(value="/company/downloadFile/{fileID}", method = RequestMethod.GET)
		@ResponseBody
	    public String downloadFile(@PathVariable("fileID") Integer fileID,  HttpServletResponse response, HttpSession session, ModelMap model, HttpServletRequest request) {
	    	CompanyFiles compFiles= companyFilesService.getCompanyFilesId(fileID);
	    	try{
	    		 String mimetype = session.getServletContext().getMimeType(compFiles.getFileName()+"."+compFiles.getFileExt());
	        	 response.setContentType(mimetype);
	        	 response.setContentLength(compFiles.getFileData().length );
	
	        	 response.setHeader("Content-Disposition", "attachment; filename=\"" + compFiles.getFileName()+"."+compFiles.getFileExt() + "\"");
	        	 FileCopyUtils.copy(compFiles.getFileData(), response.getOutputStream());

	//	    	} else{
	//      		  model.addAttribute("errorMessage", "access.noauth");
	//      		  return "error";
	//      	  }
	        } catch (Exception e) {
	      	  model.addAttribute("errorMessage", "login.nologin");
	            e.printStackTrace();
	  		  return "error";
	        }
        
	    	return null;
		}
		
		@RequestMapping(value="/company/addCompFiles", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject newCompany(@ModelAttribute(value="companyFiles") @Valid CompanyFiles companyFiles,int companyID) {
	    	ResponseObject res= new ResponseObject();
	    	
	    	try{
	    		
	    		companyFiles.setCompany(companyService.getCompanyId(companyID));
	    	    boolean hasFile = StringUtils.isEmpty(companyFiles);
	    		
				if(!hasFile){
					companyFiles.setFileData(companyFiles.getFile().getBytes());
					companyFilesService.addCompanyFiles(companyFiles);
				}
	    		
	    		res.setOk(true);
	    		
	    	}catch(Exception e){
	    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
	    		e.printStackTrace();
	    	}
	    	
	    	
	        return res;
	    }
		

		@RequestMapping(value="/company/addCompShares", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject newShareHolders(@ModelAttribute(value="addCompShares") @Valid CompanyShareholders companyShareHolders,int companyID) {
	    	ResponseObject res= new ResponseObject();
	    	
	    	try{
	    		
	    		companyShareHolders.setCompany(companyService.getCompanyId(companyID));
	    	    companyShareHolderService.addCompanyShareholders(companyShareHolders);
				
	    		res.setOk(true);
	    		
	    	}catch(Exception e){
	    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
	    		e.printStackTrace();
	    	}
	    	
	    	
	        return res;
	    }
		
		@RequestMapping(value="/company/updateCompShares", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject updateShareHolders(@ModelAttribute(value="updateCompShares") @Valid CompanyShareholders companyShareHolders,int companyID) {
	    	ResponseObject res= new ResponseObject();
	    	
	    	try{
	    		
	    		companyShareHolders.setCompany(companyService.getCompanyId(companyID));
	    	    companyShareHolderService.updateCompanyShareholders(companyShareHolders);
				
	    		res.setOk(true);
	    		
	    	}catch(Exception e){
	    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
	    		e.printStackTrace();
	    	}
	    	
	    	
	        return res;
	    }
		 
		@RequestMapping(value="/company/updateCompanyContact", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject updateCompanyContact(HttpSession session, HttpServletRequest request,ContactDetails compContact, int companyID) {
	    	
			ResponseObject res= new ResponseObject();
			Company comp = companyService.getCompanyId(companyID);
			
			int compContactId = compContact.getId();
			if(compContactId == 0){
				contactService.addContactDetails(compContact);
				comp.setContact(contactService.getContactDetailsById(compContact.getId()));
				companyService.updateCompany(comp);
			}else{
				contactService.updateContactDetails(compContact);
			}
			    		
     		res.setMessage(messageSource.getMessage("company.save", null, Locale.US));
    		res.setOk(true);
    		res.setId(comp.getId());
    		
	    	return res;
	    }
		
		
		@RequestMapping(value="/company/updateCompanyDetails", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject updateCompanyDetails(HttpSession session, HttpServletRequest request,Company company, long inputdate){
	    	
			ResponseObject res= new ResponseObject();
			java.sql.Date iDate = new Date(inputdate);
			java.util.Date date= new java.util.Date();
	    	String sessName = userService.hasSession(session, request);
	    	int contactID = company.getContact().getId();
	    	
	    	company.setInputDate(iDate);
	    	company.setUpdated_by(sessName);
	    	company.setUpdated_date(new Timestamp(date.getTime()));
	    	System.out.println(" sdf" + company.getContact().getId());
	    	
	    	if(contactID != 0){
	    		company.setContact(contactService.getContactDetailsById(contactID));
	    	}else{
	    		company.setContact(null);
	    	}
	    	
		    
    		companyService.updateCompany(company);
    		
     		res.setMessage(messageSource.getMessage("company.save", null, Locale.US));
    		res.setOk(true);
    		res.setId(company.getId());
    		
	    	return res;
	    }
				
		@RequestMapping(value = { "/company/viewCompFiles" }, method = RequestMethod.GET)
	    @ResponseBody
	    public List<CompanyFiles> getCompanyFiles(int id) {
			return companyFilesService.list(id);
		}
		
		@RequestMapping(value = { "/company/viewALMSeller" }, method = RequestMethod.GET)
	    @ResponseBody
	    public List<CompanyALMSeller> getCompanyALMSeller(int id) {
			return companyALMSellerService.list(id);
		}
		
		@RequestMapping(value = { "/company/viewALMProvider" }, method = RequestMethod.GET)
	    @ResponseBody
	    public CompanyALMProvider getCompanyALMProvider(int id) {
			return companyALMProviderService.getCompanyId(id);
		}
		
		@RequestMapping(value = { "/company/getCompany" }, method = RequestMethod.GET)
	    @ResponseBody
		public Company getCompany(int id) {
			return companyService.getCompanyId(id);
		}
		
		
		@RequestMapping(value = { "/company/viewCompShares" }, method = RequestMethod.GET)
	    @ResponseBody
	    public List<CompanyShareholders> getCompanyShares(int id) {
			return companyShareHolderService.list(id);
		}
		
	    @RequestMapping(value={"company/phones"}, method=RequestMethod.POST)
	    @ResponseBody
	    public List<CompanyPhone> getPhones(@ModelAttribute("phoneID") Id phoneID ){
	    	return companyPhonesService.nameList(phoneID.getId());
	    }
	   
		public String saveCompany(HttpSession session, HttpServletRequest request,Company company,int contactID){
			
			java.util.Date date= new java.util.Date();
			String sessName = userService.hasSession(session, request); 
			boolean isOK = false;
			int id = 0;
			String msg = "Company name is an empty string..";
			
			if(company.getName() == ""){
				isOK = true;
	    	}else{
		    	try{
		    		
		    		if(contactID != 0){
		    			company.setContact(contactService.getContactDetailsById(contactID));
		    		}
		    		company.setInputBy(sessName);
		    		company.setInputDate(new Date(date.getTime()));
		    		company.setUpdated_by(sessName);
		    		company.setUpdated_date(new Timestamp(date.getTime()));
		    		company.setIsActive(true);
		    		companyService.addCompany(company);
		    		
		    		
		    		msg = "Data successfully saved!";
		    		isOK = true;
		    		id = company.getId();
		    		
		    		
		    		System.out.println(" asfas  " + msg);
		    	}catch(Exception e){
		    		
		    		msg = "An error has occured while trying to access the page.";
		    		e.printStackTrace();
		    	}
	    	}
			
			return isOK + "_"+id+"_"+msg;
		}
		
		@RequestMapping(value= "/company/add", method = RequestMethod.POST)
	    @ResponseBody
	    public ResponseObject createCompany(@ModelAttribute(value="company") @Valid Company company,int contactID, HttpSession session, HttpServletRequest request) {
	    	
			ResponseObject res= new ResponseObject();
              
			String details = this.saveCompany(session,request,company,contactID);
			String[] splitDetails = details.split("_");
	    	
	    		res.setMessage(splitDetails[2]);
	    		res.setOk(Boolean.valueOf(splitDetails[0]));
				res.setId(Integer.parseInt(splitDetails[1]));
				
	    	return res;
	    }
		
		
		@RequestMapping(value="/company/addAlmSeller", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject newALMSeller(@ModelAttribute(value="addALMSeller") @Valid CompanyALMSeller companyALMSeller,int companyID) {
	    	ResponseObject res= new ResponseObject();
	    	
	    	try{
	    		
	    		companyALMSeller.setCompany(companyService.getCompanyId(companyID));
	    		companyALMSellerService.addCompany(companyALMSeller);
				
	    		res.setOk(true);
	    		
	    	}catch(Exception e){
	    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
	    		e.printStackTrace();
	    	}
	    	
	    	
	        return res;
	    }
		
		@RequestMapping(value="/company/addAlmProvider", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject newALMprovider(@ModelAttribute(value="addALMProvider") @Valid CompanyALMProvider companyALMprovider, int providerID,int companyID) {
	    	ResponseObject res= new ResponseObject();
	    	
	    	try{
	    		
	    		companyALMprovider.setAlmProvider(companyService.getCompanyId(providerID));
	    		companyALMprovider.setCompany(companyService.getCompanyId(companyID));
	    		companyALMProviderService.addCompany(companyALMprovider);
				
	    		res.setOk(true);
	    		
	    	}catch(Exception e){
	    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
	    		e.printStackTrace();
	    	}
	    	
	    	
	        return res;
	    }
		
		@RequestMapping(value="/company/update", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject editCompany(@ModelAttribute(value="company") @Valid Company company) {
	    	ResponseObject res= new ResponseObject();
	    	try{
	    		companyService.updateCompany(company);
	    		res.setMessage(messageSource.getMessage("company.save", null, Locale.US));
	    		res.setOk(true);
	    		
	    	}catch(Exception e){
	    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
	    		e.printStackTrace();
	    	}
	    	
	        return res;
	    }
		
		@RequestMapping(value={"/company/delete"}, method=RequestMethod.POST)
	    @ResponseBody
	    public ResponseObject deleteCompany(@ModelAttribute("companyId")Id companyId, HttpSession session, HttpServletRequest request){
	    	ResponseObject res= new ResponseObject();
	    	java.util.Date date= new java.util.Date();
			String sessName = userService.hasSession(session, request);
	    	try{
	    		if(deleteCompShares(companyId) && deleteCompFiles(companyId) && deleteCompPhones(companyId)){

		    		Company company= companyService.getCompanyId(companyId.getId());
		    		if(company.getContact() != null){
		    			this.deleteCompContacts(companyId);
		    		}
		    		company.setUpdated_by(sessName);
		    		company.setUpdated_date(new Timestamp(date.getTime()));
		    		company.setIsActive(false);
		    		companyService.updateCompany(company);
	    		}
	    		res.setOk(true);
	    		res.setMessage(messageSource.getMessage("data.delete", null, Locale.US));
	    	}catch(Exception e){
	    		res.setMessage(messageSource.getMessage("validation.notexists", null, Locale.US));
	    		e.printStackTrace();
	    	}
	    	
	        return res;
	    }
		 
		public boolean deleteCompShares(@ModelAttribute("CompId") Id CompId ){
			boolean ok = false;
	    	
	    	try{
	    		List<CompanyShareholders> compShares = companyShareHolderService.list(CompId.getId());
	    		
	    		for(int i = 0; i < compShares.size(); i++){
	    			companyShareHolderService.removeCompanyShareholders(compShares.get(i));
	    		}
	    		ok = true;
	    	}catch(Exception e){
	    		ok = false;
	    	}
	    	
	        return ok;
		}
		
		public boolean deleteCompFiles(@ModelAttribute("CompId") Id CompId ){
			boolean ok = false;
	    	
	    	try{
	    		List<CompanyFiles> compFiles = companyFilesService.list(CompId.getId());
	    		
	    		for(int i = 0; i < compFiles.size(); i++){
	    			companyFilesService.removeCompanyFiles(compFiles.get(i));
	    		}
	    		ok = true;
	    	}catch(Exception e){
	    		ok = false;
	    	}
	    	
	        return ok;
		}
		
		public boolean deleteCompContacts(@ModelAttribute("CompId") Id CompId ){
			boolean ok = false;
	    	
	    	try{
	    	      ContactDetails compConDetails = contactService.getContactDetailsById(CompId.getId());
	    		
	    	      contactService.removeContactDetails(compConDetails);
	    		  ok = true;
	    	}catch(Exception e){
	    		ok = false;
	    	}
	    	
	        return ok;
		}
		
		public boolean deleteCompPhones(@ModelAttribute("CompId") Id CompId ){
			boolean ok = false;
	    	
	    	try{
	    		List<CompanyPhone> cphone = companyPhonesService.nameList(CompId.getId());
	    		
	    		for(int i = 0; i < cphone.size(); i++){
	    			Phone phone = phoneService.getPhoneById(cphone.get(i).getId());
	    			
	    			companyPhonesService.removeCompanyPhones(cphone.get(i));
	    			phoneService.removePhone(phone);
	    		}
	    		ok = true;
	    	}catch(Exception e){
	    		ok = false;
	    	}
	    	
	        return ok;
		}
		
		@RequestMapping(value = {"/companies" }, method = RequestMethod.GET)
		public String companies(HttpSession session, HttpServletRequest request,ModelMap model) {
			
			String sessType = userService.getType(session, request);
			String sessName = userService.hasSession(session, request);
			
			model.addAttribute("user", sessName);
		   	model.addAttribute("isAdmin", sessType);
			 return "company";
		}
		
		@RequestMapping(value = {"/company" }, method = RequestMethod.GET)
		public String index(HttpSession session, HttpServletRequest request, ModelMap model) {
		    	
			String sessName = userService.hasSession(session, request);
			String sessType = userService.getType(session, request);
			
			if(!sessName.equals("invalid")){
			   	model.addAttribute("page", "/companies");
			   	model.addAttribute("user", sessName);
			   	model.addAttribute("isAdmin", sessType);
			   
			   	return "index";
		     }
		    	
			return "redirect:/";
		 }
		
		@RequestMapping(value = {"/addComp" }, method = RequestMethod.GET)
		public String addComp() {
			 
			 return "newCompany";
		}
		
		
		@RequestMapping(value = {"/company/new" }, method = RequestMethod.GET)
		public String newComp(HttpSession session, HttpServletRequest request, ModelMap model) {
			 
			String sessType = userService.getType(session, request);
			String sessName = userService.hasSession(session, request);
			    	
			if(!sessName.equals("invalid")){
			   	model.addAttribute("page", "/addComp");
			   	model.addAttribute("user", sessName);
			   	model.addAttribute("isAdmin", sessType);

			   	return "index";
		     }
		    	
			return "redirect:/";
		}
			
		/*------------------------------------------------------------------------------------------------------------------------------------ */
		/*CONTACT */
		
		@RequestMapping(value= "/contact/add", method = RequestMethod.POST)
	    @ResponseBody
	    public ResponseObject createContact(@ModelAttribute(value="contact") @Valid ContactDetails contact, HttpSession session) {
	    	ResponseObject res= new ResponseObject();
	    
	    	int count = 0;
	    	
	    	if(contact.getAddress1() == ""){count++;}
	    	if(contact.getAddress2() == ""){count++;}
	    	if(contact.getAddress3() == ""){count++;}
	    	if(contact.getEmail() == ""){count++;}
	    	if(contact.getLinkedIn() == ""){count++;}
	    	if(contact.getTwitter() == ""){count++;}
	    	if(contact.getYoutube() == ""){count++;}
	    	if(contact.getSkype() == ""){count++;}
	    	
	       if(count == 8){
	    		res.setMessage(messageSource.getMessage("contact.empty", null, Locale.US));
	    		res.setOk(true);
				res.setId(0);
	    	}else{
	    		try{
		    		contactService.addContactDetails(contact);
		    		res.setMessage(messageSource.getMessage("data.save", null, Locale.US));
		    		res.setOk(true);
					res.setId(contact.getId());
				}catch(Exception e){
		    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
		    		e.printStackTrace();
		    	}
	    	}
	    	
	    	return res;
	    }
		
		
		/*------------------------------------------------------------------------------------------------------------------------------------ */
		/*PHONE */
		
		@RequestMapping(value= "/phone/add", method = RequestMethod.POST)
	    @ResponseBody
	    public ResponseObject createPhone(@ModelAttribute(value="phone") @Valid Phone phone, HttpSession session, int companyID) {
	    	
			ResponseObject res= new ResponseObject();
	    	CompanyPhone cp = new CompanyPhone();
				    	
	    	if(phone.getNumber() == ""){
	    		res.setMessage(messageSource.getMessage("phone.empty", null, Locale.US));
	    		res.setOk(true);
				res.setId(0);
	    	}else{
		    	try{
		    		phoneService.addPhone(phone);
		    		
		    		cp.setCompany(companyService.getCompanyId(companyID));
		    		cp.setPhone(phoneService.getPhoneById(phone.getId()));
		    		
		    		companyPhonesService.addCompanyPhones(cp);
		    		
		    		res.setMessage(messageSource.getMessage("data.save", null, Locale.US));
		    		res.setOk(true);
					res.setId(phone.getId());
					
				}catch(Exception e){
		    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
		    		e.printStackTrace();
		    	}
	    	}
	    	
	        return res;
	    }
		
		@RequestMapping(value= "/phone/updatePhone", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject updateNumbers(HttpSession session, HttpServletRequest request, String[] numbers,int id) {
	    	
			ResponseObject res= new ResponseObject();
	    	
			res.setOk(this.saveContacts(numbers, id));
			this.updateComp(session,request,id);
			
	    	return res;
	    }
		
		public boolean saveContacts(String[] connections, int compID){
			
			boolean isSave = false;
			boolean noNumber = false;
			int[] arrayInt = new int[connections.length];
			int[] isTrue = new int[connections.length];
			
			Phone phone = new Phone();
			CompanyPhone cp = new CompanyPhone();
			
			for(int i = 0; i < connections.length; i++){

				String[] splitData = connections[i].split("-");
				int aID = Integer.parseInt(splitData[5]);
				
				
				if(splitData.length == 6){
					
					if(splitData[3].equals("0") && splitData[4].equals("0")){
						noNumber = true;
					}
									
					phone.setPhoneType(splitData[1]);
					phone.setCountry(countryService.getCountryById(Integer.parseInt(splitData[2])));
					phone.setAreaCode(splitData[3]);
					phone.setNumber(splitData[4]);
					
					if(splitData[5].equals("0") && noNumber == false && !splitData[2].equals("0")){
						phoneService.addPhone(phone);
						
						cp.setPhone(phoneService.getPhoneById(phone.getId()));
						cp.setCompany(companyService.getCompanyId(compID));
						companyPhonesService.addCompanyPhones(cp);
					}else if(!splitData[5].equals("0") && noNumber == false  && !splitData[2].equals("0")){
						
						phone.setId(aID);
						phoneService.updatePhone(phone);
						
					}else if(!splitData[5].equals("0") && noNumber == true){
						phone.setId(aID);
						CompanyPhone cPhones = companyPhonesService.getCompanyPhonesId(aID);
						
						companyPhonesService.removeCompanyPhones(cPhones);
						phoneService.removePhone(phone);
					}
					
					isTrue[i] = 1;
					
				}else if(splitData[4].equals("")){
					if(!splitData[5].equals("0")){
						phoneService.removePhone(phoneService.getPhoneById(aID));
					}
					isTrue[i] = 1;
				}else{
					isTrue[i] = 0;
				}
				
				arrayInt[i] = aID;
				
			}
		
			if(!this.contains(isTrue,0)){
				isSave = true;
			}else{
				isSave = false;
			}
			
			return isSave;
			
		}
		
		@RequestMapping(value="/updateComp", method = RequestMethod.POST)
		@ResponseBody
	    public ResponseObject updateComp(HttpSession session, HttpServletRequest request,int id) {
	    	
			ResponseObject res= new ResponseObject();
	    	Company comp = companyService.getCompanyId(id);
	    	java.util.Date date= new java.util.Date();
			String sessName = userService.hasSession(session, request);    

	    	try{
	    		comp.setUpdated_by(sessName);
	    		comp.setUpdated_date(new Timestamp(date.getTime()));
	    		companyService.updateCompany(comp);
	    		res.setMessage(messageSource.getMessage("country.save", null, Locale.US));
	    		res.setOk(true);
	    		
	    	}catch(Exception e){
	    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
	    		e.printStackTrace();
	    	}
	    	
	    	return res;
	    }
		
		 public boolean contains(int[] arr, int item) {
		      for (int n : arr) {
		         if (item == n) {
		            return true;
		         }
		      }
		      return false;
		  }
		
}
