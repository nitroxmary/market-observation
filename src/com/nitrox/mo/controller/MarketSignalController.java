package com.nitrox.mo.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nitrox.mo.model.ConnectedCompany;
import com.nitrox.mo.model.ConnectedCountries;
import com.nitrox.mo.model.ConnectedIndividual;
import com.nitrox.mo.model.Countries;
import com.nitrox.mo.model.Company;
import com.nitrox.mo.model.Individual;
import com.nitrox.mo.model.MarketSignal;
import com.nitrox.mo.model.jsonobject.Id;
import com.nitrox.mo.model.jsonobject.ResponseObject;
import com.nitrox.mo.service.CompanyService;
import com.nitrox.mo.service.ConnectedCompanyService;
import com.nitrox.mo.service.ConnectedCountriesService;
import com.nitrox.mo.service.ConnectedIndividualService;
import com.nitrox.mo.service.CountryService;
import com.nitrox.mo.service.IndividualService;
import com.nitrox.mo.service.MarketSignalService;
import com.nitrox.mo.service.UserService;

@Controller
@RequestMapping("/")
public class MarketSignalController {

	@Autowired
	private MarketSignalService market_signalService;
	
	@Autowired
	private ConnectedIndividualService connected_individualService;
	
	@Autowired
	private ConnectedCountriesService connected_countriesService;
	
	@Autowired
	private ConnectedCompanyService connected_companyService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private IndividualService indService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MessageSource messageSource;
	
	
	@RequestMapping(value = { "/marketSignal/countryList" }, method = RequestMethod.GET)
    @ResponseBody
    public List<Countries> getCountryList() {
	    return countryService.nameList();
	}
	
	@RequestMapping(value = { "/marketSignal/companyList" }, method = RequestMethod.GET)
    @ResponseBody
    public List<Company> getCompanyList() {
		return companyService.nameList();
	}
	
	@RequestMapping(value = { "/marketSignal/individualList" }, method = RequestMethod.GET)
    @ResponseBody
    public List<Individual> getIndividualList() {
		return indService.nameList();
	}
	 
	@RequestMapping(value = "/marketSignal/updateCountries" , method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateCountry(String[] countries,int id,HttpSession session, HttpServletRequest request) {
    	
		ResponseObject res= new ResponseObject();
		
		res.setOk(this.saveCountries(countries, id));
		this.updateSignal(session,request,id);
		
    	return res;
    }
	
	@RequestMapping(value="/marketSignal/updateIndividual", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateInd(String[] individual,int id,HttpSession session, HttpServletRequest request) {
    	
		ResponseObject res= new ResponseObject();
		
	    res.setOk(this.saveIndividual(individual, id));
	    this.updateSignal(session,request,id);
   
    	return res;
    }
	
	@RequestMapping(value="/marketSignal/updateCompanies", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateCompany(String[] company,int id,HttpSession session, HttpServletRequest request) {
    	
		ResponseObject res= new ResponseObject();
    	
	    res.setOk(this.saveCompanies(company, id));
	    this.updateSignal(session,request,id);
	    
    	return res;
    }
	
	@RequestMapping(value="/updatedSignal", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateSignal(HttpSession session, HttpServletRequest request,int id) {
    	
		ResponseObject res= new ResponseObject();
    	MarketSignal mSignal = market_signalService.getSignalId(id);
    	java.util.Date date= new java.util.Date();
    	String sessName = userService.hasSession(session, request);  
    	
    	try{
    		mSignal.setUpdated_by(sessName);
    		mSignal.setUpdated_date(new Timestamp(date.getTime()));
    		market_signalService.updateMarketSignal(mSignal);
    		res.setMessage(messageSource.getMessage("signal.save", null, Locale.US));
    		res.setOk(true);
    		
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
    		e.printStackTrace();
    	}
    	
    	return res;
    }
	
	@RequestMapping(value = {"/marketSignal" }, method = RequestMethod.GET)
	public String marketSignal(HttpSession session, HttpServletRequest request, ModelMap model) {
		
		String sessName = userService.hasSession(session, request);
		
		if(!sessName.equals("invalid")){
			model.addAttribute("countryList", countryService.nameList());
			return "marketSignal";
		}
	    return "index_logIn";
	}
	
	@RequestMapping(value = {"/signal","" })
	public String index(HttpSession session, HttpServletRequest request, ModelMap model) {
	    	
		String sessName = userService.hasSession(session, request);    
		String sessType = userService.getType(session, request);
		if(!sessName.equals("invalid")){
			model.addAttribute("page", "marketSignal");
			model.addAttribute("user", sessName);
			model.addAttribute("isAdmin", sessType);
			return "index";
		}
		model.addAttribute("page", "homepage");
	   	return "index_logIn";
	  }
	
	@RequestMapping(value={"/marketSignal/delete"}, method=RequestMethod.POST)
    @ResponseBody
    public ResponseObject deleteSignal(@ModelAttribute("signalId") Id signalId ){
    	ResponseObject res= new ResponseObject();
    	
    	try{
    		if(deleteConnComp(signalId) && deleteConnInd(signalId) && deleteConnCountry(signalId)){
    			MarketSignal ms = market_signalService.getSignalId(signalId.getId());
    			market_signalService.removeMarketSignal(ms);
    		}
    		res.setOk(true);
    		res.setMessage(messageSource.getMessage("data.delete", null, Locale.US));
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("validation.notexists", null, Locale.US));
    		e.printStackTrace();
    	}
    	
        return res;
    }
	
	public boolean deleteConnComp(@ModelAttribute("signalId") Id signalId ){
		boolean ok = false;
    	
    	try{
    		List<ConnectedCompany> connComp = connected_companyService.list(signalId.getId());
    		
    		for(int i = 0; i < connComp.size(); i++){
    			connected_companyService.removeConnectedCompany(connComp.get(i));
    		}
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	public boolean deleteConnInd(@ModelAttribute("signalId") Id signalId ){
    
		boolean ok = false;
    	try{
    		List<ConnectedIndividual> connInd = connected_individualService.list(signalId.getId());
    		
    		for(int i = 0; i < connInd.size(); i++){
    			connected_individualService.removeConnectedIndividual(connInd.get(i));
    		}
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	public boolean deleteConnCountry(@ModelAttribute("signalId") Id signalId ){
    
		boolean ok = false;
    	try{
    		List<ConnectedCountries> connCountry = connected_countriesService.list(signalId.getId());
    		
    		for(int i = 0; i < connCountry.size(); i++){
    			connected_countriesService.removeConnectedCountries(connCountry.get(i));
    		}
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	@RequestMapping(value= "/marketSignal/updateDetails1", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject updateDetails(int id, String signalDate,String factor, String reportDate, String link, String origTitle,String transTitle,
	           int company, String overview, String background, String implication, long inputDate, String inputBy,HttpSession session, HttpServletRequest request) throws ParseException {
    	
		ResponseObject res= new ResponseObject();
		MarketSignal sig = new MarketSignal();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		java.util.Date utilDate1 = sdf.parse(signalDate + " 00:00:00");
		java.util.Date utilDate2 = sdf.parse(reportDate + " 00:00:00");
		java.sql.Date sDate = new Date(utilDate1.getTime());
		java.sql.Date rDate = new Date(utilDate2.getTime());
		java.sql.Date iDate = new Date(inputDate);
		java.util.Date date= new java.util.Date();
		String sessName = userService.hasSession(session, request);    

    	try{
    		sig.setId(id);
    		sig.setFactor(factor);
    		sig.setLink(link);
    		sig.setOrigTitle(origTitle);
    		sig.setTransTitle(transTitle);
    		sig.setCompany(companyService.getCompanyId(company));
    		sig.setOverview(overview);
    		sig.setBackground(background);
    		sig.setImplication(implication);
    		sig.setSignalDate(sDate);
    		sig.setReportDate(rDate);
    		sig.setInputBy(inputBy);
    		sig.setInputDate(iDate);
    		sig.setUpdated_by(sessName);
    		sig.setUpdated_date(new Timestamp(date.getTime()));
    		
    		market_signalService.updateMarketSignal(sig);
			res.setMessage(messageSource.getMessage("signal.save", null, Locale.US));
			res.setOk(true);
			
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
    		e.printStackTrace();
    	}
        return res;
    }
	
	@RequestMapping(value= "/marketSignal/new", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject newSignal(@ModelAttribute(value="signal")MarketSignal mSignal, HttpSession session) {
    	ResponseObject res= new ResponseObject();
    	
    	try{
    		market_signalService.addMarketSignal(mSignal);
			res.setMessage(messageSource.getMessage("signal.save", null, Locale.US));
			res.setOk(true);
			
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
    		e.printStackTrace();
    	}
        return res;
    }
	
	
	@RequestMapping(value= "/marketSignal/data", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject data(String signalDate,String factor, String reportDate, String link, String origTitle,String transTitle,
    		           int company, String overview, String background, String implication,String[] connectedCompanies,
    		           String[] connectedCountries,String[] connectedAuthors,HttpSession session, HttpServletRequest request) throws ParseException {
		
		/*Date conversion*/
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		java.util.Date utilDate1 = sdf.parse(signalDate + " 00:00:00");
		java.util.Date utilDate2 = sdf.parse(reportDate + " 00:00:00");
		java.sql.Date sDate = new Date(utilDate1.getTime());
		java.sql.Date rDate = new Date(utilDate2.getTime());
		java.util.Date date= new java.util.Date();
		String sessName = userService.hasSession(session, request);   

	    MarketSignal ms = new MarketSignal();
		ResponseObject res= new ResponseObject();
		
		/*saving market signal*/
		ms.setSignalDate(sDate);
		ms.setFactor(factor);
		ms.setReportDate(rDate);
		ms.setLink(link);
		ms.setOrigTitle(origTitle);
		ms.setTransTitle(transTitle);
		ms.setCompany(companyService.getCompanyId(company));
		ms.setOverview(overview);
		ms.setBackground(background);
		ms.setImplication(implication);
		ms.setInputBy(sessName);
		ms.setInputDate(new Date(date.getTime()));
		
		market_signalService.addMarketSignal(ms);
		
		boolean a = this.saveCompanies(connectedCompanies, ms.getId());
		boolean b = this.saveIndividual(connectedAuthors, ms.getId());
		boolean c = this.saveCountries(connectedCountries, ms.getId());
				
		/*response object*/
		res.setMessage(messageSource.getMessage("data.save", null, Locale.US));
		res.setId(ms.getId());
		
		if(a && b && c){
			res.setOk(true);
		}else{
			res.setOk(false);
		}
		
		return res;
    }
	
	public boolean saveCompanies(String[] connections, int signalID){
		
		boolean ok = false;
		int[] arrayInt = new int[connections.length];
		int[] isTrue = new int[connections.length];
		ConnectedCompany company = new ConnectedCompany();
		
		for(int i = 0; i < connections.length; i++ ){
			
			String[] splitConnections = connections[i].split("-");
			int connectionID = Integer.parseInt(splitConnections[1]);
			int aID = Integer.parseInt(splitConnections[3]);
						
			if(connectionID != 0){
				
				company.setMarketSignal(market_signalService.getSignalId(signalID));
				company.setCompany(companyService.getCompanyId(connectionID));
				company.setType(Integer.parseInt(splitConnections[2]));
				
				if(aID == 0){
					connected_companyService.addConnectedCompany(company);
				}else{
					company.setId(aID);
					connected_companyService.updateConnectedCompany(company);
				}
				
				isTrue[i] = 1;
				arrayInt[i] = aID;
			}
			else{
				isTrue[i] = 0;
				ok = true;
				return ok;
			}
		}
		
		if(!this.contains(isTrue,0)){
			ok = true;
		}else{
			ok = false;
		}
		
		return ok;
	}
	  
	
	public boolean saveCountries(String[] connections,int signalID){
		
		boolean ok = false;
		int[] arrayInt = new int[connections.length];
		int[] isTrue = new int[connections.length];
		
	//	List<ConnectedCountries> eList = connected_countriesService.list(signalID);
		ConnectedCountries countries = new ConnectedCountries();		
		
		for(int i = 0; i < connections.length; i++ ){
			
			String[] splitConnections = connections[i].split("-");
			int connectionID = Integer.parseInt(splitConnections[1]);
			int aID = Integer.parseInt(splitConnections[2]);
			
			countries.setMarketSignal(market_signalService.getSignalId(signalID));
			countries.setCountry(countryService.getCountryById(connectionID));
				
			if(connectionID == 1){
				ok = true;
				return ok;
			}
			
			if(aID == 0){
				connected_countriesService.addConnectedCountries(countries);
				isTrue[i] = 1;
			}else if(aID != 0){
				countries.setId(aID);
				connected_countriesService.updateConnectedCountries(countries);
				isTrue[i] = 1;
			}else{
				isTrue[i] = 0;
			}
			arrayInt[i] = aID;
		}
		
//		for(int i = 0; i < eList.size(); i++){
//			if(!this.contains(arrayInt,eList.get(i).getId())){
//				connected_countriesService.removeConnectedCountries(eList.get(i));
//			}
//		}
		
		if(!this.contains(isTrue,0)){
			ok = true;
		}else{
			ok = false;
		}
		
		return ok;
	}
	
	
	public boolean saveIndividual(String[] connections, int signalID){
		boolean ok = false;
		int[] arrayInt = new int[connections.length];
		int[] isTrue = new int[connections.length];
		
		ConnectedIndividual ind = new ConnectedIndividual();
//		List<ConnectedIndividual> eList = connected_individualService.list(signalID);
		
		for(int i = 0; i < connections.length; i++ ){
			
			String[] splitConnections = connections[i].split("-");
			int connectionID = Integer.parseInt(splitConnections[1]);
			int aID = Integer.parseInt(splitConnections[3]);
			
			if(connectionID != 0){
				
				ind.setMarketSignal(market_signalService.getSignalId(signalID));
				ind.setInd(indService.getIndividualById(connectionID));
				ind.setType(Integer.parseInt(splitConnections[2]));
				
				if(aID == 0){
					connected_individualService.addConnectedIndividual(ind);
				}else if(aID != 0){
					ind.setId(aID);
					connected_individualService.updateConnectedIndividual(ind);
				}
				isTrue[i] = 1;
				arrayInt[i] = aID;
			}
			else{
				isTrue[i] = 0;
				ok = true;
				return ok;
			}
		}
		
		if(!this.contains(isTrue,0)){
			ok = true;
		}else{
			ok = false;
		}
		
		return ok;
	}
	
	 public boolean contains(int[] arr, int item) {
	      for (int n : arr) {
	         if (item == n) {
	            return true;
	         }
	      }
	      return false;
	  }
	
	
}
