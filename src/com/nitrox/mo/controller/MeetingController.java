package com.nitrox.mo.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
 
import com.nitrox.mo.model.Meetings;
import com.nitrox.mo.model.MeetingsIndividual;
import com.nitrox.mo.model.fileClass.MeetingClass;
import com.nitrox.mo.model.jsonobject.Id;
import com.nitrox.mo.model.jsonobject.ResponseObject;
import com.nitrox.mo.service.CompanyService;
import com.nitrox.mo.service.CountryService;
import com.nitrox.mo.service.IndividualService;
import com.nitrox.mo.service.MeetingsIndividualService;
import com.nitrox.mo.service.MeetingsService;
import com.nitrox.mo.service.UserService;


@Controller
@RequestMapping("/meeting")
public class MeetingController {

	@Autowired
	private IndividualService indService;
	
	@Autowired
	private MeetingsService meetingService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private MeetingsIndividualService meetIndService;
	
		
	@RequestMapping(value="/updateDetails", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject editDetails1(@ModelAttribute(value="indID") @Valid MeetingClass meet,int id,HttpSession session, HttpServletRequest request) throws ParseException {
		System.out.println(meet.getEventDate() + " -- " );

		String[] splitDate = meet.getEventDate().split("/");
		String d = splitDate[2] + "-" + splitDate[0] + "-" + splitDate[1];
		System.out.println(meet.getEventDate() + " -- " + d);
		ResponseObject res= new ResponseObject();
		Meetings meeting = meetingService.getMeetingsById(id);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		java.util.Date utilDate1 = sdf.parse( d + " 00:00:00");
		java.util.Date date= new java.util.Date();
		String sessName = userService.hasSession(session, request);    

		meeting.setCompany(companyService.getCompanyId(meet.getCompany()));
		meeting.setCountry(countryService.getCountryById(meet.getLocCountry()));
		meeting.setSummary(meet.getSummary());
		meeting.setFollow_up(meet.getFollowUp());
		meeting.setLocation(meet.getLocAddress());
		meeting.setDate(utilDate1);
		meeting.setUpdated_by(sessName);
		meeting.setUpdated_date(new Timestamp(date.getTime()));
		
		meetingService.updateMeetings(meeting);
		res.setOk(true);
		
        return res;
    }
	
	
	@RequestMapping(value="/updateAttendees", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateAttendees(HttpSession session, HttpServletRequest request, String[] attendees,int id) {
    	
		
		ResponseObject res= new ResponseObject();
    	
		System.out.println(Arrays.toString(attendees));
		res.setOk(this.saveIndividuals(attendees, id));
		this.updateMeeting(session,request,id);
		
    	return res;
    }
	
	@RequestMapping(value="/updatedMeet", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateMeeting(HttpSession session, HttpServletRequest request,int id) {
    	
		ResponseObject res= new ResponseObject();
    	Meetings meet = meetingService.getMeetingsById(id);
    	java.util.Date date= new java.util.Date();
		String sessName = userService.hasSession(session, request);    

    	try{
    		meet.setUpdated_by(sessName);
    		meet.setUpdated_date(new Timestamp(date.getTime()));
    		meetingService.updateMeetings(meet);
    		res.setMessage(messageSource.getMessage("meet.save", null, Locale.US));
    		res.setOk(true);
    		
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
    		e.printStackTrace();
    	}
    	
    	return res;
    }
	
	@RequestMapping(value = {"/new" }, method = RequestMethod.GET)
	public String meetingPage() {
			 
		return "meeting";
	}
	
	@RequestMapping(value = { "/","" }, method = RequestMethod.GET)
	public String meetings(HttpSession session, HttpServletRequest request, ModelMap model) {
		    	
		String sessType = userService.getType(session, request);
		String sessName = userService.hasSession(session, request);    
		
		if(!sessName.equals("invalid")){
			   	model.addAttribute("page", "meeting/new");
			   	model.addAttribute("user", sessName);
			   	model.addAttribute("isAdmin", sessType);

			   	return "index";
		}
		return "redirect:/";
	  }
	
	@RequestMapping(value="/data", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject data(MeetingClass meet,HttpSession session, HttpServletRequest request) throws ParseException {
		
		ResponseObject res = new ResponseObject();
		Meetings meetings = new  Meetings();
		String sessName = userService.hasSession(session, request);   

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		java.util.Date utilDate1 = sdf.parse(meet.getEventDate() + " 00:00:00");
		java.util.Date date= new java.util.Date();
		
		meetings.setInput_date(new Date(date.getTime()));
		meetings.setInput_by(sessName);
		meetings.setSummary(meet.getSummary());
		meetings.setFollow_up(meet.getFollowUp());
		meetings.setLocation(meet.getLocAddress());
		meetings.setDate(utilDate1);
		meetings.setCountry(countryService.getCountryById(meet.getLocCountry()));
		meetings.setCompany(companyService.getCompanyId(meet.getCompany()));
		
		meetingService.addMeetings(meetings);
		
		this.saveIndividuals(meet.getIndividuals(), meetings.getId());
		res.setOk(true);
		
		return res;
	}
	
	@RequestMapping(value={"/delete"}, method=RequestMethod.POST)
    @ResponseBody
    public ResponseObject deleteMeeting(@ModelAttribute("meetingId") Id meetingId ){
    	ResponseObject res= new ResponseObject();
    	
    	try{
    		this.deleteMeetingIndividual(meetingId);
    		Meetings meetings= meetingService.getMeetingsById(meetingId.getId());
    		meetingService.removeMeetings(meetings);
    		res.setOk(true);
    		res.setMessage(messageSource.getMessage("data.delete", null, Locale.US));
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("validation.notexists", null, Locale.US));
    		e.printStackTrace();
    	}
    	
        return res;
    }
	
	 public ResponseObject deleteMeetingIndividual(@ModelAttribute("meetINDId") Id meetINDId ){
    	ResponseObject res= new ResponseObject();
    	
    	try{
    		List<MeetingsIndividual> meetInd= meetIndService.list(meetINDId.getId());
    		
    		for(int i = 0; i < meetInd.size(); i++){
    			meetIndService.removeMeetingsIndividual(meetInd.get(i));
    		}
    		res.setOk(true);
    		res.setMessage(messageSource.getMessage("data.delete", null, Locale.US));
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("validation.notexists", null, Locale.US));
    		e.printStackTrace();
    	}
    	
        return res;
	}
	
	public boolean saveIndividuals(String[] individuals, int meetingID){

		boolean isSave = false;
		int[] arrayInt = new int[individuals.length];
		int[] isTrue = new int[individuals.length];
		MeetingsIndividual ind = new MeetingsIndividual();
		
		for(int i = 0; i < individuals.length; i++){
			
			String[] splitData = individuals[i].split("-");
			int aID = Integer.parseInt(splitData[2]);
			
			ind.setInd(indService.getIndividualById(Integer.parseInt(splitData[1])));
			ind.setMeeting(meetingService.getMeetingsById(meetingID));
			
			if(splitData[2].equals("0") && !splitData[1].equals("0")){
				meetIndService.addMeetingsIndividual(ind);
				isTrue[i] = 1;
			}else if(!splitData[2].equals("0") && !splitData[1].equals("0")){
				ind.setId(aID);
				meetIndService.updateMeetingsIndividual(ind);
				isTrue[i] = 1;
			}else{
				isTrue[i] = 0;
			}
			arrayInt[i] = aID;
			
		}
		if(!this.contains(isTrue,0)){
			isSave = true;
		}else{
			isSave = false;
		}
		
		return isSave;
	}
	
	
	 public boolean contains(int[] arr, int item) {
	      for (int n : arr) {
	         if (item == n) {
	            return true;
	         }
	      }
	      return false;
	  }
	
		
}
