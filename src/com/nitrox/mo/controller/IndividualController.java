package com.nitrox.mo.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nitrox.mo.model.*;
import com.nitrox.mo.model.fileClass.*;
import com.nitrox.mo.service.*;
import com.nitrox.mo.model.jsonobject.Id;
import com.nitrox.mo.model.jsonobject.ResponseObject;

@Controller
@RequestMapping("/individual")
public class IndividualController {

	@Autowired
	private IndividualService indService;
	
	@Autowired
	private CompanyService compService;
	
	@Autowired
	private CompanyInvolvedService compInvolvedService;
	
	@Autowired
	private AwardsService awardsService;
	
	@Autowired
	private PhoneService phoneService;
		
	@Autowired
	private NationalitiesService nationService;
	
	@Autowired
	private EducationService educService;
	
	@Autowired
	private IndividualPhoneService indPhoneService;
	
	@Autowired
	private ContactDetailsService contactService;
		
	@Autowired
	private LanguageService langService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private CountryService countriesService;
	
	
	@RequestMapping(value= "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject createIndividual(@ModelAttribute(value="contact") @Valid Individual ind, HttpSession session, HttpServletRequest request) {
    	
		ResponseObject res= new ResponseObject();
    	String sessName = userService.hasSession(session, request);
    	java.util.Date date= new java.util.Date();
    	
    	try{
    		ind.setInput_by(sessName);
    		ind.setInput_date(date);
    		indService.addIndividual(ind);
    		res.setMessage(messageSource.getMessage("data.save", null, Locale.US));
    		res.setOk(true);
			res.setId(ind.getId());
		}catch(Exception e){
    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
    		e.printStackTrace();
    	}
    	
    	return res;
    }
	
	
	@RequestMapping(value={"/delete"}, method=RequestMethod.POST)
    @ResponseBody
    public ResponseObject deleteIndividual(@ModelAttribute("signalId") Id indId ){
    	ResponseObject res= new ResponseObject();
    	
    	try{
    		if(deleteCompInv(indId) && deleteEducBack(indId) && deleteAwards(indId) && deleteLang(indId) && deletePhones(indId) && deleteNation(indId)){
    			
    			Individual ind = indService.getIndividualById(indId.getId());
    			if(ind.getContact() != null){
    				this.deleteContacts(ind.getContact().getId());
    			}
    			indService.removeIndividual(ind);
    		}
    		res.setOk(true);
    		res.setMessage(messageSource.getMessage("data.delete", null, Locale.US));
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("validation.notexists", null, Locale.US));
    		e.printStackTrace();
    	}
    	
        return res;
    }
	
	
	public boolean deleteCompInv(@ModelAttribute("INDId") Id INDId ){
		boolean ok = false;
    	
    	try{
    		List<CompanyInvolved> compInv = compInvolvedService.listCompanies(INDId.getId());
    		
    		for(int i = 0; i < compInv.size(); i++){
    			compInvolvedService.removeCompanyInvolved(compInv.get(i));
    		}
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	public boolean deleteEducBack(@ModelAttribute("INDId") Id INDId ){
		boolean ok = false;
    	
    	try{
    		List<Education> educ = educService.list(INDId.getId());
    		
    		for(int i = 0; i < educ.size(); i++){
    			educService.removeEducation(educ.get(i));
    		}
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	public boolean deleteAwards(@ModelAttribute("INDId") Id INDId ){
		boolean ok = false;
    	
    	try{
    		List<Awards> awards = awardsService.list(INDId.getId());
    		
    		for(int i = 0; i < awards.size(); i++){
    			awardsService.removeAwards(awards.get(i));
    		}
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	public boolean deleteLang(@ModelAttribute("INDId") Id INDId ){
		boolean ok = false;
    	
    	try{
    		List<Language> lang = langService.list(INDId.getId());
    		
    		for(int i = 0; i < lang.size(); i++){
    			langService.removeLanguage(lang.get(i));
    		}
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	public boolean deleteContacts(@ModelAttribute("INDId") int id ){
		boolean ok = false;
    	
    	try{
    		ContactDetails cd = contactService.getContactDetailsById(id);
    		contactService.removeContactDetails(cd);
    		
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	public boolean deletePhones(@ModelAttribute("INDId") Id INDId ){
		boolean ok = false;
    	
    	try{
    		List<IndividualPhones> iphone = indPhoneService.list(INDId.getId());
    		
    		for(int i = 0; i < iphone.size(); i++){
    			Phone phone = phoneService.getPhoneById(iphone.get(i).getId());
    			
    			indPhoneService.removeIndividualPhones(iphone.get(i));
    			phoneService.removePhone(phone);
    		}
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	public boolean deleteNation(@ModelAttribute("INDId") Id INDId ){
		boolean ok = false;
    	
    	try{
    		List<Nationalities> nationalities = nationService.list(INDId.getId());
    		
    		for(int i = 0; i < nationalities.size(); i++){
    			nationService.removeNationalities(nationalities.get(i));
    		}
    		ok = true;
    	}catch(Exception e){
    		ok = false;
    	}
    	
        return ok;
	}
	
	@RequestMapping(value="/updateDetails1", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject editDetails1(@ModelAttribute(value="indID") @Valid Individual ind,HttpSession session, HttpServletRequest request) throws ParseException {
    	
		ResponseObject res= new ResponseObject();
    	Individual newIND = indService.getIndividualById(ind.getId());
    	String sessName = userService.hasSession(session, request);
    	
    	try{
    		ind.setUpdated_by(sessName);
    		ind.setInput_date(newIND.getInput_date());
    		ind.setInput_by(newIND.getInput_by());
    		indService.updateIndividual(ind);
    		
    		res.setMessage(messageSource.getMessage("country.save", null, Locale.US));
    		res.setOk(true);
    		
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
    		e.printStackTrace();
    	}
    	
        return res;
    }
	
	@RequestMapping(value="/updateDetails2", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateDetails2(@ModelAttribute(value="indID") @Valid ContactDetails contact, int contactId, int indId,HttpSession session, HttpServletRequest request) {
    	
		ResponseObject res= new ResponseObject();
    	ContactDetails cd = new ContactDetails();
    	Individual individual = indService.getIndividualById(indId);
    
    	try{
    		
    		int count = 0;
	    	
	    	if(contact.getAddress1() == ""){count++;}
	    	if(contact.getAddress2() == ""){count++;}
	    	if(contact.getAddress3() == ""){count++;}
	    	if(contact.getEmail() == ""){count++;}
	    	if(contact.getLinkedIn() == ""){count++;}
	    	if(contact.getTwitter() == ""){count++;}
	    	if(contact.getYoutube() == ""){count++;}
	    	if(contact.getSkype() == ""){count++;}
	    	
	       if(count == 8){
	    	   res.setMessage(messageSource.getMessage("data.empty", null, Locale.US));
	    		res.setOk(true);
				res.setId(0);
	       }else{
	    		cd.setAddress1(contact.getAddress1());
	    		cd.setAddress2(contact.getAddress2());
	    		cd.setAddress3(contact.getAddress3());
	    		cd.setLinkedIn(contact.getLinkedIn());
	    		cd.setEmail(contact.getEmail());
	    		cd.setSkype(contact.getSkype());
	    		cd.setTwitter(contact.getTwitter());
	    		cd.setYoutube(contact.getYoutube());
	    		
	    		if(contactId == 0){
	    			contactService.addContactDetails(cd);
	    			
	    			individual.setContact(contactService.getContactDetailsById(cd.getId()));
	    			indService.updateIndividual(individual);
	    		}else{
	    			cd.setId(contactId);
	    			contactService.updateContactDetails(cd);
	    		}
	    		
	    		res.setMessage(messageSource.getMessage("contact.save", null, Locale.US));
	    		res.setOk(true);
	       }
	       
	       this.updateInd(session,request,indId);
    		
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
    		e.printStackTrace();
    	}
    	
        return res;
    }
	
	@RequestMapping(value="/updateNumbers", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateNumbers(HttpSession session, HttpServletRequest request, String[] numbers,int id) {
    	
		ResponseObject res= new ResponseObject();
    	
		res.setOk(this.saveContacts(numbers, id));
		this.updateInd(session,request,id);
		
    	return res;
    }
	
	
	@RequestMapping(value="/updateAwards", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateAwards(HttpSession session, HttpServletRequest request,String[] awards,int id) {
    	
		ResponseObject res= new ResponseObject();
		
		res.setOk(this.saveAwards(awards, id));
		this.updateInd(session,request,id);
		
    	return res;
    }
	
	@RequestMapping(value="/updateNation", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateNation(HttpSession session, HttpServletRequest request,String[] nationalities,int id) {
    	
		ResponseObject res= new ResponseObject();
    	
		res.setOk(this.saveNation(nationalities, id));
		this.updateInd(session,request,id);
		
    	return res;
    }
		
	@RequestMapping(value="/updatedIND", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateInd(HttpSession session, HttpServletRequest request,int id) {
    	
		ResponseObject res= new ResponseObject();
    	Individual newIND = indService.getIndividualById(id);
    	java.util.Date date= new java.util.Date();
		String sessName = userService.hasSession(session, request);    

    	try{
    		newIND.setUpdated_by(sessName);
    		newIND.setUpdated_date(new Timestamp(date.getTime()));
    		indService.updateIndividual(newIND);
    		res.setMessage(messageSource.getMessage("country.save", null, Locale.US));
    		res.setOk(true);
    		
    	}catch(Exception e){
    		res.setMessage(messageSource.getMessage("error.unknown", null, Locale.US));
    		e.printStackTrace();
    	}
    	
    	return res;
    }
	
	@RequestMapping(value="/updateLanguage", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateLanguage(HttpSession session, HttpServletRequest request,String[] language,int id) {
    	
		ResponseObject res= new ResponseObject();
    	
		res.setOk(this.saveLanguages(language, id));
		this.updateInd(session,request,id);
		
    	return res;
    }
	
	@RequestMapping(value="/updateCompany", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateCompany(HttpSession session, HttpServletRequest request,String[] company,int id) {
    	
		ResponseObject res= new ResponseObject();
    	
		res.setOk(this.saveCompany(company, id));
		this.updateInd(session,request,id);
		
    	return res;
    }
	
	@RequestMapping(value="/updateBackground", method = RequestMethod.POST)
	@ResponseBody
    public ResponseObject updateBackground(HttpSession session, HttpServletRequest request,String[] background,int id) {
    	
		ResponseObject res= new ResponseObject();
    	
		res.setOk(this.saveEducation(background, id));
		this.updateInd(session, request,id);
		
    	return res;
    }
	
	@RequestMapping(value = { "/holdings" }, method = RequestMethod.GET)
    @ResponseBody
    public List<Company> getHoldingsList() {
		return compService.holdings();
	}
	
	@RequestMapping(value = {"/new" }, method = RequestMethod.GET)
	public String page() {
		 
		 return "individual";
	}
	
	@RequestMapping(value = { "/","" }, method = RequestMethod.GET)
	public String ind(HttpSession session, HttpServletRequest request, ModelMap model) {
		
		String sessType = userService.getType(session, request);
		String sessName = userService.hasSession(session, request);    
		
		if(!sessName.equals("invalid")){
			model.addAttribute("page", "individual/new");
			model.addAttribute("user", sessName);
			model.addAttribute("isAdmin", sessType);
	
		   	return "index";
		}
		return "redirect:/";
	  }

	@RequestMapping(value="/data", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject data(IndividualClass ind,HttpSession session, HttpServletRequest request) throws ParseException {
		
		ResponseObject res= new ResponseObject();
		Individual individual = new Individual();
		ContactDetails cd = new ContactDetails();
		java.util.Date date= new java.util.Date();
		String sessName = userService.hasSession(session, request);   
		
		int count = 0;
    	
    	if(ind.getAddress1() == ""){count++;}
    	if(ind.getAddress2() == ""){count++;}
    	if(ind.getAddress3() == ""){count++;}
    	if(ind.getEmail() == ""){count++;}
    	if(ind.getLinkedIn() == ""){count++;}
    	if(ind.getTwitter() == ""){count++;}
    	if(ind.getYoutube() == ""){count++;}
    	if(ind.getSkype() == ""){count++;}
    	
       if(count == 8){
    	   res.setMessage(messageSource.getMessage("data.empty", null, Locale.US));
    		res.setOk(true);
			res.setId(0);
       }else{
			cd.setAddress1(ind.getAddress1());
			cd.setAddress2(ind.getAddress2());
			cd.setAddress3(ind.getAddress3());
			cd.setLinkedIn(ind.getLinkedIn());
			cd.setEmail(ind.getEmail());
			cd.setSkype(ind.getSkype());
			cd.setTwitter(ind.getTwitter());
			cd.setYoutube(ind.getYoutube());
			
			contactService.addContactDetails(cd);
        }
		
		individual.setCountry(countriesService.getCountryById(ind.getPersonBase()));
		individual.setFirstName(ind.getFirstName());
		individual.setLastName(ind.getLastName());
		individual.setMiddleInitial(ind.getMiddleInitial());
		individual.setInput_by(sessName);
		individual.setInput_date(date);
		
		if(count == 8){
			individual.setContact(contactService.getContactDetailsById(cd.getId()));
		}
		indService.addIndividual(individual);
		
		this.saveCompany(ind.getCompInvolvement(),individual.getId());
		this.saveEducation(ind.getEducBackground(),individual.getId());
		this.saveAwards(ind.getAwards(),individual.getId());
		this.saveNation(ind.getNationalities(),individual.getId());
		this.saveLanguages(ind.getLanguages(),individual.getId());
		
		this.saveContacts(ind.getPhones(),individual.getId());
		
		res.setOk(true);
		
        return res;
    }
	
	
	public boolean saveAwards(String[] connections, int indID){
		
		boolean isSave = false;
		int[] arrayInt = new int[connections.length];
		int[] isTrue = new int[connections.length];
		Awards awards = new Awards();
		List<Awards> aList = awardsService.list(indID);
		
		for(int i = 0; i < connections.length; i++){
			
			String[] splitData = connections[i].split("-");
			int aID = Integer.parseInt(splitData[3]);
			
			if(splitData.length > 2){
				awards.setName(splitData[1]);
				awards.setLocation(splitData[2]);
			}else if(splitData.length == 2){
				awards.setName(splitData[1]);
			}
			
			awards.setInd(indService.getIndividualById(indID));
			
			if(splitData[3].equals("0") && !splitData[1].equals("")){
				awardsService.addAwards(awards);
				isTrue[i] = 1;
			}else if(!splitData[3].equals("0") && !splitData[1].equals("")){
				awards.setId(aID);
				awards.setInd(indService.getIndividualById(indID));
				awardsService.updateAwards(awards);
				
				isTrue[i] = 1;
			}else if(splitData[1].equals("")){
				if(!splitData[3].equals("0")){
					awardsService.removeAwards(awardsService.getAwardsId(aID));
				}
				isTrue[i] = 1;
			}else{
				isTrue[i] = 0;
			}
			
			arrayInt[i] = aID;
		}
				
		for(int i = 0; i < aList.size(); i++){
			if(!this.contains(arrayInt,aList.get(i).getId())){
				awardsService.removeAwards(aList.get(i));
			}
		}
		
		if(!this.contains(isTrue,0)){
			isSave = true;
		}else{
			isSave = false;
		}
		
		return isSave;
	}
	
	
	public boolean saveNation(String[] connections, int indID){
		
		boolean isSave = false;
		int[] arrayInt = new int[connections.length];
		int[] isTrue = new int[connections.length];
		List<Nationalities> nList = nationService.list(indID);
		Nationalities nat = new Nationalities();
		
		for(int i = 0; i < connections.length; i++){
			
			String[] splitData = connections[i].split("-");
			int aID = Integer.parseInt(splitData[2]);
			
			nat.setCountry(countriesService.getCountryById(Integer.parseInt(splitData[1])));
			nat.setInd(indService.getIndividualById(indID));
						
			if(splitData[2].equals("0") && !splitData[1].equals("0")){
				nationService.addNationalities(nat);
				isTrue[i] = 1;
			}else if(!splitData[2].equals("0") && !splitData[1].equals("0")){
				nat.setId(aID);
				nationService.updateNationalities(nat);
				isTrue[i] = 1;
			}else if(splitData[1].equals("0")){
				if(!splitData[2].equals("0")){
					nationService.removeNationalities(nationService.getNationalitiesById(aID));
				}
				isTrue[i] = 1;
			}else{
				isTrue[i] = 0;
			}
			
			arrayInt[i] = aID;
		}
		
		for(int i = 0; i < nList.size(); i++){
			if(!this.contains(arrayInt,nList.get(i).getId())){
				nationService.removeNationalities(nList.get(i));
			}
		}
		
		if(!this.contains(isTrue,0)){
			isSave = true;
		}else{
			isSave = false;
		}
		
		return isSave;
	}
	
    public boolean saveCompany(String[] connections, int indID){
		
    	boolean isSave = false;
		int[] arrayInt = new int[connections.length];
		int[] isTrue = new int[connections.length];
		List<CompanyInvolved> cList = compInvolvedService.listCompanies(indID);
		CompanyInvolved compIn = new CompanyInvolved();
	
		for(int i = 0; i < connections.length; i++){
			
			String[] splitData = connections[i].split("-");
			int aID = 0;
			
			if(!splitData[1].equals("0")){
				
				aID = Integer.parseInt(splitData[8]);
			    compIn.setCompany(compService.getCompanyId(Integer.parseInt(splitData[1])));
				compIn.setGrpCompany(compService.getCompanyId(Integer.parseInt(splitData[2])));
				compIn.setTitle(splitData[3]);
				
				if(Integer.parseInt(splitData[4]) == 1){
					compIn.setActive(true);
				}else{
					compIn.setActive(false);
				}
				
				compIn.setBase(countriesService.getCountryById(Integer.parseInt(splitData[5])));
				compIn.setCompanyCity(splitData[6]);
				compIn.setHeadquarter(countriesService.getCountryById(Integer.parseInt(splitData[7])));
				compIn.setInd(indService.getIndividualById(indID));
				
				System.out.println(" -- " + aID);
				if(splitData[8].equals("0")){
					compInvolvedService.addCompanyInvolved(compIn);
					isTrue[i] = 1;
				}else if(!splitData[8].equals("0")){
					compIn.setId(aID);
					compIn.setInd(indService.getIndividualById(indID));
					compInvolvedService.updateCompanyInvolved(compIn);
					
					isTrue[i] = 1;
				}else{
					isTrue[i] = 0;
				}
			}else{
				isTrue[i] = 1;
			}
			arrayInt[i] = aID;
		}
		
		for(int i = 0; i < cList.size(); i++){
			if(!this.contains(arrayInt,cList.get(i).getId())){
				compInvolvedService.removeCompanyInvolved(cList.get(i));
			}
		}
		
		if(!this.contains(isTrue,0)){
			isSave = true;
		}else{
			isSave = false;
		}
		
		return isSave;
	}
	
	public boolean saveEducation(String[] connections, int indID){
		
		boolean isSave = false;
		int[] arrayInt = new int[connections.length];
		int[] isTrue = new int[connections.length];
		List<Education> eList = educService.list(indID);
		Education educ = new Education();
		
		for(int i = 0; i < connections.length; i++){
		
			String[] splitData = connections[i].split("-");
			int aID = Integer.parseInt(splitData[4]);
			
			educ.setDiscipline(splitData[1]);
			educ.setDegree(splitData[2]);
			educ.setUniversity(splitData[3]);
			educ.setInd(indService.getIndividualById(indID));
			
			if(splitData[1].equals("") && splitData[2].equals("") && splitData[3].equals("")){
				if(!splitData[4].equals("0")){
					educService.removeEducation(educService.getEducationById(aID));
				}
				isTrue[i] = 1;
			}else if(splitData[4].equals("0")){
				educService.addEducation(educ);
				isTrue[i] = 1;
			}else if(!splitData[4].equals("0")){
				educ.setId(aID);
				educ.setInd(indService.getIndividualById(indID));
				educService.updateEducation(educ);
				
				isTrue[i] = 1;
			}else{
				isTrue[i] = 0;
			}
			
			arrayInt[i] = aID;
		}
		
		for(int i = 0; i < eList.size(); i++){
			if(!this.contains(arrayInt,eList.get(i).getId())){
				educService.removeEducation(eList.get(i));
			}
		}
		
		if(!this.contains(isTrue,0)){
			isSave = true;
		}else{
			isSave = false;
		}
		
		return isSave;
	}
	
	
	public boolean saveLanguages(String[] connections, int indID){
		
		boolean isSave = false;
		int[] arrayInt = new int[connections.length];
		int[] isTrue = new int[connections.length];
		List<Language> eList = langService.list(indID);
		Language lang = new Language();
		
		for(int i = 0; i < connections.length; i++){
			
			String[] splitData = connections[i].split("-");
			int aID = Integer.parseInt(splitData[2]);
			
			lang.setName(splitData[1]);
			lang.setInd(indService.getIndividualById(indID));
			
			if(splitData[2].equals("0") && !splitData[1].equals("")){
				langService.addLanguage(lang);
				isTrue[i] = 1;
			}else if(!splitData[2].equals("0") && !splitData[1].equals("")){
				lang.setId(aID);
				lang.setInd(indService.getIndividualById(indID));
				langService.updateLanguage(lang);
				
				isTrue[i] = 1;
			}else if(splitData[1].equals("")){
				if(!splitData[2].equals("0")){
					langService.removeLanguage(langService.getLanguageById(aID));
				}
				isTrue[i] = 1;
			}else{
				isTrue[i] = 0;
			}
			arrayInt[i] = aID;
		}
		
		for(int i = 0; i < eList.size(); i++){
			if(!this.contains(arrayInt,eList.get(i).getId())){
				langService.removeLanguage(eList.get(i));
			}
		}
		
		if(!this.contains(isTrue,0)){
			isSave = true;
		}else{
			isSave = false;
		}
		
		return isSave;
	}
	
	public boolean saveContacts(String[] connections, int indID){
		
		boolean isSave = false;
		boolean noNumber = false;
		int[] arrayInt = new int[connections.length];
		int[] isTrue = new int[connections.length];
		
		Phone phone = new Phone();
		IndividualPhones ip = new IndividualPhones();
		
		for(int i = 0; i < connections.length; i++){

			String[] splitData = connections[i].split("-");
			int aID = Integer.parseInt(splitData[5]);
			
			
			if(splitData.length == 6){
				
				if(splitData[3].equals("0") && splitData[4].equals("0")){
					noNumber = true;
				}
								
				phone.setPhoneType(splitData[1]);
				phone.setCountry(countriesService.getCountryById(Integer.parseInt(splitData[2])));
				phone.setAreaCode(splitData[3]);
				phone.setNumber(splitData[4]);
				
				if(splitData[5].equals("0") && noNumber == false && !splitData[2].equals("0")){
					phoneService.addPhone(phone);
					
					ip.setPhone(phoneService.getPhoneById(phone.getId()));
					ip.setInd(indService.getIndividualById(indID));
					indPhoneService.addIndividualPhones(ip);
				}else if(!splitData[5].equals("0") && noNumber == false  && !splitData[2].equals("0")){
					
					phone.setId(aID);
					phoneService.updatePhone(phone);
					
				}else if(!splitData[5].equals("0") && noNumber == true){
					phone.setId(aID);
					IndividualPhones iPhones = indPhoneService.getIndividualPhonesById(aID);
					
					indPhoneService.removeIndividualPhones(iPhones);
					phoneService.removePhone(phone);
				}
				
				isTrue[i] = 1;
				
			}else if(splitData[4].equals("")){
				if(!splitData[5].equals("0")){
					phoneService.removePhone(phoneService.getPhoneById(aID));
				}
				isTrue[i] = 1;
			}else{
				isTrue[i] = 0;
			}
			
			arrayInt[i] = aID;
			
		}
		System.out.println("--" + Arrays.toString(arrayInt));
//		for(int i = 0; i < iList.size(); i++){
//			if(!this.contains(arrayInt,iList.get(i).getId())){
//				indPhoneService.removeIndividualPhones(iList.get(i));
//				phoneService.removePhone(phoneService.getPhoneById(iList.get(i).getId()));
//			}
//		}
		
		if(!this.contains(isTrue,0)){
			isSave = true;
		}else{
			isSave = false;
		}
		
		return isSave;
		
	}
	
	 public boolean contains(int[] arr, int item) {
	      for (int n : arr) {
	         if (item == n) {
	            return true;
	         }
	      }
	      return false;
	  }
	

}
