package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "education")
public class Education {

	@Id
	@GeneratedValue
	@Column(name= "educ_id")
	private int id;
	
	@Column(name = "educ_discipline")
	private String discipline;
	
	@Column(name = "educ_degree")
	private String degree;
	
	@Column(name = "university")
	private String university;
	
	@ManyToOne
	@JoinColumn(name = "ind_id")
	private Individual ind;
	
	public Education(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDiscipline() {
		return discipline;
	}

	public void setDiscipline(String discipline) {
		this.discipline = discipline;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public Individual getInd() {
		return ind;
	}

	public void setInd(Individual ind) {
		this.ind = ind;
	}
	
	
	
}
