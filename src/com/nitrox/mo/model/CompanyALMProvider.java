package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "company_alm_provider")
public class CompanyALMProvider {

	@Id
	@GeneratedValue
	@Column(name= "comp_AP_id")
	private int id;
	
	@Column(name = "comp_AP_Name")
	private String appName;
	
	@ManyToOne
	@JoinColumn(name = "comp_Ap_provider")
	private Company almProvider;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public Company getAlmProvider() {
		return almProvider;
	}

	public void setAlmProvider(Company almProvider) {
		this.almProvider = almProvider;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}


}
