package com.nitrox.mo.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "company")  
public class Company {

	@Id
	@GeneratedValue
	@Column(name= "company_id")
	private int id;
	
	@Column(name = "company_name")
	private String name;
	
	@Column(name = "company_website")
	private String website;
	
	@Column(name = "company_notes")
	private String notes;
	
	@Column(name = "company_type")
	private int type;
	
	@Column(name = "holding_co")
	private boolean isHolding;
	
	@Column(name = "company_active")
	private boolean isActive;
	
	@ManyToOne
	@JoinColumn(name = "country_id")
	private Countries country;
	
	@ManyToOne
	@JoinColumn(name = "contact_dts_id")
	private ContactDetails contact;
		
	@Column(name = "input_by")
	private String inputBy;
	
	@Column(name = "input_date")
	private Date inputDate;
	
	@Column(name = "updated_by")
	private String updated_by;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	public Company(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean getIsHolding() {
		return isHolding;
	}

	public void setIsHolding(boolean isHolding) {
		this.isHolding = isHolding;
	}
	
	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Countries getCountry() {
		return country;
	}

	public void setCountry(Countries country) {
		this.country = country;
	}

	public ContactDetails getContact() {
		return contact;
	}

	public void setContact(ContactDetails contact) {
		this.contact = contact;
	}

	public String getInputBy() {
		return inputBy;
	}

	public void setInputBy(String inputBy) {
		this.inputBy = inputBy;
	}

	public Date getInputDate() {
		return inputDate;
	}

	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public Timestamp getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}

}
