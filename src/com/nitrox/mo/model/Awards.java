package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "awards")
public class Awards {

	@Id
	@GeneratedValue
	@Column(name= "awards_id")
	private int id;
	
	@Column(name = "awards_name")
	private String name;
	
	@Column(name = "awards_event")
	private String location;
	
	@ManyToOne
	@JoinColumn(name = "ind_id")
	private Individual ind;
	
	public Awards(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Individual getInd() {
		return ind;
	}

	public void setInd(Individual ind) {
		this.ind = ind;
	}
	
}
