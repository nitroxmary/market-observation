package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity  
@Table(name = "countries")  
public class Countries {

	@Id
	@GeneratedValue
	@Column(name= "country_id")
	private int id;
	
	@Column(name = "country_name")
	private String name;
	
	@Column(name = "country_region")
	private String region;
	
	@Column(name = "country_code")
	private String code;
	
	public Countries(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
    
}
