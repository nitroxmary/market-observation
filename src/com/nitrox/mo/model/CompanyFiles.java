package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

@Entity  
@Table(name = "company_files")
public class CompanyFiles {

	@Id
	@GeneratedValue
	@Column(name= "comp_file_id")
	private int id;
	
	@Column(name = "comp_file_type")
	private int fileType;
	
	@Column(name = "comp_file_desc")
	private String fileDesc;
	
	@Column(name = "comp_file_name")
	private String fileName;
	
	@Column(name = "comp_file_ext")
	private String fileExt;
	
	@Column(name = "comp_file_data")
	@Lob
	private byte[] fileData;
	
	@Column(name = "comp_file_year")
	private int fileYear;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	@Transient
	private MultipartFile file;
	
	public CompanyFiles(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFileType() {
		return fileType;
	}

	public String getFileDesc() {
		return fileDesc;
	}

	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

	public int getFileYear() {
		return fileYear;
	}

	public void setFileYear(int fileYear) {
		this.fileYear = fileYear;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	
	
}
