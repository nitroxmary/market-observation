package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "connected_company")
public class ConnectedCompany {

	@Id
	@GeneratedValue
	@Column(name= "con_comp_id")
	private int id;
	
	@Column(name = "con_comp_type")
	private int type;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name = "mSignal_id")
	private MarketSignal marketSignal;
	
	
	public ConnectedCompany(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public MarketSignal getMarketSignal() {
		return marketSignal;
	}

	public void setMarketSignal(MarketSignal marketSignal) {
		this.marketSignal = marketSignal;
	}
	
	
}
