package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "meetings_individual")
public class MeetingsIndividual {

	@Id
	@GeneratedValue
	@Column(name= "meeting_ind_id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "meeting_id")
	private Meetings meeting;
	
	@ManyToOne
	@JoinColumn(name = "ind_id")
	private Individual ind;
	
	public MeetingsIndividual(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Meetings getMeeting() {
		return meeting;
	}

	public void setMeeting(Meetings meeting) {
		this.meeting = meeting;
	}

	public Individual getInd() {
		return ind;
	}

	public void setInd(Individual ind) {
		this.ind = ind;
	}
	
	
}
