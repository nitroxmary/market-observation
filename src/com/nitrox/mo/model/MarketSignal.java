package com.nitrox.mo.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "market_signal")
public class MarketSignal {

	@Id
	@GeneratedValue
	@Column(name= "mSignal_id")
	private int id;
	
	@Column(name = "mSignal_date")
	private Date signalDate;
	
	@Column(name = "mSignal_titleOrig")
	private String origTitle;
	
	@Column(name = "mSignal_titleTrans")
	private String transTitle;
	
	@Column(name = "mSignal_link")
	private String link;
	
	@Column(name = "mSignal_overview")
	private String overview;
	
	@Column(name = "mSignal_background")
	private String background;
	
	@Column(name = "mSignal_implication")
	private String implication;
	
	@Column(name = "mSignal_reportDate")
	private Date reportDate;
	
	@Column(name = "mSignal_factorName")
	private String factor;
	
	@Column(name = "input_by")
	private String inputBy;
	
	@Column(name = "input_date")
	private Date inputDate;
	
	@Column(name = "updated_by")
	private String updated_by;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	public MarketSignal(){
		
	}

	public Date getSignalDate() {
		return signalDate;
	}

	public void setSignalDate(Date signalDate) {
		this.signalDate = signalDate;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrigTitle() {
		return origTitle;
	}

	public void setOrigTitle(String origTitle) {
		this.origTitle = origTitle;
	}

	public String getTransTitle() {
		return transTitle;
	}

	public void setTransTitle(String transTitle) {
		this.transTitle = transTitle;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getImplication() {
		return implication;
	}

	public void setImplication(String implication) {
		this.implication = implication;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getFactor() {
		return factor;
	}

	public void setFactor(String factor) {
		this.factor = factor;
	}

	public String getInputBy() {
		return inputBy;
	}

	public void setInputBy(String inputBy) {
		this.inputBy = inputBy;
	}

	public Date getInputDate() {
		return inputDate;
	}

	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public Date getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}

	
}
