package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "phone")
public class Phone {

	@Id
	@GeneratedValue
	@Column(name= "phone_id")
	private int id;
	
	@Column(name = "phone_type")
	private String phoneType;
	
	@Column(name = "phone_acode")
	private String areaCode;
	
	@Column(name = "phone_num")
	private String number;
	
	@ManyToOne
	@JoinColumn(name = "country_id")
	private Countries country;

	
	public Phone(){
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
    
	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Countries getCountry() {
		return country;
	}

	public void setCountry(Countries country) {
		this.country = country;
	}
	
	
}
