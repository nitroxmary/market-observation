package com.nitrox.mo.model.jsonobject;

public class Id{
	
   private int id;
   private String toggle;
   
   public Id(){}
   
   public Id(int id){
	this.id= id;   
   }
   
   public void setId(int id){
	   this.id= id;
   }
   
   public int getId(){
	   return id;
   }
   
   public void setToggle(String toggle){
	   this.toggle= toggle;
   }
   
   public String getToggle(){
	   return toggle;
   }
 }