package com.nitrox.mo.model.jsonobject;

public class ResponseObject{
	   public int id;
	   public String message;
	   public boolean ok;
	   
	   public void setId(int id){
		   this.id= id;
	   }
	   
	   public int getId(){
		   return id;
	   }
	   
	   public void setMessage(String message){
		   this.message= message;
	   }
	   
	   public String getMessage(){
		   return message;
	   }
	   
	   public void setOk(boolean ok){
		   this.ok= ok;
	   }
	   
	   public boolean getOk(){
		   return ok;
	   }
	   
 }
