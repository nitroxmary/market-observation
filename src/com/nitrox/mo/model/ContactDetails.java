package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity  
@Table(name = "contact_details")
public class ContactDetails {

	@Id
	@GeneratedValue
	@Column(name= "contact_dts_id")
	private int id;
	
	@Column(name = "contact_addr1")
	private String address1;
	
	@Column(name = "contact_addr2")
	private String address2;
	
	@Column(name = "contact_addr3")
	private String address3;
	
	@Column(name = "contact_email")
	private String email;
	
	@Column(name = "contact_linkedln")
	private String linkedIn;
	
	@Column(name = "contact_skype")
	private String skype;
	
	@Column(name = "contact_youtube")
	private String youtube;
	
	@Column(name = "contact_twitter")
	private String twitter;
	
	public ContactDetails(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLinkedIn() {
		return linkedIn;
	}

	public void setLinkedIn(String linkedIn) {
		this.linkedIn = linkedIn;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getYoutube() {
		return youtube;
	}

	public void setYoutube(String youtube) {
		this.youtube = youtube;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	
		
}
