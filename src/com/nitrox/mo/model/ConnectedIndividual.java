package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "connected_individual")
public class ConnectedIndividual {

	@Id
	@GeneratedValue
	@Column(name= "ctd_ind_id")
	public int id;
	
	@Column(name = "ctd_ind_type")
	public int type;
	
	@ManyToOne
	@JoinColumn(name = "mSignal_id")
	public MarketSignal marketSignal;
	
	@ManyToOne
	@JoinColumn(name = "ind_id")
	public Individual ind;
	
	public ConnectedIndividual(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public MarketSignal getMarketSignal() {
		return marketSignal;
	}

	public void setMarketSignal(MarketSignal marketSignal) {
		this.marketSignal = marketSignal;
	}

	public Individual getInd() {
		return ind;
	}

	public void setInd(Individual ind) {
		this.ind = ind;
	}
	
	
}
