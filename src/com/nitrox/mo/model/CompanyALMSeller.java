package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "company_alm_seller")
public class CompanyALMSeller {
	
	@Id
	@GeneratedValue
	@Column(name= "comp_AS_id")
	private int id;
	
	@Column(name = "comp_AS_appName")
	private String appname;
	
	@Column(name = "comp_AS_desc")
	private String almDesc;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public String getAlmDesc() {
		return almDesc;
	}

	public void setAlmDesc(String almDesc) {
		this.almDesc = almDesc;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	
}
