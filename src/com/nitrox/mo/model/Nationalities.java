package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "nationalities")
public class Nationalities {

	@Id
	@GeneratedValue
	@Column(name= "nationalities_id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "country_id")
	private Countries country;
	
	@ManyToOne
	@JoinColumn(name = "ind_id")
	private Individual ind;
	
	public Nationalities(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Countries getCountry() {
		return country;
	}

	public void setCountry(Countries country) {
		this.country = country;
	}

	public Individual getInd() {
		return ind;
	}

	public void setInd(Individual ind) {
		this.ind = ind;
	}
	
	
}
