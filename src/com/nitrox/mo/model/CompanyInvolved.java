package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "company_involved")
public class CompanyInvolved {

	@Id
	@GeneratedValue
	@Column(name= "comp_involved_id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name = "ind_id")
	private Individual ind;
	
	@ManyToOne
	@JoinColumn(name = "group_company")
	private Company grpCompany;
	
	@Column(name = "company_title")
	private String title;
	
	@ManyToOne
	@JoinColumn(name = "company_base")
	private Countries base;
	
	@ManyToOne
	@JoinColumn(name = "company_headQuarter")
	private Countries headquarter;
	
	@Column(name = "company_active")
	private boolean active;
	
	@Column(name = "company_baseCity")
	private String companyCity;
	
	public CompanyInvolved(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Individual getInd() {
		return ind;
	}

	public void setInd(Individual ind) {
		this.ind = ind;
	}

	public Company getGrpCompany() {
		return grpCompany;
	}

	public void setGrpCompany(Company grpCompany) {
		this.grpCompany = grpCompany;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Countries getBase() {
		return base;
	}

	public void setBase(Countries base) {
		this.base = base;
	}

	public Countries getHeadquarter() {
		return headquarter;
	}

	public void setHeadquarter(Countries headquarter) {
		this.headquarter = headquarter;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCompanyCity() {
		return companyCity;
	}

	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}
	
	
}
