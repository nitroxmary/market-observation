package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "connected_countries")
public class ConnectedCountries {

	@Id
	@GeneratedValue
	@Column(name= "con_count_id")
	public int id;
	
	@ManyToOne
	@JoinColumn(name = "mSignal_id")
	public MarketSignal marketSignal;
	
	@ManyToOne
	@JoinColumn(name = "country_id")
	public Countries country;
	
	public ConnectedCountries(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public MarketSignal getMarketSignal() {
		return marketSignal;
	}

	public void setMarketSignal(MarketSignal marketSignal) {
		this.marketSignal = marketSignal;
	}

	public Countries getCountry() {
		return country;
	}

	public void setCountry(Countries country) {
		this.country = country;
	}
	
	
}
