package com.nitrox.mo.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "individual")
public class Individual {

	@Id
	@GeneratedValue
	@Column(name= "ind_id")
	private int id;
	
	@Column(name = "ind_fname")
	private String firstName;
	
	@Column(name = "ind_lname")
	private String lastName;
	
	@Column(name = "ind_MI")
	private String middleInitial;
	
	@Column(name = "input_by")
	private String input_by;
	
	@Column(name = "input_date")
	private Date input_date;
	
	@Column(name = "updated_by")
	private String updated_by;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
		
	@ManyToOne
	@JoinColumn(name = "country_id")
	private Countries country;
	
	@ManyToOne
	@JoinColumn(name = "contact_dts_id")
	private ContactDetails contact;
	
	public Individual(){
		
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getMiddleInitial() {
		return middleInitial;
	}


	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}


	public String getInput_by() {
		return input_by;
	}


	public void setInput_by(String input_by) {
		this.input_by = input_by;
	}


	public Date getInput_date() {
		return input_date;
	}


	public void setInput_date(Date input_date) {
		this.input_date = input_date;
	}


	public Countries getCountry() {
		return country;
	}


	public void setCountry(Countries country) {
		this.country = country;
	}


	public ContactDetails getContact() {
		return contact;
	}


	public void setContact(ContactDetails contact) {
		this.contact = contact;
	}


	public String getUpdated_by() {
		return updated_by;
	}


	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}


	public Date getUpdated_date() {
		return updated_date;
	}


	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}
	
	
	
}
