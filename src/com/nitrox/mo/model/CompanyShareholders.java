package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "company_shareholders")
public class CompanyShareholders {

	@Id
	@GeneratedValue
	@Column(name= "csholder_id")
	private int id;
	
	@Column(name = "csholder_type")
	private int type;
	
	@ManyToOne
	@JoinColumn(name = "csCompany_id")
	private Company csCompany;
	
	@ManyToOne
	@JoinColumn(name = "csInd_id")
	private Individual csInd;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	@Column(name = "csholder_percentage")
	private String percentage;
	
	public CompanyShareholders(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int name) {
		this.type = name;
	}
		
	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getCsCompany() {
		return csCompany;
	}

	public void setCsCompany(Company csCompany) {
		this.csCompany = csCompany;
	}

	public Individual getCsInd() {
		return csInd;
	}

	public void setCsInd(Individual csInd) {
		this.csInd = csInd;
	}

}
