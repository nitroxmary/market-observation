package com.nitrox.mo.model.fileClass;

public class SignalClass {
	
	String signal_date;    
	String factorName;        
	String report_date;      
	String signal_orig;     
	String signal_trans;   
	String signal_link;
	String signal_summary;     
	String signal_background;  
	String signal_implication;
	int dropdownCompany;
	String connectedCompany[];
	String connectedCountries[];
	String connectedAuthors[];
	
	
	public SignalClass(){
		
	}

	public String getSignal_date() {
		return signal_date;
	}

	public void setSignal_date(String signal_date) {
		this.signal_date = signal_date;
	}

	public String getFactorName() {
		return factorName;
	}

	public void setFactorName(String factorName) {
		System.out.println("ret " + factorName);
		this.factorName = factorName;
	}

	public String getReport_date() {
		return report_date;
	}

	public void setReport_date(String report_date) {
		this.report_date = report_date;
	}

	public String getSignal_orig() {
		return signal_orig;
	}

	public void setSignal_orig(String signal_orig) {
		this.signal_orig = signal_orig;
	}

	public String getSignal_trans() {
		return signal_trans;
	}

	public void setSignal_trans(String signal_trans) {
		this.signal_trans = signal_trans;
	}

	public String getSignal_link() {
		return signal_link;
	}

	public void setSignal_link(String signal_link) {
		this.signal_link = signal_link;
	}
	
	public int getDropdownCompany() {
		return dropdownCompany;
	}

	public void setDropdownCompany(int dropdownCompany) {
		this.dropdownCompany = dropdownCompany;
	}

	public String getSignal_summary() {
		return signal_summary;
	}

	public void setSignal_summary(String signal_summary) {
		this.signal_summary = signal_summary;
	}

	public String getSignal_background() {
		return signal_background;
	}

	public void setSignal_background(String signal_background) {
		this.signal_background = signal_background;
	}

	public String getSignal_implication() {
		return signal_implication;
	}

	public void setSignal_implication(String signal_implication) {
		this.signal_implication = signal_implication;
	}
	
	public String[] getConnectedCompany() {
		return connectedCompany;
	}

	public void setConnectedCompany(String[] connectedCompany) {
		this.connectedCompany = connectedCompany;
	}

	public String[] getConnectedCountries() {
		return connectedCountries;
	}

	public void setConnectedCountries(String[] connectedCountries) {
		this.connectedCountries = connectedCountries;
	}

	public String[] getConnectedAuthors() {
		return connectedAuthors;
	}

	public void setConnectedAuthors(String[] connectedAuthors) {
		this.connectedAuthors = connectedAuthors;
	}

	@Override
	 public String toString() {
		return new StringBuffer(" Signal : ")
 	   .append(this.factorName)
 	   .append(" date : ")
 	   .append(this.signal_date)
 	   .append(" title : ")
 	   .append(this.signal_trans +" - "+ this.signal_orig)
 	   .append(" company : " + this.dropdownCompany)
 	   .append(" summary : " + this.signal_summary)
 	   .append(" background : " + this.signal_background)
 	   .append(" implication : " + this.signal_implication)
 	   .toString();
	   }
	
}
