package com.nitrox.mo.model.fileClass;

public class IndividualClass{


	String firstName;
	String lastName;
	String middleInitial;
	String address1;
	String address2;
	String address3;
	String email;
	String linkedIn;
	String skype;
	String youtube;
	String twitter;
	String[] compInvolvement;
	String[] educBackground;
	String[] awards;
	String[] nationalities;
	String[] languages;
	String[] phones;
	int personBase;
	
	public IndividualClass(){
	
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLinkedIn() {
		return linkedIn;
	}

	public void setLinkedIn(String linkedIn) {
		this.linkedIn = linkedIn;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getYoutube() {
		return youtube;
	}

	public void setYoutube(String youtube) {
		this.youtube = youtube;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String[] getCompInvolvement() {
		return compInvolvement;
	}

	public void setCompInvolvement(String[] compInvolvement) {
		this.compInvolvement = compInvolvement;
	}

	public String[] getEducBackground() {
		return educBackground;
	}

	public void setEducBackground(String[] educBackground) {
		this.educBackground = educBackground;
	}

	public String[] getAwards() {
		return awards;
	}

	public void setAwards(String[] awards) {
		this.awards = awards;
	}

	public String[] getNationalities() {
		return nationalities;
	}

	public void setNationalities(String[] nationalities) {
		this.nationalities = nationalities;
	}

	public String[] getLanguages() {
		return languages;
	}

	public void setLanguages(String[] languages) {
		this.languages = languages;
	}

	public String[] getPhones() {
		return phones;
	}

	public void setPhones(String[] phones) {
		this.phones = phones;
	}

	public int getPersonBase() {
		return personBase;
	}

	public void setPersonBase(int personBase) {
		this.personBase = personBase;
	}

	
    @Override
    public String toString(){
    	return new StringBuffer(" Individual : ")
    	.append(this.compInvolvement).toString();
    }
	
}
