package com.nitrox.mo.model.fileClass;

public class MeetingClass {

	String eventDate;
	int company;
	String summary;
	String followUp;
	String locAddress;
	int locCountry;
    String[] individuals;
    
    public MeetingClass(){
    	
    }

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public int getCompany() {
		return company;
	}

	public void setCompany(int company) {
		this.company = company;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getFollowUp() {
		return followUp;
	}

	public void setFollowUp(String followUp) {
		this.followUp = followUp;
	}

	public String getLocAddress() {
		return locAddress;
	}

	public void setLocAddress(String locAddress) {
		this.locAddress = locAddress;
	}

	public int getLocCountry() {
		return locCountry;
	}

	public void setLocCountry(int locCountry) {
		this.locCountry = locCountry;
	}

	public String[] getIndividuals() {
		return individuals;
	}

	public void setIndividuals(String[] individuals) {
		this.individuals = individuals;
	}
	

 }
