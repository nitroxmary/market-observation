package com.nitrox.mo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity  
@Table(name = "individual_phones")
public class IndividualPhones {
	@Id
	@GeneratedValue
	@Column(name= "ind_contact_id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "ind_id")
	private Individual ind;
	
	@ManyToOne
	@JoinColumn(name = "phone_id")
	private Phone phone;
	
	public IndividualPhones(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Individual getInd() {
		return ind;
	}

	public void setInd(Individual ind) {
		this.ind = ind;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	
	
}
