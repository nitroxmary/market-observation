package com.nitrox.mo.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nitrox.mo.model.User;

public interface UserService {
	
	public void addUser(User c);
	public void updateUser(User c);
	public void removeUser(User c);
	
	public List<User> list(int id);
	public User getUserId(int id);
	public User getUser(String uname);
	public boolean authenticateUser(String username, String password)throws Exception;
	public String hasSession(HttpSession session, HttpServletRequest request);
	public String getType(HttpSession session, HttpServletRequest request);
} 
