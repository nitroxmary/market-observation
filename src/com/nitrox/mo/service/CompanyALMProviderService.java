package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.CompanyALMProvider;

public interface CompanyALMProviderService {

	public void addCompany(CompanyALMProvider c);
	public void updateCompany(CompanyALMProvider c);
	public void removeCompany(CompanyALMProvider c);
	
	public List<CompanyALMProvider> list();
		
	public CompanyALMProvider getCompanyId(int id);
	
	
	
} 
