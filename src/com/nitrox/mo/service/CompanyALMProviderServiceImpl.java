package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.CompanyALMProviderDAO;
import com.nitrox.mo.model.CompanyALMProvider;

@Service  
@Transactional
public class CompanyALMProviderServiceImpl implements CompanyALMProviderService{

	@Autowired
	private CompanyALMProviderDAO companyDao;
	
	@Override
	public void addCompany(CompanyALMProvider c){
		companyDao.addCompany(c);
	}
	
	@Override
	public void updateCompany(CompanyALMProvider c){
		companyDao.updateCompany(c);
	}
	
	@Override
	public void removeCompany(CompanyALMProvider c){
		companyDao.removeCompany(c);
	}
	
	@Override
	public List<CompanyALMProvider> list(){
		return companyDao.list();
	}
	
	@Override
	public CompanyALMProvider getCompanyId(int id){
		return companyDao.getCompanyId(id);
	}
	
}