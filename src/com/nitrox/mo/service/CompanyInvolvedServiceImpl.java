package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.CompanyInvolvedDAO;
import com.nitrox.mo.model.CompanyInvolved;

@Service  
@Transactional
public class CompanyInvolvedServiceImpl implements CompanyInvolvedService{

	@Autowired
	private CompanyInvolvedDAO companyInvDao;
	
	@Override
	public void addCompanyInvolved(CompanyInvolved c){
		companyInvDao.addCompanyInvolved(c);
	}
	
	@Override
	public void updateCompanyInvolved(CompanyInvolved c){
		companyInvDao.updateCompanyInvolved(c);
	}
	
	@Override
	public void removeCompanyInvolved(CompanyInvolved c){
		companyInvDao.removeCompanyInvolved(c);
	}
	
	@Override
	public List<CompanyInvolved> list(){
		return companyInvDao.list();
	}
	
	@Override
	public List<CompanyInvolved> list(int id){
		return companyInvDao.list(id);
	}
	
	@Override
	public CompanyInvolved getCompanyInvolvedById(int id){
		return companyInvDao.getCompanyInvolvedById(id);
	}
	
	@Override
	public List<CompanyInvolved> listCompanies(int id){
		return companyInvDao.listCompanies(id);
	}
}