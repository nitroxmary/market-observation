package com.nitrox.mo.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.nitrox.mo.dao.UserDAO;
import com.nitrox.mo.model.User;

@Service  
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDAO userDao;
		
	@Override
	public void addUser(User c){
		userDao.addUser(c);
	}
	
	@Override
	public void updateUser(User c){
		userDao.updateUser(c);
	}
	
	@Override
	public void removeUser(User c){
		userDao.removeUser(c);
	}
	
	@Override
	public List<User> list(int id){
		return userDao.list(id);
	}
	
	@Override
	public User getUserId(int id){
		return userDao.getUserId(id);
	}
	
	@Override
	public User getUser(String uname){
		return userDao.getUser(uname);
	}
	
	@Override
	public boolean authenticateUser(String username, String password) throws Exception{
		boolean ok = false;
		String pwrd = userDao.getUser(username).getPsword();
		
		if(!StringUtils.isEmpty(pwrd)){
			
			if(pwrd.equals(password))
				ok= true;
		}
		return ok;
	}
	
	public String hasSession(HttpSession session, HttpServletRequest request){
		String ok = "invalid";
		session = request.getSession(true);
		
		if(!StringUtils.isEmpty(session.getAttribute("userName"))){
			ok = (String)session.getAttribute("userName");
		}
	    return ok;
	}
	
	public String getType(HttpSession session, HttpServletRequest request){
		String type = "";
		session = request.getSession(true);
		
		if(!StringUtils.isEmpty(session.getAttribute("userType"))){
			type = (String)session.getAttribute("userType");
		}
	    return type;
	}
}
