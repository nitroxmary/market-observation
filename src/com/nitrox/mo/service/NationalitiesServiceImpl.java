package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.NationalitiesDAO;
import com.nitrox.mo.model.Nationalities;

@Service  
@Transactional
public class NationalitiesServiceImpl implements NationalitiesService{

	@Autowired
	private NationalitiesDAO natDao;
	
	@Override
	public void addNationalities(Nationalities c){
		natDao.addNationalities(c);
	}
	
	@Override
	public void updateNationalities(Nationalities c){
		natDao.updateNationalities(c);
	}
	
	@Override
	public void removeNationalities(Nationalities c){
		natDao.removeNationalities(c);
	}
	
	@Override
	public List<Nationalities> list(int id){
		return natDao.list(id);
	}
	
	
	@Override
	public List<Nationalities> list(){
		return natDao.list();
	}
	
	@Override
	public Nationalities getNationalitiesById(int id){
		return natDao.getNationalitiesById(id);
	}
	
	
}

