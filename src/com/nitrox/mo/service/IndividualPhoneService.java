package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.IndividualPhones;

public interface IndividualPhoneService {
	
	public void addIndividualPhones(IndividualPhones c);
	public void updateIndividualPhones(IndividualPhones c);
	public void removeIndividualPhones(IndividualPhones c);
	
	
	public IndividualPhones getIndividualPhonesById(int id);
	public String getMaxInd();
	public List<IndividualPhones> list(int id);
	
} 
