package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.CompanyDAO;
import com.nitrox.mo.model.Company;

@Service  
@Transactional
public class CompanyServiceImpl implements CompanyService{

	@Autowired
	private CompanyDAO companyDao;
	
	@Override
	public void addCompany(Company c){
		companyDao.addCompany(c);
	}
	
	@Override
	public void updateCompany(Company c){
		companyDao.updateCompany(c);
	}
	
	@Override
	public void removeCompany(Company c){
		companyDao.removeCompany(c);
	}
	
	@Override
	public List<Company> list(){
		return companyDao.list();
	}
	
	@Override
	public List<Company> holdings(){
		return companyDao.holdings();
	}
	
	@Override
	public List<Company> nameList(){
		//System.out.println("asdf:"+companyDao.list());
		return companyDao.nameList();
	}
	@Override
	public Company getCompanyId(int id){
		return companyDao.getCompanyId(id);
	}
	
}