package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.CompanyShareholders;

public interface CompanyShareholdersService {

public void addCompanyShareholders(CompanyShareholders c);
public void updateCompanyShareholders(CompanyShareholders c);
public void removeCompanyShareholders(CompanyShareholders c);

public List<CompanyShareholders> list(int id);
public List<CompanyShareholders> indLst(int id);
public CompanyShareholders getCompanyShareholdersById(int id);
public List<CompanyShareholders> list();
public List<CompanyShareholders> compList(int id);
} 
