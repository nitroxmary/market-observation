package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.ConnectedCompany;

public interface ConnectedCompanyService {
		
	public void addConnectedCompany(ConnectedCompany c);
	public void updateConnectedCompany(ConnectedCompany c);
	public void removeConnectedCompany(ConnectedCompany c);
	
	public List<ConnectedCompany> list(int id);
	public String getMaxComp();
	public ConnectedCompany getConnectedCompanyById(int id);
	
} 
