package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.PhoneDAO;
import com.nitrox.mo.model.Phone;

@Service  
@Transactional
public class PhoneServiceImpl implements PhoneService{

	@Autowired
	private PhoneDAO phoneDao;
	
	@Override
	public void addPhone(Phone c){
		phoneDao.addPhone(c);
	}
	
	@Override
	public void updatePhone(Phone c){
		phoneDao.updatePhone(c);
	}
	
	@Override
	public void removePhone(Phone c){
		phoneDao.removePhone(c);
	}
	
	@Override
	public List<Phone> list(){
		return phoneDao.list();
	}
	
	@Override
	public Phone getPhoneById(int id){
		return phoneDao.getPhoneById(id);
	}
}
