package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.CountryDAO;
import com.nitrox.mo.model.Countries;

@Service  
@Transactional
public class CountryServiceImpl implements CountryService{

	@Autowired
	private CountryDAO countryDao;
	
	@Override
	public void addCountry(Countries c){
		countryDao.addCountry(c);
	}
	
	@Override
	public void updateCountry(Countries c){
		countryDao.updateCountry(c);
	}
	
	@Override
	public void removeCountry(Countries c){
		countryDao.removeCountry(c);
	}
	
	@Override
	public List<Countries> list(){
		//System.out.println("asdf:"+countryDao.list());
		return countryDao.list();
	}
	
	@Override
	public List<Countries> nameList(){
		return countryDao.nameList();
	}
	
	@Override
	public Countries getCountryById(int id){
		return countryDao.getCountryById(id);
	}
	
	
}
