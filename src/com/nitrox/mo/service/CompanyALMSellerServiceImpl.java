package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.CompanyALMSellerDAO;
import com.nitrox.mo.model.CompanyALMSeller;

@Service  
@Transactional
public class CompanyALMSellerServiceImpl implements CompanyALMSellerService{

	@Autowired
	private CompanyALMSellerDAO companyDao;
	
	@Override
	public void addCompany(CompanyALMSeller c){
		companyDao.addCompany(c);
	}
	
	@Override
	public void updateCompany(CompanyALMSeller c){
		companyDao.updateCompany(c);
	}
	
	@Override
	public void removeCompany(CompanyALMSeller c){
		companyDao.removeCompany(c);
	}
	
	@Override
	public List<CompanyALMSeller> list(){
		return companyDao.list();
	}
	
	@Override
	public List<CompanyALMSeller> list(int id){
		return companyDao.list(id);
	}
	
	@Override
	public CompanyALMSeller getCompanyId(int id){
		return companyDao.getCompanyId(id);
	}
	
	@Override
	public String getMaxSeller(){
		return companyDao.getMaxSeller();
	}
}