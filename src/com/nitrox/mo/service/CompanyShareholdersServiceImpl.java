package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.CompanyShareholdersDAO;
import com.nitrox.mo.model.CompanyShareholders;

@Service  
@Transactional
public class CompanyShareholdersServiceImpl implements CompanyShareholdersService{

	@Autowired
	private CompanyShareholdersDAO companySHldrsDao;
	
	@Override
	public void addCompanyShareholders(CompanyShareholders c){
		companySHldrsDao.addCompanyShareholders(c);
	}
	
	@Override
	public void updateCompanyShareholders(CompanyShareholders c){
		companySHldrsDao.updateCompanyShareholders(c);
	}
	
	@Override
	public void removeCompanyShareholders(CompanyShareholders c){
		companySHldrsDao.removeCompanyShareholders(c);
	}
	
	@Override
	public List<CompanyShareholders> list(int id){
		return companySHldrsDao.list(id);
	}
	
	@Override
	public List<CompanyShareholders> list(){
		return companySHldrsDao.list();
	}
	
	@Override
	public List<CompanyShareholders> compList(int id){
		return companySHldrsDao.compList(id);
	}
	
	@Override
	public List<CompanyShareholders> indLst(int id){
		return companySHldrsDao.indList(id);
	}
	
	@Override
	public CompanyShareholders getCompanyShareholdersById(int id){
		return companySHldrsDao.getCompanyShareholdersById(id);
	}
	
	
}
