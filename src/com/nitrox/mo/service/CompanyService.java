package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.Company;

public interface CompanyService {

	public void addCompany(Company c);
	public void updateCompany(Company c);
	public void removeCompany(Company c);
	
	public List<Company> list();
	public List<Company> nameList();
	public List<Company> holdings();
		
	public Company getCompanyId(int id);
	
	
	
} 
