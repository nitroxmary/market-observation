package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.CompanyFiles;

public interface CompanyFilesService {

	public void addCompanyFiles(CompanyFiles c);
	public void updateCompanyFiles(CompanyFiles c);
	public void removeCompanyFiles(CompanyFiles c);
	
	public List<CompanyFiles> list(int id);
	public CompanyFiles getCompanyFilesId(int id);
	public List<CompanyFiles> list();
} 