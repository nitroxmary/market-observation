package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.ConnectedCompanyDAO;
import com.nitrox.mo.model.ConnectedCompany;

@Service  
@Transactional
public class ConnectedCompanyServiceImpl implements ConnectedCompanyService{

	@Autowired
	private ConnectedCompanyDAO connectedCompDao;
	
	@Override
	public void addConnectedCompany(ConnectedCompany c){
		connectedCompDao.addConnectedCompany(c);
	}
	
	@Override
	public void updateConnectedCompany(ConnectedCompany c){
		connectedCompDao.updateConnectedCompany(c);
	}
	
	@Override
	public void removeConnectedCompany(ConnectedCompany c){
		connectedCompDao.removeConnectedCompany(c);
	}
	
	@Override
	public List<ConnectedCompany> list(int id){
		return connectedCompDao.list(id);
	}
	
	@Override
	public String getMaxComp(){
		return connectedCompDao.getMaxComp();
	}
	
	@Override
	public ConnectedCompany getConnectedCompanyById(int id){
		return connectedCompDao.getConnectedCompanyById(id);
	}
}
