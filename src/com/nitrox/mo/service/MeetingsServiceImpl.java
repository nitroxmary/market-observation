package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.MeetingsDAO;
import com.nitrox.mo.model.Meetings;

@Service  
@Transactional
public class MeetingsServiceImpl implements MeetingsService{

	@Autowired
	private MeetingsDAO meetingsDao;
	
	@Override
	public void addMeetings(Meetings c){
		meetingsDao.addMeetings(c);
	}
	
	@Override
	public void updateMeetings(Meetings c){
		meetingsDao.updateMeetings(c);
	}
	
	@Override
	public void removeMeetings(Meetings c){
		meetingsDao.removeMeetings(c);
	}
	
	@Override
	public List<Meetings> list(){
		return meetingsDao.list();
	}
	
	@Override
	public List<Meetings> list(int id){
		return meetingsDao.list(id);
	}
	
	@Override
	public Meetings getMeetingsById(int id){
		return meetingsDao.getMeetingsById(id);
	}
}