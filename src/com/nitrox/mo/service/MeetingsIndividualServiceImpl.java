package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.MeetingsIndividualDAO;
import com.nitrox.mo.model.MeetingsIndividual;

@Service  
@Transactional
public class MeetingsIndividualServiceImpl implements MeetingsIndividualService{

	@Autowired
	private MeetingsIndividualDAO meetIndDao;
	
	@Override
	public void addMeetingsIndividual(MeetingsIndividual c){
		meetIndDao.addMeetingsIndividual(c);
	}
	
	@Override
	public void updateMeetingsIndividual(MeetingsIndividual c){
		meetIndDao.updateMeetingsIndividual(c);
	}
	
	@Override
	public void removeMeetingsIndividual(MeetingsIndividual c){
		meetIndDao.removeMeetingsIndividual(c);
	}
	
	@Override
	public List<MeetingsIndividual> list(int id){
		return meetIndDao.list(id);
	}
	
	@Override
	public List<MeetingsIndividual> indList(int id){
		return meetIndDao.indList(id);
	}
	
	@Override
	public String getMaxAttendees(){
		return meetIndDao.getMaxAttendees();
	}
	
	@Override
	public MeetingsIndividual getMeetingsIndividualById(int id){
		return meetIndDao.getMeetingsIndividualById(id);
	}
}
