package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.LanguageDAO;
import com.nitrox.mo.model.Language;

@Service  
@Transactional
public class LanguageServiceImpl implements LanguageService{

	@Autowired
	private LanguageDAO languageDao;
	
	@Override
	public void addLanguage(Language c){
		languageDao.addLanguage(c);
	}
	
	@Override
	public void updateLanguage(Language c){
		languageDao.updateLanguage(c);
	}
	
	@Override
	public void removeLanguage(Language c){
		languageDao.removeLanguage(c);
	}
	
	@Override
	public List<Language> list(int id){
		return languageDao.list(id);
	}
	
	@Override
	public Language getLanguageById(int id){
		return languageDao.getLanguageById(id);
	}
}
