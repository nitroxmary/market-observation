package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.Nationalities;

public interface NationalitiesService {

	public void addNationalities(Nationalities c);
	public void updateNationalities(Nationalities c);
	public void removeNationalities(Nationalities c);
	
	public List<Nationalities> list(int id);
	public List<Nationalities> list();
	public Nationalities getNationalitiesById(int id);
	
} 
