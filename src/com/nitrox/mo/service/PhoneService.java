package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.Phone;

public interface PhoneService {

	public void addPhone(Phone c);

	public void updatePhone(Phone c);

	public void removePhone(Phone c);

	public List<Phone> list();

	public Phone getPhoneById(int id);

}
