package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.ContactDetailsDAO;
import com.nitrox.mo.model.ContactDetails;

@Service  
@Transactional
public class ContactDetailsServiceImpl implements ContactDetailsService{

	@Autowired
	private ContactDetailsDAO contactDao;
	
	@Override
	public void addContactDetails(ContactDetails c){
		contactDao.addContactDetails(c);
	}
	
	@Override
	public void updateContactDetails(ContactDetails c){
		contactDao.updateContactDetails(c);
	}
	
	@Override
	public void removeContactDetails(ContactDetails c){
		contactDao.removeContactDetails(c);
	}
	
	@Override
	public List<ContactDetails> list(){
		return contactDao.list();
	}
	
	@Override
	public ContactDetails getContactDetailsById(int id){
		return contactDao.getContactDetailsById(id);
	}
}