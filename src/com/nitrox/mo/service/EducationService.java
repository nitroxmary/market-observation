package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.Education;

public interface EducationService {

	public void addEducation(Education c);
	public void updateEducation(Education c);
	public void removeEducation(Education c);
	
	public List<Education> list(int id);
	public List<Education> list();
	public Education getEducationById(int id);
	
} 