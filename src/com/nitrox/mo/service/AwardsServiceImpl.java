package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.AwardsDAO;
import com.nitrox.mo.model.Awards;

@Service  
@Transactional
public class AwardsServiceImpl implements AwardsService{

	@Autowired
	private AwardsDAO awardsDao;
	
	@Override
	public void addAwards(Awards c){
		awardsDao.addAwards(c);
	}
	
	@Override
	public void updateAwards(Awards c){
		awardsDao.updateAwards(c);
	}
	
	@Override
	public void removeAwards(Awards c){
		awardsDao.removeAwards(c);
	}
	
	@Override
	public List<Awards> list(int id){
		return awardsDao.list(id);
	}
	
	@Override
	public Awards getAwardsId(int id){
		return awardsDao.getAwardsId(id);
	}
}
