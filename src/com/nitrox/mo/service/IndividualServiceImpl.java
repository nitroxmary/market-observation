package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.IndividualDAO;
import com.nitrox.mo.model.Individual;

@Service  
@Transactional
public class IndividualServiceImpl implements IndividualService{

	@Autowired
	private IndividualDAO indDao;
	
	@Override
	public void addIndividual(Individual c){
		indDao.addIndividual(c);
	}
	
	@Override
	public void updateIndividual(Individual c){
		indDao.updateIndividual(c);
	}
	
	@Override
	public void removeIndividual(Individual c){
		indDao.removeIndividual(c);
	}
	
	@Override
	public List<Individual> list(){
		return indDao.list();
	}
	
	@Override
	public List<Individual> nameList(){
		return indDao.nameList();
	}
	
	@Override
	public Individual getIndividualById(int id){
		return indDao.getIndividualById(id);
	}
}
