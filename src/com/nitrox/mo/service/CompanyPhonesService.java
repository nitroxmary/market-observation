package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.CompanyPhone;

public interface CompanyPhonesService {

	public void addCompanyPhones(CompanyPhone c);
	public void updateCompanyPhones(CompanyPhone c);
	public void removeCompanyPhones(CompanyPhone c);
	
	public List<CompanyPhone> list();
	public List<CompanyPhone> nameList(int id);
		
	public CompanyPhone getCompanyPhonesId(int id);
		
} 
