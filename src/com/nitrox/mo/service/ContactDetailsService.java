package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.ContactDetails;

public interface ContactDetailsService {

	public void addContactDetails(ContactDetails c);
	public void updateContactDetails(ContactDetails c);
	public void removeContactDetails(ContactDetails c);
	
	public List<ContactDetails> list();
	public ContactDetails getContactDetailsById(int id);
	
}
