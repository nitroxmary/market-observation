package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.ConnectedCountriesDAO;
import com.nitrox.mo.model.ConnectedCountries;

@Service  
@Transactional
public class ConnectedCountriesServiceImpl implements ConnectedCountriesService{

	@Autowired
	private ConnectedCountriesDAO connectedCountriesDao;
	
	@Override
	public void addConnectedCountries(ConnectedCountries c){
		connectedCountriesDao.addConnectedCountries(c);
	}
	
	@Override
	public void updateConnectedCountries(ConnectedCountries c){
		connectedCountriesDao.updateConnectedCountries(c);
	}
	
	@Override
	public void removeConnectedCountries(ConnectedCountries c){
		connectedCountriesDao.removeConnectedCountries(c);
	}
	
	@Override
	public List<ConnectedCountries> list(int id){
		return connectedCountriesDao.list(id);
	}
	public String getMaxCountries(){
		return connectedCountriesDao.getMaxCountries();
	}
	@Override
	public ConnectedCountries getConnectedCountriesById(int id){
		return connectedCountriesDao.getConnectedCountriesById(id);
	}
}
