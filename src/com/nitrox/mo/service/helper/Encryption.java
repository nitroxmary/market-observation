package com.nitrox.mo.service.helper;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.paypal.base.codec.binary.Base64;

@Service  
@Transactional
public class Encryption {

	private final static byte[] key= {(byte)0x65, (byte)0x91, (byte)0x85, (byte)0x79, (byte)0x11, (byte)0x3a, (byte)0xb9, (byte)0x47, (byte)0xd6, (byte)0x56, (byte)0x01, (byte)0x87, 
		(byte)0x90, (byte)0x60, (byte)0x64, (byte)0x9e};


	public String encrypt(String strToEncrypt) throws Exception{
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");  
		final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		String encryptedString = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes()));
		return encryptedString;
	}
	
	public String decrypt(String strToDecrypt) throws Exception{
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
		return decryptedString;
	}
}
