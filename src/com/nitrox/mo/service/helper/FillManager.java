package com.nitrox.mo.service.helper;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nitrox.mo.model.Company;
import com.nitrox.mo.model.CompanyALMProvider;
import com.nitrox.mo.model.CompanyALMSeller;
import com.nitrox.mo.model.CompanyFiles;
import com.nitrox.mo.model.CompanyInvolved;
import com.nitrox.mo.model.CompanyShareholders;
import com.nitrox.mo.model.ConnectedCompany;
import com.nitrox.mo.model.ConnectedCountries;
import com.nitrox.mo.model.ConnectedIndividual;
import com.nitrox.mo.model.ContactDetails;
import com.nitrox.mo.model.Education;
import com.nitrox.mo.model.Individual;
import com.nitrox.mo.model.IndividualPhones;
import com.nitrox.mo.model.MarketSignal;
import com.nitrox.mo.model.Meetings;
import com.nitrox.mo.model.MeetingsIndividual;
import com.nitrox.mo.model.Nationalities;
import com.nitrox.mo.service.CompanyALMSellerService;
import com.nitrox.mo.service.CompanyPhonesService;
import com.nitrox.mo.service.CompanyService;
import com.nitrox.mo.service.ConnectedCompanyService;
import com.nitrox.mo.service.ConnectedCountriesService;
import com.nitrox.mo.service.ConnectedIndividualService;
import com.nitrox.mo.service.IndividualPhoneService;
import com.nitrox.mo.service.IndividualService;
import com.nitrox.mo.service.MeetingsIndividualService;

@Service  
@Transactional
public class FillManager {
 
 /**
  * Fills the report with content
  * 
  * @param worksheet
  * @param startRowIndex starting row offset
  * @param startColIndex starting column offset
  * @param datasource the data source
  */
	
	  
 @Autowired
 private ConnectedCompanyService connCompService;
 
 @Autowired
 private CompanyService companyService;
 
 @Autowired
 private CompanyPhonesService compPhonesService;
 
 @Autowired
 private IndividualService indService;
 
 @Autowired
 private CompanyALMSellerService compALMSellerService;
 	 
 @Autowired
 private ConnectedIndividualService connIndService;
	 
 @Autowired
 private ConnectedCountriesService connCountService;
 
 @Autowired
 private MeetingsIndividualService meetIndService;
 
 @Autowired
 private IndividualPhoneService indPhoneService;
 
	 
 public  void IndividualReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<Individual> datasource) {
  // Row offset
  startRowIndex += 2;
   
  // Create cell style for the body
  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  bodyCellStyle.setWrapText(true);
   
  // Create body
  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
   // Create a new row
   HSSFRow row = worksheet.createRow((short) i+1);
 
   HSSFCell cell1 = row.createCell(startColIndex+0);
   cell1.setCellValue(datasource.get(i-2).getLastName());
   cell1.setCellStyle(bodyCellStyle);
 
   HSSFCell cell2 = row.createCell(startColIndex+1);
   cell2.setCellValue(datasource.get(i-2).getMiddleInitial());
   cell2.setCellStyle(bodyCellStyle);
 
   HSSFCell cell3 = row.createCell(startColIndex+2);
   cell3.setCellValue(datasource.get(i-2).getFirstName());
   cell3.setCellStyle(bodyCellStyle);
 
   HSSFCell cell4 = row.createCell(startColIndex+3);
   cell4.setCellValue(datasource.get(i-2).getCountry().getName());
   cell4.setCellStyle(bodyCellStyle);
 
  }
 }
 
 public  void ConnectedIndividualReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<ConnectedIndividual> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	   
	   String[] signalDate = (datasource.get(i-2).getMarketSignal().getSignalDate().toString()).split(" ");
	   
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   cell1.setCellValue(datasource.get(i-2).getInd().getFirstName() + " " + datasource.get(i-2).getInd().getMiddleInitial() + ". " + datasource.get(i-2).getInd().getLastName());
	   cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   cell2.setCellValue(signalDate[0]);
	   cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   cell3.setCellValue(datasource.get(i-2).getMarketSignal().getFactor());
	   cell3.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   cell4.setCellValue(datasource.get(i-2).getMarketSignal().getCompany().getName());
	   cell4.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell5 = row.createCell(startColIndex+4);
	   cell5.setCellValue(datasource.get(i-2).getMarketSignal().getOrigTitle());
	   cell5.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell6 = row.createCell(startColIndex+5);
	   cell6.setCellValue(datasource.get(i-2).getMarketSignal().getTransTitle());
	   cell6.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell7 = row.createCell(startColIndex+6);
	   cell7.setCellValue(datasource.get(i-2).getMarketSignal().getLink());
	   cell7.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell8 = row.createCell(startColIndex+7);
	   cell8.setCellValue(datasource.get(i-2).getMarketSignal().getOverview());
	   cell8.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell9 = row.createCell(startColIndex+8);
	   cell9.setCellValue(datasource.get(i-2).getMarketSignal().getBackground());
	   cell9.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell10 = row.createCell(startColIndex+9);
	   cell10.setCellValue(datasource.get(i-2).getMarketSignal().getImplication());
	   cell10.setCellStyle(bodyCellStyle);
	   
	   int countComp = 0;
	   int countInd = 0;
	   int countCount = 0;
	   int lastColCompCount = startColIndex+10; 
	   int lastColCountryCount = lastColCompCount+getMaxComp(); 
	   int lastColIndCount = lastColCountryCount+getMaxCountries(); 
	   
	   List<ConnectedCompany> dataCompSource = getCompList(datasource.get(i-2).getMarketSignal().getId());
	   countComp = dataCompSource.size();
	   if(countComp > 0){
		   for(int x=0; x < countComp; x++){
			   HSSFCell xx = row.createCell(lastColCompCount + x);
			   xx.setCellValue(dataCompSource.get(x).getCompany().getName());
			   xx.setCellStyle(bodyCellStyle);
		   }
	   }
	 
	   List<ConnectedCountries> dataCountSource = getCountryList(datasource.get(i-2).getMarketSignal().getId());
	   countCount = dataCountSource.size();
	   if(countCount > 0){
		   for(int y=0; y < countCount; y++){
			   HSSFCell yy = row.createCell(lastColCountryCount + y);
			   yy.setCellValue(dataCountSource.get(y).getCountry().getName());
			   yy.setCellStyle(bodyCellStyle);
		   }
	   }
	   
	   List<ConnectedIndividual> dataIndSource = getIndList(datasource.get(i-2).getMarketSignal().getId());
	   countInd = dataIndSource.size();
	   if(countInd > 0){
		   for(int z=0; z < countInd; z++){
			   HSSFCell zz = row.createCell(lastColIndCount + z);
			   zz.setCellValue(dataIndSource.get(z).getInd().getFirstName() +" " + dataIndSource.get(z).getInd().getMiddleInitial() +". " + dataIndSource.get(z).getInd().getLastName());
			   zz.setCellStyle(bodyCellStyle);
		   }
	   }
	  }
}
 
 public  void MarketSignalReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<MarketSignal> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	   
	   String[] inputDate = (datasource.get(i-2).getInputDate().toString()).split(" ");
	   String[] signalDate = (datasource.get(i-2).getSignalDate().toString()).split(" ");
	   
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   cell1.setCellValue(inputDate[0]);
	   cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   cell2.setCellValue(signalDate[0]);
	   cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   cell3.setCellValue(datasource.get(i-2).getFactor());
	   cell3.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   cell4.setCellValue(datasource.get(i-2).getCompany().getName());
	   cell4.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell5 = row.createCell(startColIndex+4);
	   cell5.setCellValue(datasource.get(i-2).getOrigTitle());
	   cell5.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell6 = row.createCell(startColIndex+5);
	   cell6.setCellValue(datasource.get(i-2).getTransTitle());
	   cell6.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell7 = row.createCell(startColIndex+6);
	   cell7.setCellValue(datasource.get(i-2).getLink());
	   cell7.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell8 = row.createCell(startColIndex+7);
	   cell8.setCellValue(datasource.get(i-2).getOverview());
	   cell8.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell9 = row.createCell(startColIndex+8);
	   cell9.setCellValue(datasource.get(i-2).getBackground());
	   cell9.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell10 = row.createCell(startColIndex+9);
	   cell10.setCellValue(datasource.get(i-2).getImplication());
	   cell10.setCellStyle(bodyCellStyle);
	   
	   int countComp = 0;
	   int countInd = 0;
	   int countCount = 0;
	   int lastColCompCount = startColIndex+10; 
	   int lastColCountryCount = lastColCompCount+getMaxComp(); 
	   int lastColIndCount = lastColCountryCount+getMaxCountries(); 
	   
	   List<ConnectedCompany> dataCompSource = getCompList(datasource.get(i-2).getId());
	   countComp = dataCompSource.size();
	   if(countComp > 0){
		   for(int x=0; x < countComp; x++){
			   HSSFCell xx = row.createCell(lastColCompCount + x);
			   xx.setCellValue(dataCompSource.get(x).getCompany().getName());
			   xx.setCellStyle(bodyCellStyle);
		   }
	   }
	 
	   List<ConnectedCountries> dataCountSource = getCountryList(datasource.get(i-2).getId());
	   countCount = dataCountSource.size();
	   if(countCount > 0){
		   for(int y=0; y < countCount; y++){
			   HSSFCell yy = row.createCell(lastColCountryCount + y);
			   yy.setCellValue(dataCountSource.get(y).getCountry().getName());
			   yy.setCellStyle(bodyCellStyle);
		   }
	   }
	   
	   List<ConnectedIndividual> dataIndSource = getIndList(datasource.get(i-2).getId());
	   countInd = dataIndSource.size();
	   if(countInd > 0){
		   for(int z=0; z < countInd; z++){
			   HSSFCell zz = row.createCell(lastColIndCount + z);
			   zz.setCellValue(dataIndSource.get(z).getInd().getFirstName() +" " + dataIndSource.get(z).getInd().getMiddleInitial() +". " + dataIndSource.get(z).getInd().getLastName());
			   zz.setCellStyle(bodyCellStyle);
		   }
	   }
	   
	  }
 }
 
 
 public  void CompanyReport1(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<Company> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   cell1.setCellValue(datasource.get(i-2).getName());
	   cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   cell2.setCellValue(datasource.get(i-2).getWebsite());
	   cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   cell3.setCellValue(datasource.get(i-2).getNotes());
	   cell3.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   cell4.setCellValue(this.getType(datasource.get(i-2).getType()));
	   cell4.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell5 = row.createCell(startColIndex+4);
	   cell5.setCellValue(this.isActive(datasource.get(i-2).getIsHolding()));
	   cell5.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell6 = row.createCell(startColIndex+5);
	   cell6.setCellValue(datasource.get(i-2).getCountry().getName());
	   cell6.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell7 = row.createCell(startColIndex+6);
	   cell7.setCellValue(this.isActive(datasource.get(i-2).getIsActive()));
	   cell7.setCellStyle(bodyCellStyle);
	   
	   if(datasource.get(i-2).getContact() != null){
		   HSSFCell cell8 = row.createCell(startColIndex+7);
		   cell8.setCellValue(datasource.get(i-2).getContact().getAddress1());
		   cell8.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell9 = row.createCell(startColIndex+8);
		   cell9.setCellValue(datasource.get(i-2).getContact().getAddress2());
		   cell9.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell10 = row.createCell(startColIndex+9);
		   cell10.setCellValue(datasource.get(i-2).getContact().getAddress3());
		   cell10.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell11 = row.createCell(startColIndex+10);
		   cell11.setCellValue(datasource.get(i-2).getContact().getEmail());
		   cell11.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell12 = row.createCell(startColIndex+11);
		   cell12.setCellValue(datasource.get(i-2).getContact().getLinkedIn());
		   cell12.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell13 = row.createCell(startColIndex+12);
		   cell13.setCellValue(datasource.get(i-2).getContact().getYoutube());
		   cell13.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell14 = row.createCell(startColIndex+12);
		   cell14.setCellValue(datasource.get(i-2).getContact().getTwitter());
		   cell14.setCellStyle(bodyCellStyle);
	   }
	  }
}
 
 public  void MeetingReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<Meetings> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   String[] meetDate = (datasource.get(i-2).getDate().toString()).split(" ");

	   
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   cell1.setCellValue(meetDate[0]);
	   cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   cell2.setCellValue(datasource.get(i-2).getCompany().getName());
	   cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   cell3.setCellValue(datasource.get(i-2).getSummary());
	   cell3.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   cell4.setCellValue(datasource.get(i-2).getFollow_up());
	   cell4.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell5 = row.createCell(startColIndex+4);
	   cell5.setCellValue(datasource.get(i-2).getLocation());
	   cell5.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell6 = row.createCell(startColIndex+5);
	   cell6.setCellValue(datasource.get(i-2).getCountry().getName());
	   cell6.setCellStyle(bodyCellStyle);
	 
	   int countInd = 0;
	   int lastColIndCount = startColIndex+6;  
	   
	   List<MeetingsIndividual> dataIndSource = getAttendeeList(datasource.get(i-2).getId());
	   countInd = dataIndSource.size();
	   if(countInd > 0){
		   for(int z=0; z < countInd; z++){
			   HSSFCell zz = row.createCell(lastColIndCount + z);
			   zz.setCellValue(dataIndSource.get(z).getInd().getFirstName() +" " + dataIndSource.get(z).getInd().getMiddleInitial() +". " + dataIndSource.get(z).getInd().getLastName());
			   zz.setCellStyle(bodyCellStyle);
		   }
	   }
	   
	  }
}
 
 public  void IndMeetingReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<MeetingsIndividual> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   String[] inputDate = (datasource.get(i-2).getMeeting().getDate().toString()).split(" ");

	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   cell1.setCellValue(inputDate[0]);
	   cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   cell2.setCellValue(datasource.get(i-2).getMeeting().getCompany().getName());
	   cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   cell3.setCellValue(datasource.get(i-2).getMeeting().getSummary());
	   cell3.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   cell4.setCellValue(datasource.get(i-2).getMeeting().getFollow_up());
	   cell4.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell5 = row.createCell(startColIndex+4);
	   cell5.setCellValue(datasource.get(i-2).getMeeting().getLocation());
	   cell5.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell6 = row.createCell(startColIndex+5);
	   cell6.setCellValue(datasource.get(i-2).getMeeting().getCountry().getName());
	   cell6.setCellStyle(bodyCellStyle);
	 
	   int countInd = 0;
	   int lastColIndCount = startColIndex+6;  
	   
	   List<MeetingsIndividual> dataIndSource = getAttendeeList(datasource.get(i-2).getMeeting().getId());
	   countInd = dataIndSource.size();
	   if(countInd > 0){
		   for(int z=0; z < countInd; z++){
			   HSSFCell zz = row.createCell(lastColIndCount + z);
			   zz.setCellValue(dataIndSource.get(z).getInd().getFirstName() +" " + dataIndSource.get(z).getInd().getMiddleInitial() +". " + dataIndSource.get(z).getInd().getLastName());
			   zz.setCellStyle(bodyCellStyle);
		   }
	   }
	   
	  }
}

 public  void CompanyReport2(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<CompanyALMSeller> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
		   HSSFRow row = worksheet.createRow((short) i+1);
		 
		   HSSFCell cell1 = row.createCell(startColIndex+0);
		   cell1.setCellValue(datasource.get(i-2).getCompany().getName());
		   cell1.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell2 = row.createCell(startColIndex+1);
		   cell2.setCellValue(this.getType(datasource.get(i-2).getCompany().getType()));
		   cell2.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell3 = row.createCell(startColIndex+2);
		   cell3.setCellValue(datasource.get(i-2).getCompany().getCountry().getName());
		   cell3.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell4 = row.createCell(startColIndex+3);
		   cell4.setCellValue(this.isActive(datasource.get(i-2).getCompany().getIsActive()));
		   cell4.setCellStyle(bodyCellStyle);
		   
	       HSSFCell cellxx = row.createCell(startColIndex+4);
		   cellxx.setCellValue(datasource.get(i-2).getAppname());
		   cellxx.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cellxxx = row.createCell(startColIndex+5);
		   cellxxx.setCellValue(datasource.get(i-2).getAlmDesc());
		   cellxxx.setCellStyle(bodyCellStyle);
	   
	   }
}
 
 public  void CompanyReport3(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<CompanyALMProvider> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
		   HSSFRow row = worksheet.createRow((short) i+1);
		 
		   HSSFCell cell1 = row.createCell(startColIndex+0);
		   cell1.setCellValue(datasource.get(i-2).getCompany().getName());
		   cell1.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell2 = row.createCell(startColIndex+1);
		   cell2.setCellValue(this.getType(datasource.get(i-2).getCompany().getType()));
		   cell2.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell3 = row.createCell(startColIndex+2);
		   cell3.setCellValue(datasource.get(i-2).getCompany().getCountry().getName());
		   cell3.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell4 = row.createCell(startColIndex+3);
		   cell4.setCellValue(this.isActive(datasource.get(i-2).getCompany().getIsActive()));
		   cell4.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cellxx = row.createCell(startColIndex+4);
		   cellxx.setCellValue(datasource.get(i-2).getAppName());
		   cellxx.setCellStyle(bodyCellStyle);
		
		   HSSFCell cellxxx = row.createCell(startColIndex+5);
		   cellxxx.setCellValue(datasource.get(i-2).getAlmProvider().getName());
		   cellxxx.setCellStyle(bodyCellStyle);
		   
	   }
}
 
 public  void CompanyReport4(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<CompanyShareholders> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
		   HSSFRow row = worksheet.createRow((short) i+1);
		 
		   HSSFCell cell1 = row.createCell(startColIndex+0);
		   cell1.setCellValue(datasource.get(i-2).getCompany().getName());
		   cell1.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell2 = row.createCell(startColIndex+1);
		   cell2.setCellValue(this.getType(datasource.get(i-2).getCompany().getType()));
		   cell2.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell3 = row.createCell(startColIndex+2);
		   cell3.setCellValue(datasource.get(i-2).getCompany().getCountry().getName());
		   cell3.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell4 = row.createCell(startColIndex+3);
		   cell4.setCellValue(this.isActive(datasource.get(i-2).getCompany().getIsActive()));
		   cell4.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cellxx = row.createCell(startColIndex+4);
		   cellxx.setCellValue(datasource.get(i-2).getCsCompany().getName());
		   cellxx.setCellStyle(bodyCellStyle);
		
		   HSSFCell cellxxx = row.createCell(startColIndex+5);
		   cellxxx.setCellValue(datasource.get(i-2).getPercentage());
		   cellxxx.setCellStyle(bodyCellStyle);
		   
	   }
}
 
 public  void CompanyReport5(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<CompanyFiles> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
		   HSSFRow row = worksheet.createRow((short) i+1);
		 
		   HSSFCell cell1 = row.createCell(startColIndex+0);
		   cell1.setCellValue(datasource.get(i-2).getCompany().getName());
		   cell1.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell2 = row.createCell(startColIndex+1);
		   cell2.setCellValue(this.getType(datasource.get(i-2).getCompany().getType()));
		   cell2.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell3 = row.createCell(startColIndex+2);
		   cell3.setCellValue(datasource.get(i-2).getCompany().getCountry().getName());
		   cell3.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell4 = row.createCell(startColIndex+3);
		   cell4.setCellValue(this.isActive(datasource.get(i-2).getCompany().getIsActive()));
		   cell4.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell5 = row.createCell(startColIndex+4);
		   cell5.setCellValue(datasource.get(i-2).getFileName());
		   cell5.setCellStyle(bodyCellStyle);
		
		   HSSFCell cell6 = row.createCell(startColIndex+5);
		   cell6.setCellValue(datasource.get(i-2).getFileDesc());
		   cell6.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell7 = row.createCell(startColIndex+6);
		   cell7.setCellValue(datasource.get(i-2).getFileType());
		   cell7.setCellStyle(bodyCellStyle);
		
		   HSSFCell cell8 = row.createCell(startColIndex+7);
		   cell8.setCellValue(datasource.get(i-2).getFileExt());
		   cell8.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell9 = row.createCell(startColIndex+8);
		   cell9.setCellValue(datasource.get(i-2).getFileYear());
		   cell9.setCellStyle(bodyCellStyle);
		   
	   }
}
 
 public  void IndCompSharesReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<CompanyShareholders> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   cell1.setCellValue(datasource.get(i-2).getCompany().getName());
	   cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   cell2.setCellValue(this.getType(datasource.get(i-2).getCompany().getType()));
	   cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   cell3.setCellValue(datasource.get(i-2).getCompany().getCountry().getName());
	   cell3.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   cell4.setCellValue(datasource.get(i-2).getPercentage());
	   cell4.setCellStyle(bodyCellStyle);
	   
	   }
}
 
 public  void CompanySharesReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<CompanyShareholders> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   cell1.setCellValue(datasource.get(i-2).getCompany().getName());
	   cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   cell2.setCellValue(this.getType(datasource.get(i-2).getCompany().getType()));
	   cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   cell3.setCellValue(datasource.get(i-2).getCompany().getCountry().getName());
	   cell3.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   cell4.setCellValue(this.isActive(datasource.get(i-2).getCompany().getIsActive()));
	   cell4.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell41 = row.createCell(startColIndex+4);
	   cell41.setCellValue(this.shareType(datasource.get(i-2).getType()));
	   cell41.setCellStyle(bodyCellStyle);
	   
	   if(datasource.get(i-2).getCsCompany() != null){
		   
		   HSSFCell cell5 = row.createCell(startColIndex+5);
		   cell5.setCellValue(datasource.get(i-2).getCsCompany().getName());
		   cell5.setCellStyle(bodyCellStyle);
		   
		   
	   }else{
		   HSSFCell cell5 = row.createCell(startColIndex+5);
		   cell5.setCellValue(this.getIndName(datasource.get(i-2).getCsInd().getId()));
		   cell5.setCellStyle(bodyCellStyle);
	   }
	   
	   HSSFCell cell6 = row.createCell(startColIndex+6);
	   cell6.setCellValue(datasource.get(i-2).getPercentage());
	   cell6.setCellStyle(bodyCellStyle);
	   
	   }
}

 
  
 public  void IndDetailsReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, int id, ContactDetails cd, int type) {
	  // Row offset
	  startRowIndex += 2;
	// Create font style for the headers
	  Font font = worksheet.getWorkbook().createFont();
	       font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	 
	// Create cell style for the headers
	  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
	  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
	  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
	  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
	  headerCellStyle.setWrapText(true);
	  headerCellStyle.setFont(font);
	  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
	  
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  // Create a new row
	   HSSFRow row1 = worksheet.createRow((short) startRowIndex+1);
	   row1.setHeight((short) 500);
	   HSSFCell hcell1 = row1.createCell(startColIndex);
	   hcell1.setCellValue("Contact Address 1 : ");
	   hcell1.setCellStyle(headerCellStyle);
	  
	   HSSFRow row2 = worksheet.createRow((short) startRowIndex+2);
	   row2.setHeight((short) 500);
	   HSSFCell hcell2 = row2.createCell(startColIndex);
	   hcell2.setCellValue("Contact Address 2 : ");
	   hcell2.setCellStyle(headerCellStyle);
			   
	   HSSFRow row3 = worksheet.createRow((short) startRowIndex+3);
	   row3.setHeight((short) 500);
	   HSSFCell hcell3 = row3.createCell(startColIndex);
	   hcell3.setCellValue("Contact Address 3 : ");
	   hcell3.setCellStyle(headerCellStyle);
	   
	   HSSFRow row4 = worksheet.createRow((short) startRowIndex+4);
	   row4.setHeight((short) 500);
	   HSSFCell hcell4 = row4.createCell(startColIndex);
	   hcell4.setCellValue("Email : ");
	   hcell4.setCellStyle(headerCellStyle);
	   
	   HSSFRow row5 = worksheet.createRow((short) startRowIndex+5);
	   row5.setHeight((short) 500);
	   HSSFCell hcell5 = row5.createCell(startColIndex);
	   hcell5.setCellValue("LinkeIn : ");
	   hcell5.setCellStyle(headerCellStyle);
	   
	   HSSFRow row6 = worksheet.createRow((short) startRowIndex+6);
	   row6.setHeight((short) 500);
	   HSSFCell hcell6 = row6.createCell(startColIndex);
	   hcell6.setCellValue("Skype : ");
	   hcell6.setCellStyle(headerCellStyle);
	   
	   HSSFRow row7 = worksheet.createRow((short) startRowIndex+7);
	   row7.setHeight((short) 500);
	   HSSFCell hcell7 = row7.createCell(startColIndex);
	   hcell7.setCellValue("Youtube : ");
	   hcell7.setCellStyle(headerCellStyle);

	   HSSFRow row8 = worksheet.createRow((short) startRowIndex+8);
	   row8.setHeight((short) 500);
	   HSSFCell hcell8 = row8.createCell(startColIndex);
	   hcell8.setCellValue("Twitter : ");
	   hcell8.setCellStyle(headerCellStyle);
	   
	   HSSFRow rowheader9 = worksheet.createRow((short) startRowIndex + 9);
	   rowheader9.setHeight((short) 500);
	   
	   HSSFCell hcell9 = rowheader9.createCell(startColIndex + 1);
	   hcell9.setCellValue("Phone Number");
	   hcell9.setCellStyle(headerCellStyle);
	   
	   HSSFCell hcell10 = rowheader9.createCell(startColIndex + 2);
	   hcell10.setCellValue("Phone Type");
	   hcell10.setCellStyle(headerCellStyle);
	   
	   HSSFCell hcell11 = rowheader9.createCell(startColIndex + 3);
	   hcell11.setCellValue("Area Code");
	   hcell11.setCellStyle(headerCellStyle);
	   
	   HSSFCell hcell12 = rowheader9.createCell(startColIndex + 4);
	   hcell12.setCellValue("Country Code");
	   hcell12.setCellStyle(headerCellStyle);
	   
	   if(type == 1){
		   
		   HSSFCell h1 = row1.createCell(startColIndex + 3);
		   h1.setCellValue("Company Name : ");
		   h1.setCellStyle(headerCellStyle);
		   
		   HSSFCell h11 = row1.createCell(startColIndex + 4);
		   h11.setCellValue(this.getCompany(id).getName());
		   h11.setCellStyle(bodyCellStyle);
		   
		   HSSFCell h2 = row2.createCell(startColIndex + 3);
		   h2.setCellValue("Website : ");
		   h2.setCellStyle(headerCellStyle);
				   
		   HSSFCell h21 = row2.createCell(startColIndex + 4);
		   h21.setCellValue(this.getCompany(id).getWebsite());
		   h21.setCellStyle(bodyCellStyle);
		   
		   HSSFCell h3 = row3.createCell(startColIndex + 3);
		   h3.setCellValue("Notes : ");
		   h3.setCellStyle(headerCellStyle);
		   
		   HSSFCell h31 = row3.createCell(startColIndex + 4);
		   h31.setCellValue(this.getCompany(id).getNotes());
		   h31.setCellStyle(bodyCellStyle);
		   
		   HSSFCell h4 = row4.createCell(startColIndex + 3);
		   h4.setCellValue("Type : ");
		   h4.setCellStyle(headerCellStyle);
		   
		   HSSFCell h41 = row4.createCell(startColIndex + 4);
		   h41.setCellValue(this.getType(getCompany(id).getType()));
		   h41.setCellStyle(bodyCellStyle);
		   
		   HSSFCell h5 = row5.createCell(startColIndex + 3);
		   h5.setCellValue("Holding Company : ");
		   h5.setCellStyle(headerCellStyle);
		   
		   HSSFCell h51 = row5.createCell(startColIndex + 4);
		   h51.setCellValue(this.isActive(getCompany(id).getIsHolding()));
		   h51.setCellStyle(bodyCellStyle);
		   
		   HSSFCell h6 = row6.createCell(startColIndex + 3);
		   h6.setCellValue("Country : ");
		   h6.setCellStyle(headerCellStyle);
		   
		   HSSFCell h61 = row6.createCell(startColIndex + 4);
		   h61.setCellValue(this.getCompany(id).getCountry().getName());
		   h61.setCellStyle(bodyCellStyle);
		   
		   HSSFCell h7 = row7.createCell(startColIndex + 3);
		   h7.setCellValue("Active : ");
		   h7.setCellStyle(headerCellStyle);
		   
		   HSSFCell h71 = row7.createCell(startColIndex + 4);
		   h71.setCellValue(this.isActive(getCompany(id).getIsActive()));
		   h71.setCellStyle(bodyCellStyle);
	   }

	   
	  if(cd != null){
		   HSSFCell cell1 = row1.createCell(startColIndex+1);
		   cell1.setCellValue(cd.getAddress1());
		   cell1.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell2 = row2.createCell(startColIndex+1);
		   cell2.setCellValue(cd.getAddress2());
		   cell2.setCellStyle(bodyCellStyle);
		 
		   HSSFCell cell3 = row3.createCell(startColIndex+1);
		   cell3.setCellValue(cd.getAddress3());
		   cell3.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell4 = row4.createCell(startColIndex+1);
		   cell4.setCellValue(cd.getEmail());
		   cell4.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell5 = row5.createCell(startColIndex+1);
		   cell5.setCellValue(cd.getLinkedIn());
		   cell5.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell6 = row6.createCell(startColIndex+1);
		   cell6.setCellValue(cd.getSkype());
		   cell6.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell7 = row7.createCell(startColIndex+1);
		   cell7.setCellValue(cd.getYoutube());
		   cell7.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell8 = row8.createCell(startColIndex+1);
		   cell8.setCellValue(cd.getTwitter());
		   cell8.setCellStyle(bodyCellStyle);
		   		   
		   List<IndividualPhones> dataIndSource = this.getIndPhoneList(id);
		   int countIndPhones = dataIndSource.size();
		   startRowIndex = startRowIndex+10;
		   
		   if(countIndPhones > 0){
			   for(int z=0; z < countIndPhones; z++){
				  
				   HSSFRow row9 = worksheet.createRow((short) startRowIndex + z);
				   row9.setHeight((short) 500);
				   
				   HSSFCell cell0 = row9.createCell(startColIndex + 0);
				   cell0.setCellValue((z + 1));
				   cell0.setCellStyle(bodyCellStyle);
				   
				   HSSFCell cell9 = row9.createCell(startColIndex + 1);
				   cell9.setCellValue(dataIndSource.get(z).getPhone().getNumber());
				   cell9.setCellStyle(bodyCellStyle);
				   
				   HSSFCell cell10 = row9.createCell(startColIndex + 2);
				   cell10.setCellValue(dataIndSource.get(z).getPhone().getPhoneType());
				   cell10.setCellStyle(bodyCellStyle);
				   
				   HSSFCell cell11 = row9.createCell(startColIndex + 3);
				   cell11.setCellValue(dataIndSource.get(z).getPhone().getAreaCode());
				   cell11.setCellStyle(bodyCellStyle);
				   
				   HSSFCell cell12 = row9.createCell(startColIndex + 4);
				   cell12.setCellValue(dataIndSource.get(z).getPhone().getCountry().getCode());
				   cell12.setCellStyle(bodyCellStyle);
			   }
		   }
		 }
	 }

 public  void CompInvolvementReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<CompanyInvolved> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   cell1.setCellValue(datasource.get(i-2).getCompany().getName());
	   cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell11 = row.createCell(startColIndex+1);
	   cell11.setCellValue(datasource.get(i-2).getInd().getLastName());
	   cell11.setCellStyle(bodyCellStyle);
	   	   
	   HSSFCell cell12 = row.createCell(startColIndex+2);
	   cell12.setCellValue(datasource.get(i-2).getInd().getMiddleInitial());
	   cell12.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell13 = row.createCell(startColIndex+3);
	   cell13.setCellValue(datasource.get(i-2).getInd().getFirstName());
	   cell13.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell14 = row.createCell(startColIndex+4);
	   cell14.setCellValue(datasource.get(i-2).getInd().getCountry().getName());
	   cell14.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell2 = row.createCell(startColIndex+5);
	   if(datasource.get(i-2).getGrpCompany() != null){
		   cell2.setCellValue(datasource.get(i-2).getGrpCompany().getName());
	   }
	   cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+6);
	   cell3.setCellValue(datasource.get(i-2).getTitle());
	   cell3.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell4 = row.createCell(startColIndex+7);
	   cell4.setCellValue(datasource.get(i-2).getBase().getName());
	   cell4.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell5 = row.createCell(startColIndex+8);
	   cell5.setCellValue(datasource.get(i-2).getCompanyCity());
	   cell5.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell6 = row.createCell(startColIndex+9);
	   if(datasource.get(i-2).getHeadquarter() != null){
		   cell6.setCellValue(datasource.get(i-2).getHeadquarter().getName());
	   }
	   cell6.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell7 = row.createCell(startColIndex+10);
	   cell7.setCellValue(this.isActive(datasource.get(i-2).isActive()));
	   cell7.setCellStyle(bodyCellStyle);
	   
	   }
}
 
 public  void IndCompInvolvedReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<CompanyInvolved> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   cell1.setCellValue(datasource.get(i-2).getCompany().getName());
	   cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   if(datasource.get(i-2).getGrpCompany() != null){
		   cell2.setCellValue(datasource.get(i-2).getGrpCompany().getName());
	   }
	   cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   cell3.setCellValue(datasource.get(i-2).getTitle());
	   cell3.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   cell4.setCellValue(datasource.get(i-2).getBase().getName());
	   cell4.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell5 = row.createCell(startColIndex+4);
	   cell5.setCellValue(datasource.get(i-2).getCompanyCity());
	   cell5.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell6 = row.createCell(startColIndex+5);
	   if(datasource.get(i-2).getHeadquarter() != null){
		   cell6.setCellValue(datasource.get(i-2).getHeadquarter().getName());
	   }
	   cell6.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell7 = row.createCell(startColIndex+6);
	   cell7.setCellValue(this.isActive(datasource.get(i-2).isActive()));
	   cell7.setCellStyle(bodyCellStyle);
	   
	   }
}
 
 
 public  void PositionReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<CompanyInvolved> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   	cell1.setCellValue(datasource.get(i-2).getInd().getLastName());
	   	cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   	cell2.setCellValue(datasource.get(i-2).getInd().getMiddleInitial());
	   	cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   	cell3.setCellValue(datasource.get(i-2).getInd().getFirstName());
	   	cell3.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   	cell4.setCellValue(datasource.get(i-2).getInd().getCountry().getName());
	   	cell4.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell5 = row.createCell(startColIndex+4);
	   	cell5.setCellValue(datasource.get(i-2).getCompany().getName());
	   	cell5.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell6 = row.createCell(startColIndex+5);
	   if(datasource.get(i-2).getGrpCompany() != null){
		   cell6.setCellValue(datasource.get(i-2).getGrpCompany().getName());
		   cell6.setCellStyle(bodyCellStyle);
	   }
	  
	   HSSFCell cell7 = row.createCell(startColIndex+6);
		   cell7.setCellValue(datasource.get(i-2).getTitle());
		   cell7.setCellStyle(bodyCellStyle);
		   
	   HSSFCell cell8 = row.createCell(startColIndex+7);
	   if(datasource.get(i-2).getHeadquarter() != null){
		   cell8.setCellValue(datasource.get(i-2).getHeadquarter().getName());
		   cell8.setCellStyle(bodyCellStyle);
	   }
		   
	   HSSFCell cell9 = row.createCell(startColIndex+8);
	   	cell9.setCellValue(datasource.get(i-2).getCompanyCity());
	   	cell9.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell10 = row.createCell(startColIndex+9);
		   if(datasource.get(i-2).isActive() == true){
			 	cell10.setCellValue("yes");
		   }else{
			   cell10.setCellValue("no");
		   }
		   cell10.setCellStyle(bodyCellStyle);
	 
	  }
 }
 
 public void EducationalReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<Education> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   	cell1.setCellValue(datasource.get(i-2).getInd().getLastName());
	   	cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   	cell2.setCellValue(datasource.get(i-2).getInd().getMiddleInitial());
	   	cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   	cell3.setCellValue(datasource.get(i-2).getInd().getFirstName());
	   	cell3.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   	cell4.setCellValue(datasource.get(i-2).getInd().getCountry().getName());
	   	cell4.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell5 = row.createCell(startColIndex+4);
	   	cell5.setCellValue(datasource.get(i-2).getDegree());
	   	cell5.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell6 = row.createCell(startColIndex+5);
		   cell6.setCellValue(datasource.get(i-2).getDiscipline());
		   cell6.setCellStyle(bodyCellStyle);
	  
	   HSSFCell cell7 = row.createCell(startColIndex+6);
		   cell7.setCellValue(datasource.get(i-2).getUniversity());
		   cell7.setCellStyle(bodyCellStyle);
		 
	 
	  }
	 }
 
 @SuppressWarnings("deprecation")
 public void ALMSellerHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex, int id,List<CompanyALMSeller> datasource1, CompanyALMProvider datasource) {
   // Create font style for the headers
   Font font = worksheet.getWorkbook().createFont();
         font.setBoldweight(Font.BOLDWEIGHT_BOLD);
  
         // Create cell style for the headers
   HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
   headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
   headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
   headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
   headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
   headerCellStyle.setWrapText(true);
   headerCellStyle.setFont(font);
   headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
    
   // Create the column headers
   HSSFRow rowHeader1 = worksheet.createRow((short) startRowIndex +1);
   rowHeader1.setHeight((short) 500);
   
   HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
   bodyCellStyle.setWrapText(true);
    
   HSSFRow row2 = worksheet.createRow((short) startRowIndex +2);
   row2.setHeight((short) 500);
   
   HSSFCell c1 = rowHeader1.createCell(startColIndex+0);
   c1.setCellValue("Company Name");
   c1.setCellStyle(headerCellStyle);
   
   HSSFCell c2 = rowHeader1.createCell(startColIndex+1);
   c2.setCellValue("Application Name");
   c2.setCellStyle(headerCellStyle);
  
   HSSFCell c3 = rowHeader1.createCell(startColIndex+2);
   c3.setCellValue("Provider");
   c3.setCellStyle(headerCellStyle);
   
   if(datasource !=null){
	   
	   HSSFCell c11 = row2.createCell(startColIndex+0);
	   c11.setCellValue(datasource.getCompany().getName());
	   c11.setCellStyle(bodyCellStyle);
	   
	   HSSFCell c22 = row2.createCell(startColIndex+1);
	   c22.setCellValue(datasource.getAppName());
	   c22.setCellStyle(bodyCellStyle);
	   
	   HSSFCell c33 = row2.createCell(startColIndex+2);
	   c33.setCellValue(datasource.getAlmProvider().getName());
	   c33.setCellStyle(bodyCellStyle);
   }
   
   
   //--------------------//
   
   HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +5);
   rowHeader.setHeight((short) 500);
   
      
   HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
   cell1.setCellValue("Company Name");
   cell1.setCellStyle(headerCellStyle);
      
   HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
   cell2.setCellValue("Company Type");
   cell2.setCellStyle(headerCellStyle);
      
   HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
   cell3.setCellValue("Location");
   cell3.setCellStyle(headerCellStyle);
      
   HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
   cell4.setCellValue("Active");
   cell4.setCellStyle(headerCellStyle);
     
   HSSFCell cellii = rowHeader.createCell(startColIndex+4);
   cellii.setCellValue("Application Name");
   cellii.setCellStyle(headerCellStyle);
   
   HSSFCell celliii = rowHeader.createCell(startColIndex+5);
   celliii.setCellValue("Description");
   celliii.setCellStyle(headerCellStyle);
   
   
   
   for (int i=0; i < datasource1.size(); i++) {
	   
	   if(datasource1.size() > 0){
		   HSSFRow row = worksheet.createRow((short) i +6);
		   rowHeader.setHeight((short) 500);
		   
		   HSSFCell cell11 = row.createCell(startColIndex+0);
		   cell11.setCellValue(datasource1.get(i).getCompany().getName());
		   cell11.setCellStyle(bodyCellStyle);

		   HSSFCell cell21 = row.createCell(startColIndex+1);
		   cell21.setCellValue(this.getType(datasource1.get(i).getCompany().getType()));
		   cell21.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell31 = row.createCell(startColIndex+2);
		   cell31.setCellValue(datasource1.get(i).getCompany().getCountry().getName());
		   cell31.setCellStyle(bodyCellStyle);

		   HSSFCell cell41 = row.createCell(startColIndex+3);
		   cell41.setCellValue(this.isActive(datasource1.get(i).getCompany().getIsActive()));
		   cell41.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell51 = row.createCell(startColIndex+4);
		   cell51.setCellValue(datasource1.get(i).getAppname());
		   cell51.setCellStyle(bodyCellStyle);
		   
		   HSSFCell cell61 = row.createCell(startColIndex+5);
		   cell61.setCellValue(datasource1.get(i).getAlmDesc());
		   cell61.setCellStyle(bodyCellStyle);
	   }
	}
  }
 
 
 public void NationalityReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<Nationalities> datasource) {
	  // Row offset
	  startRowIndex += 2;
	   
	  // Create cell style for the body
	  HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
	  bodyCellStyle.setWrapText(true);
	   
	  // Create body
	  for (int i=startRowIndex; i+startRowIndex-2< datasource.size()+2; i++) {
	   // Create a new row
	   HSSFRow row = worksheet.createRow((short) i+1);
	 
	   HSSFCell cell1 = row.createCell(startColIndex+0);
	   	cell1.setCellValue(datasource.get(i-2).getInd().getLastName());
	   	cell1.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell2 = row.createCell(startColIndex+1);
	   	cell2.setCellValue(datasource.get(i-2).getInd().getMiddleInitial());
	   	cell2.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell3 = row.createCell(startColIndex+2);
	   	cell3.setCellValue(datasource.get(i-2).getInd().getFirstName());
	   	cell3.setCellStyle(bodyCellStyle);
	 
	   HSSFCell cell4 = row.createCell(startColIndex+3);
	   	cell4.setCellValue(datasource.get(i-2).getInd().getCountry().getName());
	   	cell4.setCellStyle(bodyCellStyle);
	   
	   HSSFCell cell5 = row.createCell(startColIndex+4);
	   	cell5.setCellValue(datasource.get(i-2).getCountry().getName());
	   	cell5.setCellStyle(bodyCellStyle);
	 
	  	 
	  }
}
 
 private String getIndName(int id){
		
		return indService.getIndividualById(id).getFirstName() + " " +
		       indService.getIndividualById(id).getMiddleInitial() + ". " + 
			   indService.getIndividualById(id).getLastName();
		
	}


private  List<ConnectedCompany> getCompList(int id) {
	return connCompService.list(id);
}

private Company getCompany(int id) {
	return companyService.getCompanyId(id);
}

private  List<ConnectedIndividual> getIndList(int id) {
	return connIndService.list(id);
}

private  List<IndividualPhones> getIndPhoneList(int id) {
	return indPhoneService.list(id);
}


private  List<MeetingsIndividual> getAttendeeList(int id) {
	return meetIndService.list(id);
}

private  List<ConnectedCountries> getCountryList(int id) {
	return connCountService.list(id);
}

public int getMaxComp(){
	 return Integer.parseInt(this.connCompService.getMaxComp());
}

public int getMaxAttendees(){
	 return Integer.parseInt(this.meetIndService.getMaxAttendees());
}

public int getMaxSeller(){
	 return Integer.parseInt(this.compALMSellerService.getMaxSeller());
}

public int getMaxCompPhones(){
	 return Integer.parseInt(this.meetIndService.getMaxAttendees());
}

public int getMaxIndPhones(){
	 return Integer.parseInt(this.indPhoneService.getMaxInd());
}

public int getMaxCountries(){
	 return Integer.parseInt(this.connCountService.getMaxCountries());
}

public int getMaxIndividual(){
	 return Integer.parseInt(this.connIndService.getMaxInd());
}

public String getType(int type){
	 switch (type){
		case 1:
			return "Bank";
		case 2:
			return "Central Bank";
		case 3:
			return "Insurance";
		case 4:
			return "Industry Association";
		case 5:
			return "Media";
		default:
			return "Not Specified";
	 }
}

public String isActive(boolean type){
	if(type){
		return "yes";
	}else{
		return "no";
	}
}

public String shareType(int type){
	if(type == 1){
		return "company";
	}else{
		return "individual";
	}
}

}