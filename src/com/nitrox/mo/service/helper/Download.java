package com.nitrox.mo.service.helper;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.hibernate.SessionFactory;

import com.nitrox.mo.model.Company;
import com.nitrox.mo.model.CompanyALMProvider;
import com.nitrox.mo.model.CompanyALMSeller;
import com.nitrox.mo.model.CompanyFiles;
import com.nitrox.mo.model.CompanyInvolved;
import com.nitrox.mo.model.CompanyShareholders;
import com.nitrox.mo.model.ConnectedIndividual;
import com.nitrox.mo.model.ContactDetails;
import com.nitrox.mo.model.Education;
import com.nitrox.mo.model.Individual;
import com.nitrox.mo.model.IndividualPhones;
import com.nitrox.mo.model.MarketSignal;
import com.nitrox.mo.model.Meetings;
import com.nitrox.mo.model.MeetingsIndividual;
import com.nitrox.mo.model.Nationalities;
import com.nitrox.mo.service.CompanyALMProviderService;
import com.nitrox.mo.service.CompanyALMSellerService;
import com.nitrox.mo.service.CompanyFilesService;
import com.nitrox.mo.service.CompanyInvolvedService;
import com.nitrox.mo.service.CompanyService;
import com.nitrox.mo.service.CompanyShareholdersService;
import com.nitrox.mo.service.ConnectedIndividualService;
import com.nitrox.mo.service.ContactDetailsService;
import com.nitrox.mo.service.EducationService;
import com.nitrox.mo.service.IndividualPhoneService;
import com.nitrox.mo.service.IndividualService;
import com.nitrox.mo.service.MarketSignalService;
import com.nitrox.mo.service.MeetingsIndividualService;
import com.nitrox.mo.service.MeetingsService;
import com.nitrox.mo.service.NationalitiesService;
import com.nitrox.mo.service.helper.FillManager;
import com.nitrox.mo.service.helper.Layouter;
import com.nitrox.mo.service.helper.Writer;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Service for processing Apache POI-based reports
 * 
 * @author Krams at {@link http://krams915@blogspot.com}
 */
@Service("downloadService")
@Transactional
public class Download {

private Logger logger = Logger.getLogger("service");

@Autowired
private IndividualService indService;

@Autowired
private MeetingsIndividualService indMeetService;

@Autowired
private MarketSignalService mSignalService;

@Autowired
private EducationService educService;

@Autowired
private NationalitiesService natService;

@Autowired
private MeetingsService meetingService;

@Autowired
private CompanyInvolvedService involvedService;

@Autowired
private ConnectedIndividualService connIndService;

@Autowired
private CompanyShareholdersService compIndSharesService;

@Autowired
private CompanyInvolvedService compInvolvedService;

@Autowired
private ContactDetailsService contactService;

@Autowired
private CompanyService companyService;

@Autowired
private CompanyALMSellerService companyALMSellerService;

@Autowired
private CompanyALMProviderService companyALMProviderService;

@Autowired
private CompanyShareholdersService companyShareHoldersService;

@Autowired
private CompanyFilesService companyFilesService;

@Autowired
private FillManager fllManage;

@Resource(name="sessionFactory")
private SessionFactory sessionFactory;

/**
* Processes the download for Excel format.
* It does the following steps:
* <pre>1. Create new workbook
* 2. Create new worksheet
* 3. Define starting indices for rows and columns
* 4. Build layout 
* 5. Fill report
* 6. Set the HttpServletResponse properties
* 7. Write to the output stream
* </pre>
*/

@SuppressWarnings("unchecked")
public void downloadIndividualXLS(HttpServletResponse response) throws ClassNotFoundException {
	logger.debug("Downloading Excel report");

// 1. Create new workbook
HSSFWorkbook workbook = new HSSFWorkbook();

// 2. Create new worksheet
HSSFSheet worksheet1 = workbook.createSheet("Persons");
HSSFSheet worksheet2 = workbook.createSheet("Positions");
HSSFSheet worksheet3 = workbook.createSheet("Study");
HSSFSheet worksheet4 = workbook.createSheet("Nationality");
HSSFSheet worksheet5 = workbook.createSheet("amalgamated data");
HSSFSheet worksheet6 = workbook.createSheet("amala data");

// 3. Define starting indices for rows and columns
int startRowIndex = 0;
int startColIndex = 0;

// 4. Build layout 
// Build title, date, and column headers
Layouter lyter = new Layouter();

lyter.buildReport1(worksheet1, startRowIndex, startColIndex);
lyter.buildReport2(worksheet2, startRowIndex, startColIndex);
lyter.buildReport3(worksheet3, startRowIndex, startColIndex);
lyter.buildReport4(worksheet4, startRowIndex, startColIndex);
lyter.buildReport1(worksheet5, startRowIndex, startColIndex);
lyter.buildReport1(worksheet6, startRowIndex, startColIndex);

// 5. Fill report
fllManage.IndividualReport(worksheet1, startRowIndex, startColIndex, getIndividuals());
fllManage.PositionReport(worksheet2, startRowIndex, startColIndex, getPositions());
fllManage.EducationalReport(worksheet3, startRowIndex, startColIndex, getEducation());
fllManage.NationalityReport(worksheet4, startRowIndex, startColIndex, getNationalities());
fllManage.IndividualReport(worksheet5, startRowIndex, startColIndex, getIndividuals());
fllManage.IndividualReport(worksheet6, startRowIndex, startColIndex, getIndividuals());

// 6. Set the response properties
String fileName = "SocialCapial.xls";
response.setHeader("Content-Disposition", "inline; filename=" + fileName);
// Make sure to set the correct content type
response.setContentType("application/vnd.ms-excel");

//7. Write to the output stream
Writer wrter = new Writer();
wrter.write(response, worksheet1);
wrter.write(response, worksheet2);
wrter.write(response, worksheet3);
wrter.write(response, worksheet4);
wrter.write(response, worksheet5);
wrter.write(response, worksheet6);

}



@SuppressWarnings("unchecked")
public void downloadMarketSignalXLS(HttpServletResponse response) throws ClassNotFoundException {
	logger.debug("Downloading Excel report");

	// 1. Create new workbook
	HSSFWorkbook workbook = new HSSFWorkbook();
	
	// 2. Create new worksheet
	HSSFSheet worksheet1 = workbook.createSheet("Sample Market Data");
	
	// 3. Define starting indices for rows and columns
	int startRowIndex = 0;
	int startColIndex = 0;
	
	// 4. Build layout 
	// Build title, date, and column headers
	Layouter layOuter = new Layouter();
	layOuter.buildSignalReport(worksheet1, startRowIndex, startColIndex,fllManage.getMaxComp(),fllManage.getMaxCountries(), fllManage.getMaxIndividual());
	
	
	// 5. Fill report
	fllManage.MarketSignalReport(worksheet1, startRowIndex, startColIndex, getMarketSignals());
	System.out.println(" =--- " + fllManage.getMaxComp());
	// 6. Set the response properties
	String fileName = "MarketSignal.xls";
	response.setHeader("Content-Disposition", "inline; filename=" + fileName);
	// Make sure to set the correct content type
	response.setContentType("application/vnd.ms-excel");
	
	//7. Write to the output stream
	Writer writer = new Writer();
	writer.write(response, worksheet1);

}

@SuppressWarnings("unchecked")
public void downloadMeetingXLS(HttpServletResponse response) throws ClassNotFoundException {
	logger.debug("Downloading Excel report");

	// 1. Create new workbook
	HSSFWorkbook workbook = new HSSFWorkbook();
	
	// 2. Create new worksheet
	HSSFSheet worksheet1 = workbook.createSheet("Meetings List");
	
	// 3. Define starting indices for rows and columns
	int startRowIndex = 0;
	int startColIndex = 0;
	
	// 4. Build layout 
	// Build title, date, and column headers
	Layouter layOuter = new Layouter();
	layOuter.buildMeetingReport(worksheet1, startRowIndex, startColIndex, fllManage.getMaxAttendees());
	
	
	// 5. Fill report
	fllManage.MeetingReport(worksheet1, startRowIndex, startColIndex, getMeetingList());
	
	// 6. Set the response properties
	String fileName = "Meetings.xls";
	response.setHeader("Content-Disposition", "inline; filename=" + fileName);
	// Make sure to set the correct content type
	response.setContentType("application/vnd.ms-excel");
	
	//7. Write to the output stream
	Writer writer = new Writer();
	writer.write(response, worksheet1);

}

@SuppressWarnings("unchecked")
public void downloadCompanyXLS(HttpServletResponse response) throws ClassNotFoundException {
	logger.debug("Downloading Excel report");

	// 1. Create new workbook
	HSSFWorkbook workbook = new HSSFWorkbook();
	
	// 2. Create new worksheet
	HSSFSheet worksheet1 = workbook.createSheet("Company List");
	HSSFSheet worksheet2 = workbook.createSheet("ALM Company Seller");
	HSSFSheet worksheet3 = workbook.createSheet("ALM Company Provider");
	HSSFSheet worksheet4 = workbook.createSheet("Company ShareHolders");
	HSSFSheet worksheet5 = workbook.createSheet("Company With Files");
	
	
	// 3. Define starting indices for rows and columns
	int startRowIndex = 0;
	int startColIndex = 0;
	
	// 4. Build layout 
	// Build title, date, and column headers
	Layouter layOuter = new Layouter();
	layOuter.buildCompanyReport1(worksheet1, startRowIndex, startColIndex);
	layOuter.buildCompanyReport2(worksheet2, startRowIndex, startColIndex);
	layOuter.buildCompanyReport3(worksheet3, startRowIndex, startColIndex);
	layOuter.buildCompanyReport4(worksheet4, startRowIndex, startColIndex);
	layOuter.buildCompanyReport5(worksheet5, startRowIndex, startColIndex);
	
	
	// 5. Fill report
	fllManage.CompanyReport1(worksheet1, startRowIndex, startColIndex, getCompanyList());
	fllManage.CompanyReport2(worksheet2, startRowIndex, startColIndex, getCompanyALMSellerList());
	fllManage.CompanyReport3(worksheet3, startRowIndex, startColIndex, getCompanyALMProviderList());
	fllManage.CompanyReport4(worksheet4, startRowIndex, startColIndex, getCompanyShareholdersList());
	fllManage.CompanyReport5(worksheet5, startRowIndex, startColIndex, getCompanyFileList());
	
	// 6. Set the response properties
	String fileName = "Company.xls";
	response.setHeader("Content-Disposition", "inline; filename=" + fileName);
	// Make sure to set the correct content type
	response.setContentType("application/vnd.ms-excel");
	
	//7. Write to the output stream
	Writer writer = new Writer();
	writer.write(response, worksheet1);
	writer.write(response, worksheet2);
	writer.write(response, worksheet3);
	writer.write(response, worksheet4);
	writer.write(response, worksheet5);

}


@SuppressWarnings("unchecked")
public void downloadCompanyConnectionsXLS(HttpServletResponse response, int id) throws ClassNotFoundException {
	logger.debug("Downloading Excel report");
	
	// 1. Create new workbook
	HSSFWorkbook workbook = new HSSFWorkbook();
	
	// 2. Create new worksheet
	HSSFSheet worksheet1 = workbook.createSheet("Market Signals");
	HSSFSheet worksheet2 = workbook.createSheet("Connected Individuals");
	HSSFSheet worksheet3 = workbook.createSheet("Company Shareholders");
	HSSFSheet worksheet4 = workbook.createSheet("Company Meeetings");
	HSSFSheet worksheet5 = workbook.createSheet("Company Files");
    HSSFSheet worksheet6 = workbook.createSheet("ALM Provider - Seller");
	HSSFSheet worksheet7 = workbook.createSheet("Company Details");
	
	// 3. Define starting indices for rows and columns
	int startRowIndex = 0;
	int startColIndex = 0;
	
	// 4. Build layout 
	// Build title, date, and column headers
	Layouter layOuter = new Layouter();
	layOuter.buildCompConnReport1(worksheet1, startRowIndex, startColIndex, fllManage.getMaxComp(),fllManage.getMaxCountries(), fllManage.getMaxIndividual());
	layOuter.buildCompConnReport2(worksheet2, startRowIndex, startColIndex);
	layOuter.buildCompConnReport3(worksheet3, startRowIndex, startColIndex);
	layOuter.buildCompConnReport4(worksheet4, startRowIndex, startColIndex, fllManage.getMaxAttendees());
	layOuter.buildCompConnReport5(worksheet5, startRowIndex, startColIndex);
	layOuter.buildCompConnReport7(worksheet6, startRowIndex, startColIndex);
	layOuter.buildCompConnReport6(worksheet7, startRowIndex, startColIndex);
	
	// 5. Fill report
	fllManage.MarketSignalReport(worksheet1, startRowIndex, startColIndex, getCompMarketSignals(id));
	fllManage.CompInvolvementReport(worksheet2, startRowIndex, startColIndex, getCompanyInvolvement(id));
	fllManage.CompanySharesReport(worksheet3, startRowIndex, startColIndex, getCompanyShares(id));
	fllManage.MeetingReport(worksheet4, startRowIndex, startColIndex, getCompanyMeetings(id));
	fllManage.CompanyReport5(worksheet5, startRowIndex, startColIndex, getCompanyFilesList(id));
	fllManage.ALMSellerHeaders(worksheet6, startRowIndex, startColIndex,id, getCompanyALMSellerList(id), getCompanyALMProviderList(id));
	fllManage.IndDetailsReport(worksheet7, startRowIndex, startColIndex,id, getCompanyDetails(id),1);
	
	// 6. Set the response properties
	String fileName =this.getCompanyName(id) + ".xls";
	response.setHeader("Content-Disposition", "inline; filename=\"" + fileName+"\"");
	// Make sure to set the correct content type
	response.setContentType("application/vnd.ms-excel");
	
	//7. Write to the output stream
	Writer writer = new Writer();
	writer.write(response, worksheet1);

}

@SuppressWarnings("unchecked")
public void downloadIndividualConnectionsXLS(HttpServletResponse response, int id) throws ClassNotFoundException {
	logger.debug("Downloading Excel report");
	
	// 1. Create new workbook
	HSSFWorkbook workbook = new HSSFWorkbook();
	
	// 2. Create new worksheet
	HSSFSheet worksheet1 = workbook.createSheet("Market Signals");
	HSSFSheet worksheet2 = workbook.createSheet("Meetings");
	HSSFSheet worksheet3 = workbook.createSheet("Company Shareholders");
	HSSFSheet worksheet4 = workbook.createSheet("Company Involvement");
	HSSFSheet worksheet5 = workbook.createSheet("Individual Details");
	
	// 3. Define starting indices for rows and columns
	int startRowIndex = 0;
	int startColIndex = 0;
	
	// 4. Build layout 
	// Build title, date, and column headers
	Layouter layOuter = new Layouter();
	layOuter.buildIndConnReport1(worksheet1, startRowIndex, startColIndex, fllManage.getMaxComp(),fllManage.getMaxCountries(), fllManage.getMaxIndividual());
	layOuter.buildIndConnReport2(worksheet2, startRowIndex, startColIndex, fllManage.getMaxAttendees());
    layOuter.buildIndConnReport3(worksheet3, startRowIndex, startColIndex);
	layOuter.buildIndConnReport4(worksheet4, startRowIndex, startColIndex);
	layOuter.buildIndConnReport5(worksheet5, startRowIndex, startColIndex, fllManage.getMaxIndPhones());

	
	// 5. Fill report
	fllManage.ConnectedIndividualReport(worksheet1, startRowIndex, startColIndex, getConnectedIndividuals(id));
	fllManage.IndMeetingReport(worksheet2, startRowIndex, startColIndex, getMeetingsIndividuals(id));
	fllManage.IndCompSharesReport(worksheet3, startRowIndex, startColIndex, getIndCompanyShares(id));
	fllManage.IndCompInvolvedReport(worksheet4, startRowIndex, startColIndex, getIndividualInvolvement(id));
	fllManage.IndDetailsReport(worksheet5, startRowIndex, startColIndex,id, getIndividualDetails(id), 0);

	// 6. Set the response properties
	String fileName = this.getIndName(id) + ".xls";
	response.setHeader("Content-Disposition", "inline; filename=\"" + fileName+"\"");
	// Make sure to set the correct content type
	response.setContentType("application/vnd.ms-excel");
	
	//7. Write to the output stream
	Writer writer = new Writer();
	writer.write(response, worksheet1);

}

/**
  * Retrieves the datasource as as simple Java List.
  * The datasource is retrieved from a Hibernate HQL query.
  */

@SuppressWarnings("unchecked")
private List<Individual> getIndividuals() {
	return indService.list();
}

@SuppressWarnings("unchecked")
private List<ConnectedIndividual> getConnectedIndividuals(int id) {
	return connIndService.indList(id);
}

@SuppressWarnings("unchecked")
private List<MeetingsIndividual> getMeetingsIndividuals(int id) {
	return indMeetService.indList(id);
}

@SuppressWarnings("unchecked")
private List<Meetings> getCompanyMeetings(int id) {
	return meetingService.list(id);
}

@SuppressWarnings("unchecked")
private List<CompanyInvolved> getIndividualInvolvement(int id) {
	return compInvolvedService.listCompanies(id);
}

@SuppressWarnings("unchecked")
private List<CompanyInvolved> getCompanyInvolvement(int id) {
	return compInvolvedService.list(id);
}

@SuppressWarnings("unchecked")
private ContactDetails getIndividualDetails(int id) {
	int contactID = 0;
	if(indService.getIndividualById(id).getContact() != null){
		contactID = indService.getIndividualById(id).getContact().getId();
	}
	return contactService.getContactDetailsById(contactID);
}

@SuppressWarnings("unchecked")
private ContactDetails getCompanyDetails(int id) {
	int contactID = 0;
	if(companyService.getCompanyId(id).getContact() != null){
		contactID = companyService.getCompanyId(id).getContact().getId();
	}
	return contactService.getContactDetailsById(contactID);
}

@SuppressWarnings("unchecked")
private List<MarketSignal> getMarketSignals() {
	return mSignalService.list();
}

@SuppressWarnings("unchecked")
private List<MarketSignal> getCompMarketSignals(int id) {
	return mSignalService.list(id);
}

@SuppressWarnings("unchecked")
private List<CompanyALMSeller> getCompanyALMSellerList() {
	return companyALMSellerService.list();
}

@SuppressWarnings("unchecked")
private List<CompanyALMProvider> getCompanyALMProviderList() {
	return companyALMProviderService.list();
}

@SuppressWarnings("unchecked")
private List<CompanyALMSeller> getCompanyALMSellerList(int id) {
	return companyALMSellerService.list(id);
}

@SuppressWarnings("unchecked")
private CompanyALMProvider getCompanyALMProviderList(int id) {
	return companyALMProviderService.getCompanyId(id);
}

@SuppressWarnings("unchecked")
private List<CompanyFiles> getCompanyFileList() {
	return companyFilesService.list();
}

@SuppressWarnings("unchecked")
private List<CompanyFiles> getCompanyFilesList(int id) {
	return companyFilesService.list(id);
}

@SuppressWarnings("unchecked")
private List<CompanyShareholders> getCompanyShareholdersList() {
	return companyShareHoldersService.list();
}

@SuppressWarnings("unchecked")
private List<Meetings> getMeetingList() {
	return meetingService.list();
}

@SuppressWarnings("unchecked")
private List<Company> getCompanyList() {
	return companyService.list();
}

@SuppressWarnings("unchecked")
private List<CompanyShareholders> getIndCompanyShares(int id) {
	return compIndSharesService.indLst(id);
}

@SuppressWarnings("unchecked")
private List<CompanyShareholders> getCompanyShares(int id) {
	System.out.println("id " + id);
	System.out.println("size " + compIndSharesService.compList(id).size());
	return compIndSharesService.compList(id);
}

private String getIndName(int id){
	
	return indService.getIndividualById(id).getFirstName() + " " +
	       indService.getIndividualById(id).getMiddleInitial() + ". " + 
		   indService.getIndividualById(id).getLastName();
	
}

private String getCompanyName(int id){
	
	return companyService.getCompanyId(id).getName();
}


@SuppressWarnings("unchecked")
private List<CompanyInvolved> getPositions() {
	return involvedService.list();
}

@SuppressWarnings("unchecked")
private List<Education> getEducation() {
	return educService.list();
}

@SuppressWarnings("unchecked")
private List<Nationalities> getNationalities() {
	return natService.list();
}

}
