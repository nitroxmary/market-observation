package com.nitrox.mo.service.helper;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.nitrox.mo.service.UserService;

@Service  
@Transactional
public class Validation {

	@Autowired
	private UserService userService;
	
	public String validateUser(String username, String password){
		String message= "admin";
		
		try{
			if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
				message= "login.empty";
			}
			else if(StringUtils.isEmpty(userService.getUser(username)) || !userService.authenticateUser(username, password)){
				message= "login.invalid";
			}
			else if(!userService.getUser(username).getIsAdmin()){
				message= "user";
			}
		}catch(Exception e){
			e.printStackTrace();
			message= "error.unknown";
		}
		System.out.println(" - " + message);
		return message;
	}
}
