package com.nitrox.mo.service.helper;

import java.util.Date;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nitrox.mo.service.helper.FillManager;
 
/**
 * Builds the report layout, the template, the design, the pattern or whatever synonym you may want to call it.
 * 
 * @author Krams at {@link http://krams915@blogspot.com}
 */
@Service  
@Transactional
public class Layouter {
 
 private Logger logger = Logger.getLogger("service");

 @Autowired
 private FillManager fm;
 /**
  * Builds the report layout. 
  * <p>
  * This doesn't have any data yet. This is your template.
  */
 public void buildReport1(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
  // Set column widths
  worksheet.setColumnWidth(0, 5000);
  worksheet.setColumnWidth(1, 5000);
  worksheet.setColumnWidth(2, 5000);
  worksheet.setColumnWidth(3, 5000);
  worksheet.setColumnWidth(4, 5000);
  worksheet.setColumnWidth(5, 5000);
   
  // Build the title and date headers
  buildTitle(worksheet, startRowIndex, startColIndex);
  // Build the column headers
  IndividualHeaders(worksheet, startRowIndex, startColIndex);
 }
  
 public void buildSignalReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, int comp, int country, int ind) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  MarketSignalHeaders(worksheet, startRowIndex, startColIndex,comp, country, ind);
} 
 
 public void buildMeetingReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, int ind) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  MeetingHeaders(worksheet, startRowIndex, startColIndex,ind);
} 
 
 public void buildCompanyReport1(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  CompanyHeaders(worksheet, startRowIndex, startColIndex);
} 
 
 public void buildReport2(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  PositionHeaders(worksheet, startRowIndex, startColIndex);
 }
 
 public void buildIndConnReport1(HSSFSheet worksheet, int startRowIndex, int startColIndex,int comp, int country, int ind) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  IndMarketSignalHeaders(worksheet, startRowIndex, startColIndex,comp, country, ind);
 }
 
 public void buildCompConnReport1(HSSFSheet worksheet, int startRowIndex, int startColIndex,int comp, int country, int ind) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  CompMarketSignalHeaders(worksheet, startRowIndex, startColIndex,comp, country, ind);
}
 
 @SuppressWarnings("deprecation")
 public void CompMarketSignalHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex, int comp, int countries, int individual) {
  
 	
  // Create font style for the headers
   Font font = worksheet.getWorkbook().createFont();
         font.setBoldweight(Font.BOLDWEIGHT_BOLD);
  
         // Create cell style for the headers
   HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
   headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
   headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
   headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
   headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
   headerCellStyle.setWrapText(true);
   headerCellStyle.setFont(font);
   headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
    
   // Create the column headers
   HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
   rowHeader.setHeight((short) 500);
    
   HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
   cell1.setCellValue("Input Date");
   cell1.setCellStyle(headerCellStyle);
  
   HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
   cell2.setCellValue("Market Signal Date");
   cell2.setCellStyle(headerCellStyle);
  
   HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
   cell3.setCellValue("Factor");
   cell3.setCellStyle(headerCellStyle);
  
   HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
   cell4.setCellValue("Company/Institution");
   cell4.setCellStyle(headerCellStyle);
   
   HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
   cell5.setCellValue("Heading (Original)");
   cell5.setCellStyle(headerCellStyle);
  
   HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
   cell6.setCellValue("Heading (Translation)");
   cell6.setCellStyle(headerCellStyle);
  
   HSSFCell cell7 = rowHeader.createCell(startColIndex+6);
   cell7.setCellValue("Link");
   cell7.setCellStyle(headerCellStyle);
  
   HSSFCell cell8 = rowHeader.createCell(startColIndex+7);
   cell8.setCellValue("Summary");
   cell8.setCellStyle(headerCellStyle);
  
   HSSFCell cell9 = rowHeader.createCell(startColIndex+8);
   cell9.setCellValue("Background");
   cell9.setCellStyle(headerCellStyle);
  
   HSSFCell cell10 = rowHeader.createCell(startColIndex+9);
   cell10.setCellValue("Implication");
   cell10.setCellStyle(headerCellStyle);
   
   int lastColCompCount = startColIndex+9; 
   int lastColCountryCount = lastColCompCount+comp; 
   int lastColIndCount = lastColCountryCount+countries; 
   
  for(int i = 0; i < comp; i++){
 	  HSSFCell ii = rowHeader.createCell(lastColCompCount+(i + 1));
 	  ii.setCellValue("Company/Institutions");
 	  ii.setCellStyle(headerCellStyle);
  }
  
  for(int j = 0; j < countries; j++){
 	  HSSFCell jj = rowHeader.createCell(lastColCountryCount+(j+ 1));
 	  jj.setCellValue("Countries");
 	  jj.setCellStyle(headerCellStyle);
 }
  for(int x = 0; x < individual; x++){
 	  HSSFCell xx = rowHeader.createCell(lastColIndCount+(x + 1));
 	  xx.setCellValue("Individuals");
 	  xx.setCellStyle(headerCellStyle);
 }
  
 }
 public void buildCompConnReport2(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  ConnectedIndividualHeaders(worksheet, startRowIndex, startColIndex);
}
 
 @SuppressWarnings("deprecation")
 public void ConnectedIndividualHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
   // Create font style for the headers
   Font font = worksheet.getWorkbook().createFont();
         font.setBoldweight(Font.BOLDWEIGHT_BOLD);
  
         // Create cell style for the headers
   HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
   headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
   headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
   headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
   headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
   headerCellStyle.setWrapText(true);
   headerCellStyle.setFont(font);
   headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
    
   // Create the column headers
   HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
   rowHeader.setHeight((short) 500);
   
   HSSFCell cell5 = rowHeader.createCell(startColIndex+0);
   cell5.setCellValue("Company");
   cell5.setCellStyle(headerCellStyle);
   
   HSSFCell cell1 = rowHeader.createCell(startColIndex+1);
   cell1.setCellValue("Last Name");
   cell1.setCellStyle(headerCellStyle);
  
   HSSFCell cell2 = rowHeader.createCell(startColIndex+2);
   cell2.setCellValue("MI");
   cell2.setCellStyle(headerCellStyle);
  
   HSSFCell cell3 = rowHeader.createCell(startColIndex+3);
   cell3.setCellValue("First Name");
   cell3.setCellStyle(headerCellStyle);
  
   HSSFCell cell4 = rowHeader.createCell(startColIndex+4);
   cell4.setCellValue("Country/Nationality");
   cell4.setCellStyle(headerCellStyle);
   
   HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
   cell6.setCellValue("Group Company");
   cell6.setCellStyle(headerCellStyle);
  
   HSSFCell cell7 = rowHeader.createCell(startColIndex+6);
   cell7.setCellValue("Title");
   cell7.setCellStyle(headerCellStyle);
  
   HSSFCell cell8 = rowHeader.createCell(startColIndex+7);
   cell8.setCellValue("Location - Country");
   cell8.setCellStyle(headerCellStyle);
 
   HSSFCell cell9 = rowHeader.createCell(startColIndex+8);
   cell9.setCellValue("Location - City");
   cell9.setCellStyle(headerCellStyle);
   
   HSSFCell cell91 = rowHeader.createCell(startColIndex+9);
   cell91.setCellValue("Location - Headquarter");
   cell91.setCellStyle(headerCellStyle);
  
   HSSFCell cell10 = rowHeader.createCell(startColIndex+10);
   cell10.setCellValue("Active");
   cell10.setCellStyle(headerCellStyle);
  
  }

 
 public void buildCompConnReport3(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  buildCompanyReport4(worksheet, startRowIndex, startColIndex);
}
 
 public void buildCompConnReport4(HSSFSheet worksheet, int startRowIndex, int startColIndex, int ind) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  MeetingHeaders(worksheet, startRowIndex, startColIndex,ind);
}
 
 
 public void buildCompConnReport5(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  buildCompanyReport5(worksheet, startRowIndex, startColIndex);
}
 
 public void buildCompConnReport6(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	 
}
 public void buildCompConnReport7(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  //ALMSellerHeaders(worksheet, startRowIndex, startColIndex);
}
 
 public void buildIndConnReport2(HSSFSheet worksheet, int startRowIndex, int startColIndex,int ind) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  IndMeetingHeaders(worksheet, startRowIndex, startColIndex, ind);
 }
 
 public void buildIndConnReport3(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	 IndCompSharesHeaders(worksheet, startRowIndex, startColIndex);
 }
 
 public void buildIndConnReport4(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  IndCompInvHeaders(worksheet, startRowIndex, startColIndex);
 }
 
 public void buildIndConnReport5(HSSFSheet worksheet, int startRowIndex, int startColIndex,int indPhones) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
}
 
 public void buildReport3(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  EducationalHeaders(worksheet, startRowIndex, startColIndex);
}
 
 public void buildReport4(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Set column widths
	  worksheet.setColumnWidth(0, 5000);
	  worksheet.setColumnWidth(1, 5000);
	  worksheet.setColumnWidth(2, 5000);
	  worksheet.setColumnWidth(3, 5000);
	  worksheet.setColumnWidth(4, 5000);
	  worksheet.setColumnWidth(5, 5000);
	   
	  // Build the title and date headers
	  buildTitle(worksheet, startRowIndex, startColIndex);
	  // Build the column headers
	  NationalityHeaders(worksheet, startRowIndex, startColIndex);
	 }
 
 /**
  * Builds the report title and the date header
  * 
  * @param worksheet
  * @param startRowIndex starting row offset
  * @param startColIndex starting column offset
  */
 public void buildTitle(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
  // Create font style for the report title
  Font fontTitle = worksheet.getWorkbook().createFont();
  fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
  fontTitle.setFontHeight((short) 280);
   
        // Create cell style for the report title
        HSSFCellStyle cellStyleTitle = worksheet.getWorkbook().createCellStyle();
        cellStyleTitle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyleTitle.setWrapText(true);
        cellStyleTitle.setFont(fontTitle);
   
        // Create report title
  HSSFRow rowTitle = worksheet.createRow((short) startRowIndex);
  rowTitle.setHeight((short) 500);
//  HSSFCell cellTitle = rowTitle.createCell(startColIndex);
//  cellTitle.setCellValue("Social Capital");
//  cellTitle.setCellStyle(cellStyleTitle);
      
  // Create date header
  HSSFRow dateTitle = worksheet.createRow((short) startRowIndex);
  HSSFCell cellDate = dateTitle.createCell(startColIndex);
  cellDate.setCellValue("This report was generated at " + new Date());
 }
  
 /**
  * Builds the column headers
  * 
  * @param worksheet
  * @param startRowIndex starting row offset
  * @param startColIndex starting column offset
  */
@SuppressWarnings("deprecation")
public void IndividualHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
  // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Last Name");
  cell1.setCellStyle(headerCellStyle);
 
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("MI");
  cell2.setCellStyle(headerCellStyle);
 
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("First Name");
  cell3.setCellStyle(headerCellStyle);
 
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("Country");
  cell4.setCellStyle(headerCellStyle);
 
 }


@SuppressWarnings("deprecation")
public void buildCompanyReport2(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
  // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Company Name");
  cell1.setCellStyle(headerCellStyle);
  
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Company Type");
  cell2.setCellStyle(headerCellStyle);
  
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Location");
  cell3.setCellStyle(headerCellStyle);
  
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("Active");
  cell4.setCellStyle(headerCellStyle);
 
  HSSFCell cellii = rowHeader.createCell(startColIndex+4);
  cellii.setCellValue("Application Name");
  cellii.setCellStyle(headerCellStyle);

  HSSFCell celliii = rowHeader.createCell(startColIndex+5);
  celliii.setCellValue("Description");
  celliii.setCellStyle(headerCellStyle);
  
 }

@SuppressWarnings("deprecation")
public void buildCompanyReport3(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
  // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Company Name");
  cell1.setCellStyle(headerCellStyle);
  
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Company Type");
  cell2.setCellStyle(headerCellStyle);
  
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Location");
  cell3.setCellStyle(headerCellStyle);
  
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("Active");
  cell4.setCellStyle(headerCellStyle);
 
  
  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
  cell5.setCellValue("Application Name");
  cell5.setCellStyle(headerCellStyle);
  
  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
  cell6.setCellValue("Provider");
  cell6.setCellStyle(headerCellStyle);

}

@SuppressWarnings("deprecation")
public void buildCompanyReport4(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
  // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Company Name");
  cell1.setCellStyle(headerCellStyle);
  
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Company Type");
  cell2.setCellStyle(headerCellStyle);
  
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Location");
  cell3.setCellStyle(headerCellStyle);
  
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("Active");
  cell4.setCellStyle(headerCellStyle);
 
  HSSFCell cell51 = rowHeader.createCell(startColIndex+4);
  cell51.setCellValue("Shareholder Type");
  cell51.setCellStyle(headerCellStyle);
  
  HSSFCell cell5 = rowHeader.createCell(startColIndex+5);
  cell5.setCellValue("Shareholder Company/Individual Name");
  cell5.setCellStyle(headerCellStyle);
  
  HSSFCell cell6 = rowHeader.createCell(startColIndex+6);
  cell6.setCellValue("Percentage [%]");
  cell6.setCellStyle(headerCellStyle);
}

@SuppressWarnings("deprecation")
public void buildCompanyReport5(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
  // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Company Name");
  cell1.setCellStyle(headerCellStyle);
  
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Company Type");
  cell2.setCellStyle(headerCellStyle);
  
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Location");
  cell3.setCellStyle(headerCellStyle);
  
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("Active");
  cell4.setCellStyle(headerCellStyle);
   
  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
  cell5.setCellValue("File Name");
  cell5.setCellStyle(headerCellStyle);
  
  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
  cell6.setCellValue("File Description");
  cell6.setCellStyle(headerCellStyle);
  
  HSSFCell cell7 = rowHeader.createCell(startColIndex+6);
  cell7.setCellValue("File Type");
  cell7.setCellStyle(headerCellStyle);
  
  HSSFCell cell8 = rowHeader.createCell(startColIndex+7);
  cell8.setCellValue("File Extenstion");
  cell8.setCellStyle(headerCellStyle);
  
  HSSFCell cell9 = rowHeader.createCell(startColIndex+8);
  cell9.setCellValue("File Year");
  cell9.setCellStyle(headerCellStyle);
  

}

@SuppressWarnings("deprecation")
public void MarketSignalHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex, int comp, int countries, int individual) {
 
	
 // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Input Date");
  cell1.setCellStyle(headerCellStyle);
 
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Market Signal Date");
  cell2.setCellStyle(headerCellStyle);
 
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Factor");
  cell3.setCellStyle(headerCellStyle);
 
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("Company/Institution");
  cell4.setCellStyle(headerCellStyle);
  
  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
  cell5.setCellValue("Heading (Original)");
  cell5.setCellStyle(headerCellStyle);
 
  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
  cell6.setCellValue("Heading (Translation)");
  cell6.setCellStyle(headerCellStyle);
 
  HSSFCell cell7 = rowHeader.createCell(startColIndex+6);
  cell7.setCellValue("Link");
  cell7.setCellStyle(headerCellStyle);
 
  HSSFCell cell8 = rowHeader.createCell(startColIndex+7);
  cell8.setCellValue("Summary");
  cell8.setCellStyle(headerCellStyle);
 
  HSSFCell cell9 = rowHeader.createCell(startColIndex+8);
  cell9.setCellValue("Background");
  cell9.setCellStyle(headerCellStyle);
 
  HSSFCell cell10 = rowHeader.createCell(startColIndex+9);
  cell10.setCellValue("Implication");
  cell10.setCellStyle(headerCellStyle);
  
  int lastColCompCount = startColIndex+9; 
  int lastColCountryCount = lastColCompCount+comp; 
  int lastColIndCount = lastColCountryCount+countries; 
  
 for(int i = 0; i < comp; i++){
	  HSSFCell ii = rowHeader.createCell(lastColCompCount+(i + 1));
	  ii.setCellValue("Company/Institutions");
	  ii.setCellStyle(headerCellStyle);
 }
 
 for(int j = 0; j < countries; j++){
	  HSSFCell jj = rowHeader.createCell(lastColCountryCount+(j+ 1));
	  jj.setCellValue("Countries");
	  jj.setCellStyle(headerCellStyle);
}
 for(int x = 0; x < individual; x++){
	  HSSFCell xx = rowHeader.createCell(lastColIndCount+(x + 1));
	  xx.setCellValue("Individuals");
	  xx.setCellStyle(headerCellStyle);
}
 
}


@SuppressWarnings("deprecation")
public void MeetingHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex, int individual) {
 
	
 // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Event Date");
  cell1.setCellStyle(headerCellStyle);
 
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Company");
  cell2.setCellStyle(headerCellStyle);
 
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Meeting Summary");
  cell3.setCellStyle(headerCellStyle);
 
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("To-Follow");
  cell4.setCellStyle(headerCellStyle);
  
  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
  cell5.setCellValue("Location (Specific Address)");
  cell5.setCellStyle(headerCellStyle);
 
  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
  cell6.setCellValue("Location (Country)");
  cell6.setCellStyle(headerCellStyle);
   
  int lastColIndCount = startColIndex+5; 
	for(int x = 0; x < individual; x++){
		  HSSFCell xx = rowHeader.createCell(lastColIndCount+(x + 1));
		  xx.setCellValue("Attendees");
		  xx.setCellStyle(headerCellStyle);
	}
 
}

@SuppressWarnings("deprecation")
public void CompanyHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
 
	
 // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Name");
  cell1.setCellStyle(headerCellStyle);
 
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Website");
  cell2.setCellStyle(headerCellStyle);
 
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Notes");
  cell3.setCellStyle(headerCellStyle);
 
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("Type");
  cell4.setCellStyle(headerCellStyle);
  
  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
  cell5.setCellValue("Holding Company");
  cell5.setCellStyle(headerCellStyle);
 
  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
  cell6.setCellValue("Location (Country)");
  cell6.setCellStyle(headerCellStyle);
     
  HSSFCell cell7 = rowHeader.createCell(startColIndex+6);
  cell7.setCellValue("Active");
  cell7.setCellStyle(headerCellStyle);
  
  HSSFCell cell8 = rowHeader.createCell(startColIndex+7);
  cell8.setCellValue("Address 1");
  cell8.setCellStyle(headerCellStyle);
 
  HSSFCell cell9 = rowHeader.createCell(startColIndex+8);
  cell9.setCellValue("Address 2");
  cell9.setCellStyle(headerCellStyle);
   
  HSSFCell cell10 = rowHeader.createCell(startColIndex+9);
  cell10.setCellValue("Address 3");
  cell10.setCellStyle(headerCellStyle);
  
  HSSFCell cell11 = rowHeader.createCell(startColIndex+10);
  cell11.setCellValue("Email");
  cell11.setCellStyle(headerCellStyle);
 
  HSSFCell cell12 = rowHeader.createCell(startColIndex+11);
  cell12.setCellValue("LinkedIn");
  cell12.setCellStyle(headerCellStyle);
   
  HSSFCell cell13 = rowHeader.createCell(startColIndex+12);
  cell13.setCellValue("Youtube");
  cell13.setCellStyle(headerCellStyle);
  
  HSSFCell cell14 = rowHeader.createCell(startColIndex+13);
  cell14.setCellValue("Twitter");
  cell14.setCellStyle(headerCellStyle);
  
}

@SuppressWarnings("deprecation")
public void IndMeetingHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex, int individual) {
 
	
 // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Event Date");
  cell1.setCellStyle(headerCellStyle);
 
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Company");
  cell2.setCellStyle(headerCellStyle);
 
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Meeting Summary");
  cell3.setCellStyle(headerCellStyle);
 
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("To-Follow");
  cell4.setCellStyle(headerCellStyle);
  
  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
  cell5.setCellValue("Location (Specific Address)");
  cell5.setCellStyle(headerCellStyle);
 
  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
  cell6.setCellValue("Location (Country)");
  cell6.setCellStyle(headerCellStyle);
   
  int lastColIndCount = startColIndex+5; 
	for(int x = 0; x < individual; x++){
		  HSSFCell xx = rowHeader.createCell(lastColIndCount+(x + 1));
		  xx.setCellValue("Attendees");
		  xx.setCellStyle(headerCellStyle);
	}
 
}

@SuppressWarnings("deprecation")
public void IndCompSharesHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
 
	
 // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Company");
  cell1.setCellStyle(headerCellStyle);
 
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Company Type");
  cell2.setCellStyle(headerCellStyle);
  
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Location");
  cell3.setCellStyle(headerCellStyle);
  
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("Percentage (%)");
  cell4.setCellStyle(headerCellStyle);
 
 }

@SuppressWarnings("deprecation")
public void IndCompInvHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
 
	
 // Create font style for the headers
  Font font = worksheet.getWorkbook().createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
 
        // Create cell style for the headers
  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
  headerCellStyle.setWrapText(true);
  headerCellStyle.setFont(font);
  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
   
  // Create the column headers
  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
  rowHeader.setHeight((short) 500);
   
  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
  cell1.setCellValue("Company");
  cell1.setCellStyle(headerCellStyle);
 
  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
  cell2.setCellValue("Group Company");
  cell2.setCellStyle(headerCellStyle);
  
  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
  cell3.setCellValue("Title");
  cell3.setCellStyle(headerCellStyle);
  
  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
  cell4.setCellValue("Company (Base)");
  cell4.setCellStyle(headerCellStyle);
  
  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
  cell5.setCellValue("Location (City)");
  cell5.setCellStyle(headerCellStyle);
  
  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
  cell6.setCellValue("Location (Headquarter)");
  cell6.setCellStyle(headerCellStyle);
  
  HSSFCell cell7 = rowHeader.createCell(startColIndex+6);
  cell7.setCellValue("Active");
  cell7.setCellStyle(headerCellStyle);
 
 }


 public void PositionHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Create font style for the headers
	  Font font = worksheet.getWorkbook().createFont();
	        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	 
	        // Create cell style for the headers
	  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
	  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
	  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
	  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
	  headerCellStyle.setWrapText(true);
	  headerCellStyle.setFont(font);
	  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
	   
	  // Create the column headers
	  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
	  rowHeader.setHeight((short) 500);
	   
	  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
	  cell1.setCellValue("Last Name");
	  cell1.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
	  cell2.setCellValue("MI");
	  cell2.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
	  cell3.setCellValue("First Name");
	  cell3.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
	  cell4.setCellValue("Country");
	  cell4.setCellStyle(headerCellStyle);
	  
	  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
	  cell5.setCellValue("Company");
	  cell5.setCellStyle(headerCellStyle);
	  
	  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
	  cell6.setCellValue("Group Company");
	  cell6.setCellStyle(headerCellStyle);
	  
	  HSSFCell cell7 = rowHeader.createCell(startColIndex+6);
	  cell7.setCellValue("Title");
	  cell7.setCellStyle(headerCellStyle);
	  
	  HSSFCell cell8 = rowHeader.createCell(startColIndex+7);
	  cell8.setCellValue("Location - Country");
	  cell8.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell9 = rowHeader.createCell(startColIndex+8);
	  cell9.setCellValue("Location - City");
	  cell9.setCellStyle(headerCellStyle);
	  
	  HSSFCell cell10 = rowHeader.createCell(startColIndex+9);
	  cell10.setCellValue("Active");
	  cell10.setCellStyle(headerCellStyle);
	 }
 
 public void EducationalHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Create font style for the headers
	  Font font = worksheet.getWorkbook().createFont();
	        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	 
	        // Create cell style for the headers
	  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
	  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
	  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
	  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
	  headerCellStyle.setWrapText(true);
	  headerCellStyle.setFont(font);
	  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
	   
	  // Create the column headers
	  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
	  rowHeader.setHeight((short) 500);
	   
	  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
	  cell1.setCellValue("Last Name");
	  cell1.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
	  cell2.setCellValue("MI");
	  cell2.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
	  cell3.setCellValue("First Name");
	  cell3.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
	  cell4.setCellValue("Country");
	  cell4.setCellStyle(headerCellStyle);
	  
	  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
	  cell5.setCellValue("Degree");
	  cell5.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
	  cell6.setCellValue("Discipline");
	  cell6.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell7 = rowHeader.createCell(startColIndex+6);
	  cell7.setCellValue("University");
	  cell7.setCellStyle(headerCellStyle);
	 
	 }
 
 public void IndMarketSignalHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex,int comp, int countries, int individual) {
	  
	// Create font style for the headers
	  Font font = worksheet.getWorkbook().createFont();
	        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	 
	        // Create cell style for the headers
	  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
	  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
	  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
	  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
	  headerCellStyle.setWrapText(true);
	  headerCellStyle.setFont(font);
	  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
	   
	  // Create the column headers
	  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
	  rowHeader.setHeight((short) 500);
	   
	  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
	  cell1.setCellValue("Input Date");
	  cell1.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
	  cell2.setCellValue("Market Signal Date");
	  cell2.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
	  cell3.setCellValue("Factor");
	  cell3.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
	  cell4.setCellValue("Company/Institution");
	  cell4.setCellStyle(headerCellStyle);
	  
	  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
	  cell5.setCellValue("Heading (Original)");
	  cell5.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell6 = rowHeader.createCell(startColIndex+5);
	  cell6.setCellValue("Heading (Translation)");
	  cell6.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell7 = rowHeader.createCell(startColIndex+6);
	  cell7.setCellValue("Link");
	  cell7.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell8 = rowHeader.createCell(startColIndex+7);
	  cell8.setCellValue("Summary");
	  cell8.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell9 = rowHeader.createCell(startColIndex+8);
	  cell9.setCellValue("Background");
	  cell9.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell10 = rowHeader.createCell(startColIndex+9);
	  cell10.setCellValue("Implication");
	  cell10.setCellStyle(headerCellStyle);
	  
	  int lastColCompCount = startColIndex+9; 
	  int lastColCountryCount = lastColCompCount+comp; 
	  int lastColIndCount = lastColCountryCount+countries; 
	  
	 for(int i = 0; i < comp; i++){
		  HSSFCell ii = rowHeader.createCell(lastColCompCount+(i + 1));
		  ii.setCellValue("Company/Institutions");
		  ii.setCellStyle(headerCellStyle);
	 }
	 
	 for(int j = 0; j < countries; j++){
		  HSSFCell jj = rowHeader.createCell(lastColCountryCount+(j+ 1));
		  jj.setCellValue("Countries");
		  jj.setCellStyle(headerCellStyle);
	}
	 for(int x = 0; x < individual; x++){
		  HSSFCell xx = rowHeader.createCell(lastColIndCount+(x + 1));
		  xx.setCellValue("Individuals");
		  xx.setCellStyle(headerCellStyle);
	}
	 
	}

 public void NationalityHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex) {
	  // Create font style for the headers
	  Font font = worksheet.getWorkbook().createFont();
	        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	 
	        // Create cell style for the headers
	  HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
	  headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
	  headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
	  headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	  headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
	  headerCellStyle.setWrapText(true);
	  headerCellStyle.setFont(font);
	  headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
	   
	  // Create the column headers
	  HSSFRow rowHeader = worksheet.createRow((short) startRowIndex +1);
	  rowHeader.setHeight((short) 500);
	   
	  HSSFCell cell1 = rowHeader.createCell(startColIndex+0);
	  cell1.setCellValue("Last Name");
	  cell1.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell2 = rowHeader.createCell(startColIndex+1);
	  cell2.setCellValue("MI");
	  cell2.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell3 = rowHeader.createCell(startColIndex+2);
	  cell3.setCellValue("First Name");
	  cell3.setCellStyle(headerCellStyle);
	 
	  HSSFCell cell4 = rowHeader.createCell(startColIndex+3);
	  cell4.setCellValue("Country");
	  cell4.setCellStyle(headerCellStyle);
	  
	  HSSFCell cell5 = rowHeader.createCell(startColIndex+4);
	  cell5.setCellValue("Nationality");
	  cell5.setCellStyle(headerCellStyle);
	 	 
	 }
 
}
