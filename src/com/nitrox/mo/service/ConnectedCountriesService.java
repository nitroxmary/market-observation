package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.ConnectedCountries;

public interface ConnectedCountriesService {

	public void addConnectedCountries(ConnectedCountries c);
	public void updateConnectedCountries(ConnectedCountries c);
	public void removeConnectedCountries(ConnectedCountries c);
	
	public List<ConnectedCountries> list(int id);
	public String getMaxCountries();
	public ConnectedCountries getConnectedCountriesById(int id);
	
} 
