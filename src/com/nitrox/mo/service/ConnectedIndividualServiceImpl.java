package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.ConnectedIndividualDAO;
import com.nitrox.mo.model.ConnectedIndividual;

@Service  
@Transactional
public class ConnectedIndividualServiceImpl implements ConnectedIndividualService{

	@Autowired
	private ConnectedIndividualDAO connectedIndDao;
	
	@Override
	public void addConnectedIndividual(ConnectedIndividual c){
		connectedIndDao.addConnectedIndividual(c);
	}
	
	@Override
	public void updateConnectedIndividual(ConnectedIndividual c){
		connectedIndDao.updateConnectedIndividual(c);
	}
	
	@Override
	public void removeConnectedIndividual(ConnectedIndividual c){
		connectedIndDao.removeConnectedIndividual(c);
	}
	
	@Override
	public List<ConnectedIndividual> list(int id){
		return connectedIndDao.list(id);
	}
	
	@Override
	public List<ConnectedIndividual> indList(int id){
		return connectedIndDao.indList(id);
	}
	
	public String getMaxInd(){
		return connectedIndDao.getMaxInd();
	}
	
	@Override
	public ConnectedIndividual getConnectedIndividualById(int id){
		return connectedIndDao.getConnectedIndividualById(id);
	}
}
