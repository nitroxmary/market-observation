package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.Countries;

public interface CountryService {

	public void addCountry(Countries c);
	public void updateCountry(Countries c);
	public void removeCountry(Countries c);
	
	public List<Countries> list();
	public List<Countries> nameList();
	
	public Countries getCountryById(int id);
} 