package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.IndividualPhonesDAO;
import com.nitrox.mo.model.IndividualPhones;

@Service  
@Transactional
public class IndividualPhoneServiceImpl implements IndividualPhoneService{

	@Autowired
	private IndividualPhonesDAO indContactDao;
	
	@Override
	public List<IndividualPhones> list(int id){
		return indContactDao.list(id);
	}

	@Override
	public void addIndividualPhones(IndividualPhones c) {
		indContactDao.addIndividualPhones(c);
		
	}

	@Override
	public void updateIndividualPhones(IndividualPhones c) {
		indContactDao.updateIndividualPhones(c);
		
	}

	@Override
	public void removeIndividualPhones(IndividualPhones c) {
		indContactDao.removeIndividualPhones(c);
		
	}

	@Override
	public IndividualPhones getIndividualPhonesById(int id) {
		return indContactDao.getIndividualPhonesById(id);
	}
	
	public String getMaxInd(){
		return indContactDao.getMaxInd();
	}
	
}