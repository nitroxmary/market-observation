package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.EducationDAO;
import com.nitrox.mo.model.Education;

@Service  
@Transactional
public class EducationServiceImpl implements EducationService{

	@Autowired
	private EducationDAO educDao;
	
	@Override
	public void addEducation(Education c){
		educDao.addEducation(c);
	}
	
	@Override
	public void updateEducation(Education c){
		educDao.updateEducation(c);
	}
	
	@Override
	public void removeEducation(Education c){
		educDao.removeEducation(c);
	}
	
	@Override
	public List<Education> list(int id){
		return educDao.list(id);
	}
	
	@Override
	public List<Education> list(){
		return educDao.list();
	}
	
	@Override
	public Education getEducationById(int id){
		return educDao.getEducationById(id);
	}
}
