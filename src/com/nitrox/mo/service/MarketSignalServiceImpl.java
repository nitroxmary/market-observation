package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.MarketSignalDAO;
import com.nitrox.mo.model.MarketSignal;

@Service  
@Transactional
public class MarketSignalServiceImpl implements MarketSignalService{

	@Autowired
	private MarketSignalDAO mktSignalDao;
	
	@Override
	public void addMarketSignal(MarketSignal c){
		mktSignalDao.addMarketSignal(c);
	}
	
	@Override
	public void updateMarketSignal(MarketSignal c){
		mktSignalDao.updateMarketSignal(c);
	}
	
	@Override
	public void removeMarketSignal(MarketSignal c){
		mktSignalDao.removeMarketSignal(c);
	}
	
	@Override
	public List<MarketSignal> list(){
		return mktSignalDao.list();
	}
	
	@Override
	public List<MarketSignal> list(int id){
		return mktSignalDao.list(id);
	}
	
	@Override
	public MarketSignal getSignalId(int id){
		return mktSignalDao.getSignalId(id);
	}
}