package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.CompanyFilesDAO;
import com.nitrox.mo.model.CompanyFiles;

@Service  
@Transactional
public class CompanyFilesServiceImpl implements CompanyFilesService{

	@Autowired
	private CompanyFilesDAO companyFilesDao;
	
	@Override
	public void addCompanyFiles(CompanyFiles c){
		companyFilesDao.addCompanyFiles(c);
	}
	
	@Override
	public void updateCompanyFiles(CompanyFiles c){
		companyFilesDao.updateCompanyFiles(c);
	}
	
	@Override
	public void removeCompanyFiles(CompanyFiles c){
		companyFilesDao.removeCompanyFiles(c);
	}
	
	@Override
	public List<CompanyFiles> list(int id){
		return companyFilesDao.list(id);
	}
	
	@Override
	public List<CompanyFiles> list(){
		return companyFilesDao.list();
	}
	
	@Override
	public CompanyFiles getCompanyFilesId(int id){
		return companyFilesDao.getCompanyFilesId(id);
	}
}
