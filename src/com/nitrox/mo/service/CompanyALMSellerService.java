package com.nitrox.mo.service;

import java.util.List;

import com.nitrox.mo.model.CompanyALMSeller;

public interface CompanyALMSellerService {

	public void addCompany(CompanyALMSeller c);
	public void updateCompany(CompanyALMSeller c);
	public void removeCompany(CompanyALMSeller c);
	
	public List<CompanyALMSeller> list();
	public List<CompanyALMSeller> list(int id);	
	public CompanyALMSeller getCompanyId(int id);
	public String getMaxSeller();
	
	
} 
