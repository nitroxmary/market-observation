//package com.nitrox.mo.service;
//
//import java.util.List;  
//
////import com.nitrox.mo.model.ResearchEmployee;
//import com.nitrox.mo.model.Session;  
//
//public interface SessionService {
//	
//	//saves session to database
//	public void saveSession ( Session session );  
//	
//	//deletes existing session from the database
//	public void deleteSession (Session session);  
//	
//
//	//returns all sessions saved in database
//	public List<Session> getSessions();  
//  
//	//returns session by id
//	public Session getSession(int id);
//	
//
//	
//	//returns session by session string
//	public Session getSession(String sessionString);
//	
//	//deletes session by research employee
//	public void deleteSession (ResearchEmployee employee); 
//	
//	//returns session by research employee
//	public Session getSession(ResearchEmployee employee);
//  
//}  