package com.nitrox.mo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nitrox.mo.dao.CompanyPhonesDAO;
import com.nitrox.mo.model.CompanyPhone;

@Service  
@Transactional
public class CompanyPhonesServiceImpl implements CompanyPhonesService{

	@Autowired
	private CompanyPhonesDAO companyDao;
	
	@Override
	public void addCompanyPhones(CompanyPhone c){
		System.out.println("camp :"+c);
		companyDao.addCompanyContacts(c);
	}
	
	@Override
	public void updateCompanyPhones(CompanyPhone c){
		companyDao.updateCompanyContacts(c);
	}
	
	@Override
	public void removeCompanyPhones(CompanyPhone c){
		companyDao.removeCompanyContacts(c);
	}
	
	@Override
	public List<CompanyPhone> list(){
		return companyDao.list();
	}
	
	@Override
	public List<CompanyPhone> nameList(int id){
		return companyDao.nameList(id);
	}
	@Override
	public CompanyPhone getCompanyPhonesId(int id){
		return companyDao.getCompanyId(id);
	}
}