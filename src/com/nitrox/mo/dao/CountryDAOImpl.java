package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.Countries;

@Repository("countryDAO")
public class CountryDAOImpl extends AbstractDAO implements CountryDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addCountry(Countries c){
		persist(c);
	}
		
	@Override
	public void updateCountry(Countries c){
		edit(c);
	}
		
	@Override
	public void removeCountry(Countries c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Countries> list(){
		
		Criteria criteria = getSession().createCriteria(Countries.class)
				.setProjection(Projections.projectionList()
			        .add(Projections.property("id"),"id")
			        .add(Projections.property("code"),"code"))
			        .setResultTransformer(Transformers.aliasToBean(Countries.class));
		return (List<Countries>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Countries> nameList(){
		Criteria criteria = getSession().createCriteria(Countries.class);
		return (List<Countries>) criteria.list();
	}
		
	@Override
	public Countries getCountryById(int id){
		Criteria criteria = getSession().createCriteria(Countries.class).add(Restrictions.eq("id", id));
		return (Countries) criteria.uniqueResult();
		
	}
	
	
}
