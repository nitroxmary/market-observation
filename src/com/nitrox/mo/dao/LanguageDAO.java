package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.Language;

public interface LanguageDAO {

	public void addLanguage(Language c);
	public void updateLanguage(Language c);
	public void removeLanguage(Language c);
	
	public List<Language> list(int id);
	public Language getLanguageById(int id);
	
} 
