package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.MarketSignal;

public interface MarketSignalDAO {

	public void addMarketSignal(MarketSignal ms);
	public void updateMarketSignal(MarketSignal ms);
	public void removeMarketSignal(MarketSignal ms);
	
	public List<MarketSignal> list();
	public MarketSignal getSignalId(int id);
	public List<MarketSignal> list(int id) ;
}
