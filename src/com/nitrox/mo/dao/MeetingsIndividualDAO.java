package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.MeetingsIndividual;

public interface MeetingsIndividualDAO {
	
	public void addMeetingsIndividual(MeetingsIndividual c);
	public void updateMeetingsIndividual(MeetingsIndividual c);
	public void removeMeetingsIndividual(MeetingsIndividual c);
	
	public List<MeetingsIndividual> list(int id);
	public List<MeetingsIndividual> indList(int id);
	public String getMaxAttendees();
	public MeetingsIndividual getMeetingsIndividualById(int id);
	
} 
