package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.Awards;

@Repository("awardsDAO")
public class AwardsDAOImpl extends AbstractDAO  implements AwardsDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addAwards(Awards c){
		persist(c);
	}
	
	@Override
	public void updateAwards(Awards c){
		edit(c);
	}
	
	@Override
	public void removeAwards(Awards c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Awards> list(int id){
		Criteria criteria = getSession().createCriteria(Awards.class).add(Restrictions.eq("ind.id", id));
		return (List<Awards>) criteria.list();
	}
	
	@Override
	public Awards getAwardsId(int id){
		Criteria criteria = getSession().createCriteria(Awards.class).add(Restrictions.eq("id", id));
	return (Awards) criteria.uniqueResult();
	}
}
	
