package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.MeetingsIndividual;

@Repository("mettingsIndividualDAO")
public class MeetingsIndividualDAOImpl extends AbstractDAO implements MeetingsIndividualDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addMeetingsIndividual(MeetingsIndividual c){
		persist(c);
	}
	
	@Override
	public void updateMeetingsIndividual(MeetingsIndividual c){
		edit(c);
	}
	
	@Override
	public void removeMeetingsIndividual(MeetingsIndividual c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MeetingsIndividual> list(int id){
		Criteria criteria = getSession().createCriteria(MeetingsIndividual.class).add(Restrictions.eq("meeting.id", id));
		return (List<MeetingsIndividual>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MeetingsIndividual> indList(int id){
		Criteria criteria = getSession().createCriteria(MeetingsIndividual.class).add(Restrictions.eq("ind.id", id));
		return (List<MeetingsIndividual>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getMaxAttendees(){
		 Query q = getSession().createSQLQuery("SELECT COUNT(*) as count FROM meetings_individual GROUP BY meeting_id ORDER BY count DESC");
		 
		 if(q.list().size() == 0){
			 return "0";
		 }else{
			 return q.list().get(0).toString();
		 }
	}
	
	@Override
	public MeetingsIndividual getMeetingsIndividualById(int id){
		Criteria criteria = getSession().createCriteria(MeetingsIndividual.class).add(Restrictions.eq("id", id));
	return (MeetingsIndividual) criteria.uniqueResult();
	}
}

