package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.Individual;

@Repository("individualDAO")
public class IndividualDAOImpl extends AbstractDAO implements IndividualDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addIndividual(Individual c){
		persist(c);
	}
	
	@Override
	public void updateIndividual(Individual c){
		edit(c);
	}
	
	@Override
	public void removeIndividual(Individual c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Individual> list(){
		Criteria criteria = getSession().createCriteria(Individual.class)
				           .addOrder(Order.asc("lastName"))
				           .addOrder(Order.asc("middleInitial"));
		return (List<Individual>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Individual> nameList(){
		Criteria criteria = getSession().createCriteria(Individual.class)
				.addOrder(Order.asc("firstName"));
		return (List<Individual>) criteria.list();
	}
		
	@Override
	public Individual getIndividualById(int id){
		Criteria criteria = getSession().createCriteria(Individual.class).add(Restrictions.eq("id", id));
	return (Individual) criteria.uniqueResult();
	}
}
