package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.CompanyInvolved;

public interface CompanyInvolvedDAO {

	public void addCompanyInvolved(CompanyInvolved c);
	public void updateCompanyInvolved(CompanyInvolved c);
	public void removeCompanyInvolved(CompanyInvolved c);
	
	public List<CompanyInvolved> list();
	public List<CompanyInvolved> list(int id);
	public List<CompanyInvolved> listCompanies(int id);
	public CompanyInvolved getCompanyInvolvedById(int id);
	
}