package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.Nationalities;

@Repository("nationalititesDAO")
public class NationalitiesDAOImpl extends AbstractDAO implements NationalitiesDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addNationalities(Nationalities c){
		persist(c);
	}
	
	@Override
	public void updateNationalities(Nationalities c){
		edit(c);
	}
	
	@Override
	public void removeNationalities(Nationalities c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Nationalities> list(int id){
		Criteria criteria = getSession().createCriteria(Nationalities.class).add(Restrictions.eq("ind.id", id));
		return (List<Nationalities>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Nationalities> list(){
		Criteria criteria = getSession().createCriteria(Nationalities.class);
		return (List<Nationalities>) criteria.list();
	}
	
	@Override
	public Nationalities getNationalitiesById(int id){
		Criteria criteria = getSession().createCriteria(Nationalities.class).add(Restrictions.eq("id", id));
	return (Nationalities) criteria.uniqueResult();
	}
}
