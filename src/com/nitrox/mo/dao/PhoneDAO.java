package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.Phone;

public interface PhoneDAO {

	public void addPhone(Phone c);
	public void updatePhone(Phone c);
	public void removePhone(Phone c);
	
	public List<Phone> list();
	public Phone getPhoneById(int id);
	
} 
