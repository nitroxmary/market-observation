package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.Countries;


public interface CountryDAO {

	public void addCountry(Countries c);
	public void updateCountry(Countries c);
	public void removeCountry(Countries c);
	
	public List<Countries> list();
	public Countries getCountryById(int id);
	
	public List<Countries> nameList();
	
} 
