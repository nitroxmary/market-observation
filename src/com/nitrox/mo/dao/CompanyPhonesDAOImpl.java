package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.CompanyPhone;

@Repository("companyContactsDAO")
public class CompanyPhonesDAOImpl extends AbstractDAO implements CompanyPhonesDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addCompanyContacts(CompanyPhone c){
		persist(c);
	}
	
	@Override
	public void updateCompanyContacts(CompanyPhone c){
		edit(c);
	}
	
	@Override
	public void removeCompanyContacts(CompanyPhone c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyPhone> list(){
		Criteria criteria = getSession().createCriteria(CompanyPhone.class);
		return (List<CompanyPhone>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyPhone> nameList(int id){
		Criteria criteria = getSession().createCriteria(CompanyPhone.class)
						   .add(Restrictions.eq("company.id", id));
		return (List<CompanyPhone>) criteria.list();
	}
	
	@Override
	public CompanyPhone getCompanyId(int id){
		Criteria criteria = getSession().createCriteria(CompanyPhone.class).add(Restrictions.eq("id", id));
	return (CompanyPhone) criteria.uniqueResult();
	}
	
//	@SuppressWarnings("unchecked")
//	@Override
//	public String getMaXPhones(){
//		 Query q = getSession().createSQLQuery("SELECT COUNT(*) as count FROM company_phones GROUP BY company_id ORDER BY count DESC");
//		 
//		 if(q.list().size() == 0){
//			 return "0";
//		 }else{
//			 return q.list().get(0).toString();
//		 }
//	}
}