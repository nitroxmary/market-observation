package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.Phone;

@Repository("phoneDAO")
public class PhoneDAOImpl extends AbstractDAO implements PhoneDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addPhone(Phone c){
		persist(c);
	}
	
	@Override
	public void updatePhone(Phone c){
		edit(c);
	}
	
	@Override
	public void removePhone(Phone c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Phone> list(){
		Criteria criteria = getSession().createCriteria(Phone.class);
		return (List<Phone>) criteria.list();
	}
	
	@Override
	public Phone getPhoneById(int id){
		Criteria criteria = getSession().createCriteria(Phone.class).add(Restrictions.eq("id", id));
	return (Phone) criteria.uniqueResult();
	}
}
