package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.IndividualPhones;

@Repository("individualPhonesDAOImplDAO")
public class IndividualPhonesDAOImpl extends AbstractDAO implements IndividualPhonesDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<IndividualPhones> list(int id){
		Criteria criteria = getSession().createCriteria(IndividualPhones.class).add(Restrictions.eq("ind.id", id));
		return (List<IndividualPhones>) criteria.list();
	}
	
	

	@Override
	public void addIndividualPhones(IndividualPhones c) {
		persist(c);
		
	}

	@Override
	public void updateIndividualPhones(IndividualPhones c) {
		edit(c);
		
	}

	@Override
	public void removeIndividualPhones(IndividualPhones c) {
		delete(c);
		
	}

	@Override
	public IndividualPhones getIndividualPhonesById(int id) {
		Criteria criteria = getSession().createCriteria(IndividualPhones.class).add(Restrictions.eq("phone.id", id));
		return (IndividualPhones) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getMaxInd(){
		 Query q = getSession().createSQLQuery("SELECT COUNT(*) as count FROM individual_phones GROUP BY ind_id ORDER BY count DESC");
		 
		 if(q.list().size() == 0){
			 return "0";
		 }else{
			 return q.list().get(0).toString();
		 }
	}
	
}

