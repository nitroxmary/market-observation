package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.Company;

@Repository("companyDAO")
public class CompanyDAOImpl extends AbstractDAO implements CompanyDAO{


	@Override
	public void addCompany(Company c){
		persist(c);
	}
	
	@Override
	public void updateCompany(Company c){
		edit(c);
	}
	
	@Override
	public void removeCompany(Company c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Company> list(){
		Criteria criteria = getSession().createCriteria(Company.class);
		return (List<Company>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Company> nameList(){
		Criteria criteria = getSession().createCriteria(Company.class)
				           .add(Restrictions.eq("isActive",true))
				           .addOrder(Order.asc("name")); 
		return (List<Company>) criteria.list();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Company> holdings(){
		Criteria criteria = getSession().createCriteria(Company.class)
				            .add(Restrictions.eq("isHolding",true))
				            .addOrder(Order.asc("name")); 
		return (List<Company>) criteria.list();
	}
	
	@Override
	public Company getCompanyId(int id){
		Criteria criteria = getSession().createCriteria(Company.class).add(Restrictions.eq("id", id));
	return (Company) criteria.uniqueResult();
	}
}