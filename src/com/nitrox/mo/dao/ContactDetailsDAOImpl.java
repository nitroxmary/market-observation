package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.ContactDetails;

@Repository("contactDetailsDAO")
public class ContactDetailsDAOImpl extends AbstractDAO implements ContactDetailsDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addContactDetails(ContactDetails c){
		persist(c);
	}
	
	@Override
	public void updateContactDetails(ContactDetails c){
		edit(c);
	}
	
	@Override
	public void removeContactDetails(ContactDetails c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ContactDetails> list(){
		Criteria criteria = getSession().createCriteria(ContactDetails.class);
		return (List<ContactDetails>) criteria.list();
	}
	
	@Override
	public ContactDetails getContactDetailsById(int id){
		Criteria criteria = getSession().createCriteria(ContactDetails.class).add(Restrictions.eq("id", id));
	return (ContactDetails) criteria.uniqueResult();
	}
}
