package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.CompanyALMProvider;

@Repository("companyALMProviderDAO")
public class CompanyALMProviderDAOImpl extends AbstractDAO implements CompanyALMProviderDAO{

	
	@Override
	public void addCompany(CompanyALMProvider c){
		persist(c);
	}
	
	@Override
	public void updateCompany(CompanyALMProvider c){
		edit(c);
	}
	
	@Override
	public void removeCompany(CompanyALMProvider c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyALMProvider> list(){
		Criteria criteria = getSession().createCriteria(CompanyALMProvider.class);
		return (List<CompanyALMProvider>) criteria.list();
	}
	
	@Override
	public CompanyALMProvider getCompanyId(int id){
		Criteria criteria = getSession().createCriteria(CompanyALMProvider.class).add(Restrictions.eq("company.id", id));
	return (CompanyALMProvider) criteria.uniqueResult();
	}
}