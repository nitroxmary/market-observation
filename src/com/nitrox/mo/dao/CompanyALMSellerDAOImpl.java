package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.CompanyALMSeller;

@Repository("companyALMSellerDAO")
public class CompanyALMSellerDAOImpl extends AbstractDAO implements CompanyALMSellerDAO{

	
	@Override
	public void addCompany(CompanyALMSeller c){
		persist(c);
	}
	
	@Override
	public void updateCompany(CompanyALMSeller c){
		edit(c);
	}
	
	@Override
	public void removeCompany(CompanyALMSeller c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyALMSeller> list(){
		Criteria criteria = getSession().createCriteria(CompanyALMSeller.class);
		return (List<CompanyALMSeller>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyALMSeller> list(int id){
		Criteria criteria = getSession().createCriteria(CompanyALMSeller.class).add(Restrictions.eq("company.id", id));
		return (List<CompanyALMSeller>) criteria.list();
	}
	
	@Override
	public CompanyALMSeller getCompanyId(int id){
		Criteria criteria = getSession().createCriteria(CompanyALMSeller.class).add(Restrictions.eq("id", id));
	return (CompanyALMSeller) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getMaxSeller(){
		 Query q = getSession().createSQLQuery("SELECT COUNT(*) as count FROM company_alm_seller GROUP BY company_id ORDER BY count DESC");
		 if(q.list().size() == 0){
			 return "0";
		 }else{
			 return q.list().get(0).toString();
		 }
	}
}