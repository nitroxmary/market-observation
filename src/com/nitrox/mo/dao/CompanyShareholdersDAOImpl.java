package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.CompanyShareholders;

@Repository("companyShareholdersDAO")
public class CompanyShareholdersDAOImpl extends AbstractDAO implements CompanyShareholdersDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addCompanyShareholders(CompanyShareholders c){
		persist(c);
	}
	
	@Override
	public void updateCompanyShareholders(CompanyShareholders c){
		edit(c);
	}
	
	@Override
	public void removeCompanyShareholders(CompanyShareholders c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyShareholders> list(int id){
		Criteria criteria = getSession().createCriteria(CompanyShareholders.class).add(Restrictions.eq("company.id", id));
		return (List<CompanyShareholders>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyShareholders> list(){
		Criteria criteria = getSession().createCriteria(CompanyShareholders.class).add(Restrictions.eq("type", 1));
		return (List<CompanyShareholders>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyShareholders> indList(int id){
		Criteria criteria = getSession().createCriteria(CompanyShareholders.class).add(Restrictions.eq("csInd.id", id));
		return (List<CompanyShareholders>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyShareholders> compList(int id){
		Criteria criteria = getSession().createCriteria(CompanyShareholders.class);
		Criterion rest1 = Restrictions.and(Restrictions.eq("csCompany.id", id));
		Criterion rest2 = Restrictions.and(Restrictions.eq("company.id", id));
		criteria.add(Restrictions.or(rest1,rest2));
		return (List<CompanyShareholders>) criteria.list();
	}
	
	@Override
	public CompanyShareholders getCompanyShareholdersById(int id){
		Criteria criteria = getSession().createCriteria(CompanyShareholders.class).add(Restrictions.eq("id", id));
	return (CompanyShareholders) criteria.uniqueResult();
	}
}
