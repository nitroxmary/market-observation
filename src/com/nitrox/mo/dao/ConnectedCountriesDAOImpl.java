package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.ConnectedCountries;

@Repository("connectedCountriesDAO")
public class ConnectedCountriesDAOImpl extends AbstractDAO  implements ConnectedCountriesDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addConnectedCountries(ConnectedCountries c){
		persist(c);
	}
	
	@Override
	public void updateConnectedCountries(ConnectedCountries c){
		edit(c);
	}
	
	@Override
	public void removeConnectedCountries(ConnectedCountries c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConnectedCountries> list(int id){
		Criteria criteria = getSession().createCriteria(ConnectedCountries.class).add(Restrictions.eq("marketSignal.id", id));
		return (List<ConnectedCountries>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getMaxCountries(){
		 Query q = getSession().createSQLQuery("SELECT COUNT(*) as count FROM connected_countries GROUP BY mSignal_id ORDER BY count DESC");
		
		 if(q.list().size() == 0){
			 return "0";
		 }else{
			 return q.list().get(0).toString();
		 }
	}
	
	@Override
	public ConnectedCountries getConnectedCountriesById(int id){
		Criteria criteria = getSession().createCriteria(ConnectedCountries.class).add(Restrictions.eq("id", id));
	return (ConnectedCountries) criteria.uniqueResult();
	}
}
