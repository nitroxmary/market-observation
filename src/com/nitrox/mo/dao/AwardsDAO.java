package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.Awards;

public interface AwardsDAO {
	
	public void addAwards(Awards c);
	public void updateAwards(Awards c);
	public void removeAwards(Awards c);
	
	public List<Awards> list(int id);
	public Awards getAwardsId(int id);
	
} 
