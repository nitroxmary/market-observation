package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.Meetings;

public interface MeetingsDAO {

	public void addMeetings(Meetings c);
	public void updateMeetings(Meetings c);
	public void removeMeetings(Meetings c);
	
	public List<Meetings> list();
	public List<Meetings> list(int id);
	public Meetings getMeetingsById(int id);
	
} 
