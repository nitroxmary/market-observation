package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.ConnectedIndividual;

@Repository("connectedIndividualDAO")
public class ConnectedIndividualDAOImpl extends AbstractDAO implements ConnectedIndividualDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addConnectedIndividual(ConnectedIndividual c){
		persist(c);
	}
	
	@Override
	public void updateConnectedIndividual(ConnectedIndividual c){
		edit(c);
	}
	
	@Override
	public void removeConnectedIndividual(ConnectedIndividual c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConnectedIndividual> list(int id){
		Criteria criteria = getSession().createCriteria(ConnectedIndividual.class).add(Restrictions.eq("marketSignal.id", id));
		return (List<ConnectedIndividual>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConnectedIndividual> indList(int id){
		Criteria criteria = getSession().createCriteria(ConnectedIndividual.class).add(Restrictions.eq("ind.id", id));
		return (List<ConnectedIndividual>) criteria.list();
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public String getMaxInd(){
		 Query q = getSession().createSQLQuery("SELECT COUNT(*) as count FROM connected_individual GROUP BY mSignal_id ORDER BY count DESC");
		 
		 if(q.list().size() == 0){
			 return "0";
		 }else{
			 return q.list().get(0).toString();
		 }
	}
	
	@Override
	public ConnectedIndividual getConnectedIndividualById(int id){
		Criteria criteria = getSession().createCriteria(ConnectedIndividual.class).add(Restrictions.eq("id", id));
	return (ConnectedIndividual) criteria.uniqueResult();
	}
}