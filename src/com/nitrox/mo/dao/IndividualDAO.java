package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.Individual;

public interface IndividualDAO {
	
	public void addIndividual(Individual c);
	public void updateIndividual(Individual c);
	public void removeIndividual(Individual c);
	
	public List<Individual> list();
	public List<Individual> nameList();
	
	public Individual getIndividualById(int id);
	
} 
