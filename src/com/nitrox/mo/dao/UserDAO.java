package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.User;

public interface UserDAO {
	
	public void addUser(User u);
	public void updateUser(User u);
	public void removeUser(User u);
	
	public List<User> list(int id);
	public User getUserId(int id);
	public User getUser(String uname);
	
} 
