package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.IndividualPhones;

public interface IndividualPhonesDAO {

	public void addIndividualPhones(IndividualPhones c);
	public void updateIndividualPhones(IndividualPhones c);
	public void removeIndividualPhones(IndividualPhones c);
	
	
	public IndividualPhones getIndividualPhonesById(int id);
	public List<IndividualPhones> list(int id);
	public String getMaxInd();
	
} 
