package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.ConnectedIndividual;

public interface ConnectedIndividualDAO {
	
	public void addConnectedIndividual(ConnectedIndividual c);
	public void updateConnectedIndividual(ConnectedIndividual c);
	public void removeConnectedIndividual(ConnectedIndividual c);
	
	public List<ConnectedIndividual> list(int id);
	public List<ConnectedIndividual> indList(int id);
	public String getMaxInd();
	public ConnectedIndividual getConnectedIndividualById(int id);
	
}
