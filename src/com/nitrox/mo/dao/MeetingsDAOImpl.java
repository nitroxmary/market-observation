package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.Meetings;

@Repository("meetingsDAO")
public class MeetingsDAOImpl extends AbstractDAO implements MeetingsDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addMeetings(Meetings c){
		persist(c);
	}
	
	@Override
	public void updateMeetings(Meetings c){
		edit(c);
	}
	
	@Override
	public void removeMeetings(Meetings c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Meetings> list(){
		Criteria criteria = getSession().createCriteria(Meetings.class);
		return (List<Meetings>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Meetings> list(int id){
		Criteria criteria = getSession().createCriteria(Meetings.class).add(Restrictions.eq("company.id", id));
		return (List<Meetings>) criteria.list();
	}
	
	@Override
	public Meetings getMeetingsById(int id){
		Criteria criteria = getSession().createCriteria(Meetings.class).add(Restrictions.eq("id", id));
	return (Meetings) criteria.uniqueResult();
	}
}
