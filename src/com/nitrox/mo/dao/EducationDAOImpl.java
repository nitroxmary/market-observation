package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.Education;

@Repository("educationDAO")
public class EducationDAOImpl extends AbstractDAO implements EducationDAO{

//	@Autowired
//	private SessionFactory sessionFactory;
//	
	@Override
	public void addEducation(Education c){
		persist(c);
	}
	
	@Override
	public void updateEducation(Education c){
		edit(c);
	}
	
	@Override
	public void removeEducation(Education c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Education> list(int id){
		Criteria criteria = getSession().createCriteria(Education.class).add(Restrictions.eq("ind.id", id));
		return (List<Education>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Education> list(){
		Criteria criteria = getSession().createCriteria(Education.class);
		return (List<Education>) criteria.list();
	}
	
	@Override
	public Education getEducationById(int id){
		Criteria criteria = getSession().createCriteria(Education.class).add(Restrictions.eq("id", id));
	return (Education) criteria.uniqueResult();
	}
}
