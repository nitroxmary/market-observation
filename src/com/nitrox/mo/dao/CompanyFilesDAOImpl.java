package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.CompanyFiles;

@Repository("companyFilesDAO")
public class CompanyFilesDAOImpl extends AbstractDAO implements CompanyFilesDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addCompanyFiles(CompanyFiles c){
		persist(c);
	}
	
	@Override
	public void updateCompanyFiles(CompanyFiles c){
		edit(c);
	}
	
	@Override
	public void removeCompanyFiles(CompanyFiles c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyFiles> list(int id){
		Criteria criteria = getSession().createCriteria(CompanyFiles.class).add(Restrictions.eq("company.id", id));
		return (List<CompanyFiles>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyFiles> list(){
		Criteria criteria = getSession().createCriteria(CompanyFiles.class);
		return (List<CompanyFiles>) criteria.list();
	}
	
	@Override
	public CompanyFiles getCompanyFilesId(int id){
		Criteria criteria = getSession().createCriteria(CompanyFiles.class).add(Restrictions.eq("id", id));
	return (CompanyFiles) criteria.uniqueResult();
	}
}