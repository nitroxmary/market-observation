package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.Language;

@Repository("languageDAO")
public class LanguageDAOImpl extends AbstractDAO implements LanguageDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addLanguage(Language c){
		persist(c);
	}
	
	@Override
	public void updateLanguage(Language c){
		edit(c);
	}
	
	@Override
	public void removeLanguage(Language c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Language> list(int id){
		Criteria criteria = getSession().createCriteria(Language.class).add(Restrictions.eq("ind.id", id));
		return (List<Language>) criteria.list();
	}
	
	@Override
	public Language getLanguageById(int id){
		Criteria criteria = getSession().createCriteria(Language.class).add(Restrictions.eq("id", id));
		return (Language) criteria.uniqueResult();
	}
}
