package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.User;

@Repository("userDAO")
public class UserDAOImpl extends AbstractDAO  implements UserDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addUser(User c){
		persist(c);
	}
	
	@Override
	public void updateUser(User c){
		edit(c);
	}
	
	@Override
	public void removeUser(User c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> list(int id){
		Criteria criteria = getSession().createCriteria(User.class).add(Restrictions.eq("id", id));
		return (List<User>) criteria.list();
	}
	
	@Override
	public User getUserId(int id){
		Criteria criteria = getSession().createCriteria(User.class).add(Restrictions.eq("id", id));
	return (User) criteria.uniqueResult();
	}
	
	@Override
	public User getUser(String uname) {
		Criteria criteria = getSession().createCriteria(User.class).add(Restrictions.eq("uname", uname));
		return (User) criteria.uniqueResult();
	}
}
	
