package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.ConnectedCompany;

@Repository("connectedCompanyDAO")
public class ConnectedCompanyDAOImpl extends AbstractDAO  implements ConnectedCompanyDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addConnectedCompany(ConnectedCompany c){
		persist(c);
	}
	
	@Override
	public void updateConnectedCompany(ConnectedCompany c){
		edit(c);
	}
	
	@Override
	public void removeConnectedCompany(ConnectedCompany c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConnectedCompany> list(int id){
		Criteria criteria = getSession().createCriteria(ConnectedCompany.class).add(Restrictions.eq("marketSignal.id", id));
		return (List<ConnectedCompany>) criteria.list();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public String getMaxComp(){
		 Query q = getSession().createSQLQuery("SELECT COUNT(*) as count FROM connected_company GROUP BY mSignal_id ORDER BY count DESC");
		 if(q.list().size() == 0){
			 return "0";
		 }else{
			 return q.list().get(0).toString();
		 }
	}
	
	@Override
	public ConnectedCompany getConnectedCompanyById(int id){
		Criteria criteria = getSession().createCriteria(ConnectedCompany.class).add(Restrictions.eq("id", id));
	return (ConnectedCompany) criteria.uniqueResult();
	}
}
	
