package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.CompanyShareholders;

public interface CompanyShareholdersDAO {

public void addCompanyShareholders(CompanyShareholders c);
public void updateCompanyShareholders(CompanyShareholders c);
public void removeCompanyShareholders(CompanyShareholders c);

public List<CompanyShareholders> list(int id);
public List<CompanyShareholders> indList(int id);
public CompanyShareholders getCompanyShareholdersById(int id);
public List<CompanyShareholders> compList(int id);
public List<CompanyShareholders> list();
} 
