package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.MarketSignal;

@Repository("MarketSignalDAO")
public class MarketSignalDAOImpl extends AbstractDAO implements MarketSignalDAO{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addMarketSignal(MarketSignal ms) {
		persist(ms);
		
	}

	@Override
	public void updateMarketSignal(MarketSignal ms) {
		edit(ms);
	}

	@Override
	public void removeMarketSignal(MarketSignal ms) {
		delete(ms);
	}

    @SuppressWarnings("unchecked")
	@Override
	public List<MarketSignal> list() {
		Criteria criteria = getSession().createCriteria(MarketSignal.class);
		return (List<MarketSignal>) criteria.list();
	}

    @SuppressWarnings("unchecked")
   	@Override
   	public List<MarketSignal> list(int id) {
   		Criteria criteria = getSession().createCriteria(MarketSignal.class).add(Restrictions.eqOrIsNull("company.id", id));
   		return (List<MarketSignal>) criteria.list();
   	}
    
	@Override
	public MarketSignal getSignalId(int id) {
		Criteria criteria = getSession().createCriteria(MarketSignal.class).add(Restrictions.eqOrIsNull("id", id));
		return (MarketSignal) criteria.uniqueResult();
	}
	


}
