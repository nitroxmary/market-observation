package com.nitrox.mo.dao;

import java.util.List;

import com.nitrox.mo.model.CompanyPhone;

public interface CompanyPhonesDAO {

	public void addCompanyContacts(CompanyPhone c);
	public void updateCompanyContacts(CompanyPhone c);
	public void removeCompanyContacts(CompanyPhone c);
	
	public List<CompanyPhone> list();
	public List<CompanyPhone> nameList(int id);
	
	public CompanyPhone getCompanyId(int id);
	
} 
