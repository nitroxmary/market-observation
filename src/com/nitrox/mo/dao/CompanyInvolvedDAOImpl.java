package com.nitrox.mo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nitrox.mo.model.CompanyInvolved;

@Repository("companyInvlovedDAO")
public class CompanyInvolvedDAOImpl extends AbstractDAO implements CompanyInvolvedDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addCompanyInvolved(CompanyInvolved c){
		persist(c);
	}
	
	@Override
	public void updateCompanyInvolved(CompanyInvolved c){
		edit(c);
	}
	
	@Override
	public void removeCompanyInvolved(CompanyInvolved c){
		delete(c);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyInvolved> list(){
		Criteria criteria = getSession().createCriteria(CompanyInvolved.class);
		return (List<CompanyInvolved>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyInvolved> list(int id){
		Criteria criteria = getSession().createCriteria(CompanyInvolved.class).add(Restrictions.eq("company.id", id));
		return (List<CompanyInvolved>) criteria.list();
	}
	
	@Override
	public CompanyInvolved getCompanyInvolvedById(int id){
		Criteria criteria = getSession().createCriteria(CompanyInvolved.class).add(Restrictions.eq("id", id));
	return (CompanyInvolved) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyInvolved> listCompanies(int id){
		Criteria criteria = getSession().createCriteria(CompanyInvolved.class).add(Restrictions.eq("ind.id", id));
		return (List<CompanyInvolved>) criteria.list();
	}
}
