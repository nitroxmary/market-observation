<div class="container" style="width:80%">	

  <div id="signal_export"></div>
  <div id="signal_list"></div> 
  <div id="signal_details">
       <input type="hidden" value="0" id="userType"></input>
 	   <div id="signal_moreDetails"></div>
 	   <div id="signal_removal"></div>
       <div id="signal_confirmed"></div>
 	   <div id="signal_companies"></div>
 	   <div id="signal_countries"></div>
 	   <div id="signal_individual"></div>
 	   <div id="signal_action"></div>
 	   
 	 
     <script type="text/javascript">
    	var user = "${isAdmin}"
    		document.getElementById("userType").value = user;
    </script>
    
  </div> 
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"> </script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/marketSignal.js"> </script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/marketSignal_list.js"> </script> 