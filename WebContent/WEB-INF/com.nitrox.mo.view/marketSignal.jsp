<div class="container" style="width:900px">	
	<div class='form-group row'>
    		<strong>NEW SIGNAL :<br/></strong><hr/>
    </div>
    <div class="form-group row" id="mainDetails"> </div>
 	<div id="connections">
 		<div id ='company_conn'>
 			<br/>
 		    <label>Connected Companies :</label> <br/>
 			<div id ='comp_connection'></div>
 		</div>
 		<div id ='countries_conn'>
 		    <br/>
 			<label>Connected Countries :</label> 
 	    	<div id ='count_connection'></div>
 		</div>
 		<div id ='authors_conn'>
 		    <br/>
 			<label>Connected Individual:</label> 
 	       <div id ='authors_connection'></div>
 		</div>
 	</div>
 	<hr>
	<div><button onClick="NewSignal()" id="newSignalBtn" class="btn btn-primary">Add Signal</button></div>

</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"> </script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/marketSignal.js"> </script> 