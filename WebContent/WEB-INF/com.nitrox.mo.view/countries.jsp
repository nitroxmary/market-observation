<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Country Page</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
   	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/app.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/countries.js"> </script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.cookie.js"> </script> 
</head>
<body>

<br>
<div class="clearfix right-panel">
	<button onClick="newCountry()" id="newCountryBtn" class="btn contact-us-send-button">More Country</button>
</div>
<br>
<div id ="CountryDivs" class="col-lg-12 col-md-12">

	<div id="CountryAdd" class="col-lg-12 col-md-12">
		<h3>Country</h3>
		<table>
	      <tr><td>Country Name : </td><td> <input type="text" id="name"><br/></td></tr>
	      <tr><td>Country Region : </td><td> <input type="text" id="region"><br/></td></tr>
	      <tr><td>Country Code : </td><td> <input type="text" id="code"><br/></td></tr>
	      <tr><td colspan="2"><input type="button" value="Add Country" onclick="createCountry()"><br/></td></tr>
	      <tr><td colspan="2"><div id="info" style="color: green;"></div></td></tr>
		</table>
	</div>
	
	<div id="CountryList" class="col-lg-12 col-md-12">
		<h3>Country List</h3>
		<table id="mary" class="tg">
		<tr>
			<th width="80">ID</th>
			<th width="120">Name</th>
			<th width="120">Region</th>
			<th width="60">Code</th>
			<th width="60" colspan = "2">Actions</th>
		</tr>
		</table>
	</div>
</div>
	
	
	<div class="col-lg-12 col-md-12"><br><br></div>
</body>
</html>

