<div class="modal fade" id="modal" role="dialog" hidden="true" class="img-rounded">
	<div class="modal-dialog">
		<div class="modal-content">
			<div id="modalHeader" class="modal-header clearfix"></div>
			<div id="modalBody" class="modal-body"></div>
			<div id="modalFooter" class="modal-footer"></div>
		</div>
	</div>
</div>
