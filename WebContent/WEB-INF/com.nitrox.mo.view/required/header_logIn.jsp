<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>

	<meta charset="utf-8">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Market Observation </title>
	<meta name="description" content="">
	<meta name="author" content="NitroX">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script>var baseUrl = "${pageContext.request.contextPath}"</script>
	
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome.min.css">
	
   	<link rel="icon" href="${pageContext.request.contextPath}/images/logo.png">
   	
   	
   	
   	
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/app.css">
	<link rel="stylesheet"  type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/css/bootstrap.css">
  
<head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="${pageContext.request.contextPath}/">MARKET OBSERVATION</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="${pageContext.request.contextPath}/user/">Log-IN</a></li>
            <li><a href="${pageContext.request.contextPath}/user/register">Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <hr>