<div class="container" style="width:80%">	

  <div id="comp_list">
	  <div id="comp_removal"></div>
	  <div id="comp_confirmed"></div>
  </div> 
  <div id="comp_details">
 	 <input type="hidden" value="0" id="userType"></input>
 	  <div id="company_details1">
 	  	<div class='form-group row'>
		<strong>COMPANY DETAILS :<br/></strong><hr/>
	    	<div id ='company_details2'></div>
	    </div>
	    <div class='form-group row'>
	    <strong>ALM Seller Companies :<br/></strong><hr/>
	    	<div id ='alm_seller'></div>
	    </div>
	    <div class='form-group row'>
	    <strong>ALM Provider Company :<br/></strong><hr/>
	    	<div id ='alm_provider'></div>
	    </div>
	    <div class='form-group row'>
	    <strong>COMPANY FILES :<br/></strong><hr/>
	    	<div id ='company_viewFiles'></div>
	    </div>
	      <div class='form-group row'>
	    <strong>COMPANY SHAREHOLDERS :<br/></strong><hr/>
	    	<div id ='company_shreholders'></div>
	    </div>
	    <div class='form-group row'>
	    <strong>COMPANY CONTACTS :<br/></strong><hr/>
	    	<div id ='company_contact_details'></div>
	    </div>
	    <div class='form-group row'>
	    <strong>COMPANY NUMBERS :<br/></strong><hr/>
	    	<div id ='company_pnone_numbers'></div>
	    </div>
 	  </div> 
 	  <div id="comp_action"></div>
 	 <script type="text/javascript">
    	var user = "${isAdmin}";
    		document.getElementById("userType").value = user;
    </script>
  </div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"> </script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/newCompany.js"> </script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/company.js"> </script> 
