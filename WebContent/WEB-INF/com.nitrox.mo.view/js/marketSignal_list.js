/**
 * 
 */
$(document).ready(function(){
	loadSignal_list();
	//signalExport();
});

function loadSignal_list(){
	
	$.ajax({
	 	url: "/market_observation/view/signal/list",
	    type: "GET",
        success: function (result) {
    		var str = "<table class='table table-bordered' id='table_signal' >" +
    		           "<thead><tr><th>SIGNAL DATE</th>" +
    		           "<th>REPORT DATE</th>" +
    		           "<th>FACTOR</th>" +
    		           "<th>HEADING</th>" +
    		           "<th>MAIN COMPANY</th>" +
    		           "<th></th></tr></thead><tbody>";
    		
        	for (i=0;i<result.length;i++){
        		var d1 = new Date(result[i].signalDate);
        		var d2 = new Date(result[i].reportDate);
        		var m1 = d1.getMonth() + 1;
        		var m2 = d2.getMonth() + 1;
        		str += "<tr id='displayRow_"+result[i].id+"'>"+ 
	        				"<td> "+ m1 + "/" + d1.getDate() +  "/" + d1.getFullYear() +" </td>" +
	        				"<td> "+ m2 + "/" + d2.getDate() + "/" + d2.getFullYear() +" </td>"+
	        				"<td> "+result[i].factor+" </td>" +
	        				"<td> "+result[i].origTitle+" </td>"+
	        				"<td> "+result[i].company.name+" </td>"+
	    			        "<td>" +
	    			        "	<a href='javascript:viewDetails("+result[i].id+")'>View &nbsp;|&nbsp; </a>" +
	    			        "	<a href='javascript:removeDetails("+result[i].id+",\""+ result[i].factor +"\",\""+ result[i].company.name +"\")'>Delete</a></td>" +
        			    "</tr>";
        	}
        	str += "</tbody></table>"
    		$("#signal_list").append(str);
        	$("#table_signal").DataTable({
        		 "pageLength": 25
        	});
        	
        },
        error: function (result) {
        	console.log("error");
	     }
	  });
}

//function signalExport(){
//	var str = "<div>" +
//			  "		<div class='form-group row'> " +
//			  "			<label class='col-xs-2 col-form-label'>Inclusive Dates :</label>"+
//			  "		    <label class='col-xs-1 col-form-label'>From </label>"+
//			  "		    <div class='col-xs-3'>"+
//			  "				<input class='form-control' type='date' value='' id='export_from'>" +
//			  "			</div>"+
//			  "		    <label class='col-xs-1 col-form-label'>To </label>"+
//			  "			<div class='col-xs-3'>"+
//			  "				<input class='form-control' type='date' value='' id='export_to'>"+
//			  "			</div>";
//		
//	$("#signal_export").append(str);		
//	
//	var from = document.getElementById("export_from").value;
//	var to = document.getElementById("export_to").value;
//	var dates = from + "_" + to;
//	
//	var str1 = "			<div class='col-xs-2'>"+
//		      "   			<a href='" + baseUrl + "/view/SignalDatesXLS/"+document.getElementById("export_from").value+"'>" +
//		      "						<button type='button' class='btn btn-default'>Export</button>" +
//		      "					</a>"+
//			  "			</div></div>"+
//			  "		</div>" +
//			  "</div>";
//	
//	$("#signal_export").append(str1);
//}


function removeDetails(id,factor,company){
	
	$("#signal_removal").empty();

	var str = "<div class='modal fade' id='removeModal' role='dialog'>" + 
	   		"<div class='modal-dialog'>"+
	   		"<div class='modal-content'>"+
	   		"	<div class='modal-header'>"+
	   		"		<h4 class='modal-title'>Remove Market Signal?</h4>"+
	   		"	</div>"+
	   		"	<div class='modal-body'>" +
	   		"			Do you really want to remove signal at : <br/><strong> " + company+ "</strong> with factor : <strong>" + factor + "</strong>" + 
			"	</div>"+
           	"	<div class='modal-footer'>"+
        	"	 	<button onClick='removeSignal(\""+id+"\")' id='removeButton' class='btn btn-danger'>Delete Signal</button>"+
           	"		<button type='button' onClick='okButton()' class='btn btn-default' data-dismiss='modal' >Cancel</button>"+
           	"	</div>"+
           	"</div></div></div>";
	
	$("#signal_removal").append(str);
	$("#removeModal").modal();
}

function removeSignal(id){
	$("#signal_confirmed").empty();
	
	$.ajax({
	 	url:"/market_observation/marketSignal/delete",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	okButton();
	    	var str =   "<div class='modal fade' id='confirmModal' role='dialog'>" + 
				   		"<div class='modal-dialog'>"+
				   		"<div class='modal-content'>"+
				   		"	<div class='modal-header'>"+
				   		"		<h4 class='modal-title'>Deleted Market Signal</h4>"+
				   		"	</div>"+
				   		"	<div class='modal-body'>";
	   		
	    	if(result.ok){
	    	str +=		"			<strong> SUCCESSFULL </strong>"; 
			}
	    	else{
	    	str +=		"			<strong> NOT DELETED </strong>"; 
	    	}
	    	str +=		"	</div>"+
			           	"	<div class='modal-footer'>"+
			           	"	</div>"+
			           	"</div></div></div>";
			
			$("#meeting_confirmed").append(str);
			$("#confirmModal").modal();
		    
			setTimeout(reload, 1000);
	    }
	});
}

function reload() {
	   location.reload();
}

function okButton(){
	
	 $(".modal").modal("hide");
	 $(".modal-backdrop").hide();
	 $('html, body').css({
		   'overflow': 'auto',
		   'height': 'auto'
	});
}

function viewDetails(id){
	var user = document.getElementById("userType").value;
	$("#signal_details").show();
	$("#signal_list").hide();
	viewList();
	getCompanies(id,user);
	getCountries(id,user);
	getIndividual(id,user);
	getMoreDetails(id,user);
}

function getCompanies(id){
	$("#signal_companies").empty();
	$.ajax({
	 	url:"/market_observation/view/signal_companies",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("companies :");
	    	var resLength = result.length ;
	    	var str = "<div><br/> CONNECTED COMPANIES : " + resLength + "<hr/>" +
	    			"<div id='display_companies'>" + 
	    	          "<ul style='list-style-type:square'>";
	    	for (i=0;i<resLength;i++){
	    		 str += "<div class='row'>" +
	    		 		"	<div class='col-md-4'> <li>"+result[i].company.name+"</li></div>"  + 
	    		        " 	<div class='col-md-8'>Connected By : "+getData("companies",result[i].type)+"</div>" +
	    		        "</div>" ;
	    	}
	    	str += "  </ul>" +
 		           "	<div class='row'>" +
 		           "		<div class='col-md-9'></div>";
	    	if(user == "admin"){
	    	str += "		<div class='col-md-3'>" +
 		           "			<a href='#' onclick='updateDetails(3)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
 		           "		</div>";
	    	}
	    	str += "	</div>" +
 		           "</div>" +
 		           "<div class='updateCompanies'>" +
 		           "	<div class='form-group row' id='update_companies'></div>" +
 		           "    <div class='form-group row' id='cancelButton3'></div>" +
 		           "</div>";
            console.log(result);
            
            $("#signal_companies").append(str);
            $(".updateCompanies").hide();
            
            if(resLength != 0){
            	for (i=0;i<resLength;i++){
            		loadIndCompanies(i,result[i].company.id,resLength,result[i].type + "_" + result[i].id);
                }
            }else{
            	loadIndCompanies(0,0,resLength,1 + "_" + 0);
            }
            
            var cancel = "<div class='row'>" +
			"	<div class='col-md-12'>" +
			"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(3)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
			"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateCompanies("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
			"	</div>" +
			"</div>";
       
        $("#cancelButton3").append(cancel); 
            
        },
        error: function (result) {
        	console.log("failed");
        }
    });
}

function getCountries(id){
	$("#signal_countries").empty();
	$.ajax({
	 	url:"/market_observation/view/signal_countries",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("countries :");
	    	
	    	var resLength = result.length ;
	    	var str = "<div><br/> CONNECTED COUNTRIES : " + resLength + "<hr/>" + 
	    			"<div id='display_countries'>" + 
	    	          "<ul style='list-style-type:square'>";
	    	for (i=0;i<resLength;i++){
	    		console.log(result[i]);
	    		 str += "<li>"+result[i].country.name+"</li>" ;
	    	}
	    	str += "</ul>" +
	    		"	<div class='row'>" +
	    		"		<div class='col-md-9'></div>" ;
	    	if(user == "admin"){
	    	str +="		<div class='col-md-3'>" +
	            "			<a href='#' onclick='updateDetails(2)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
	            "		</div>";
	    	}
	    	str +="	</div>" +
	    		"</div>" +
	    		"<div class='updateCountries'>" +
	            "	<div class='form-group row' id='update_countries'></div>" +
	            "    <div class='form-group row' id='cancelButton2'></div>" +
	            "</div>";
                        
            $("#signal_countries").append(str);
            $(".updateCountries").hide();
            
            if(resLength != 0){
            	for (i=0;i<resLength;i++){
            		loadIndCountries(i,result[i].country.id,resLength + "_" + result[i].id);
            	}
            }else{
            	loadIndCountries(0,0,resLength + "_"+ 0);
            }
            
            var cancel = "<div class='row'>" +
			"	<div class='col-md-12'>" +
			"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(2)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
			"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateCountries("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
			"	</div>" +
			"</div>";
           
            $("#cancelButton2").append(cancel);
        },
        error: function (result) {
        	console.log("failed");
        }
    });
}



function getIndividual(id){
	$("#signal_individual").empty();
	$.ajax({
	 	url:"/market_observation/view/signal_individual",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("individual :");
	    	var resLength = result.length ;
	    	
	    	var str = "<div><br/> CONNECTED INDIVIDUAL : " + resLength + "<hr/>" +
	    			"<div id='display_individual'>" + 
	    	          "<ul style='list-style-type:square'>";
	    	for (i=0;i<resLength;i++){
	    		 str += " <div class='row'>" +
	    		 		"     <div class='col-md-4'><li>"+result[i].ind.firstName + " " + result[i].ind.middleInitial + " " + result[i].ind.lastName +"</li></div>"  + 
	    		        "     <div class='col-md-8'>Connected By : "+getData("individual",result[i].type)+"</div>" +
	    		        " </div>" ;
	    	}
	    	str += "  </ul>" +
	    		   "	<div class='row'>" +
	    		   "		<div class='col-md-9'></div>";
	    	if(user == "admin"){
	  		str += "		<div class='col-md-3'>" +
		           "			<a href='#' onclick='updateDetails(1)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
		           "		</div>";
	    	}
	    	str += "	</div>" +
		           "</div>" +
	               "<div class='updateIndividual'>" +
	               "	<div class='form-group row' id='update_individual'></div>" +
	               "    <div class='form-group row' id='cancelButton1'></div>" +
	               "</div>";
	    	            
            $("#signal_individual").append(str);
            $(".updateIndividual").hide();
           
            if(resLength != 0){
            	for (i=0;i<resLength;i++){
                	loadIndAuthors(i,result[i].ind.id,resLength,result[i].type + "_" + result[i].id);
                 }
            }else{
            	loadIndAuthors(0,0,resLength,1 + "_" + 0);
            }
        	            
            var cancel = "<div class='row'>" +
			"	<div class='col-md-12'>" +
			"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(1)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
			"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateIndividual("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
			"	</div>" +
			"</div>";
       
        $("#cancelButton1").append(cancel); 
            
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
    });
}

function loadIndCompanies(companyAdd,companyValue,lastCompanyCount,connectionType){
	var buttonID = companyAdd+1;
	getCompanyList("update_companies","comp_dropdownCompany_"+ companyAdd,"CompConnectionsText_" + companyAdd,buttonID, companyValue,lastCompanyCount,connectionType);
}

function loadIndCountries(countryAdd,countryValue,lastCountryCount){
	var buttonID = countryAdd+1;
	getCountryList("update_countries","count_dropdownCountry_"+ countryAdd,buttonID,countryValue,lastCountryCount);
}

function loadIndAuthors(authorAdd,authorValue,lastAuthorCount,connectionType){
	var buttonID = authorAdd+1;
	getIndividualList("update_individual","authors_dropdownAuthors_"+ authorAdd,"IndConnectionsText_" + authorAdd, buttonID,authorValue,lastAuthorCount,connectionType);
	
}

function getMoreDetails(id){
	$("#signal_moreDetails").empty();
	$.ajax({
	 	url:"/market_observation/view/signal_Details",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("details :");
	    	var d1 = new Date(result.signalDate);
    		var d2 = new Date(result.reportDate);
    		var m1 = d1.getMonth() + 1;
    		var m2 = d2.getMonth() + 1;
	    	var str =  "<strong> MARKET SIGNAL DETAILS :</strong><br/><hr/>" +
	    			   "<div id='display_details'> " + 
	    		       "<div class='row'><div class='col-md-4'>FACTOR : "+result.factor+"</div>" + 
                       "<div class='col-md-4'>SIGNAL DATE : "+ m1 + "/" + d1.getDate() +  "/" + d1.getFullYear() +"</div>" +
                       "<div class='col-md-4'>REPORT DATE : "+ m2 + "/" + d2.getDate() +  "/" + d2.getFullYear() +"</div></div>" + 
                       "<div class='row'><div class='col-md-9'>LINK: "+result.link+"</div></div>"+
	    			   "<div class='row'><div class='col-md-9'>ORIGINAL HEADING : "+result.origTitle+"</div></div>"+
	    	           "<div class='row'><div class='col-md-9'>TRANSLATED HEADING : "+result.transTitle+"</div></div>"+
	    	           "<div class='row'><div class='col-md-9'>MAIN COMPANY : "+result.company.name+"</div></div>"+
	    	           "<div class='row'><br/><label class='col-xs-3 col-form-label'>SUMMARY : </label></div>"+
	    	           "<div class='row'><div class='col-md-9'>"+result.overview+"</div></div>"+
	    	           "<div class='row'><br/><label class='col-xs-3 col-form-label'>BACKGROUND :  </label></div>"+
	    	           "<div class='row'><div class='col-md-9'>"+result.background+"</div></div>"+
	    	           "<div class='row'><br/><label class='col-xs-3 col-form-label'>IMPLICATION :  </label></div>"+
	    	           "<div class='row'><div class='col-md-9'>"+result.implication+"</div></div><br/>" +
	    	           "<div class='row'>" +
		    		   "	<div class='col-md-9'></div>";
	    	if(user == "admin"){
	    	str +=     "	<div class='col-md-3'>" +
			           "		<a href='#' onclick='updateDetails(4)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
			           "	</div>";
			}
			str +=     " </div>" +
			           "</div>" +
		               "<div class='updateDetails'>" +
		               "	<div class='form-group row' id='update_details'></div>" +
		               "    <div class='form-group row' id='cancelButton4'></div>" +
		               "</div>";
            
            $("#signal_moreDetails").append(str);
            $(".updateDetails").hide();
            loadMainDetails("update_details");
            
            var signalDay = ("0" + d1.getDate()).slice(-2);
            var reportDay = ("0" + d2.getDate()).slice(-2);
            var signalMonth = ("0" + (m1)).slice(-2);
            var reportMonth = ("0" + (m2)).slice(-2);
            
            var signal = d1.getFullYear()+"-"+(signalMonth)+"-"+(signalDay) ;
            var report = d2.getFullYear()+"-"+(reportMonth)+"-"+(reportDay) ;
            
            $('#signal_date').val(signal);
            $("#report_date").val(report);
            
            document.getElementById("factorName").value = result.factor;
            document.getElementById("signal_link").value = result.link;
            document.getElementById("signal_orig").value = result.origTitle;
            document.getElementById("signal_trans").value = result.transTitle;
            document.getElementById("signal_summary").value = result.overview;
            document.getElementById("signal_background").value = result.background;
            document.getElementById("signal_implication").value = result.implication;
            getCompanyList("signal_company","dropdownCompany", 0,0,result.company.id,0,1);
            console.log(result);
            var cancel = "" +
            "<input type='hidden' class='input_by' id='inputted_by' value='"+result.inputBy+"'>" +
            "<input type='hidden' class='inputted_date' id='inputted_date' value='"+result.inputDate+"'>" +
            "<input type='hidden' class='updated_date' id='updated_date' value='"+result.updated_date+"'>" +
            "<input type='hidden' class='updated_by' id='updated_by' value='"+result.updated_by+"'>" +
            "<input type='hidden' class='signal_id' id='signal_id' value='"+result.id+"'>" +
            "<div class='row'>" +
			"	<div class='col-md-12'>" +
			"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(4)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
			"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateMainDetails()'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
			"	</div>" +
			"</div>";
       
           $("#cancelButton4").append(cancel); 
           
	        },
	     error: function (result) {
	        console.log("failed");
	     }
    });
}

function getData(data,type){
	
   if(data != null){
	   if(data == "companies"){
		   if(type == 1){
			   return "market signal "
		   }else{
			   return "holding";
		   }
		}else if(data == "individual"){
		   if(type == 1){
				return "signal author "
			}else{
				return "signal content";
			}
	   }
	}else{
		return "";
	}
}

function updateDetails(type){
	
	if(type == 1){
		$("#display_individual").hide();
		$(".updateIndividual").show();
	}else if(type == 2){
		$("#display_countries").hide();
		$(".updateCountries").show();
	}else if(type == 3){
		$("#display_companies").hide();
		$(".updateCompanies").show();
	}else if(type == 4){
		$("#display_details").hide();
		$(".updateDetails").show();
	}
	
}

function cancelUpdate(type){
	
	if(type == 1){
		$("#display_individual").show();
		$(".updateIndividual").hide();
	}else if(type == 2){
		$("#display_countries").show();
		$(".updateCountries").hide();
	}else if(type == 3){
		$("#display_companies").show();
		$(".updateCompanies").hide();
	}else if(type == 4){
		$("#display_details").show();
		$(".updateDetails").hide();
	}
}


function viewList(){
	
	$("#signal_action").empty();
	var str = "<div class='row'><input type='button' value='Back to List' onclick='getList()'></div>";
	
	  $("#signal_action").append(str);
}

function getList(){
	location.reload();
	$("#signal_details").hide();
	$("#signal_list").show();
}

function updateCountries(signalID){
	 $.ajax({
		 	url:"/market_observation/marketSignal/updateCountries",
		 	data: "countries="+getConnectionCountries()+
                 "&id="+parseInt(signalID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(signalID);
		        	 }
		           else{
	        		  console.log("error in updating db");
		        	}
		    	},
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}

function updateIndividual(signalID){
	console.log("id " + signalID);
	 $.ajax({
		    url:"/market_observation/marketSignal/updateIndividual",
		    data: "individual="+getConnectionAuthors()+
	              "&id="+parseInt(signalID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(signalID);
		        	 }
		           else{
	        		  console.log("error in updating db");
		        	}
		    	console.log(result);
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}

function updateCompanies(signalID){
	 $.ajax({
		    url:"/market_observation/marketSignal/updateCompanies",
		    data: "company="+getConnectionCompanies()+
	              "&id="+parseInt(signalID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(signalID);
		        	 }
		           else{
	        		  console.log("error in updating db");
		        	}
		    	console.log(result);
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}

function updateMainDetails(){
	var indID = document.getElementById("signal_id").value;
	 $.ajax({
		 	url:"/market_observation/marketSignal/updateDetails1",
		    data: "id=" + indID+
		    	  "&signalDate=" + document.getElementById("signal_date").value+
		    	  "&factor="+document.getElementById("factorName").value+
		    	  "&reportDate=" +document.getElementById("report_date").value+
		    	  "&link="+document.getElementById("signal_link").value+
		    	  "&origTitle="+document.getElementById("signal_orig").value+
		    	  "&transTitle="+document.getElementById("signal_trans").value+
		    	  "&company="+document.getElementById("dropdownCompany").value+
		    	  "&overview="+document.getElementById("signal_summary").value+
		    	  "&background="+document.getElementById("signal_background").value+
		    	  "&implication="+document.getElementById("signal_implication").value+
		    	  "&inputDate="+document.getElementById("inputted_date").value+
		    	  "&inputBy="+document.getElementById("inputted_by").value,
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(indID);
		        	 }
		           else{
	        		  console.log("error in updating db");
		        	}
		    	console.log(result);
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}