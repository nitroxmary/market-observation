/**
 * 
 */

$(document).ready(function(){
	loadInd_list();
	exportFunctions();
});


function loadInd_list(){
	
	$.ajax({
	 	url: "/market_observation/view/individual/list",
	    type: "GET",
        success: function (result) {
    		var str = "<table class='table table-bordered' id='table_ind'>" +
    		           "<thead><tr><th>LAST NAME</th>" +
    		           "<th>FIRST NAME</th>" +
    		           "<th>MI</th>" +
    		           "<th>LOCATION</th>" +
    		           "<th></th></tr></thead><tbody>";
    		for (i=0;i<result.length;i++){
        		var name = result[i].firstName + " " + result[i].middleInitial + ". " + result[i].lastName;
        		str += "<tr id='displayRow_"+result[i].id+"'>"+ 
	        				"<td> "+result[i].lastName+" </td>" +
	        				"<td> "+result[i].firstName+" </td>" +
	        				"<td> "+result[i].middleInitial+" </td>"+
	        				"<td> "+result[i].country.name+" </td>"+
	    			        "<td>" +
	    			        "	<a href='javascript:viewDetails("+result[i].id+")'>View </a> &nbsp;|&nbsp; " +
	    			        "	<a href='javascript:removeDetails("+result[i].id+",\""+ name +"\",\""+ result[i].country.name +"\")'>Delete</a> &nbsp;|&nbsp; " +
	    			        "   <a href='" + baseUrl + "/view/ConnIndXLS/"+result[i].id+"'>Export</a>" +
	    			        "</td>" +
    			        "</tr>";
        	}
        	str += "</tbody></table>"
    		$("#ind_list").append(str);
        	$("#table_ind").DataTable({
        		 "pageLength": 25
        	});
        	
        },
        error: function (result) {
        	console.log("error");
	     }
	  });
	
}

function exportFunctions(){
	
	var str = "<div>" +
//				"<div class='row'>" +
//				"	<div class='col-xs-2'>" +
//				"		<a href='" + baseUrl + "/view/IndByCountryXLS/'><input type='button' value='Export Individual By Country'></a>" +
//				"	</div>" +
//				"	<div class='col-xs-2'>" +
//				"		<a href='" + baseUrl + "/view/IndByCompanyXLS/'><input type='button' value='Export Individual By Company'></a>" +
//				"	</div><div class='col-xs-4'></div>" +
//				"</div>";
			  "</div>";
	
	$("#ind_exports").append(str);
}

function removeDetails(id,name,country){
	
	$("#ind_removal").empty();

	var d = new Date(date);
	var m = d.getMonth() + 1;
	var date = m + "/" + d.getDate() +  "/" + d.getFullYear();
	var str = "<div class='modal fade' id='removeModal' role='dialog'>" + 
	   		"<div class='modal-dialog'>"+
	   		"<div class='modal-content'>"+
	   		"	<div class='modal-header'>"+
	   		"		<h4 class='modal-title'>Remove Individual?</h4>"+
	   		"	</div>"+
	   		"	<div class='modal-body'>" +
	   		"			Do you really want to remove individual : <br/> <strong>" + name+ "</strong> of <strong>" + country + "</strong>" + 
			"	</div>"+
           	"	<div class='modal-footer'>"+
        	"	 	<button onClick='removeIndividual(\""+id+"\")' id='removeButton' class='btn btn-danger'>Delete Individual</button>"+
           	"		<button type='button' onClick='okButton()' class='btn btn-default' data-dismiss='modal' >Cancel</button>"+
           	"	</div>"+
           	"</div></div></div>";
	
	$("#ind_removal").append(str);
	$("#removeModal").modal();
}

function reload() {
	location.reload();
}

function okButton(){
	
	 $(".modal").modal("hide");
	 $(".modal-backdrop").hide();
	 $('html, body').css({
		   'overflow': 'auto',
		   'height': 'auto'
	});
}
	
function exportDetails(id){
		
	$.ajax({
	 	url:"/market_observation/view/MeetingXLS",
	    type: "GET",
	    success: function (result) {
	    	console.log(" " + result);
	    }
	});
}

function removeIndividual(id){
	$("#ind_confirmed").empty();
	
	$.ajax({
	 	url:"/market_observation/individual/delete",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	okButton();
	    	var str =   "<div class='modal fade' id='confirmModal' role='dialog'>" + 
				   		"<div class='modal-dialog'>"+
				   		"<div class='modal-content'>"+
				   		"	<div class='modal-header'>"+
				   		"		<h4 class='modal-title'>Deleted Meeting</h4>"+
				   		"	</div>"+
				   		"	<div class='modal-body'>";
	   		
	    	if(result.ok){
	    	str +=		"			<strong> SUCCESSFULL </strong>"; 
			}
	    	else{
	    	str +=		"			<strong> NOT DELETED </strong>"; 
	    	}
	    	str +=		"	</div>"+
			           	"	<div class='modal-footer'>"+
			           	"	</div>"+
			           	"</div></div></div>";
			
			$("#ind_confirmed").append(str);
			$("#confirmModal").modal();
		    
			setTimeout(reload, 1000);
	    }
	});
}

function viewDetails(id){
	var user = document.getElementById("userType").value;
	$("#ind_details").show();
	$("#ind_list").hide();
	viewList();
	getLanguages(id,user);
	getNationalities(id,user);
	getAwards(id,user);
	getCompanies(id,user);
	getPhones(id,user);
	getEducation(id,user);
	getContacts(id,user);
}

function getLanguages(id,user){
	$("#ind_lang").empty();
	$.ajax({
	 	url:"/market_observation/view/ind_languages",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("languages :");
	    	var resLength = result.length ;
	    	var str = "<div><br/> LANGUAGES : " + result.length + "<hr/>"+
					  "<div id='display_lang'>"
		  	          "<ul style='list-style-type:square'>";
					    	for (i=0;i<resLength;i++){
					    	    str += "<div class='row'>" +
					    	    	   		"<div class='col-md-4'> <li>"+result[i].name+"</li></div>" +
					    	    	   		"<div class='col-md-8'></div>" +
						               "</div>";
					    	}
		  	str +=     "</ul>"+
			           "<div class='row'><div class='col-md-9'></div>";
		  	if(user == "admin"){
		  	str +=	   "<div class='col-md-3'>" +
		  			   "	<a href='#' onclick='updateDetails(6)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
		               "</div>";
		  	}
		  	str +=	  "</div></div></div>" +
			           "<div class='updateLang'><div class='form-group row' id='update_lang'></div><div class='form-group row' id='cancelButton6'></div></div>";
		 
		  $("#ind_lang").append(str);
		  $(".updateLang").hide();
		  
		  if(resLength != 0){
			  for (i=0;i<resLength;i++){
			  	var count = (i+1);
			  	
			  	loadLangList("update_lang",count,resLength);
			  	document.getElementById("lang_name_" + count).value = result[i].name;
			  	document.getElementById("lang_id_" + count).value = result[i].id;
			  	
			  }
		  }else{
			  loadLangList("update_lang",1,resLength);
		  }
		  
		  var cancel = "<div class='row'>" +
		  				"	<div class='col-md-12'>" +
		  				"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(6)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
		  				"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateLang("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
		  				"	</div>" +
		  				"</div>";
		  $("#cancelButton6").append(cancel); 
         
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
    });
}

function getNationalities(id,user){
	$("#ind_nation").empty();
	$.ajax({
	 	url:"/market_observation/view/ind_nationalities",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("nationalities :");
	    	var resLength = result.length ;
	    	var str = "<div><br/> NATIONALITES : " + resLength + "<hr/>" +
					  "<div id='display_nation'>"
			          "<ul style='list-style-type:square'>";
				    	for (i=0;i<resLength;i++){
				    	    str += "<div class='row'>" +
				    	    	   		"<div class='col-md-4'> <li>"+result[i].country.name+"</li></div>" +
				    	    	   		"<div class='col-md-8'>&nbsp;</div>" +
					               "</div>";
				    	}
			str +=     "</ul>"+
					   "<div class='row'><div class='col-md-9'></div>";
		  	if(user == "admin"){
		  	str +=	   "<div class='col-md-3'>" +
			           "	<a href='#' onclick='updateDetails(5)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
			           "</div>";
		  	}
		  	str +=	   "</div></div></div>" +
		  			   "<div class='updateNation'><div class='form-group row' id='update_nation'></div><div class='form-group row' id='cancelButton5'></div></div>";

    	    $("#ind_nation").append(str);
            $(".updateNation").hide();
            
            if(resLength != 0){
	            for (i=0;i<resLength;i++){
	            	var count = (i+1);
	            	loadNationList("update_nation",count,result[i].country.id,resLength);
	            	document.getElementById("nation_id_" + count).value = result[i].id;
	            }
            }else{
            	loadNationList("update_nation",1,0,resLength);
            }
            
            var cancel = "<div class='row'>" +
				"	<div class='col-md-12'>" +
				"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(5)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
				"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateNation("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
				"	</div>" +
				"</div>";
            
            $("#cancelButton5").append(cancel); 
            
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
    });
}

function getAwards(id,user){
	$("#ind_awards").empty();
	$.ajax({
	 	url:"/market_observation/view/ind_awards",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("awards :");
	        var resLength = result.length ;
	    	var str = "<div><br/> AWARDS : " + resLength + "<hr/>"+
	    			  "<div id='display_awards'>"
		    	          "<ul style='list-style-type:square'>";
					    	for (i=0;i<resLength;i++){
					    	    str += "<div class='row'>" +
					    	    	   		"<div class='col-md-4'> <li>"+result[i].name+"</li></div>" +
					    	    	   		"<div class='col-md-8'> At : "+result[i].location+"</div>" +
						               "</div>";
					    	}
		    	str +=     "</ul>"+
				    	   "<div class='row'><div class='col-md-9'></div>";
			  	if(user == "admin"){
			  	str +=	   "<div class='col-md-3'>" +
				           "	<a href='#' onclick='updateDetails(4)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
				           "</div>";
			  	}
			  	str +=	   "</div></div></div>" +
	    	               "<div class='updateAwards'><div class='form-group row' id='update_awards'></div><div class='form-group row' id='cancelButton4'></div></div>";
           
            $("#ind_awards").append(str);
            $(".updateAwards").hide();
            
            if(resLength != 0){
            	for (i=0;i<resLength;i++){
                	var count = (i+1);
                	
                	loadAwardsList("update_awards",count,resLength);
                	document.getElementById("awards_id_" + count).value = result[i].id;
                	document.getElementById("awards_name_" + count).value = result[i].name;
                	document.getElementById("awards_loc_" + count).value = result[i].location;
                }
            }else{
            	loadAwardsList("update_awards",1,resLength);
            }
            
            var cancel = "<div class='row'>" +
				"	<div class='col-md-12'>" +
				"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(4)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
				"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateAwards("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
				"	</div>" +
				"</div>";
           
            $("#cancelButton4").append(cancel); 
	        
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
    });
}


function getCompanies(id,user){
	$("#ind_comp").empty();
	$.ajax({
	 	url:"/market_observation/view/ind_companies",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	
	    	var str = "<div><br/> COMPANY INVOLVEMENTS : " + result.length + "<hr/>";
	    	var resLength = result.length ;
	    	str += "<div id='display_involvement'>";
        	for (i=0;i<resLength;i++){			
        		
        		str += "<div class='row'><div class='col-md-6'>Company : "+getContactsData(result[i].company)+"<br/></div>"+
                       "<div class='col-md-6'>Holding Company : "+getContactsData(result[i].grpCompany)+"<br/></div></div>"+
                       "<div class='row'><div class='col-md-6'>Title : "+result[i].title+"</div>"+
                       "<div class='col-md-6'>Active : "+isActiveInd(result[i].active)+"</div></div>"+
                       "<div class='row'><div class='col-md-6' >Location [Country] : "+getContactsData(result[i].base)+"</div>"+
                       "<div class='col-md-6'>Location [City] : "+result[i].companyCity+"</div></div>"+
                       "<div class='row'><div class='col-md-6'>Location [Headquarter] : "+getContactsData(result[i].headquarter)+"</div></div><br/>";
            }
        	str +=  "<div class='row'><div class='col-md-9'></div>";
			if(user == "admin"){
			str +=	"<div class='col-md-3'>" +
		            "	<a href='#' onclick='updateDetails(2)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
		            "</div>" ;
			}
			str +=	"</div></div>" ;
        	str += "<div class='updateInvolvements'><div class='form-group row' id='update_involvement'></div><div class='form-group row' id='cancelButton2'></div></div></div></div>";
        	
	        $("#ind_comp").append(str);
	        $(".updateInvolvements").hide();
	        
	        if(resLength != 0){
		        for (i=0;i<resLength;i++){	
	        		var count = (i + 1);
	        		var hdQrtr = 0;
	        		var company = 0;
	        		var base = 0;
	        		var holding = 0;
	        		var isActive = 1;
	        		
	        		if(result[i].active == false){
	        			isActive = 0;
	        		}
	        		if(result[i].headquarter != null){ hdQrtr = result[i].headquarter.id;}
	        		if(result[i].base != null){ base = result[i].base.id;}
	        		if(result[i].company != null){ company = result[i].company.id;}
	        		if(result[i].grpCompany != null){ holding = result[i].grpCompany.id;}
	        		
	        		loadInvolvement("update_involvement",count, base, hdQrtr, company, holding, resLength);
	        		document.getElementById("comp_title_" + count).value = result[i].title;
	        		document.getElementById("comp_active_" + count).value = isActive;
	        		document.getElementById("comp_bCity_" + count).value = result[i].companyCity;
	        		document.getElementById("comp_id_" + count).value = result[i].id;
	        	}
	        }else{
	        	loadInvolvement("update_involvement",1,0,0,0,0,0);
	        }
	        
	        var cancel = "<div class='row'>" +
				"	<div class='col-md-12'>" +
				"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(2)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
				"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateCompany("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
				"	</div>" +
				"</div>";
	       
	        $("#cancelButton2").append(cancel);  
	        
	    },
        error: function (result) {
        	console.log("failed");
        }
    });
}

function getPhones(id,user){
	$("#ind_phones").empty();
	$.ajax({
	 	url:"/market_observation/view/ind_phones",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("contact :");
	    	console.log(result);
	    	var resLength = result.length;
	    	var str ="<div><br/> CONTACT NUMBERS : " + resLength + "<hr/>";
	    	    str += "<div id='display_Phones'>";
	    	for (i=0;i<resLength;i++){
	    		str += " <div class='form-group'>" +
	    			   " <div class='row'>" +
	    			   "	<div class='col-md-6'>Phone Type: "+result[i].phone.phoneType+"</div>" +
	    		       "	<div class='col-md-6'>Country Code: "+result[i].phone.country.code+"</div>" +
	    		       " </div>" +
	    		       " <div class='row'>" +
	    		       "	<div class='col-md-6'>Area Code: "+result[i].phone.areaCode+"</div>" +
	    		       "	<div class='col-md-6'>Phone Number: "+result[i].phone.number+"</div>" +
	    		       " </div></div>" ;
	    	}
	    	    str += " <div class='row'>" +
	    		   	   "	<div class='col-md-9'></div>";
			if(user == "admin"){
			  str +=   "	<div class='col-md-3'>" +
			           "		<a href='#' onclick='updateDetails(8)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
			           "	</div>";
			}
			  str +=   " </div>" +
			           "</div>";
	    	str += "<div class='updatePhones'><div class='form-group row' id='update_phones'></div><div class='form-group row' id='cancelButton8'></div></div></div></div>";
        	
	       
	        $("#ind_phones").append(str);
	        $(".updatePhones").hide();
	        
	        if(resLength != 0){
	        	console.log( "----" +resLength);
		        for (i=0;i<resLength;i++){	
		        	var count = (i);
		        	console.log("update_phones - "+ count+"-"+result[i].phone.country.id+"-"+resLength-1);
		        	loadContactNumbers("update_phones",count,result[i].phone.country.id,resLength - 1);
		        	document.getElementById("ind_phone_id_" + count).value = result[i].phone.id;
		        	document.getElementById("ind_phone_num_" + count).value = result[i].phone.number;
	        		document.getElementById("ind_phone_area_" + count).value = result[i].phone.areaCode;
	        		document.getElementById("ind_phone_type_" + count).value = result[i].phone.phoneType;
	        	}
	        }else{
	        	loadContactNumbers("update_phones",0,0,resLength);
	        }
	        
	        var cancel = "<div class='row'>" +
			"	<div class='col-md-12'>" +
			"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(8)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
			"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateNumbers("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
			"	</div>" +
			"</div>";
       
            $("#cancelButton8").append(cancel); 
        
	     },
	     error: function (result) {
	        console.log("failed");
	     }
    });
}

function getContacts(id,user){
	$("#ind_contact").empty();
	$("#ind_names").empty();
	$.ajax({
	 	url:"/market_observation/view/ind_contacts",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("contacts :");
	    	
	    	var str ="<div><br/> CONTACT DETAILS : <hr/>";
	    	var names = "<strong> INDIVIDUAL DETAILS :</strong><br/><hr/>" ;
	    	var contactID = 0;
	    	
	    	   names += "<div id='display_Details_1'><div class='row'><div class='col-md-4'>FIRSTNAME : "+result.firstName+"</div>" + 
	    	            "<div class='col-md-4'>MI : "+result.middleInitial+"</div>" +
	    	            "<div class='col-md-4'>LASTNAME : "+result.lastName+"</div></div>" + 
	    	            "<div class='row'><div class='col-md-9'>LOCATION : "+result.country.name+"</div></div>" +
	    	            "<div class='row'><div class='col-md-9'></div>" ;
	    	   if(user == "admin"){
	    	   names += "<div class='col-md-3'>" +
			            "	<a href='#' onclick='updateDetails(1)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
			            "</div>"; 
	    	   }
	    	   names += "</div></div>" +
			            "<div id='update_Details_1'>" +
	    	            "     <div class='form-group row'> "+                  
	    	            "        <label class='col-xs-3 col-form-label'>First Name</label>"+
	    	            "        <div class='col-xs-8'>"+
	    	            "             <input class='form-control' type='text' value='"+result.firstName+"' id='first_name'>"+
	    	            "        </div>"+
	    	            "     </div>"+
	    	            "     <div class='form-group row'>"+
	    	            "        <label class='col-xs-3 col-form-label'>Last Name</label>"+
	    	            "        <div class='col-xs-8'>"+
	    	            "             <input class='form-control' type='text' value='"+result.lastName+"' id='last_name'>"+
	    	            "        </div>"+
	    	            "     </div>"+
	    	            "     <div class='form-group row'> "+                  
	    	            "         <label class='col-xs-3 col-form-label'>Middle Initial</label>"+
	    	            "         <div class='col-xs-8'>"+
	    	            "               <input class='form-control' maxlength='1' value='"+result.middleInitial+"' type='text' id='middle_initial'>"+
	    	            "     </div>"+
	    	            "     </div>"+
	    	            "     <div class='form-group row'>"+
	    	            "          <label class='col-xs-3 col-form-label'>Country/Location</label>"+
	    	            "          <div class='col-xs-8'>"+
	    	            "              <div id='individualLocation'></div>"+
	    	            "         </div>"+
	    	            "     </div>" +
	    	            "<div class='row'>" +
	    	            "	<div class='col-md-12'>" +
	    	            "		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(1)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
	    	            "		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateDetails_1("+result.id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
	    	            "	</div>" +
	    	            "</div>";
	    	    var cancel = "<div class='row'><div class='col-md-12 </div>";
		        
	    	    $(".cancelButton2").append(cancel);
		        if(result.contact != null){
		        	contactID = result.contact.id;
		        }
		        
	    		str += "<div id='display_Details_2'><div class='row'><div class='col-md-9'>Contact Address 1 : "+getContactsData(result.contact,1)+"</div></div>"+
	    		            "<div class='row'><div class='col-md-9'>Contact Address 2 : "+getContactsData(result.contact,2)+"</div></div>" +
	    		            "<div class='row'><div class='col-md-9'>Contact Address 3 : "+getContactsData(result.contact,3)+"</div></div>" +
	    		            "<div class='row'><div class='col-md-6'>Email Address  : "+getContactsData(result.contact,4)+"</div>" +
	    		            "<div class='col-md-6'>LinkedIn : "+getContactsData(result.contact,5)+"</div></div>" +
	    		            "<div class='row'><div class='col-md-6'>Skype ID : "+getContactsData(result.contact,6)+"</div>" +
	    		            "<div class='col-md-6'>Youtube : "+getContactsData(result.contact,8)+"</div></div>" +
	    		            "<div class='row'><div class='col-md-6'>Twitter Account : "+getContactsData(result.contact,7)+"</div></div>" + 
	    		            "<div class='row'><div class='col-md-9'></div>";
	    		 if(user == "admin"){
	    		 str +=     "<div class='col-md-3'>" +
			                "	<a href='#' onclick='updateDetails(7)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
			                "</div>";
	    		 }
	    		 str +=     "</div>" +
			                "</div><div id='update_Details_2'>" +
			                "     <div id='individualContactList'></div><br/>" +
			                "	  <div class='row'>" +
		    	            "		  <div class='col-md-12'>" +
		    	            "			  <div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(7)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
		    	            "			  <div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateDetails_2("+contactID+","+result.id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
		    	            "		</div>" +
		    	            "</div>";
			                "</div>";
	    		str += "</div>";
	    	
	    	getCountryListInd("individualLocation","personBase","form-control",result.country.id);
	    	
	        $("#ind_names").append(names);
	        $("#ind_contact").append(str);
	        $("#update_Details_1").hide();
	        $("#update_Details_2").hide();
	        
	        loadContactDetails("individualContactList");
	        document.getElementById(("ind_addr1")).value = getContactsData(result.contact,1); 
	        document.getElementById(("ind_addr2")).value = getContactsData(result.contact,2);
	        document.getElementById(("ind_addr3")).value = getContactsData(result.contact,3);
	        document.getElementById(("ind_email")).value = getContactsData(result.contact,4);
	        document.getElementById(("ind_linkedIn")).value = getContactsData(result.contact,5);
	        document.getElementById(("ind_skype")).value = getContactsData(result.contact,6);
	        document.getElementById(("ind_twitter")).value =getContactsData(result.contact,7);
	       
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
    });
}

function getEducation(id,user){
	$("#ind_educ").empty();
	$.ajax({
	 	url:"/market_observation/view/ind_education",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("education :");
	    	var resLength = result.length ;
	    	var str ="<div><br/> EDUCATIONAL BACKGROUND : " + result.length + "<hr/>"+
	    	         "<div id='display_background'>";
	    	
	    	for (i=0;i<resLength;i++){
	    	
	    		str +=  "<div class='row'><div class='col-md-4'>Discipline : "+result[i].discipline+"</div>" +
	 	       			"<div class='col-md-4'>Degree : "+result[i].degree+"</div>" +
	 	                "<div class='col-md-4'>University : "+result[i].university+"</div></div>";
	    		       
	    	}
	    	str +=  "<div class='row'><div class='col-md-9'></div>";
   		 if(user == "admin"){
    	 str +=     "<div class='col-md-3'>" +
		            "	<a href='#' onclick='updateDetails(3)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
		            "</div>";
   		 }
   		 str +=		"</div></div>" ;
	     str += "<div class='updateEducation'><div class='form-group row' id='update_education'></div><div class='form-group row' id='cancelButton3'></div></div></div></div>";
        	
	        $("#ind_educ").append(str);
	        $(".updateEducation").hide();
	        
	        if(resLength != 0){
		        for (i=0;i<resLength;i++){	
	        		var count = (i + 1);
	        		
	        		loadEducation("update_education",count,resLength);
	        		document.getElementById("educ_disc_" + count).value = result[i].discipline;
	        		document.getElementById("educ_degree_" + count).value = result[i].degree;
	        		document.getElementById("educ_univ_" + count).value = result[i].university;
	        		document.getElementById("educ_id_" + count).value = result[i].id;
	        	}
	        }else{
	        	loadEducation("update_education",1,resLength);
	        }
	        
	        var cancel = "<div class='row'>" +
				"	<div class='col-md-12'>" +
				"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(3)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
				"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateEduc("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
				"	</div>" +
				"</div>";
	        
	        $("#cancelButton3").append(cancel);  
	        
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
    });
}

function isActiveInd(type){
	if(type){
		return "yes";
	}
	return "no";
}

function updateDetails_1(indID){
	
	 $.ajax({
		 	url:"/market_observation/individual/updateDetails1",
		    data: "id="+indID+
	    	"&firstName="+document.getElementById("first_name").value+
	    	"&lastName="+document.getElementById("last_name").value+
	    	"&middleInitial="+document.getElementById("middle_initial").value+
	    	"&country.id="+document.getElementById("personBase").value+
	    	"&updated_by=not yet",
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(indID);
		        	 }
		           else{
	        		  console.log("error in updating db");
		        	}
		    	console.log(result);
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}

function updateDetails_2(contactID,indID){
	
	 $.ajax({
		 	url:"/market_observation/individual/updateDetails2",
		 	data: "address1="+document.getElementById(("ind_addr1")).value+
		   		  "&address2="+document.getElementById(("ind_addr2")).value+
		   		  "&address3="+document.getElementById(("ind_addr3")).value+
		   		  "&email="+document.getElementById(("ind_email")).value+
		   		  "&linkedIn="+document.getElementById(("ind_linkedIn")).value+
		   		  "&skype="+document.getElementById(("ind_skype")).value +
		   		  "&youtube="+document.getElementById(("ind_youtube")).value+
		   		  "&twitter="+document.getElementById(("ind_twitter")).value+
		   		  "&contactId="+contactID+
		   		  "&indId="+indID,
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(indID);
		        	 }
		           else{
	        		  console.log("error in updating db");
		        	}
		    	console.log(result);
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}

function updateAwards(indID){
	
	 $.ajax({
		 	url:"/market_observation/individual/updateAwards",
		    data: "awards="+getAwardsList()+
		          "&id="+parseInt(indID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(indID);
	        	}else{
        		  console.log("error in updating db");
	        	}
		     },
	         error: function (result) {
	        	console.log("failed");
	         }
	    });
}

function updateNation(indID){
	
	 $.ajax({
		 	url:"/market_observation/individual/updateNation",
		    data: "nationalities="+getNationList()+
		          "&id="+parseInt(indID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(indID);
	        	}else{
       		  console.log("error in updating db");
	        	}
		     },
	         error: function (result) {
	        	console.log("failed");
	         }
	    });
}

function updateCompany(indID){
	
	 $.ajax({
		 	url:"/market_observation/individual/updateCompany",
		    data: "company="+getInstitutionList()+
		          "&id="+parseInt(indID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(indID);
	        	}else{
      		  console.log("error in updating db");
	        	}
		     },
	         error: function (result) {
	        	console.log("failed");
	         }
	    });
}

function updateLang(indID){
	
	 $.ajax({
		 	url:"/market_observation/individual/updateLanguage",
		    data: "language="+getLanguagesList()+
		          "&id="+parseInt(indID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(indID);
	        	}else{
     		  console.log("error in updating db");
	        	}
		     },
	         error: function (result) {
	        	console.log("failed");
	         }
	    });
}

function updateEduc(indID){
	
	 $.ajax({
		 	url:"/market_observation/individual/updateBackground",
		    data: "background="+getEducBckgrndList()+
		          "&id="+parseInt(indID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(indID);
	        	}else{
    		  console.log("error in updating db");
	        	}
		     },
	         error: function (result) {
	        	console.log("failed");
	         }
	    });
}

function updateNumbers(indID){
	
	 $.ajax({
		 	url:"/market_observation/individual/updateNumbers",
		    data: "numbers="+getPhoneNum()+
		          "&id="+parseInt(indID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(indID);
	        	}else{
   		  console.log("error in updating db");
	        	}
		     },
	         error: function (result) {
	        	console.log("failed");
	         }
	    });
}

function updateDetails(type){
	
	if(type == 1){
		$("#update_Details_1").show();
		$("#display_Details_1").hide();
	}else if(type == 2){
		$("#display_involvement").hide();
		$(".updateInvolvements").show();
	}else if(type == 3){
		$("#display_background").hide();
		$(".updateEducation").show();
	}else if(type == 4){
		$("#display_awards").hide();
		$(".updateAwards").show();
	}else if(type == 5){
		$("#display_nation").hide();
		$(".updateNation").show();
	}else if(type == 6){
		$("#display_lang").hide();
		$(".updateLang").show();
	}else if(type == 7){
		$("#update_Details_2").show();
		$("#display_Details_2").hide();
	}else if(type == 8){
		$("#display_Phones").hide();
		$(".updatePhones").show();
	}
	
}

function cancelUpdate(type){
	if(type == 1){
		$("#update_Details_1").hide();
		$("#display_Details_1").show();
	}else if(type == 2){
		$(".updateInvolvements").hide();
		$("#display_involvement").show();
	}else if(type == 3){
		$("#display_background").show();
		$(".updateEducation").hide();
	}else if(type == 4){
		$("#display_awards").show();
		$(".updateAwards").hide();
	}else if(type == 5){
		$("#display_nation").show();
		$(".updateNation").hide();
	}else if(type == 6){
		$("#display_lang").show();
		$(".updateLang").hide();
	}else if(type == 7){
		$("#update_Details_2").hide();
		$("#display_Details_2").show();
	}else if(type == 8){
		$("#display_Phones").show();
		$(".updatePhones").hide();
	}
}


function additionalUpdate(count,type){
	var buttonId = type+"_"+ count;
	count++;
	
	if(type == 3){
		loadContactNumbers("update_phones",count,0,0);
	}else if(type == 4){
		loadNationList("update_nation",count,0,0);
	}else if(type == 5){
		loadLangList("update_lang",count,0);
	}else if(type == 6){
		loadInvolvement("update_involvement",count,0,0,0,0,0);
	}else if(type == 7){
		loadEducation("update_education",count,0);
	}else if(type == 8){
		loadAwardsList("update_awards",count,0);
	}
	
	$("#add_"+buttonId+"").hide();
	$("#cancel_"+buttonId+"").hide(); 
}


function viewList(){
	
	$("#ind_action").empty();
	
	var str = "<br/><div class='row'><div class='col-xs-4'><input type='button' value='Back to List' onclick='getList()'></div></div>";
	
	  $("#ind_action").append(str);
}

function getList(){
	location.reload();
	$("#ind_details").hide();
	$("#ind_list").show();
}