/**
 * 
 */
var moreContactPhone = 0;

function loadNewCompany(mainDiv,dropDown,count){
	var compareDiv = "grpCompDiv_" + count;
	var str = "<div class='modal fade' id='companyModal' role='dialog'>" + 
    		   "<div class='modal-dialog'>"+
               "<div class='modal-content'>"+
               "<div class='modal-header'>"+
               "<h4 class='modal-title'>New Company</h4>"+
               "</div>"+
               "<div class='modal-body'>";
	
	str += "<div id='newMainContact' >"+
	 	"		    <div class='form-group row'>"+                       
  		"				<label class='col-xs-3 col-form-label'>Company Name</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<input class='form-control' type='text' id='comp_name'>"+
  		"				</div>"+
  		"			</div>"+
	 	"		    <div class='form-group row'>"+
  		"				<label class='col-xs-3 col-form-label'>Company Type</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<select class='form-control' id='comp_type'>"+
    	"						<option value='0'></option>"+
	 	"		    			<option value='1'>bank</option>"+
	 	"		    			<option value='2'>central bank</option>"+
	 	"		    			<option value='3'>insurance</option>"+
	 	"		    			<option value='4'>industry association</option>"+
	 	"		    			<option value='5'>media</option>"+
    	"					</select>"+
  		"				</div>"+
  		"			</div>"+
  		"		    <div class='form-group row'>"+                       
  		"				<label class='col-xs-3 col-form-label'>Company Website</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<input class='form-control' type='text' id='comp_wbsite'>"+
  		"				</div>"+
  		"			</div>"+
  		"			<div class='form-group row'>"+                       
  		"				<label class='col-xs-3 col-form-label'>Notes  </label>"+
  		"				<div class='col-xs-8'>"+
    	"					<input class='form-control' type='text' id='comp_note'>"+
  		"				</div>"+
  		"			</div>"+
  		"		    <div class='form-group row'>"+ 
	 	"		         <label class='col-xs-3 col-form-label'> Holding Company </label> "+
	 	"                <div class='col-xs-8'><select class='form-control' id='comp_holding'>";
					if(mainDiv == compareDiv ){
				 		str +="<option selected value='1'>yes</option>" +
				 		      "<option value='0'>no</option>";
				 	}else{
				 		str +="<option value='0'>no</option>" + 
				 		      "<option value='1'>yes</option>";
				 	}	    					
	 	             str += "</select></div></div>"+
	 	"		   <div class='form-group row'>"+
  		"				<label class='col-xs-3 col-form-label'>Country</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<div id='companyLocation'></div>"+
  		"				</div>"+
  		"			</div>"+
	 	"		    <div id='company_contact'></div>"+
	 	"		    <div id='company_phoneNumbers'></div>"+
	 	"		    <button onClick='newContact(\""+mainDiv+"\")' id='addButton' class='btn'>Insert Company</button>"+
//	 	"		    <button onClick='cancelNewAddition(\""+mainDiv+"\",\""+dropDown+"\",\"newMainContact\",\""+count+"\")' id='cancelButton' class='btn'>Cancel</button>"+
	    "		 </div>" +
	    "</div>"+
        "<div class='modal-footer'>"+
        "<button type='button' class='btn btn-default' data-dismiss='modal' onClick='cancelNewAddition(\""+mainDiv+"\",\""+dropDown+"\",\"newMainContact\",\""+count+"\")'>Close</button>"+
        "</div>"+
        "</div>"+
        "</div>"+
        "</div>";
	 	             
	$("#"+mainDiv+"").append(str);
	
	$("#companyModal").modal();
}

function loadContactDetails(divContact){
	var str = "";
    var ind = "";
	if(divContact == "individualContacts"  || divContact == 'individualContactList'){
		ind = "ind_";
	}
	    str += "Contact Address :<br/>" +
	            "<div class='row'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1  <input type='text' class='address' id='"+ind+"addr1'></div>" +
    	        "<div class='row'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2  <input type='text' class='address' id='"+ind+"addr2'></div>" +
    	        "<div class='row'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3  <input type='text' class='address' id='"+ind+"addr3'></div><br/>" +
    	        "<div class='row'><div class='col-md-6'>Email &nbsp;&nbsp;: <input class='contact' type='text' id='"+ind+"email'></input></div>" +
    	        "<div class='col-md-6'>LinkedIn &nbsp;: <input type='text' class='contact' id='"+ind+"linkedIn'></input></div></div>" +
    	        "<div class='row'><div class='col-md-6'>Skype &nbsp; : <input type='text' class='contact' id='"+ind+"skype'></input></div>" + 
    	        "<div class='col-md-6'>Youtube &nbsp;&nbsp;: <input type='text' class='contact' id='"+ind+"youtube'></input></div></div>" +
		 		"<div class='row'><div class='col-md-6'>Twitter &nbsp;: <input type='text' class='contact' id='"+ind+"twitter'></input></div></div>";
		 
	
	if ($("#"+divContact+"").is(':empty')){
		$("#"+divContact+"").append(str);
	}
	
}

function loadContactNumbers(divContact,count,location,lastNum){
	var str = "";
	var countAdd = count;
	var type = 0;
    var ind = "";
    console.log("location : " + location);
	if(divContact == 'company_phoneNumbers' && location == 'main'){
		type = 4;
		ind = "main_";
	}else if(divContact == 'company_phoneNumbers' && location == 0){
		type = 1;
	}else if(divContact == 'university_phoneNumbers'){
		type = 2;
	}else if(divContact == 'individualNumbers' || divContact == 'update_phones'){
		type = 3;
		ind = "ind_";
	}
	
	   str += "<div class='"+ind+"additionalPhones' id='morePhones_"+ type+"_"+ countAdd +"'><br>" +
		     	"<div class='row'><input type='hidden' class='form-control phone_id' value = '0' id='"+ind+"phone_id_"+count+"'>" +
		     	"<div class='col-md-6'>phone type  &nbsp;&nbsp;&nbsp;<select id='"+ind+"phone_type_"+countAdd+"'>"+
		    	"<option value ='mobile' >mobile</option>" +
				"<option value ='telephone' >telephone</option>" +
				"<option value ='fax' >fax</option></select></div>" +
				"<div class='col-md-2'>country code </div><div class='col-md-4' id='"+ind+"phone_loc_"+countAdd+"'></div></div>" +
		        "<div class='row'><div class='col-md-6'>area code  &nbsp;&nbsp;&nbsp;<input type='text' value='0' id='"+ind+"phone_area_"+countAdd+"'></input></div>" +
    	        "<div class='col-md-6'>phone number  &nbsp;&nbsp;&nbsp;<input type='text' value='0' id='"+ind+"phone_num_"+countAdd+"'></input></div></div>";
		str += "<div class='pull-right'>";
		
			if(lastNum == 0 || lastNum == countAdd){
				if(divContact == 'update_phones'){
					str += "<button type='button' id='add_"+ type +"_"+countAdd+"' class='btn btn-default btn-sm' onclick='additionalUpdate("+countAdd+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
				}else{
		    	    str += "<button type='button' id='add_"+ type +"_"+countAdd+"' class='btn btn-default btn-sm' onclick='addNumbers("+countAdd+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
				}
			}
		    if(lastNum < count){
				str += "<input type='button' id='cancel_"+ type +"_"+countAdd+"' value='Cancel' onclick='cancelNumbers("+countAdd+","+type+")'>";
			}
		str += "<br /></div><br /></div>";	

	    if ($("#"+divContact+"").is(':empty') && count == 0){
	    	getCountryCode(ind+"phone_loc_"+countAdd,ind+"phone_"+type+"_loc_"+countAdd,location);
	    	$("#"+divContact+"").append(str);
	    }else if(count > 0){
	    	$("#"+divContact+"").append(str);
	    	getCountryCode(ind+"phone_loc_"+countAdd,ind+"phone_"+type+"_loc_"+countAdd,location);
	    }
	
	
}

function getCountryList(divName, dropDownName,count,txtValue){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/countryList",
	    type: "GET",
        success: function (result) {
        	var str = "";
        	
        	if(divName == "company_LocationreadOnly"){
        		for (i=0;i<result.length;i++){
    				if(txtValue == result[i].id ){
    					str +=  "<input class='form-control' type='text' readOnly value='"+result[i].name+"' id='comp_country'>";
					}
				}
        	}
        	else{
        		str += "<select class='form-control' id='"+dropDownName+"'>";
        		
    			for (i=0;i<result.length;i++){
    				str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
    			}
    	        str +="</select></br>";
        	}
    		
	        
	        if(count > 0){
	        	str +="<div class='pull-right' id='buttons_country_connection_"+count+"'>";
	        	str += "<input type='button' id='addCountry_"+count+"' value='Add More Country' onclick='addCountry("+count+")'>";
	        	
	        	if(count > 1){
	        		str += "<input type='button' id='cancelCountry_"+count+"' value='Cancel' onclick='addCountry("+count+")'>";
	        	}
	        	str +="</div>";
	        }
	        
    		
    		if ($("#"+divName+"").is(':empty')){
    			$("#"+divName+"").append(str);
    		}
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function getCountryCode(divName, dropDownName, location ){

	$.ajax({
	 	url: "/market_observation/country/countryCode",
	    type: "GET",
        success: function (result) {
        	var str = "";
    		str += "<select id='"+dropDownName+"'>";
    			for (i=0;i<result.length;i++){
	        		if(result[i].id == location){
	        			str += "<option selected value='"+result[i].id+"'>"+result[i].code+"</option>";
	        		}else{
	        			str += "<option value='"+result[i].id+"'>"+result[i].code+"</option>";
	        		}
    			}
	        str +="</select></br>";
    		$("#"+ divName +"").append(str);
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function addNumbers(count,type){
	var buttonId = type+"_"+ count;
	count++;
	console.log(buttonId);
	if(type == 4){
		loadContactNumbers("company_phoneNumbers",count,"main",0);
	}if(type == 1){
		loadContactNumbers("company_phoneNumbers",count,0,0);
	}else if (type == 2){
		loadContactNumbers("university_phoneNumbers",count,0,0);
	}else if (type == 3){
		loadContactNumbers("individualNumbers",count,0,0);
	}
	
	$("#add_"+buttonId+"").hide();
	$("#cancel_"+buttonId+"").hide(); 
}

function cancelNumbers(count,type){

	$("#morePhones_"+ type +"_"+ count +"").remove(); 
	
	count--;
	
	$("#add_"+ type +"_"+ count +"").show();
	$("#cancel_"+ type +"_"+ count +"").show();  
}


/*add new contact -  partial entry*/
function newContact(mainDiv){
	var name = document.getElementById(("comp_name")).value;
	
	if(name){
		$.ajax({
			 	url:"/market_observation/contact/add",
			    data: "address1="+document.getElementById(("addr1")).value+
		    		  "&address2="+document.getElementById(("addr2")).value+
		    		  "&address3="+document.getElementById(("addr3")).value+
		    		  "&email="+document.getElementById(("email")).value+
		    		  "&linkedIn="+document.getElementById(("linkedIn")).value+
		    		  "&skype="+document.getElementById(("skype")).value +
		    		  "&youtube="+document.getElementById(("youtube")).value+
		    		  "&twitter="+document.getElementById(("twitter")).value,
			    type: "POST",
			    success: function (result) {
			           if(result.ok){
			        	   console.log(result);
			        	   newCompany(result.id,mainDiv);
			           }
			           else{
		        		  console.log("error in updating db");
			        	}
			        },
			        error: function (result) {
			        	console.log("failed");
			        }
		    });
	}else{
		console.log("no company to be saved!");
	}
}

function getContactsData(data,type){

   if(data != null){
	   if(type == 1){
		   return data.address1;
	   }else if(type == 2){
		   return data.address2;
	   }else if(type == 3){
		   return data.address3;
	   }else if(type == 4){
		   return data.email;
	   }else if(type == 5){
		   return data.linkedIn;
	   }else if(type == 6){
		   return data.skype;
	   }else if(type == 7){
		   return data.twitter;
	   }else if(type == 8){
		   return data.youtube;
	   }else{
		   return data.name;
	   }
		
	}else{
		return "";
	}
}



/*add new company -  partial entry*/
function newCompany(contactID,mainDiv){
	console.log("company : " + document.getElementById(("comp_name")).value );
	$.ajax({
		 	url:"/market_observation/company/add",
		    data: "name="+document.getElementById(("comp_name")).value+
		          "&type="+document.getElementById(("comp_type")).value+
	    		  "&website="+document.getElementById(("comp_wbsite")).value+
	    		  "&notes="+document.getElementById(("comp_note")).value+
	    		  "&isHolding="+document.getElementById(("comp_holding")).value+
	    		  "&country.id="+document.getElementById(("mainCompanyBase")).value+
	    		  "&contactID="+contactID,
		    type: "POST",
		    success: function (result) {
		    
		           if(result.ok){
		        	   console.log(result);
		        	   newPhoneNumbers(result.id,mainDiv);
		           }
		           else{
		        	  console.log(result);
	        		  console.log("error in updating db");
		        	}
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}

/*add new phone numbers -  partial entry*/
function newPhoneNumbers(companyID,mainDiv){
	var numItems = $('.additionalPhones').length
	var newCompany = document.getElementById('dropdownCompany');
	var isOK = false;
     
	for(i = 0; i <= numItems - 1; i++){
		if(document.getElementById(("phone_num_" +i)).value != 0){
			$.ajax({
			 	url:"/market_observation/phone/add",
			    data: "phoneType="+document.getElementById(("phone_type_" +i)).value+
		    		  "&areaCode="+document.getElementById(("phone_area_" +i)).value+
		    		  "&number="+document.getElementById(("phone_num_" +i)).value+
		    		  "&country.id="+document.getElementById(("phone_1_loc_" +i)).value+
		    		  "&companyID="+companyID,
			    type: "POST",
			    success: function (result) {
			           if(result.ok){
			        	    isOK = true;
			        	    getData(companyID,mainDiv);
			           }
			           else{
		        		  console.log("error in updating db");
			        	}
			        },
			        error: function (result) {
			        	console.log("failed");
			        }
		    });
		}else{
			 getData(companyID,mainDiv);
		}
	}
}