/**
 * 
 */

$(document).ready(function(){
	loadMainDetails("mainDetails");
	getCompanyList("signal_company","dropdownCompany", 0,0,0,0,1);
    loadConnectedCompanies(0,0,0,1);
    loadConnectedAuthors(0,0,0,1);
    loadConnectedCountries(0,0,0);
	
});

function companyNames(textIdValue){

	$("#newMainContact").hide(); 
	$('#dropdownCompany').show();
	
	getCompanyList("signal_company","dropdownCompany", 0,0, textIdValue,0,1);

}

/*switch from dropdown to new Object*/
function getInputOther(divName,dropDown,divInput,count){
	
	$("#"+divName+"").on('click', "#"+ dropDown +"", function(){
	    if ((this.value) == "otherComp") {
	    	
	    	$("#"+dropDown+"").hide();
	    	$("#connectedFor_"+count+"").hide();
	    	$("#company_span_"+count+"").hide();
	    	$("#buttons_comp_connection_" +count + "").hide();
	    	$("#newMainContact").show();
	    	loadNewCompany(divName,dropDown,count);
	    	loadContactDetails("company_contact");
	    	loadContactNumbers("company_phoneNumbers",0,0,0);
	    	getCountryList("companyLocation","mainCompanyBase",0,0,0);
	    	
	    	
	     }else if ((this.value) == "otherInd") {
		    	
	    	$("#"+dropDown+"").hide();
	    	$("#connectedBy_"+count+"").hide();
	    	$("#author_span_"+count+"").hide();
	    	$("#buttons_authors_connection_" + count+"").hide();
	    	loadNewAuthor(divName,dropDown,count);
	    	getCountryList("individualLocation","authorBase",0,0,0);
		 }
	});
	
	$("#"+divInput+"").dblclick(function(){
		$("#"+divInput+"").hide();
		$("#"+dropDown+"").show();
		$("#"+dropDown+"").val(companyDefaultValue);
	});
}

/*get data[company] and store to dropdown*/
function getCompanyList(divName,dropDownName,newCompanyDiv,count,textValue,lastCompanyCount,connectionType){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/companyList",
	    type: "GET",
        success: function (result) {
        	var appendToDiv = "moreCompanies_"+count;
        	var str = "<div id='"+appendToDiv+"'>";
        	var divNum = 0;
        	var indID = 0;
        	
        	if(divName == 'update_companies'){
        		var splitType = connectionType.split("_");
        		connectionType = splitType[0];
        		indID = splitType[1];
        		divNum = 1;
        	}
    		if(divName == 'comp_connection' || divName == 'update_companies'){
    			str += "<span class='countCompanies'>"+count+". </span>" +
    			       "<input type='hidden' class='form-control ms_comp_id' value = '"+indID+"' id='ms_comp_id_"+count+"'>" +
    			       "<div id='"+ newCompanyDiv +"'></div>"+
    				   "<select class ='connected_companies' id='"+dropDownName+"'>";
    		}else{
    			newCompanyDiv = "newMainContact";
    			str += "<select required class='form-control' id='"+dropDownName+"'>";
    		}
    		
    		str += "<option value='0'> </option>";
    			
	        	for (i=0;i<result.length;i++){
	        		if(result[i].id == textValue){
	        			str += "<option selected value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}else{
	        			str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}
		        }
	        	
	        str +="<option value='otherComp' class='OtherCompany'>Input Other Company</option>"+
	        	  "</select>";
	        if(divName == 'comp_connection' || divName == 'update_companies'){
		        str +="<span id='company_span_"+count+"'>&nbsp;&nbsp;&nbsp;by : </span><select id='connectedFor_"+count+"'> ";
		        if(connectionType == 1){
	        		  str +="<option selected value='1'>market signal</option>" + 
	        		  		"<option value='2'>holding</option>";
	        	  }else{
	        		  str +="<option value='1'>market signal</option>" + 
	        		        "<option selected value='2'>holding</option>";
	        	  }
	      	   str += "</select>";
	        }
	        if(count > 0 && textValue == 0 ){
	        	str +="<div class='pull-right' id='buttons_comp_connection_"+count+"'>";
	        	str += "<button type='button' id='add_1_"+count+"' class='btn btn-default btn-sm' onclick='addConnection("+count+",1,"+divNum+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
	        	
	        	if(count > 1){
	        		str += "<button type='button' id='cancel_1_"+count+"' class='btn btn-default btn-sm' onclick='cancelConnection("+count+",1,"+divNum+")'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
	        	}
	        	str +="</div>";
	        } 
	        
	        if(count > 0 && textValue > 0){
	        	
	        	if(count == lastCompanyCount){
	        		str +="<div class='pull-right' id='buttons_comp_connection_"+count+"'>";
		        	str += "<button type='button' id='add_1_"+count+"' class='btn btn-default btn-sm' onclick='addConnection("+count+",1,"+divNum+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
		        	str +="</div>";
	        	}
	        }
    		str +="</div>";
    		    		
    		$("#"+ divName +"").append(str);
    		$("#"+newCompanyDiv+"").hide();
    		
    		getInputOther(appendToDiv,dropDownName,newCompanyDiv,count);
    		
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

/*get data[individual] and store to dropdown*/
function getIndividualList(divName, dropDownName,newAuthorDiv,count,textValue,lastAuthorCount,connectionType){
    
    $.ajax({
	 	url: "/market_observation/marketSignal/individualList",
	    type: "GET",
        success: function (result) {
        	
        	var str = "";
        	var appendToDiv = "moreAuthors_"+count;
        	var str = "<div id='"+appendToDiv+"'>";
        	var divNum = 0;
        	var indID = 0;
        	
        	if(divName == 'update_individual'){
        		var splitType = connectionType.split("_");
        		connectionType = splitType[0];
        		indID = splitType[1];
        		divNum = 1;
        	}
        	
        	if(divName == 'authors_connection' || divName == 'update_individual'){
        		str += "<span class='countAuthors'>"+count+". </span>" +
        			   "<input type='hidden' class='form-control ms_ind_id' value = '"+indID+"' id='ms_ind_id_"+count+"'>" +
        		       "<div id='"+ newAuthorDiv +"'></div>";
        	}
    		str += "<select class='connected_authors' id='"+dropDownName+"'>" +
    				"<option value='0'> </option>";
    		
    		if(result.length != 0){
    			for (i=0;i<result.length;i++){
	        		if(result[i].id == textValue){
	        			str += "<option selected value='"+result[i].id+"'>"+result[i].firstName+ " " + result[i].middleInitial+ " " + result[i].lastName+"</option>";
	     		    }else{
	        		    str += "<option value='"+result[i].id+"'>"+result[i].firstName+ " " + result[i].middleInitial+ " " + result[i].lastName+"</option>";
	     		    }
	        	}
    		}	
	        	
	        str +="<option value='otherInd' class='OtherIndividual'>Input Other Individual</option>"+
	        	  "</select>";
	        str +="<span id='author_span_"+count+"'>&nbsp;&nbsp;&nbsp;by : </span> " + 
	        	  "<select id='connectedBy_"+count+"'>";
	        	  if(connectionType == 1){
	        		  str +="<option selected value='1'>signal author</option>" + 
	        		  		"<option value='2'>signal content</option>";
	        	  }else{
	        		  str +="<option value='1'>signal author</option>" + 
	        		        "<option selected value='2'>signal content</option>";
	        	  }
	        	  	 
	        str +="</select>";
	        
	        if(count > 0 && textValue == 0){
	        	str +="<div class='pull-right' id='buttons_authors_connection_"+count+"'>";
	        	str += "<button type='button' id='add_3_"+count+"' class='btn btn-default btn-sm' onclick='addConnection("+count+",3,"+divNum+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
	        	
	        	if(count > 1){
	        		str += "<button type='button' id='cancel_3_"+count+"' class='btn btn-default btn-sm' onclick='cancelConnection("+count+",3,"+divNum+")'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
	        	}
	        	str +="</div>";
	        }
	        
	        if(count > 0 && textValue > 0){
	        	
	        	if(count == lastAuthorCount){
	        		str +="<div class='pull-right' id='buttons_authors_connection_"+count+"'>";
	        		str += "<button type='button' id='add_3_"+count+"' class='btn btn-default btn-sm' onclick='addConnection("+count+",3,"+divNum+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
	        		str +="</div>";
	        	}
	        }
	        
	        str +="</div>";
	            		
    		$("#"+ divName +"").append(str);
    		$("#"+newAuthorDiv+"").hide();
    		
    		getInputOther(appendToDiv,dropDownName,newAuthorDiv,count);
    		
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

/*get data[country] and store to dropdown*/
function getCountryList(divName, dropDownName,count,textValue,lastCountryCount){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/countryList",
	    type: "GET",
        success: function (result) {
        	var str = "";
        	var appendToDiv = "moreCountries_"+count;
        	var divNum = 0;
        	var id = 0;
        	console.log("sdf " + lastCountryCount);
        	if(divName == 'update_countries'){
        		var splitType = lastCountryCount.split("_");
        		lastCountryCount = splitType[0];
        		id = splitType[1];
        		divNum = 1;
        	}
        	
        	if(divName == 'count_connection' || divName == 'update_countries'){
        		str = "<div id='"+appendToDiv+"'>"+
        		      "<input type='hidden' class='form-control ms_count_id' value = '"+id+"' id='ms_count_id_"+count+"'>" +
             	      "<span class='countCountries'>"+count+". </span>" +
        		      "<select class='connected_countries' id='"+dropDownName+"'>";
        	}else{
        		str = "<div id='notForConnections'>";
        		str += "<select class='form-control' id='"+dropDownName+"'>";
        	}
    		
			for (i=0;i<result.length;i++){
				if(result[i].id == textValue){
					str += "<option selected value='"+result[i].id+"'>"+result[i].name+"</option>";
				}else{
        			str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
				}
	        }
	        str +="</select>";
      		
	        if(count > 0 && textValue == 0){
	        	str +="<div class='pull-right' id='buttons_country_connection_"+count+"'>";
	        	str += "<button type='button' id='add_2_"+count+"' class='btn btn-default btn-sm' onclick='addConnection("+count+",2,"+divNum+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
	        	
	        	if(count > 1){
	        		str += "<button type='button' id='cancel_2_"+count+"' class='btn btn-default btn-sm' onclick='cancelConnection("+count+",2,"+divNum+")'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
	        	}
	        	str +="</div>";
	        }
	        
	        if(count > 0 && textValue > 0){
	        	if(count == lastCountryCount){
		        	str +="<div class='pull-right' id='buttons_country_connection_"+count+"'>";
		        	str += "<button type='button' id='add_2_"+count+"' class='btn btn-default btn-sm' onclick='addConnection("+count+",2,"+divNum+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
		        	str +="</div>";
	        	}
	        }
	        
	        str +="</div>";
	       if(divName == "count_connection" || divName == "update_countries"){
	    	   $("#"+divName+"").append(str); 
	       }else{
	    	   if($("#"+divName+"").is(':empty')){
	    		   $("#"+divName+"").append(str);
	    	   }
	       }
	        
	    // document.getElementById("ms_count_id_" + count).value = id;
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

/*final data for market signal*/
function NewSignal(){
	
	$.ajax({
	 	url:"/market_observation/marketSignal/data",
	    data: "signalDate=" + document.getElementById("signal_date").value+
    		  "&factor="+document.getElementById("factorName").value+
    		  "&reportDate=" +document.getElementById("report_date").value+
    		  "&link="+document.getElementById("signal_link").value+
    		  "&origTitle="+document.getElementById("signal_orig").value+
    		  "&transTitle="+document.getElementById("signal_trans").value+
    		  "&company="+document.getElementById("dropdownCompany").value+
    		  "&overview="+document.getElementById("signal_summary").value+
    		  "&background="+document.getElementById("signal_background").value+
    		  "&implication="+document.getElementById("signal_implication").value+
    		  "&connectedCompanies="+getConnectionCompanies()+
    		  "&connectedCountries="+getConnectionCountries()+
    		  "&connectedAuthors="+getConnectionAuthors(),
	    type: "POST",
	    success: function (result) {
	    	console.log("sfh sd " + result);
	           if(result.ok){
	        	   
	        	   location.reload();
	        	   //console.log(result);
	        	 }
	           else{
        		  console.log("error in updating db");
	        	}
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
    });
}

/*add new individual -  partial entry*/
function newIndividual(mainDiv){
	
		$.ajax({
		 	url:"/market_observation/individual/add",
		    data: "firstName="+document.getElementById(("first_name")).value+
	    		  "&lastName="+document.getElementById(("last_name")).value+
	    		  "&middleInitial="+document.getElementById(("middle_initial")).value+
	    		  "&country.id="+document.getElementById(("authorBase")).value+
	    		  "&input_by=mary",
		    type: "POST",
		    success: function (result) {
		           if(result.ok){
		        	   console.log(result);
		        	   getData(result.id,mainDiv);
		           }
		           else{
	        		  console.log("error in updating db");
		        	}
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}
/*get partial entry data from page and send to SignalClass model text file*/
function getData(divID,mainDiv){
	
	var connectionNo = mainDiv.split("_");
	var compareDiv1 = "moreCompanies_" + connectionNo[1];
	var compareDiv2 = "moreAuthors_" + connectionNo[1];
	
	$(".modal").modal("hide");
	$(".modal-backdrop").hide();
	$('html, body').css({
	    'overflow': 'auto',
	    'height': 'auto'
	});
	
	if(mainDiv == "moreCompanies_0"){
		
		$("#moreCompanies_0").remove();
		getCompanyList("signal_company","dropdownCompany", 0,0, divID,0,1);
		
	}else if(compareDiv1 == mainDiv){
		
		var totalCompanies = $(".connected_companies").length;
		var connectionType = document.getElementById("connectedFor_" + connectionNo[1]).value;
		
		$("#"+mainDiv+"").remove();
		loadConnectedCompanies(connectionNo[1] - 1,divID,totalCompanies,connectionType);
		
	}else if(compareDiv2 == mainDiv){
		
		var totalAuthors = $(".connected_authors").length;
		var connectionType = document.getElementById("connectedBy_" + connectionNo[1]).value;
		
		$("#"+mainDiv+"").remove();
		loadConnectedAuthors(connectionNo[1] - 1,divID,totalAuthors,connectionType);
	}
} 

/*get connection through arrays [companies]*/
function getConnectionCompanies(){
	
	var totalCompanies = $(".connected_companies").length;
	var companies = new Array();
	
	for(i = 0; i < totalCompanies; i++){
		
		var compareDiv = "moreCompanies_"+ (i+1);
		var drpDown = "comp_dropdownCompany_"+ i;
		var connectionType = document.getElementById("connectedFor_" + (i+1)).value;	
		var id = document.getElementById("ms_comp_id_" + (i+1)).value;	
		
		companies[i] = compareDiv + "-" + document.getElementById(drpDown).value + "-" + connectionType + "-" + id;
	}
	return companies;
}

/*get connection through arrays [countries]*/
function getConnectionCountries(){
	
	var totalCountries = $(".connected_countries").length;
	var countries = new Array();
	
	for(i = 0; i < totalCountries; i++){
		
		var compareDiv = "moreCountries_"+ (i+1);
		var drpDown = "count_dropdownCountry_"+ i;
		var id = document.getElementById("ms_count_id_" + (i+1)).value;	
		
		countries[i] = compareDiv + "-" + document.getElementById(drpDown).value + "-" + id;
	}
	return countries;
}

/*get connection through arrays [authors]*/
function getConnectionAuthors(divID,mainDiv){
	
	var totalAuthors = $(".connected_authors").length;
	var authors = new Array();
	
	for(i = 0; i < totalAuthors; i++){
		
		var compareDiv = "moreAuthors_"+ (i+1);
		var drpDown = "authors_dropdownAuthors_"+ i;
		var connectionType = document.getElementById("connectedBy_" + (i+1)).value;	
		var id = document.getElementById("ms_ind_id_" + (i+1)).value;	
		
		if(mainDiv == compareDiv){
			authors[i] = compareDiv + "-"+divID + "-" + connectionType + "-" + id;
		}else{
			authors[i] = compareDiv + "-" + document.getElementById(drpDown).value + "-" + connectionType + "-" + id;
		}
	}
	return authors;
}


/*connections : company_conn, countries_conn, authors_conn*/
function loadConnectedCompanies(companyAdd,companyValue,lastCompanyCount,connectionType){
	var buttonID = companyAdd+1;
	getCompanyList("comp_connection","comp_dropdownCompany_"+ companyAdd,"CompConnectionsText_" + companyAdd,buttonID, companyValue,lastCompanyCount,connectionType);
}

function loadConnectedCountries(countryAdd,countryValue,lastCountryCount){
	var buttonID = countryAdd+1;
	getCountryList("count_connection","count_dropdownCountry_"+ countryAdd,buttonID,countryValue,lastCountryCount);
}

function loadConnectedAuthors(authorAdd,authorValue,lastAuthorCount,connectionType){
	var buttonID = authorAdd+1;
	getIndividualList("authors_connection","authors_dropdownAuthors_"+ authorAdd,"IndConnectionsText_" + authorAdd, buttonID,authorValue,lastAuthorCount,connectionType);
}

function addConnection(count,type,divNum){
	var buttonId = type+"_"+ count;
	
	$("#add_"+buttonId+"").hide();
	$("#cancel_"+buttonId+"").hide(); 

	if(divNum == 0){
		if(type == 1){
			loadConnectedCompanies(count,0,0,1);
		}else if(type == 2){
			loadConnectedCountries(count,0,0);
		}else if(type == 3){
			loadConnectedAuthors(count,0,0,1);
		}
	}else if(divNum == 1){
		if(type == 1){
			loadIndCompanies(count,0,0,1+ "_" + 0);
		}else if(type == 2){
			loadIndCountries(count,0,0+ "_" + 0);
		}else if(type == 3){
			loadIndAuthors(count,0,0,1+ "_" + 0);
		}
	}
	
}

function cancelConnection(count,type){
	
	if(type == 1){
		$("#moreCompanies_"+ count +"").remove(); 
	}else if(type == 2){
		$("#moreCountries_"+ count +"").remove(); 
	}else if(type == 3){
		$("#moreAuthors_"+ count +"").remove(); 
	}
	count--;
	var buttonId = type+"_"+ count;
	$("#add_"+ buttonId +"").show();
	$("#cancel_"+ buttonId +"").show(); 
}


/*dynamic div to append for new company*/
function loadNewAuthor(mainDiv,dropDown,count){
	var str = "<div class='modal fade' id='signalAuthorModal' role='dialog'>" + 
	          "<div class='modal-dialog'>"+
              "<div class='modal-content'>"+
              "<div class='modal-header'>"+
              "<button type='button' class='close' data-dismiss='modal'>&times;</button>"+
              "<h4 class='modal-title'>New Author</h4>"+
              "</div>"+
              "<div class='modal-body'>";
	str += "<div id='newAuthor'>"+
	 	"		    <div class='form-group row'>"+                       
  		"				<label class='col-xs-3 col-form-label'>First Name</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<input class='form-control' type='text' id='first_name'>"+
  		"				</div>"+
  		"			</div>"+
	 	"		    <div class='form-group row'>"+
  		"				<label class='col-xs-3 col-form-label'>Last Name</label>"+
  		"				<div class='col-xs-8'>"+
  		"					<input class='form-control' type='text' id='last_name'>"+
  		"				</div>"+
  		"			</div>"+
  		"		    <div class='form-group row'>"+                       
  		"				<label class='col-xs-3 col-form-label'>Middle Initial</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<input class='form-control'  maxlength='1' type='text' id='middle_initial'>"+
  		"				</div>"+
  		"			</div>"+
	 	"		   <div class='form-group row'>"+
  		"				<label class='col-xs-3 col-form-label'>Country/Location</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<div id='individualLocation'></div>"+
  		"				</div>"+
  		"			</div>"+
	 	"		    <button onClick='newIndividual(\""+mainDiv+"\")' id='individualButton' class='btn'>Insert Individual</button>"+
//	 	"		 <button onClick='cancelNewAddition(\""+mainDiv+"\",\""+dropDown+"\",\"newAuthor\",\""+count+"\")' id='cancelButton' class='btn'>Cancel</button>"+
	    "		 </div>" +
	    "</div>"+
        "<div class='modal-footer'>"+
        "<button type='button' class='btn btn-default' onClick='cancelNewAddition(\""+mainDiv+"\",\""+dropDown+"\",\"newAuthor\",\""+count+"\")' data-dismiss='modal'>Close</button>"+
        "</div>"+
        "</div>"+
        "</div>"+
        "</div>";
	$("#"+mainDiv+"").append(str);
	$("#signalAuthorModal").modal();
}

function loadMainDetails(mainDiv){
	var str = " 	<div class='form-group row'>"+
  	"		<label class='col-xs-2 col-form-label'>Signal Date</label>"+
  	"		<div class='col-xs-10'>"+
    "			<input class='form-control' type='date' value='' id='signal_date'>"+
  	"		</div>"+
	"   </div>"+
	"	<div class='form-group row'>"+
    "		<label class='col-xs-2 col-form-label'>Factor</label>"+
    "		<div class='col-xs-10'>"+
    "			<select class='form-control' id='factorName'>"+
    " 				<option value='none'></option>"+
    "  				<option value='coopetition'>Coopetition</option>"+
    "  				<option value='event'>Event</option>"+
    " 				<option value='prospect'>Prospect</option>"+
    " 				<option value='regulatory'>Regulatory</option>"+
    "			</select>"+
    "		</div>"+
    "	</div>"+
    "	<div class='form-group row'>"+
  	"		<label class='col-xs-2 col-form-label'>Report Date</label>"+
  	"		<div class='col-xs-10'>"+
    "			<input class='form-control' required type='date' value='' id='report_date'>"+
  	"		</div>"+
	"	</div>"+
	"	<div class='form-group row'>"+
  	"		<label class='col-xs-2 col-form-label'>Link</label>"+
  	"		<div class='col-xs-10'>"+
    "			<input class='form-control' required type='text' id='signal_link'>"+
  	"		</div>"+
  	"	</div>"+
  	"	<div class='form-group row'>"+
  	"		<label class='col-xs-2 col-form-label'>Original Heading</label>"+
  	"		<div class='col-xs-10'>"+
    "			<input class='form-control' required type='text' id='signal_orig'>"+
  	"		</div>"+
  	"	</div>		"+
  	"	<div class='form-group row'>"+
  	"		<label class='col-xs-2 col-form-label'>Translated Heading</label>"+
  	"		<div class='col-xs-10'>"+
    "			<input class='form-control' type='text' id='signal_trans'>"+
  	"		</div>"+
  	"	</div>"+
	"	<div class='form-group row'>"+
	"		<label class='col-xs-2 col-form-label'>Main Company:</label> "+
	"		<div class='col-xs-10' id='signal_company'></div>"+
	"	</div>"+
	"<div class='form-group'>"+
    "		<label>Summary</label>"+
    "		<textarea class='form-control' required id='signal_summary' rows='3'></textarea>"+
  	"	</div>"+
	"	<div class='form-group'>"+
    "		<label>Background</label>"+
    "		<textarea class='form-control' id='signal_background' rows='3'></textarea>"+
  	"	</div>"+
	"	<div class='form-group'>"+
    "		<label>Implication</label>"+
    "		<textarea class='form-control' id='signal_implication' rows='3'></textarea>"+
  	"	</div>";
	
	$("#"+mainDiv+"").append(str);
}

function cancelNewAddition(divName,dropDown,forRemoval,count){
	//$("#"+forRemoval+"").remove(); 
	$("#"+dropDown+"").show();
	$("#"+dropDown+"").val(0);
	
	if(divName.indexOf("moreCompanies") !== -1){
    	$("#buttons_comp_connection_" +count + "").show();
    	$("#company_span_"+count+"").show();
    	$("#connectedFor_"+count+"").show();
	}else if(divName.indexOf("moreAuthors") !== -1){
    	$("#author_span_"+count+"").show();
    	$("#connectedBy_"+count+"").show();
    	$("#buttons_authors_connection_" + count+"").show();
	}
}	





