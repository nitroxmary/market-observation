/**
 * 
 */

$(document).ready(function(){
	loadMeeting_list();
});

function loadMeeting_list(){
	$.ajax({
	 	url: "/market_observation/view/meetings/list",
	    type: "GET",
        success: function (result) {
    		var str = "<table class='table table-bordered' id='table_meet'>" +
    		           "<thead><tr><th>MEETING DATE</th>" +
    		           "<th>COMPANY</th>" +
    		           "<th>COUNTRY</th>" +
    		           "<th>ADDRESS</th>" +
    		           "<th>SUMMARY</th>" +
    		           "<th></th></tr></thead><tbody>";
    		for (i=0;i<result.length;i++){
        		var d1 = new Date(result[i].date);
        		var m1 = d1.getMonth() + 1;
        		var date = m1 + "/" + d1.getDate() +  "/" + d1.getFullYear();
        		var company = result[i].company.name;
        		
        		str += "<tr id='displayRow_"+result[i].id+"'>"+ 
	        				"<td> "+ date +" </td>" +
	        				"<td> "+company +" </td>"+
	        				"<td> "+result[i].country.name+" </td>" +
	        				"<td> "+result[i].location+" </td>"+
	        				"<td> "+result[i].summary+" </td>"+
	    			        "<td>" +
	    			        "	<a href='javascript:viewDetails("+result[i].id+")'>View</a> &nbsp;|&nbsp; " +
	    			        "	<a href='javascript:removeDetails("+result[i].id+","+ result[i].date +",\""+ company +"\")'>Delete</a>" +
	    			        "</td>" +
    			        "</tr>";
        	}
        	str += "</tbody></table>"
    		$("#meeting_list").append(str);
        	$("#table_meet").DataTable({
        		 "pageLength": 25
        	});
        	
        },
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function removeDetails(id,date,company){
	
	$("#meeting_removal").empty();

	var d = new Date(date);
	var m = d.getMonth() + 1;
	var date = m + "/" + d.getDate() +  "/" + d.getFullYear();
	var str = "<div class='modal fade' id='removeModal' role='dialog'>" + 
	   		"<div class='modal-dialog'>"+
	   		"<div class='modal-content'>"+
	   		"	<div class='modal-header'>"+
	   		"		<h4 class='modal-title'>Remove Meeting?</h4>"+
	   		"	</div>"+
	   		"	<div class='modal-body'>" +
	   		"			Do you really want to remove meeting at : <br/> " + company+ " on <strong>" + date + "</strong>" + 
			"	</div>"+
           	"	<div class='modal-footer'>"+
        	"	 	<button onClick='removeMeeting(\""+id+"\")' id='removeButton' class='btn btn-danger'>Delete Meeting</button>"+
           	"		<button type='button' onClick='okButton()' class='btn btn-default' data-dismiss='modal' >Cancel</button>"+
           	"	</div>"+
           	"</div></div></div>";
	
	$("#meeting_removal").append(str);
	$("#removeModal").modal();
}


function removeMeeting(id){
	$("#meeting_confirmed").empty();
	
	$.ajax({
	 	url:"/market_observation/meeting/delete",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	okButton();
	    	var str =   "<div class='modal fade' id='confirmModal' role='dialog'>" + 
				   		"<div class='modal-dialog'>"+
				   		"<div class='modal-content'>"+
				   		"	<div class='modal-header'>"+
				   		"		<h4 class='modal-title'>Deleted Meeting</h4>"+
				   		"	</div>"+
				   		"	<div class='modal-body'>";
	   		
	    	if(result.ok){
	    	str +=		"			<strong> SUCCESSFULL </strong>"; 
			}
	    	else{
	    	str +=		"			<strong> NOT DELETED </strong>"; 
	    	}
	    	str +=		"	</div>"+
			           	"	<div class='modal-footer'>"+
			           	"	</div>"+
			           	"</div></div></div>";
			
			$("#meeting_confirmed").append(str);
			$("#confirmModal").modal();
		    
			setTimeout(reload, 1000);
	    }
	});
}

function reload() {
   location.reload();
}

function okButton(){
	
	 $(".modal").modal("hide");
	 $(".modal-backdrop").hide();
	 $('html, body').css({
		   'overflow': 'auto',
		   'height': 'auto'
	});
}

function viewDetails(id){
	
	var user = document.getElementById("userType").value;
	$("#meeting_details").show();
	$("#meeting_list").hide();
	viewList();
	getMoreDetails(id,user);
	getIndividuals(id,user);
	
}

function getIndividuals(id,user){
	$("#meeting_individual").empty();
	$.ajax({
	 	url:"/market_observation/view/meeting_Individuals",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("individuals : ");
	    	var resLength = result.length ;
	    	var str = "<strong> ATTENDEES :</strong><br/><hr/>" +
	    		"<div id='display_Attendees'>";
	    	
	    	for (i=0;i<result.length;i++){
	    		console.log(result[i]);
	    		 str += "<div>" + (i + 1) +
	    		 		".	"+ result[i].ind.firstName + " " + result[i].ind.middleInitial + " " + result[i].ind.lastName +
	    		 		"</div>"
	    	}
	    	str += "<div class='row'><div class='col-md-9'></div>" ;
    	    if(user == "admin"){
    	    str += "<div class='col-md-3'>" +
		            "	<a href='#' onclick='updateDetails(2)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
		            "</div>"; 
    	    } ;
    	    str += "</div></div>"+    
    	    	"<div class='updateAttendees'>" +
    	    	"	<div class='form-group row' id='update_attendees'></div>" +
    	    	"    <div class='form-group row' id='cancelButton2'></div>" +
    	    	"</div>";
    	    
	    	$("#meeting_individual").append(str);
	    	$(".updateAttendees").hide();
	    	
	        if(resLength != 0){
            	for (i=0;i<resLength;i++){
            		
            		loadAttendees(i,result[i].ind.id + "_" + result[i].id,resLength);
                 }
            }else{
            	loadAttendees(0,0 + "_" + 0,resLength);
            }
	        
	        var cancel = "<div class='row'>" +
			"	<div class='col-md-12'>" +
			"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(2)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
			"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateAttendees("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
			"	</div>" +
			"</div>";
       
        $("#cancelButton2").append(cancel); 
        	   
	    }
	});
}

function getMoreDetails(id,user){
	$("#meeting_moreDetails").empty();
	$.ajax({
	 	url:"/market_observation/view/meeting_Details",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("details :");
	        console.log(result);
	    	var d1 = new Date(result.date);
    		var m1 = d1.getMonth() + 1;
    		var date = m1 + "/" + d1.getDate() +  "/" + d1.getFullYear();
    		var resLength = result.length;
	    	var str =  "<strong> MARKET SIGNAL DETAILS :</strong><br/><hr/>" +
	    			"<div id='display_Meetings'><div id='loadMeeting'></div><div class='row'><div class='col-md-9'></div>" ;
	    	   if(user == "admin"){
	    	   str += "<div class='col-md-3'>" +
			            "	<a href='#' onclick='updateDetails(1)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
			            "</div>"; 
	    	   } ;
	    	   str += "</div></div>" +
		    	      "<div class='updateMeetings'><div id='upMeeting'></div><div class='form-group row' id='update_meetings'></div><div class='form-group row' id='cancelButton1'></div></div></div></div>";
	    	   
	    	   $("#meeting_moreDetails").append(str);
	    	   $(".updateMeetings").hide();
	    	   loadMeetingDiv("loadMeeting",date,result.company.name,result.summary,result.follow_up,result.location,result.country.name);
	    	   loadMeetingDiv("upMeeting",date,result.company.id,result.summary,result.follow_up,result.location,result.country.id);

	    	   var cancel = "<div class='row'>" +
				"	<div class='col-md-12'>" +
				"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelUpdate(1)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
				"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateMeeting("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
				"	</div>" +
				"</div>";
	    	   $("#cancelButton1").append(cancel);
	    
	    }
	});
	
}
function loadMeetingDiv(div,date,company,summary,followUp,location,country){
	var readonly = "";
	var countryString = ""; 
	var countryID = "meeting_country_2";
	var companyString = "";
	var companyID = "meeting_company_2";
	var dte = "update_date";
	var sum = "update_summary";
	var flUp = "update_followUp";
	var address = "update_address";
	
	if(div == "loadMeeting"){
		dte = "d1"; sum = "s1"; flUp = "f1"; address = "a1";
		readonly = "readonly";
		countryID = "meeting_country_1";
		companyID = "meeting_company_1";
		companyString = "<input class='form-control' type='text' value='"+ company +"' "+ readonly+">";
		countryString = "<input class='form-control' type='text' value='"+ country +"' "+ readonly+">";
	}
	var str =   	   
	" 	<div class='form-group row'>" +
  	"		<label class='col-xs-2 col-form-label'>Event Date</label>" +
  	"		<div class='col-xs-10'>" +
    "			<input class='form-control' type='text' value="+ date +" "+ readonly+" id='"+dte+"'>" +
  	"		</div>" +
	"	</div>" +
	"	<div class='form-group row'>" +
  	"    	<label class='col-xs-2 col-form-label'>Company</label>" +
  	"		<div class='col-xs-10' id='"+companyID+"'>" + companyString +
    "		</div>" +
  	"	</div>" +
	"	<div class='form-group row'>" +
	"		<label class='col-xs-2 col-form-label'>Meeting Summary</label>" +
    "		<div class='col-xs-10'><textarea class='form-control' id='"+sum+"' "+ readonly+" rows='3'>"+ summary +"</textarea></div>" +
  	"	</div>" +
	"	<div class='form-group row'>" +
	"		<label class='col-xs-2 col-form-label'>To Follow-up</label>" +
    "		<div class='col-xs-10'><textarea class='form-control' id='"+flUp+"' "+ readonly+" rows='3'>"+ followUp +"</textarea></div>" +
  	"	</div>" +
  	"	<div class='form-group row'>" +
  	"    	<label class='col-xs-2 col-form-label'>Location [specific address]</label>" +
  	"		<div class='col-xs-10'>" +
    "			<input class='form-control'  value="+ location +" "+ readonly+" id='"+address+"'>" +
  	"    	</div>" +
  	"	</div>	" +
  	"	<div class='form-group row'>" +
  	"    	<label class='col-xs-2 col-form-label'>Location [Country]</label>" +
  	"		<div class='col-xs-10' id='"+countryID+"'>" +countryString +
    "		</div>" +
  	"	</div>";
	
	if(div == "upMeeting"){
		getCompanyListMeet("meeting_company_2","meeting_Comp","ind_location",company);
		getCountryListMeet("meeting_country_2","meetingBase","form-control",country);
	}
	console.log("halo");
	$("#"+div+"").append(str);
}

function updateMeeting(indID){
	console.log("**" + document.getElementById("update_date").value);
     $.ajax({
           url:"/market_observation/meeting/updateDetails",
          data: "id="+indID +
          		"&eventDate="+document.getElementById("update_date").value+
                "&company=" + document.getElementById("meeting_Comp").value+
                "&summary="+document.getElementById("update_summary").value+
                "&followUp="+document.getElementById("update_followUp").value+
                "&locAddress="+document.getElementById("update_address").value+
                "&locCountry="+document.getElementById("meetingBase").value,
           type: "POST",
          success: function (result) {
        	  console.log(result);
                 if(result.ok){
                     location.reload();
                 }
                 else{
                    console.log("error in saving");
                  }
              },
              error: function (result) {
                  console.log("saving failed");
              }
     });
}

function updateAttendees(indID){
	  $.ajax({
          url:"/market_observation/meeting/updateAttendees",
         data: "id="+indID +
         		"&attendees="+getConnectionAuthors(),
          type: "POST",
         success: function (result) {
       	  console.log(result);
                if(result.ok){
                    location.reload();
                }
                else{
                   console.log("error in saving");
                 }
             },
             error: function (result) {
                 console.log("saving failed");
             }
    });
}

function loadAttendees(authorAdd,authorValue,lastAuthorCount){
	var buttonID = authorAdd + 1;

	getIndividualList("update_attendees","authors_dropdownAuthors_"+ authorAdd,"IndConnectionsText_" + authorAdd, buttonID,authorValue,lastAuthorCount);
}

function viewList(){
	
	$("#meeting_action").empty();
	
	var str = "<br/><div class='row'><div class='col-xs-4'><input type='button' value='Back to List' onclick='getList()'></div></div>";
	
	  $("#meeting_action").append(str);
}

function getList(){
	$("#meeting_details").hide();
	$("#meeting_list").show();
}

function updateDetails(type){
	
	if(type == 1){
		$("#display_Meetings").hide();
		$(".updateMeetings").show();
	}else if(type == 2){
		$("#display_Attendees").hide();
		$(".updateAttendees").show();
	}
}

function cancelUpdate(type){
	
	if(type == 1){
		$("#display_Meetings").show();
		$(".updateMeetings").hide();
	}else if(type == 2){
		$("#display_Attendees").show();
		$(".updateAttendees").hide();
	}
}


