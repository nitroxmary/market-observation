/**
 * 
 */

$(document).ready(function(){
	
	displayLog("log-in");
	registration("register");
	
});

function displayLog(divName){
	
	var str =   "<div class='modal fade' id='userLogIn' role='dialog'>" + 
				"	<div class='modal-dialog'>"+
				"		<div class='modal-content'>"+
				"			<div class='modal-header'>"+
				"				<h4 class='modal-title'>Log In</h4>"+
				"			</div>"+
				"			<div class='modal-body'>" +
				"				<div class='form-group row'>"+                       
		  		"					<label class='col-xs-3 col-form-label'>User Name</label>"+
		  		"					<div class='col-xs-8'>"+
		    	"						<input class='form-control' type='text' id='user_name'>"+
		  		"					</div>"+
		  		"				</div>"+
		  		"				<div class='form-group row'>"+                       
		  		"					<label class='col-xs-3 col-form-label'>Password</label>"+
		  		"					<div class='col-xs-8'>"+
		    	"						<input class='form-control' type='password' id='user_pass'>"+
		  		"					</div>"+
		  		"				</div>"+
				"				<div class='form-group row'>"+                       
		  		"					<div class='col-xs-8'>"+
		    	"						<button type='button' class='btn btn-default' onclick='sendUser()' >LOGIN</button>"+
		  		"					</div>"+
		  		"				</div>"+
				"			</div>"+
				"			<div class='modal-footer'>"+
				"				<button type='button' class='btn btn-default' onclick='closeModal()' data-dismiss='modal'>Close</button>"+
				"			</div>"+
				"		</div>"+
				"	</div>"+
				"</div>";
	
	$("#"+ divName +"").append(str);
	$("#userLogIn").modal();
}

function closeModal(){
	window.location.href="/market_observation/";
}


function sendUser(){
	$.ajax({
	 	url:"/market_observation/user/admin",
	    type: "POST",
	    data: "uname="+document.getElementById("user_name").value+
    	  	  "&psword="+document.getElementById("user_pass").value,
	    success: function (result) {
	    	console.log(result);
	    	window.location.href="authUser";
	    }
	});
}

function registration(divName){
	var str =   "<div class='modal fade' id='userRegister' role='dialog'>" + 
				"	<div class='modal-dialog'>"+
				"		<div class='modal-content'>"+
				"			<div class='modal-header'>"+
				"				<h4 class='modal-title'>Register</h4>"+
				"			</div>"+
				"			<div class='modal-body'>" +
				"				<div class='form-group row'>"+                       
				"					<label class='col-xs-3 col-form-label'>User Name</label>"+
				"					<div class='col-xs-8'>"+
				"						<input class='form-control' type='text' id='user_name'>"+
				"					</div>"+
				"				</div>"+
				"				<div class='form-group row'>"+                       
				"					<label class='col-xs-3 col-form-label'>Password</label>"+
				"					<div class='col-xs-8'>"+
				"						<input class='form-control' type='password' id='user_pass'>"+
				"					</div>"+
				"				</div>"+
				" 				<div class='form-group row'>"+                       
				"					<label class='col-xs-3 col-form-label'>Confirm Password</label>"+
				"					<div class='col-xs-8'>"+
				"						<input class='form-control' type='password' id='user_cpass'>"+
				"					</div>"+
				"				</div>"+
				"				<div class='form-group row'>"+                       
				"					<div class='col-xs-8'>"+
				"						<button type='button' class='btn btn-default' onclick='registerUser()'>Register</button>"+
				"					</div>"+
				"				</div>"+
				"			</div>"+
				"			<div class='modal-footer'>"+
				"				<button type='button' class='btn btn-default' onclick='closeModal()' data-dismiss='modal'>Close</button>"+
				"			</div>"+
				"		</div>"+
				"	</div>"+
				"</div>";

$("#"+ divName +"").append(str);
$("#userRegister").modal();
}

function registerUser(){
	
	var pwrd = document.getElementById("user_pass").value;
	var cpwrd = document.getElementById("user_cpass").value;
	
	if (pwrd == cpwrd) {
		$.ajax({
		 	url:"/market_observation/user/sendUser",
		    type: "POST",
		    data: "uname="+document.getElementById("user_name").value+
	    	  	  "&psword="+cpwrd+
	    	      "&isAdmin=1",
		    success: function (result) {
		    	console.log(result);
		    	if(result.ok){
		    		console.log(result);
			    	sendUser();
		    	}else{
		    		alert(result.message);
		    	}
		    	
		    }
		});
    }
    else {
    	 document.getElementById("user_pass").style.borderColor = "#E34234";
         document.getElementById("user_cpass").style.borderColor = "#E34234";
         alert("Password Not Match!!!");
    }
	
}