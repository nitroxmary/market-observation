/**
 * 
 */
$(document).ready(function(){
	
	getCountryListInd("individualLocation","personBase","form-control",0);
	loadInvolvement("compInvolvement",1,0,0,0,0,0);
	loadEducation("education",1,0);
	loadAwardsList("awards",1,0);
	loadLangList("languages",1,0);
	loadNationList("nationalities",1,0,0);
	loadIndContacts("indContacts");
});

function getCountryListInd(divName, dropDownName,drpClass,textValue){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/countryList",
	    type: "GET",
        success: function (result) {
        	var str = "";

        	str += "<select class='"+drpClass+"' id='"+dropDownName+"'>";
			for (i=0;i<result.length;i++){
				if(result[i].id == textValue){
					str += "<option selected value='"+result[i].id+"'>"+result[i].name+"</option>";
				}else{
        			str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
				}
			 }
	        str +="</select>";
	        str +="</div>";
	        
    		$("#"+ divName +"").append(str);
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function getInputOtherInd(divName,dropDown,divInput,count){
	
	$("#"+divName+"").on('click', "#"+ dropDown +"", function(){
		if((this.value) == "OtherHolding" || (this.value) == "OtherComp" ){
	    	
	    	$("#"+dropDown+"").hide();
	    	$("#"+divInput+"").show();
	    
	    	loadNewCompany(divName,dropDown,count);
	    	loadContactDetails("company_contact");
	    	loadContactNumbers("company_phoneNumbers",0,0,0);
	    	getCountryList("companyLocation","mainCompanyBase",0,0,0);
	    }
	});
	
}

function getCompanyListInd(divName, dropDownName, newCompDiv, drpDownClass,count,textValue){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/companyList",
	    type: "GET",
        success: function (result) {
        	var str = "";
            
        	str +="<div id='"+ newCompDiv +"'></div>";
        	str += "<select class='"+drpDownClass+"' id='"+dropDownName+"'>";
        	str +="<option value='0'> </option>";
    			for (i=0;i<result.length;i++){
    				if(result[i].id == textValue){
	        			str += "<option selected value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}else{
	        			str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}
    			 }
    	    str +="<option value='OtherComp' class='OtherComp'>Input Other Company</option>"+
	        	  "</select>";
	        str +="</div>";
	        
    		$("#"+ divName +"").append(str);
    		$("#"+newCompDiv+"").hide();
    		
    		getInputOtherInd(divName,dropDownName,newCompDiv,count);
    	    
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function getHoldingsList(divName, dropDownName, newHoldingDiv, grpClassDiv,count,textValue){
	
	$.ajax({
	 	url: "/market_observation/individual/holdings",
	    type: "GET",
        success: function (result) {
        	var str = "";
            
        	str +="<div id='"+ newHoldingDiv +"'></div>";
        	str += "<select class='"+grpClassDiv+"' id='"+dropDownName+"'>";
        	str +="<option value='0'> </option>";
    			for (i=0;i<result.length;i++){
    				if(result[i].id == textValue){
	        			str += "<option selected value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}else{
	        			str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}
    			 }
    	    str +="<option value='OtherHolding' class='OtherHolding'>New Holding Company</option>"+
	        	  "</select>";
	        str +="</div>";
	        
    		$("#"+ divName +"").append(str);
    		$("#"+newHoldingDiv+"").hide();
    		
    		getInputOtherInd(divName,dropDownName,newHoldingDiv,count);
    	    
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function cancelNewAddition(divName,dropDown,forRemoval){
	//$("#"+forRemoval+"").remove(); 
	$("#"+dropDown+"").show()
	$("#"+dropDown+"").val(0);
}

function loadInvolvement(mainDiv,count,compBase,hdQrtr,comp,grpComp,lastNum){
  
    var str ="";
    var type = 6;
    var divName = "ind_involve_div_" + count;
	var ddGrpCompany = "ind_compGrp_dropDown_" + count;
	var ddCmpBase = "ind_compBase_dropDown_" + count;
	var ddCompany = "ind_comp_dropDown_" + count;
	var ddHeadQrtr = "ind_compQrtr_dropDown_" + count;
	$("#"+divName+"").remove();
	
        str += "<div class='form-group row' id='"+divName+"'><br/>" +
               "<div class='col-md-12'>" +
               "<div class='col-md-6'>" +
               "  	<label class='col-md-2 col-form-label'>"+count+".Company</label>" +
               "    <input type='hidden' class='form-control comp_id' value = '0' id='comp_id_"+count+"'>" +
               "    <div class='col-md-10'><div id='companyDiv_"+count+"'></div></div>"+
               "</div>" +
               "<div class='col-md-6'> " +
               "  	<label class='col-md-2 col-form-label'>Holding</label>" +
               "    <div class='col-md-10'><div id='grpCompDiv_"+count+"'></div></div>"+
               "</div></div>" +
               "<div class='col-md-12'>" +
               "<div class='col-md-6'>" +
               "	<label class='col-md-2 col-form-label'>Title</label>" +
               "    <div class='col-md-10'><input type='text' class='form-control' id='comp_title_"+count+"'></div>" +
               "</div>"+
               "<div class='col-md-6'>" +
               " 	<label class='col-md-2 col-form-label'>Active</label>" +
               "    <div class='col-md-10'><select class='form-control' id='comp_active_"+count+"'>" +
               "         <option value='1'>yes</option><option value='0'>no</option></select>" +
               "    </div>" +
               "</div></div>"+
               "<div class='col-md-12'>" +
               "<div class='col-md-6'>" +
               "	<label class='col-md-2 col-form-label'>Country</label>" +
               "	<div class='col-md-10'><div id='compBaseDiv_"+count+"'></div></div>" +
               "</div>"+
               "<div class='col-md-6'>" +
               "	<label class='col-md-2 col-form-label'>City</label>" +
               "	<div class='col-md-10'><input class='form-control ind_bCity' type='text' id='comp_bCity_"+count+"'></div>" +
               "</div></div>"+
               "<div class='col-md-12'>" +
               "<div class='col-md-6'>" +
               "	<label class='col-md-2 col-form-label'>Headquarter</label>" +
               "    <div class='col-md-10'><div id='headQrtrDiv_"+count+"'></div></div>"+
               "</div>";
        if(mainDiv == "update_involvement"){
        	str +=  "<div class='col-md-6'><div class='col-md-6 pull-right'>";
        }else{
        	str +=  "<div class='col-md-6'><div class='col-md-4 pull-right'>";
        }
              
        if(lastNum == 0 || lastNum == count){
        	if(mainDiv == "update_involvement"){
        		str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additionalUpdate("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
            }else{
        		str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additional("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
            }
        }
		if(lastNum < count){
			str += "<button type='button' id='cancel_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='cancelAdditional("+count+","+type+")'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
		}
		str +="</div></div></div>";
		
    getCountryListInd("compBaseDiv_"+count,ddCmpBase,"form-control",compBase); 
    getCountryListInd("headQrtrDiv_"+count,ddHeadQrtr,"form-control",hdQrtr); 
    getCompanyListInd("companyDiv_"+count,ddCompany,"newCompanyInd", "form-control ind_company",count,comp);
    getHoldingsList("grpCompDiv_"+count, ddGrpCompany, "newHoldings", "form-control",count,grpComp);
    
    $("#"+mainDiv+"").append(str);       
}

function loadEducation(mainDiv,count,lastNum){
	var str ="";
	var type =7;
	var divName = "ind_educ_div_" + count;
	
	str += "<div class='row' id='"+divName+"'>" +
	       "<div class='col-md-12'>" +
	       "	<div class='col-md-4'>" +
	       "		<label class='col-md-3 col-form-label'>" + count + ".Discipline</label>" +
	       "        <input type='hidden' class='form-control educ_id' value = '0' id='educ_id_"+count+"'>" +
	       "    	<div class='col-md-8'><input type='text' class='form-control educ_disc' id='educ_disc_"+count+"'></div>" +
	       "	</div>" +
	       "	<div class='col-md-4'>" +
	       "    	<label class='col-md-3 col-form-label'>Degree </label>" +
	       "    	<div class='col-md-8'><input type='text' class='form-control educ_degree' id='educ_degree_"+count+"'></div>" +
	       "	</div>" +
	       "	<div class='col-md-4'>" +
	       "   		<label class='col-md-3 col-form-label'>University</label>" +
	       "    	<div class='col-md-8'><input type='text' class='form-control educ_univ' id='educ_univ_"+count+"'></div>" +
	       "	</div>" +
	       "</div>" +
	       "<div class='col-md-12'><div class='col-md-10'></div><div class='pull-right col-md-2'>";
	if(lastNum == 0 || lastNum == count){
		if(mainDiv == "update_education"){
    		str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additionalUpdate("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
        }else{
        	str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additional("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
        }
	}
	if(lastNum < count){
		str += "<button type='button' id='cancel_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='cancelAdditional("+count+","+type+")'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
	}
	str +="</div></div></div>";
		
	$("#"+mainDiv+"").append(str);  
}

function loadNationList(mainDiv,count,textValue,lastNum){

	var type = 4;
	var divName = "ind_nation_div_" + count;
	var dropDownName = "ind_nation_dropDown_" + count;
	var str = "<div class='row' id='"+divName+"'>" +
			  "	<div class='col-md-12'>" +
			  "     <div class='col-md-6'>" +
			  "			<div class='col-md-1'>" + count + ".</div>" +
			  "         <input type='hidden' class='form-control nation_id' value = '0' id='nation_id_"+count+"'>" +
			  "     	<div class='col-xs-10'><div id='"+divName+"_"+count+"'></div></div>" +
			  "		</div>" ;
		if(mainDiv == "update_nation"){
			str += "<div class='col-md-6'><div class='pull-right col-md-6'>";
		}else{
			str += "<div class='col-md-6'><div class='pull-right col-md-4'>";
		}
		if(lastNum == 0 || lastNum == count){
			if(mainDiv == "update_nation"){
	    		str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additionalUpdate("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
	        }else{
	    	    str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additional("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
	        }
	    }
		if(lastNum < count){
			str += "<button type='button' id='cancel_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='cancelAdditional("+count+","+type+")'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
		}
		str +="</div></div></div></div>";
	
		getCountryListInd(divName+"_"+count,dropDownName,"form-control ind_nationBase",textValue);
	$("#"+mainDiv+"").append(str);
}

function loadLangList(mainDiv,count,lastNum){
	var type = 5;
	var divName = "ind_lang_div_" + count;
	var str = "<div class='row' id='"+divName+"'>" +
			  "<div class='col-md-12'>" +
			  "    <div class='col-md-6'>" +
			  "		   <div class='col-md-1'>" + count + ".</div>" +
			  "         <input type='hidden' class='form-control lang_id' value = '0' id='lang_id_"+count+"'>" +
			  "		    <div class='col-md-10'><input type='text' class='form-control lang_name' id='lang_name_"+count+"'></div>" +
			  "    </div>" ;
	    if(mainDiv == "update_lang"){
	    	str += "<div class='col-md-6'><div class='pull-right col-md-6'>";
	    }else{
	    	str += "<div class='col-md-6'><div class='pull-right col-md-4'>";
	    }
		if(lastNum == 0 || lastNum == count){
			if(mainDiv == "update_lang"){
	    		str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additionalUpdate("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
	        }else{
	    	    str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additional("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
		    }
		}
		if(lastNum < count){
			str += "<button type='button' id='cancel_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='cancelAdditional("+count+","+type+")'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
		}
		str +="</div></div></div>";
	
	$("#"+mainDiv+"").append(str);

}

function loadAwardsList(mainDiv,count,lastNum){
	var type = 8;
	var divName = "ind_awards_div_" + count;
	var str = "<div class='row' id='"+divName+"'>";
		str += "<div class='col-md-12'>" +
			   "	<div class='col-md-4'>" +
			   "		<div class='col-md-1'>" + count + ".</div>" +
			   "        <div class='col-md-10'>" +
			   "<input type='hidden' class='form-control awards_id' value = '0' id='awards_id_"+count+"'>" +
			   "<input type='text' class='form-control awards_name' id='awards_name_"+count+"'></div>" +
		       "    </div>" +
		       "    <div class='col-md-8'> " +
		       "        <div class='col-md-1'>At</div>" +
		       "        <div class='col-md-8'><input type='text' class='form-control awards_location' id='awards_loc_"+count+"'></div>" +
		       "        <div class='pull-right col-md-3'>";
		if(lastNum == 0 || lastNum == count){
			if(mainDiv == "update_awards"){
	    		str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additionalUpdate("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
	        }else{
	        	str += "<button type='button' id='add_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='additional("+count+","+type+")'><span class='glyphicon glyphicon-plus'></span>Add</button>";
	        }
	    }
		if(lastNum < count){
			str += "<button type='button' id='cancel_"+ type +"_"+count+"' class='btn btn-default btn-sm' onclick='cancelAdditional("+count+","+type+")'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
		}
		str +="</div></div></div>";
	
  	$("#"+mainDiv+"").append(str);
}

function loadIndContacts(mainDiv){
	
	var divName = "ind_contacts_div";
	var str = "<div id='"+divName+"'>";
	    str += "<div id='individualContacts'></div>" + 
	           "<div id='individualNumbers'></div>"; 
		str += "</div>";
	    
		$("#"+mainDiv+"").append(str);
		
	loadContactDetails("individualContacts");
	loadContactNumbers("individualNumbers",0,0,0);
}

function additional(count,type){
	var buttonId = type+"_"+ count;
	count++;
	
	if(type == 4){
		loadNationList("nationalities",count,0,0);
	}else if(type == 5){
		loadLangList("languages",count,0);
	}else if(type == 6){
		loadInvolvement("compInvolvement",count,0,0,0,0,0);
	}else if(type == 7){
		loadEducation("education",count,0);
	}else if(type == 8){
		loadAwardsList("awards",count,0);
	}
	
	$("#add_"+buttonId+"").hide();
	$("#cancel_"+buttonId+"").hide(); 
}

function cancelAdditional(count,type){

	if(type == 4){
		$("#ind_nation_div_"+ count +"").remove();
	}else if(type == 5){
		$("#ind_lang_div_"+ count +"").remove();
	}else if(type == 6){
		$("#ind_involve_div_"+ count +"").remove();
	}else if(type == 7){
		$("#ind_educ_div_"+ count +"").remove();
	}else if(type == 8){
		$("#ind_awards_div_"+ count +"").remove();
	}
	
	count--;
	
	$("#add_"+ type +"_"+ count +"").show();
	$("#cancel_"+ type +"_"+ count +"").show();  
}


function newIndividual(){
	getPhoneNum();
	$.ajax({
	 	url:"/market_observation/individual/data",
	    data:  "firstName="+document.getElementById("first_name").value+
			  "&lastName="+document.getElementById("last_name").value +
			  "&middleInitial="+document.getElementById("middle_initial").value+
			  "&personBase="+document.getElementById("personBase").value+
			  "&address1="+document.getElementById(("ind_addr1")).value+
			  "&address2="+document.getElementById(("ind_addr2")).value+
			  "&address3="+document.getElementById(("ind_addr3")).value+
			  "&email="+document.getElementById(("ind_email")).value+
			  "&linkedIn="+document.getElementById(("ind_linkedIn")).value+
			  "&skype="+document.getElementById(("ind_skype")).value +
			  "&youtube="+document.getElementById(("ind_youtube")).value+
			  "&twitter="+document.getElementById(("ind_twitter")).value+
			  "&compInvolvement="+getInstitutionList(0,0)+
			  "&educBackground="+getEducBckgrndList()+
			  "&awards="+getAwardsList()+ 
			  "&nationalities="+getNationList()+
			  "&languages="+getLanguagesList() +
			  "&phones="+getPhoneNum(),
	    type: "POST",
	    success: function (result) {
	           if(result.ok){
	        	   
	        	     location.reload();
	        	   //console.log(result);
	        	 }
	           else{
        		  console.log("error in updating db");
	        	}
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
    });
}

function getInstitutionList(divID,mainDiv){
	console.log(divID+"---"+mainDiv);
	var totalCompany = $(".ind_company").length;
	var institutions = new Array();
	
    for(i = 0; i < totalCompany; i++){
		
		var compareDiv1 = "companyDiv_"+ (i+1);
		var compareDiv2 = "grpCompDiv_"+ (i+1);
		var involve = "ind_involve_div_" + (i+1);
		
		var company = document.getElementById("ind_comp_dropDown_"+ (i+1)).value;
		var holding = document.getElementById("ind_compGrp_dropDown_"+ (i+1)).value;
		var title = document.getElementById("comp_title_"+ (i+1)).value;	
		var active = document.getElementById("comp_active_"+ (i+1)).value;
		var country = document.getElementById("ind_compBase_dropDown_"+ (i+1)).value;
		var city = document.getElementById("comp_bCity_"+ (i+1)).value;
		var headQ = document.getElementById("ind_compQrtr_dropDown_"+ (i+1)).value;
		var id = document.getElementById("comp_id_"+ (i+1)).value;
		
		if(mainDiv == compareDiv1){
			institutions[i] = mainDiv + "-" +divID+ "-" +holding+ "-"+title+ "-" +active+ "-" +country+ "-"+city + "-" + headQ+ "-" + id;
		}else if(mainDiv == compareDiv2){
			institutions[i] = mainDiv + "-" +company+ "-" +divID+ "-"+title+ "-" +active+ "-" +country+ "-"+city + "-" + headQ+ "-" + id;
		}else{
			institutions[i] = involve + "-" +company+ "-" +holding+ "-"+title+ "-" +active+ "-" +country+ "-"+city + "-" + headQ+ "-" + id;
		}
	}
   console.log(institutions);
    return institutions;
}

function getEducBckgrndList(){
	var totalEdBackground = $(".educ_disc").length;
	var education = new Array();
	
	for(i = 0; i < totalEdBackground; i++){
		
		var compareDiv = "ind_educ_div_"+ (i+1);
		var disc = document.getElementById("educ_disc_"+ (i+1)).value;
		var degree = document.getElementById("educ_degree_"+ (i+1)).value;
		var univ = document.getElementById("educ_univ_"+ (i+1)).value;	
		var id = document.getElementById("educ_id_"+ (i+1)).value;	
		
		education[i] = compareDiv + "-" +disc+ "-" +degree+ "-"+univ+ "-"+id;
	}
	return education;
}

function getAwardsList(){
	var totalAwards = $(".awards_name").length;
	var awards = new Array(); 
	
	for(i = 0; i < totalAwards; i++){
		var mainDiv = "ind_awards_div_"+(i+1);
    	var aName = document.getElementById("awards_name_"+(i+1)).value;
    	var aLoc = document.getElementById("awards_loc_"+(i+1)).value;
    	var id = document.getElementById("awards_id_"+(i+1)).value;
    	awards[i] = mainDiv + "-" + aName + "-" + aLoc + "-" + id;
	}
	return awards;
}

function getNationList(){
	var totalNation = $(".ind_nationBase").length;
	var nationalities = new Array(); 
	
	for(i = 0; i < totalNation; i++){
		var mainDiv = "ind_nation_div_"+(i+1);
    	var nation = document.getElementById("ind_nation_dropDown_"+(i+1)).value;
    	var id = document.getElementById("nation_id_"+(i+1)).value;
    	nationalities[i] = mainDiv + "-" + nation + "-" + id;
	}
	return nationalities;
}


function getLanguagesList(){
    var totalName = $(".lang_name").length;
    var language = new Array(); 
    
    for(i = 0; i < totalName; i++ ){
    	var mainDiv = "ind_lang_div_"+(i+1);
    	var lang = document.getElementById("lang_name_"+(i+1)).value;
    	var id = document.getElementById("lang_id_"+(i+1)).value;
    	language[i] = mainDiv + "-" + lang+"-" + id;
    }
    return language;
}

function getPhoneNum(){
	var totalPhone = $(".ind_additionalPhones").length;
	var numbers = new Array();
	
	for(i = 0; i < totalPhone; i++){
		var mainDiv = "morePhones_3_"+i;
		var pType = document.getElementById("ind_phone_type_"+i).value;
		var cCode = document.getElementById("ind_phone_3_loc_"+i).value;
		var aCode = document.getElementById("ind_phone_area_"+i).value;
		var pNum = document.getElementById("ind_phone_num_"+i).value;
		var id = document.getElementById("ind_phone_id_"+i).value;
		
		numbers[i] = mainDiv + "-" + pType + "-" + cCode + "-" +aCode + "-" +pNum + "-" +id;
	}
	return numbers;
}

function getData(divID,mainDiv){
	
	var splitCompany = mainDiv.split("_");
	var count = splitCompany[1];
	var totalCompany = $(".ind_company").length;
    var compareDiv1 = "companyDiv_"+ count;
	var compareDiv2 = "grpCompDiv_"+ count;
	var title = document.getElementById("comp_title_"+ count).value;
	var active = document.getElementById("comp_active_"+ count).value;
	var city = document.getElementById("comp_bCity_"+ count).value;
    var country = document.getElementById("ind_compBase_dropDown_"+ count).value;
	var headQ = document.getElementById("ind_compQrtr_dropDown_"+ count).value;
	var company = "";
    var holding = "";
    
    $(".modal").modal("hide");
	$(".modal-backdrop").hide();
	$('html, body').css({
	    'overflow': 'auto',
	    'height': 'auto'
	});
	
	if(mainDiv == compareDiv1){
		 company = divID;
		 holding = document.getElementById("ind_compGrp_dropDown_"+ count).value;
	}else if(mainDiv == compareDiv2){
		 company = document.getElementById("ind_comp_dropDown_"+ count).value;
		 holding = divID;
	}	
	
	loadInvolvement("compInvolvement",count,country,headQ,company,holding,totalCompany);
	
	document.getElementById("comp_title_" + count).value = title;
	document.getElementById("comp_active_" + count).value = active;
	document.getElementById("comp_bCity_" + count).value = city;
} 

