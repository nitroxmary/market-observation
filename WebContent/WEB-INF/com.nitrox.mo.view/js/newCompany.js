/**
 * 
 */

$(document).ready(function(){
	loadCompany("company_details",0,0,0,0,0);
	loadCompOptions("company_options");
	loadFiles("company_files",0,0,0,"","","","","");
	loadCompShareholders("company_shreHolders",0,0,0,0,0,1);
	loadContacts("company_contacts");
	loadContactDetails("company_contact");
	loadContactNumbers("company_phoneNumbers",0,"main",0);
		
});

function loadCompOptions(mainDiv){
	var str = "<div id='option1'>" +
			   "<div class='row'>" +
			   "	<label class='col-xs-6 col-form-label'>Does this company sell a risk management and/or ALM module?</label>" +
			   "    <div class='col-xs-6'>" +
			   "		<input type='radio' onclick='sellCheck(this.value)' name='sellOption' value='yes'>&nbsp;&nbsp;yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			   "		<input type='radio' onclick='sellCheck(this.value)' name='sellOption' value='no' checked >&nbsp;&nbsp;no" +
			   "	</div>" +
			   "</div>" +
			   "<div id='companyDescription'>" +
			   "	<div class='col-xs-8'>" +
			   "		<input class='form-control' value='' id='comp_sellOptionName_0' type='text'>" +
			   "	</div>"+
			   "<hr/>" +
			   "</div>" +
			  "</div>" +
			  "<div id='option2'>" +
			  "<div class='row'>" +
			   "	<label class='col-xs-6 col-form-label'>Does it use a risk management and/or ALM module?</label>" +
			   "    <div class='col-xs-6'>" +
			   "		<input type='radio' onclick='useCheck(this.value)' name='useOption' value='yes'>&nbsp;&nbsp;yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			   "	    <input type='radio' onclick='useCheck(this.value)' name='useOption' value='no' checked>&nbsp;&nbsp;no" +
			   "	</div>" +
			   "</div>" +
			   "<div id='companyOffers'>" +
			   "	<div class='col-xs-8'>" +
			   "		<input class='form-control' value='' id='comp_uselOptionName' type='text'>" +
			   "	</div>"+
			   "<hr/></div>" +
			  "</div>";
	
	$("#"+mainDiv+"").append(str);
	$("#companyDescription").hide();
	$("#companyOffers").hide();
}

function getInputOtherProv(divName,dropDown,divInput,count){
	
	$("#"+divName+"").on('click', "#"+ dropDown +"", function(){
	
	    if ((this.value) == "OtherComp" ){
	    	
	    	$("#"+dropDown+"").hide();
	    	$("#"+divInput+"").show();
	    	loadNewCompany(divName,dropDown,count);
	    	loadContactDetails("company_contact");
	    	loadContactNumbers("company_phoneNumbers",0,0);
	    	getCountryList("companyLocation","mainCompanyBase",0,0,0);
	    }
	});
	
}

function getCompanyProvider(divName,newCompDiv,dropDownName,textValue,readOnly){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/companyList",
	    type: "GET",
        success: function (result) {
        	var str = "";
            
        	str +="<div id='"+newCompDiv+"'></div>";
        	if(readOnly == "readOnly"){
        		for (i=0;i<result.length;i++){
					if(result[i].id == textValue){
	        			str += "<input type='text' class='form-control' "+readOnly+" value='"+result[i].name+"'>";
	        		}
    			 }
        	}else{
	        	str += "<select class='form-control' id='"+dropDownName+"'>";
	        	str +="<option value='0'> </option>";
	    			for (i=0;i<result.length;i++){
						if(result[i].id == textValue){
		        			str += "<option selected value='"+result[i].id+"'>"+result[i].name+"</option>";
		        		}else{
		        			str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
		        		}
	    			 }
	    	    str +="<option value='OtherComp' class='OtherComp'>Input Other Company</option>"+
		        	  "</select>";
        	}
	        str +="</div>";
	        
    		$("#"+ divName +"").append(str);
    		$("#"+newCompDiv+"").hide();
    		
    		getInputOtherProv(divName,dropDownName,newCompDiv);
    	    
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function loadCompDesc(mainDiv,count, nameValue, descValue,readOnly){
	
	var str = "<div id ='moreDivs_111_"+count+"' class='almSeller'>"+
		       " <label class='col-xs-3'>"+(count+1)+". Application Name </label>" +
			   "    <div class='col-xs-8'>" +
			   "		<input class='form-control' "+readOnly+"  value='"+nameValue+"' id='"+readOnly+"comp_sellOptionName_"+count+"' type='text'>" +
			   "	</div><br/><br/>" +
			   "	<label class='col-xs-3'>Description </label>" +
			   "    <div class='col-xs-8'>" +
			   "		 <textarea class='form-control' "+readOnly+" id='"+readOnly+"comp_sellOptionDesc_"+count+"' rows='3'>"+descValue+"</textarea><br/>" +
			   "	</div>";
	
	if(mainDiv != "view_seller"){
		str +="	<div class='row'><div class='col-md-8'></div><div class='pull-right col-md-3'>"+
		  "		<button type='button' id='add_111_"+count+"' class='btn btn-default btn-sm' onclick='additional(\""+mainDiv+"\","+count+",111)'><span class='glyphicon glyphicon-plus'></span>Add</button>";
		
		if(0 < count){
			str += "<button type='button' id='cancel_111_"+count+"' class='btn btn-default btn-sm' onclick='cancelAdditional("+count+",111)'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
		}

	}
	
	
	str +="</div></div></div>";
	$("#"+mainDiv+"").append(str);
}

function loadOffers(mainDiv, nameValue, compValue, readOnly){ 
	var str = "<label class='col-xs-3'>Application Name </label>" +
	   "    <div class='col-xs-8'>" +
	   "		<input class='form-control' "+readOnly+" value='"+nameValue+"' id='"+readOnly+"comp_uselOptionName' type='text'>" +
	   "	</div><br/><br/>" +
	   "	<label class='col-xs-3'>Provider </label>" +
	   "    <div class='col-xs-8'>" +
	   "		<div id='almProvider'></div>" +
	   "	</div>";
	$("#"+mainDiv+"").append(str);
	
	if(compValue != 0){
		getCompanyProvider("almProvider","newCompProv",readOnly+"comp_uselOptionfrom",compValue, readOnly);
	}else{
		getCompanyProvider("almProvider","newCompProv",readOnly+"comp_uselOptionfrom",0,readOnly);
	}
}

function sellCheck(optionValue){
	if(optionValue == "yes"){
		$("#companyDescription").empty();
		$("#companyDescription").show();
		loadCompDesc("companyDescription",0,"","","");
	}else{
		$("#companyDescription").empty();
	}
}

function useCheck(optionValue){
	if(optionValue == "yes"){
		$("#companyOffers").empty();
		$("#companyOffers").show();
		loadOffers("companyOffers","",0,"");
	}else{
		$("#companyOffers").empty();
	}
}

function loadCompShareholders(mainDiv,count,compID,type,valueID,percentage,totalCompShares){
	var countAdd = count + 1;
	var readOnly = "";

	if(mainDiv == "view_shareHolders"){
		readOnly = "readOnly";
	}
	
	var str ="<div id ='moreDivs_1_"+count+"'>"+
	    "<div class='row'>"+     
        "	<input type='hidden' class='comp_shares_id' value = '"+compID+"' id='"+readOnly+"comp_shares_id"+count+"'>" +
        "	<label class='col-xs-2 col-form-label'>Shareholder Type </label>"+
		"	<div class='col-xs-3 csHolderType' id='csHolderType_"+mainDiv+"_"+countAdd+"'>";
	if(mainDiv == "company_shreHolders"){

	  str +="		<select id='shrHolder_type_"+count+"'  onchange='showMe(\""+mainDiv+"\",this.value,"+countAdd+",\""+readOnly+"\");' class='form-control'>" +
	 	    "			<option value='1'>company</option>" +
	 	    "			<option value='2'>individual</option>" +
	 	    "		</select>";
	}else if(mainDiv == "update_shareHolders"){
		var indType = "";var compType = "";
		
		if(type == 1){compType = "selected";}
		if(type == 2){indType = "selected";}
		
		  str +="		<select id='shrHolder_type_"+count+"'  onchange='showMe(\""+mainDiv+"\",this.value,"+countAdd+",\""+readOnly+"\");' class='form-control'>" +
		 	    "			<option "+compType+" value='1'>company</option>" +
		 	    "			<option "+indType+" value='2'>individual</option>" +
		 	    "		</select>";
		}else{
	  str +="<input class='form-control' "+readOnly+"  value='"+getShareType(type)+"' type='text' id='"+readOnly+"shrHolder_type_"+count+"'>";	
	}
		
	str +="	</div>"+
		"	<label class='col-xs-2 col-form-label'>Percentage ownership </label>"+
		"	<div class='col-xs-4'>"+
 	    "		<input class='form-control'  "+readOnly+"  value='"+percentage+"' type='text' id='"+readOnly+"percentage_"+count+"'>"+
 	    "	</div>"+
		" </div><div class='row' id='shareholder_"+mainDiv+"_"+countAdd+"'></div>";
	if(mainDiv == "company_shreHolders" || mainDiv == "update_shareHolders"){
	str +="	<div class='col-md-9'></div><div class='pull-right col-md-3'>";
		if(totalCompShares > count && mainDiv == "company_shreHolders"){
			str +="	<button type='button' id='add_1_"+count+"' class='btn btn-default btn-sm' onclick='additional(\""+mainDiv+"\","+count+",1)'><span class='glyphicon glyphicon-plus'></span>Add shareholder</button>";
		}else if(totalCompShares == (count+1) && mainDiv == "update_shareHolders"){
			str +="	<button type='button' id='add_1_"+count+"' class='btn btn-default btn-sm' onclick='additional(\""+mainDiv+"\","+count+",1)'><span class='glyphicon glyphicon-plus'></span>Add shareholder</button>";
		}
		if(0 < count){
	    	console.log("count " + count);
			str += "<button type='button' id='cancel_1_"+count+"' class='btn btn-default btn-sm' onclick='cancelAdditional("+count+",1)'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
		}
	
		str +="</div></br>";
    }
	str +="</div>";
	
	$("#"+mainDiv+"").append(str);
	
	showHideOther(mainDiv,type,valueID,count,readOnly);
	
}

function showHideOther(mainDiv,type,valueID,count,readOnly){
	
    if (type == 2) {
    	loadShareHoldersInd(mainDiv,count,valueID,type,readOnly);  
    } else {
    	loadShareHoldersCompanies(mainDiv,count,valueID,type,readOnly);
    }
}

function showMe(mainDiv,val,count,readOnly){
	
	$("#shareholder_"+mainDiv+"_"+count).empty();
	
	$("div#csHolderType_"+mainDiv+"_"+(count)+" select").val(val);
	showHideOther(mainDiv,val,0,count-1,readOnly);
}

function loadShareHoldersCompanies(mainDiv,companyAdd,companyValue,connectionType,readOnly){

	var buttonID = companyAdd+1;
	getShareHoldersList("shareholder_"+mainDiv+"_"+buttonID,readOnly+"shareHolderCompany_"+ companyAdd,"CompConnectionsText_" + companyAdd,buttonID, companyValue,connectionType,readOnly);
}

function getShareHoldersList(divName,dropDownName,newCompanyDiv,count,textValue,type,readOnly){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/companyList",
	    type: "GET",
        success: function (result) {
        	var appendToDiv = "moreCompanies_"+count;
        	var str = "<div id='"+appendToDiv+"'>";
        	var indID = 0;
        	
        
    		newCompanyDiv = "newShareHolders";
    		str +=  "<div class='col-xs-2'><span class='countCompanies'>"+count+". </span>" +
		            "<input type='hidden' class='form-control ms_comp_id' value = '"+indID+"' id='ms_comp_id_"+count+"'>" +
		            "<label class='col-form-label'>Select Company</label></div>"+
		            "<div class='col-xs-9'>";
            if(readOnly == "readOnly"){
        		for (i=0;i<result.length;i++){
    				if(textValue == result[i].id ){
    					str +=  "<input class='form-control' type='text' readOnly value='"+result[i].name+"'>";
					}
				}
        	}
        	else{
        		str +=  "<select class='form-control' required id='"+dropDownName+"'>"+
    		        "<option value='0'> </option>";
    			
	        	for (i=0;i<result.length;i++){
	        		if(result[i].id == textValue){
	        			str += "<option selected value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}else{
	        			str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}
		        }
	        	
		        str +="<option value='otherComp' class='OtherCompany'>Input Other Company</option>"+
		        	  "</select>";
        	}
            str +="</div></div>";
    		    		
    		$("#"+ divName +"").append(str);
    		$("#"+newCompanyDiv+"").hide();
    		
    		getInputOtherType(appendToDiv,dropDownName,newCompanyDiv,count);
    		
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function getInputOtherType(divName,dropDown,divInput,count){
	
	$("#"+divName+"").on('click', "#"+ dropDown +"", function(){
	    if ((this.value) == "otherComp") {
	    	
	    	$("#"+dropDown+"").hide();
	    	$("#connectedFor_"+count+"").hide();
	    	$("#company_span_"+count+"").hide();
	    	$("#buttons_comp_connection_" +count + "").hide();
	    	$("#newMainContact").show();
	    	loadNewCompany(divName,dropDown,count);
	    	loadContactDetails("company_contact");
	    	loadContactNumbers("company_phoneNumbers",0,"main",0);
	    	getCountryList("companyLocation","mainCompanyBase",0,0,0);
	    
	    }else if ((this.value) == "otherInd") {
		    	
	    	$("#"+dropDown+"").hide();
	    	$("#connectedBy_"+count+"").hide();
	    	$("#author_span_"+count+"").hide();
	    	$("#buttons_authors_connection_" + count+"").hide();
	    	loadNewAuthor(divName,dropDown,count);
	    	getCountryList("individualLocation","authorBase",0,0,0);
		 }
	});
	
	$("#"+divInput+"").dblclick(function(){
		$("#"+divInput+"").hide();
		$("#"+dropDown+"").show();
		$("#"+dropDown+"").val(companyDefaultValue);
	});
}

function loadNewAuthor(mainDiv,dropDown,count){
	var str = "<div class='modal fade' id='signalAuthorModal' role='dialog'>" + 
	          "<div class='modal-dialog'>"+
              "<div class='modal-content'>"+
              "<div class='modal-header'>"+
              "<button type='button' class='close' data-dismiss='modal'>&times;</button>"+
              "<h4 class='modal-title'>New Author</h4>"+
              "</div>"+
              "<div class='modal-body'>";
	str += "<div id='newAuthor'>"+
	 	"		    <div class='form-group row'>"+                       
  		"				<label class='col-xs-3 col-form-label'>First Name</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<input class='form-control' type='text' id='first_name'>"+
  		"				</div>"+
  		"			</div>"+
	 	"		    <div class='form-group row'>"+
  		"				<label class='col-xs-3 col-form-label'>Last Name</label>"+
  		"				<div class='col-xs-8'>"+
  		"					<input class='form-control' type='text' id='last_name'>"+
  		"				</div>"+
  		"			</div>"+
  		"		    <div class='form-group row'>"+                       
  		"				<label class='col-xs-3 col-form-label'>Middle Initial</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<input class='form-control'  maxlength='1' type='text' id='middle_initial'>"+
  		"				</div>"+
  		"			</div>"+
	 	"		   <div class='form-group row'>"+
  		"				<label class='col-xs-3 col-form-label'>Country/Location</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<div id='individualLocation'></div>"+
  		"				</div>"+
  		"			</div>"+
	 	"		    <button onClick='newIndividual(\""+mainDiv+"\")' id='individualButton' class='btn'>Insert Individual</button>"+
//	 	"		 <button onClick='cancelNewAddition(\""+mainDiv+"\",\""+dropDown+"\",\"newAuthor\",\""+count+"\")' id='cancelButton' class='btn'>Cancel</button>"+
	    "		 </div>" +
	    "</div>"+
        "<div class='modal-footer'>"+
        "<button type='button' class='btn btn-default' onClick='cancelNewAddition(\""+mainDiv+"\",\""+dropDown+"\",\"newAuthor\",\""+count+"\")' data-dismiss='modal'>Close</button>"+
        "</div>"+
        "</div>"+
        "</div>"+
        "</div>";
	$("#"+mainDiv+"").append(str);
	$("#signalAuthorModal").modal();
}

function loadShareHoldersInd(mainDiv,authorAdd,authorValue,connectionType,readOnly){
	var buttonID = authorAdd+1;
	getShareHoldersIndList("shareholder_"+mainDiv+"_"+buttonID,readOnly+"sharHolderIndividual_"+ authorAdd,"IndConnectionsText_" + authorAdd, buttonID,authorValue,connectionType,readOnly);
}

function getShareHoldersIndList(divName, dropDownName,newAuthorDiv,count,textValue,type,readOnly){
    
    $.ajax({
	 	url: "/market_observation/marketSignal/individualList",
	    type: "GET",
        success: function (result) {
        	
        	var str = "";
        	var appendToDiv = "moreAuthors_"+count;
        	var str = "<div id='"+appendToDiv+"'>";
        	var indID = 0;
        	
       	    
        	str += "<div class='col-xs-2'><span class='countAuthors'>"+count+". </span>" +
    			   "<input type='hidden' class='form-control ms_ind_id' value = '"+indID+"' id='ms_ind_id_"+count+"'>" +
    			   "<label class='col-form-label'>Select Individual</label></div>"+
    			   "<div id='"+ newAuthorDiv +"'></div>"+
    			   "<div class='col-xs-9'>";
        
    		if(readOnly == "readOnly"){
        		for (i=0;i<result.length;i++){
    				if(textValue == result[i].id ){
    					str +=  "<input class='form-control' type='text' readOnly value='"+result[i].firstName+ " " + result[i].middleInitial+ " " + result[i].lastName+"'>";
					}
				}
        	}
        	else{
        		 str +="<select class='connected_authors form-control' id='"+dropDownName+"'>" +
	    				"<option value='0'> </option>";
	    		
	    		if(result.length != 0){
	    			for (i=0;i<result.length;i++){
		        		if(result[i].id == textValue){
		        			str += "<option selected value='"+result[i].id+"'>"+result[i].firstName+ " " + result[i].middleInitial+ " " + result[i].lastName+"</option>";
		     		    }else{
		        		    str += "<option value='"+result[i].id+"'>"+result[i].firstName+ " " + result[i].middleInitial+ " " + result[i].lastName+"</option>";
		     		    }
		        	}
	    		}	
		        	
		        str +="<option value='otherInd' class='OtherIndividual'>Input Other Individual</option>"+
		        	  "</select>";
	       }
    		str +="</div></div>";
	            		
    		$("#"+ divName +"").append(str);
    		$("#"+newAuthorDiv+"").hide();
    		
    		getInputOtherType(appendToDiv,dropDownName,newAuthorDiv,count);
    		
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function loadCompany(mainDiv,companyName,companyType,companyWebsite,companyholding,companyCountry,companyNotes){

	var compName = "";
	var readOnly = "";
	var website = "";
	var notes = "";
	var compType = 0;
	var holding = 0;
	var country = 1;
	
	
	if(mainDiv == "view_comp" || mainDiv ==  "update_comp"){
		compName = companyName;
		compType = companyType;
		website = companyWebsite;
		notes = companyNotes;
		if(companyholding == 1){
			holding = "yes";
		}else{
			holding = "no";
		}
		country = companyCountry;
	}
	
	if(mainDiv == "view_comp"){
		readOnly = "readOnly";
	}
 
	var str =   
			    "<div class='form-group row'>"+                       
				"	<label class='col-xs-3 col-form-label'>Company </label>"+
				"	<div class='col-xs-8'>"+
		 	    "		<input class='form-control' type='text' "+readOnly+" value='"+compName+"' id='"+readOnly+"comp_name1'>"+
				"	</div>"+
				"</div>"+
			 	"<div class='form-group row'>"+
				"	<label class='col-xs-3 col-form-label'>Type</label>"+
				"	<div class='col-xs-8' id='select_type_"+mainDiv+"'></div>"+
				"</div>"+
				"<div class='form-group row'>"+                       
				"	<label class='col-xs-3 col-form-label'>Website/Url</label>"+
				"	<div class='col-xs-8'>"+
		 	    "		<input class='form-control' type='text' "+readOnly+" value='"+website+"' id='"+readOnly+"comp_wbsite'>"+
				"	</div>"+
				"</div>"+
				"<div class='form-group row'>"+                       
		  		"	<label class='col-xs-3 col-form-label'>Notes  </label>"+
		  		"	<div class='col-xs-8'>"+
		    	"		<input class='form-control' type='text' "+readOnly+" value='"+notes+"' id='"+readOnly+"comp_note'>"+
		  		"	</div>"+
		  		"</div>"+
				"<div class='form-group row'>"+ 
			 	"	<label class='col-xs-3 col-form-label'> Holding Company </label> "+
			 	"   <div class='col-xs-8'>" 
	if(readOnly != "readOnly"){
		str +=	"		<select class='form-control' id='"+readOnly+"comp_holding'>"+
	 			"			<option value='0'>no</option>"+
				"			<option value='1'>yes</option>"+
				"		</select>" ;
	}else{
		str +=  "	<input class='form-control' type='text' "+readOnly+" value='"+holding+"' id='comp_holding'>";
	}
	    str +=	"	</div>" +
				"</div>"+
			 	"<div class='form-group row'>"+
				"	<label class='col-xs-3 col-form-label'>Country/Location</label>"+
				"		<div class='col-xs-8'>"+
		 	    "			<div id='company_Location"+readOnly+"'></div>"+
				"		</div>"+
				"</div>";
	
	   
	var str1 = setCompanyType(mainDiv,compType,readOnly);
	$("#"+mainDiv+"").append(str);
	$("#select_type_"+mainDiv+"").append(str1);
	
	getCountryList("company_Location"+readOnly,readOnly+"companyBase",0,country,0);
	
}

function setCompanyType(mainDiv,elementID,readOnly){

	var elementString = "";

	if(mainDiv == "company_details"){
		elementString = 
			"<select class='form-control' id='comp_type_"+mainDiv+"'>"+
				"<option value='0'></option>" +
				"<option value='1'>bank</option>" +
				"<option value='2'>central bank</option>" +
				"<option value='3'>insurance</option>" +
				"<option value='4'>industry association</option>"+
				"<option value='5'>media</option>" +
			"</select>";
	}
	
	if(mainDiv == "view_comp"){
		elementString = 
	 	    "<input class='form-control' type='text' "+readOnly+" value='"+getCompType(elementID)+"' id='"+readOnly+"comp_type'>";
	}
	
	if(mainDiv == "update_comp"){
		var selectedID1 = "---";
		var selectedID2 = "---";
		var selectedID3 = "---";
		var selectedID4 = "---";
		var selectedID5 = "---";
		var selectedID0 = "---";

		if(elementID == 0){
			selectedID0 = "selected";	 
		}
		if(elementID == 1){
			selectedID1 = "selected";	  
		}
		if(elementID == 2){
			selectedID2 = "selected";	  
		}
		if(elementID == 3){
			selectedID3 = "selected";	  
		}
		if(elementID == 4){
			selectedID4 = "selected";	  
		}
		if(elementID == 5){
			selectedID5 = "selected";	 
		}
		
	  	elementString = 
			"<select class='form-control' id='comp_type_"+mainDiv+"'>"+
				"<option value='0' "+selectedID0+"></option>" +
				"<option value='1' "+selectedID1+">bank</option>" +
				"<option value='2' "+selectedID2+">central bank</option>" +
				"<option value='3' "+selectedID3+">insurance</option>" +
				"<option value='4' "+selectedID4+">industry association</option>"+
				"<option value='5' "+selectedID5+">media</option>" +
				"<option value='6' "+selectedID5+">mefdgdfgdia</option>" +
			"</select>";
    }	
	return elementString;
}


function getCompType(elementID){
	
	switch(elementID){
	
	case 1:
		return "bank";
	case 2:
		return "central bank";
	case 3:
		return "insurance";
	case 4:
		return "industry association";
	case 5:
		return "media";
	default:
		return "none";
	}
}

function getShareType(type){
	
	switch(type){
	case 1:
		return "company";
	case 2:
		return "individual";
	default:
		return "company";
	}
}


function additional(mainDiv,count,type,id){
	var buttonId = type+"_"+ count;
	
	$("#add_"+buttonId+"").hide();
	$("#cancel_"+buttonId+"").hide(); 
	
	count++;
	if(type == 0){
		loadFiles(mainDiv,count,id,0,"","","","","");
	}else if(type == 111){
		loadCompDesc(mainDiv,count);
	}else{
		loadCompShareholders(mainDiv,count,0,0,0,0,count + 1);
	}
	
}

function cancelAdditional(count,type){
	
	$("#moreDivs_"+ type+"_"+ count +"").remove(); 
	
	count--;
	var buttonId = type+"_"+ count;
	
	$("#add_"+ buttonId +"").show();
	$("#cancel_"+ buttonId +"").show(); 
} 

function addConnection(count,type){
	var buttonId = type+"_"+ count;
	
	$("#add_"+buttonId+"").hide();
	$("#cancel_"+buttonId+"").hide(); 

	if(type == 2){
		loadShareHoldersInd(0,0,type);  
	}else{
		loadShareHoldersCompanies(0,0,0,type);
	}
}

function cancelConnection(count,type){
	
	$("#moreAuthors_"+ count +"").remove(); 
	
	count--;
	var buttonId = type+"_"+ count;
	$("#add_"+ buttonId +"").show();
	$("#cancel_"+ buttonId +"").show(); 
}

function loadContacts(mainDiv){
	 var str = "<div id='company_contact'></div>"+
	 	       "<div id='company_phoneNumbers'></div>";
	 $("#"+mainDiv+"").append(str);
}

function loadFiles(mainDiv,count,compId,fileID,type,desc,name,ext,year){
	var readOnly = "";
	var className = "fileList";
	
	if(fileID != 0 && mainDiv != "update_files"){
		readOnly = "readOnly";
	}
	if(mainDiv == "update_files"){
		className = "updatedFileList";
	}
	
	var d = new Date();
	var maxYear = d.getFullYear();
	var str = "<div id ='moreDivs_0_"+count+"' class='"+className+"'>"+
		"<div class='row'>"+
		"	<input class='form-control' type='hidden' id='compFileID' value='"+fileID+"'>"+
		"	<label class='col-xs-2 col-form-label'>File Type</label>"+
		"	<div class='col-xs-3'>";
	if(fileID == 0){
		str +="<select class='form-control' type='text' id='file_type_"+count+"'>" +
	    "			<option value='1'>financial statements</option>" +
		"			<option value='2'>annual report</option>" +
		"			<option value='3'>white paper</option>" +
		"			<option value='4'>other</option>" +
		"		</select>";
	}else{
		str +="<input class='form-control' "+readOnly+"  value = '"+getFileType(type)+"' type='text' id='"+readOnly+"file_type_"+count+"'>";	
	}
		str +="</div>" +
		"	<label class='col-xs-2 col-form-label'>Description</label>"+
		"	<div class='col-xs-4'>"+
		"		<input class='form-control' "+readOnly+" value='"+desc+"' type='text' id='"+readOnly+"file_desc_"+count+"'>"+
		"	</div>" +
		"</div>" +
		"<div class='row'>"+
		"	<label class='col-xs-2 col-form-label'>Year</label>"+
		"	<div class='col-xs-3'>"+
		"		<input class='form-control' value='"+year+"'  "+readOnly+" type='number' maxlength='1' min='1990' max='"+maxYear+"' id='"+readOnly+"file_year_"+count+"'>"+
		"	</div>";
		
	if(fileID == 0){
	str +="	<label class='col-xs-2 col-form-label'>File to Upload</label>" +
		"	<div class='col-xs-4'>"+
		"<input type='file' id='"+readOnly+"file_"+count+"' name='file' class='cfile'>";
	}else{
	str +="	<label class='col-xs-2 col-form-label'>Uploaded File</label>" +
		"	<div class='col-xs-4'>"+
		"<a href=company/viewFile/"+fileID+">View</a>  | <a href=company/downloadFile/"+fileID+">Download</a>";	
	}
		str +="</div></div>";
		
	if(fileID == 0  || mainDiv == "update_files"){
		str +="<div class='col-md-9'></div><div class='pull-right col-md-3'>"+
	    "	<button type='button' id='add_0_"+count+"' class='btn btn-default btn-sm' onclick='additional(\""+mainDiv+"\","+count+",0,"+compId+")'><span class='glyphicon glyphicon-plus'></span>Add Files</button>";
	    if(0 < count){
			str += "<button type='button' id='cancel_0_"+count+"' class='btn btn-default btn-sm' onclick='cancelAdditional("+count+",0)'><span class='glyphicon glyphicon-remove'></span>Cancel</button>";
		}
		str +="</div>";
	}
	str +="<br/></div>";
	 $("#"+mainDiv+"").append(str);
}

function getFileType(type){
	
	switch(type){
	case 1:
		return "financial statement";
	case 2:
		return "annual report";
	case 3:
		return "white paper";
	case 4:
	default:
		return "other";
	}
}


function getFileDetails(compFileId){
	
	return document.getElementById(("file_"+compFileId)).value.split(/(\\|\/)/g).pop().split(".");
}

function checkShareHolders(companyID,type){
	

	var checkType = document.getElementById("shrHolder_type_0").value;
	var checkShare = 0;
	if(checkType == 1){
		checkShare = document.getElementById("shareHolderCompany_0").value;
	}else if(checkType == 2){
		checkShare = document.getElementById("sharHolderIndividual_0").value;
	}
	
	if(checkShare != 0 ){
		insertShareholders(companyID,type);
	}else{
		insertCompanyFiles(companyID,type);
	}
}

function newMainCompany(){
	
	var formData= new FormData();
	var checkNum = isEmptyNum(document.getElementById("phone_num_0"));
	var isOK = false;
	
	var add1 = document.getElementById("addr1").value;
	var add2 = document.getElementById("addr2").value;
	var add3 = document.getElementById("addr3").value;
	var add4 = document.getElementById("email").value;
	var add5 = document.getElementById("linkedIn").value;
	var add6 = document.getElementById("skype").value;
	var add7 = document.getElementById("youtube").value;
	var add8 = document.getElementById("twitter").value;
	var checkContact = isEmptyAddress(add1,add2,add3,add4,add5,add6,add7,add8);
	var contactID = 0;
	
	inserContact(add1,add2,add3,add4,add5,add6,add7,add8, function(obj){
		contactID = obj.id;
		 formData.append("name",document.getElementById(("comp_name1")).value);
		 formData.append("type",document.getElementById(("comp_type_company_details")).value);
		 formData.append("website",document.getElementById(("comp_wbsite")).value);
		 formData.append("notes",document.getElementById(("comp_note")).value);
		 formData.append("isHolding",document.getElementById(("comp_holding")).value);
		 formData.append("country.id",document.getElementById(("companyBase")).value);
		 formData.append("contactID",contactID);
	
		 $.ajax({
		 	url: "/market_observation/company/add",
		    data: formData,
		    type: "POST",
		    processData: false,
		    contentType: false,
		    success: function (result) {
		           if(result.ok){
		        	  console.log("hala this  " + result);
		        	  
		        	  insertALMProvider(result.id);
		           }
		           else{
		        	   console.log("not Ok");
		        	}
		        },
		        error: function (result) {
		        	console.log("check out");
		        }
		    });
	});
	 
	 
}


function insertShareholders(companyID,type){

	var totalShareHolders = $(".csHolderType").length;
	
	for(i = 0; i < totalShareHolders; i++){
		
		var formData = new FormData();
		var selectValue = document.getElementById("shrHolder_type_" + i).value;
		var sPercentage = document.getElementById("percentage_" + i).value;
		var hasID = document.getElementById("comp_shares_id"+i).value;
		var sValue = 0;
		var thisUrl = "/market_observation/company/addCompShares";
		
		if(selectValue == 1){
			sValue = document.getElementById("shareHolderCompany_"+i).value;
			formData.append("csCompany.id",sValue);
		}else{
			sValue = document.getElementById("sharHolderIndividual_"+i).value;
			formData.append("csInd.id",sValue);
		}
		
		if(hasID != 0){
			thisUrl = "/market_observation/company/updateCompShares";
			formData.append("id",hasID);
		}
		
		 formData.append("type",selectValue);
		 formData.append("percentage",sPercentage);
		 formData.append("companyID",companyID);
		 
		 var lastNum = totalShareHolders - 1;
		 var currNum = i;
		 
		 $.ajax({
		 	url: thisUrl,
		    data: formData,
		    type: "POST",
		    processData: false,
		    contentType: false,
		    success: function (result) {
		    	
		    	if(result.ok){
		    		console.log("Ok");
		    		if(type == 1){
		    			viewDetails(companyID);
		    		}
		    		if(lastNum == currNum && result.ok == true){
		    			insertCompanyFiles(companyID,type);
		    		}
		    		else{
		    			console.log(result.ok);
		    		}
		    	}
		    	else{
		    		console.log("Not Ok");
		    	}
		    },
	        error: function (result) {
	        	console.log("check out");
	        }
		});
	}
}

function insertCompanyFiles(companyID,type){
	
	var totalFiles = $(".cfile").length;
	var checkFile = $("#file_0").val();
	
	if(checkFile.length != 0){
		for(i = 0; i < totalFiles; i++){
			var formData= new FormData();
			var fileDetails = getFileDetails(i);
		
			var files = document.getElementById("file_"+ i).files;
			
			 formData.append("file",files[0]);
			 formData.append("fileType",document.getElementById(("file_type_"+ i)).value);
			 formData.append("fileDesc",document.getElementById(("file_desc_"+ i)).value);
			 formData.append("fileName",fileDetails[0]);
			 formData.append("fileExt",fileDetails[1]);
			 formData.append("fileYear",document.getElementById(("file_year_"+ i)).value);
			 formData.append("companyID",companyID);
			 
			 var lastNum = totalFiles - 1;
			 var currNum = i;
				
			 $.ajax({
			 	url: "/market_observation/company/addCompFiles",
			    data: formData,
			    type: "POST",
			    processData: false,
			    contentType: false,
			    success: function (result) {
			       console.log(lastNum + " - " + currNum);
			    	if(result.ok){
			    	   if(type == 1){
			    			viewDetails(companyID);
			    		}
			    	   if(lastNum == currNum && result.ok == true){
			    		   insertPhoneNumbers(companyID);
			    	   }else{
			    			console.log(result.ok);
			    		}
			    	}
			    	else{
			    		console.log("Not Ok");
			    	}
			    },
		        error: function (result) {
		        	console.log("check out");
		        }
			});
		}
	}else{
		insertPhoneNumbers(companyID);
	}	
}


function insertPhoneNumbers(companyID){
	var numItems = $('.main_additionalPhones').length
	     
	for(i = 0; i < numItems ; i++){
		if(document.getElementById(("main_phone_num_" +i)).value != 0){
			
			var lastNum = numItems - 1;
			var currNum = i;
			 
			$.ajax({
			 	url:"/market_observation/phone/add",
			    data: "phoneType="+document.getElementById(("main_phone_type_" +i)).value+
		    		  "&areaCode="+document.getElementById(("main_phone_area_" +i)).value+
		    		  "&number="+document.getElementById(("main_phone_num_" +i)).value+
		    		  "&country.id="+document.getElementById(("main_phone_4_loc_" +i)).value+
		    		  "&companyID="+companyID,
			    type: "POST",
			    success: function (result) {
			        var lastNum = numItems - 1;  
			    		if(result.ok == true && lastNum == currNum){
			    			 location.reload();
			            }else if(result.ok){
			            	console.log(result.ok);
			            }
			           else{
		        		  console.log("error in updating db");
			        	}
			        },
			        error: function (result) {
			        	console.log("failed");
			        }
		    });
		}else{
			 location.reload();
			
		}
	}
}


function insertALMSeller(companyID, type){
	var numItems = $('.almSeller').length
	
	if(document.getElementById("comp_sellOptionName_0").value != ""){
	     
		for(i = 0; i < numItems; i++){
			
				
				var lastNum = numItems - 1;
				var currNum = i;
				$.ajax({
				 	url:"/market_observation/company/addAlmSeller",
				    data: "appname="+document.getElementById(("comp_sellOptionName_" +i)).value+
			    		  "&almDesc="+document.getElementById(("comp_sellOptionDesc_" +i)).value+
			    		  "&companyID="+companyID,
				    type: "POST",
				    success: function (result) {
				    	   if(result.ok == true && lastNum == currNum){
				    		   checkShareHolders(companyID,0);
				    	   }else if(result.ok ){
				    		   if(type == 21){
				    			   viewDetails(companyID);
				    		   }
				    	   }
				           else{
			        		  console.log("error in updating db");
				        	}
				        },
				        error: function (result) {
				        	console.log("failed");
				        }
			    });
			
		}
	}else{
		checkShareHolders(companyID,0);
	}
}

function insertALMProvider(companyID, type){

	     
	if(document.getElementById(("comp_uselOptionName")).value != ""){
		console.log("asa ka");
		$.ajax({
		 	url:"/market_observation/company/addAlmProvider",
		    data: "appName="+document.getElementById(("comp_uselOptionName")).value+
	    		  "&providerID="+document.getElementById(("comp_uselOptionfrom")).value+
	    		  "&companyID="+companyID,
		    type: "POST",
		    success: function (result) {
		    	   if(result.ok){
		    		   if(type == 22){
		    			   viewDetails(companyID);
		    		   }else{
		    			   insertALMSeller(companyID,1);
		    		   }
		    		}
		           else{
	        		  console.log("error in updating db");
		        	}
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
	}else{
		console.log("ari ka");
		 insertALMSeller(companyID,1);
	}
}

function inserContact(add1,add2,add3,add4,add5,add6,add7,add8,callback){
	var ID = 0;
	$.ajax({
		url:"/market_observation/contact/add",
	    data: "address1="+add1+
    		  "&address2="+add2+
    		  "&address3="+add3+
    		  "&email="+add4+
    		  "&linkedIn="+add5+
    		  "&skype="+add6+
    		  "&youtube="+add7+
    		  "&twitter="+add8,
	    type: "POST",
	    success: function (result) {
	           if(result.ok){
	        	   console.log(result);
	        	   ID =  result.id;
	           }
	        },
	        error: function (result) {
	        	console.log("failed");
	        },
	   complete: function(){
		   var obj = {id:ID};
		   if(callback)
			   callback(obj);
	   }
    });
}


function getData(divID,mainDiv){
	
	$(".modal").modal("hide");
	$(".modal-backdrop").hide();
	$('html, body').css({
	    'overflow': 'auto',
	    'height': 'auto'
	});

	getCompanyProvider("almProvider","newCompProv","comp_uselOptionfrom",divID,"")
	
} 

function isEmptyNum(val){
	if (val == "0"){
		return true;
	}
    return false;
}

function isEmptyAddress(add1,add2,add3,add4,add5,add6,add7,add8){
	
	if(add1.length == 0 && add2.length == 0 && add3.length == 0 && add4.length == 0 && add5.length == 0 && add6.length == 0 && add7.length == 0 && add8.length == 0){
		return true;
	}
	
	return false;
}