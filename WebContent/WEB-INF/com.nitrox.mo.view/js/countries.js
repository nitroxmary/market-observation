/**
 * 
 */

$(document).ready(function(){
	countryList();
});


function countryList(){
	$("#CountryAdd").addClass("hide");
	$("#CountryUpdate").addClass("hide");
	$(".forUpdateRow").addClass("hide");
	
	$.ajax({
	 	url: "/market_observation/country/list",
	    type: "GET",
        success: function (result) {
    		var str = "";
    		
        	for (i=0;i<result.length;i++){
        		str += "<tr id='displayRow_"+result[i].id+"'>"+ 
	        				"<td>"+result[i].id+"</td>" +
	        				"<td>"+result[i].name+"</td>" +
	        				"<td>"+result[i].region+"</td>"+
	    			        "<td>"+result[i].code+"</td>" +
	    			        "<td><a href='javascript:updateCountryRow("+result[i].id+")'>Update</a></td>" +
	    			        "<td><a href='javascript:deleteCountries("+result[i].id+")'>Delete</a></td>" +
    			        "</tr><tr class='forUpdateRow' id='updateRow_"+result[i].id+"'>" +
	        				"<td>"+result[i].id+"</td>" +
	        				"<td><input type='text' name='name' id='name_" +result[i].id + "' value='"+result[i].name+"'></td>" +
	        				"<td><input type='text' name='region' id='region_" +result[i].id + "' value='"+result[i].region+"'></td>"+
	    			        "<td><input type='text' name='code' id='code_" +result[i].id + "' value='"+result[i].code+"'></td>" +
	    			        "<td><input type='button' value='Save' onclick='editCountry("+result[i].id+")'></td>" +
	    			        "<td><input type='button' value='Cancel' onclick='cancelChanges("+result[i].id+")'></td>" +
    			        "</tr>";
        		
        	}
        	
    		$("#mary").append(str);
        },
        error: function (result) {
        	console.log("error");
	     }
	  });
	
}


	function updateCountryRow(id){
		$("#displayRow_" +id).addClass("hide");
		$("#updateRow_" + id).show();
	}
	
	function cancelChanges(id){
		$("#displayRow_" +id).show();
		$("#updateRow_" + id).hide();
	}

	function newCountry(){
		$("#newCountryBtn").addClass("hide");
		$("#CountryList").addClass("hide");
		$("#CountryAdd").show();
	}


    function createCountry(){
		 $.ajax({
			 	url:"/market_observation/country/add",
			    data: "&name="+document.getElementById(("name")).value+
		    		  "&region="+document.getElementById(("region")).value+
		    		  "&code="+document.getElementById(("code")).value,
			    type: "POST",
			    success: function (result) {
			           if(result.ok){
			        	   countryList();
			        	   $("#newCountryBtn").show();
			        	   $("#CountryList").show();
			        	 }
			           else{
		        		  console.log("error in updating db");
			        	}
			        },
			        error: function (result) {
			        	console.log("failed");
			        }
		    });
    }

	function editCountry(countryId){
		 $.ajax({
			 	url:"/market_observation/country/update",
			    data: "id="+countryId+
		    	"&name="+document.getElementById(("name_"+countryId)).value+
		    	"&region="+document.getElementById(("region_"+countryId)).value+
		    	"&code="+document.getElementById(("code_"+countryId)).value,
			    type: "POST",
			    success: function (result) {
			    	if(result.ok){
			    		   location.reload();
			        	   countryList();
			        	   $("#newCountryBtn").show();
			        	   $("#CountryList").show();
			        	 }
			           else{
		        		  console.log("error in updating dba");
			        	}
			    	console.log(result);
			        },
			        error: function (result) {
			        	console.log("failed");
			        }
		    });
	}
	
	
	function deleteCountries(countryId){
		var result = confirm("Are you sure you want to delete this data?");
		if (result) {
			 $.ajax({
				 	url:"/market_observation/country/delete",
				    type: "POST",
				    data: "id="+countryId,
				    success: function (result) {
				           if(result.ok){
				        	   countryList();
				        	   $("#newCountryBtn").show();
				        	   $("#CountryList").show();
				           }
				           else{
				        		  console.log("error in updating dba");
					        	}
					    	console.log(result);
					        },
					        error: function (result) {
					        	console.log("failed");
					        }
			    });
		}
		
		
	}
