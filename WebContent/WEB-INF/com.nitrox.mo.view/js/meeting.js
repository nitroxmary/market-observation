/**
 * 
 */

$(document).ready(function(){
	loadMeeting("meet_details");
	loadConnectedAuthors(0,0,0);
	getCompanyListMeet("meeting_company","meeting_Comp","ind_location",0);
	getCountryListMeet("meeting_country","meetingBase","form-control",0);
	
});

function loadMeeting(mainDiv){
	var str =
	    "<div class='form-group row'>"+
	  	"	<label class='col-xs-2 col-form-label'>Event Date</label>"+
	  	"	<div class='col-xs-10'>"+
	    "		<input class='form-control' type='date' value='' id='meeting_date'>"+
	  	"	</div>"+
		"</div>"+
		"<div class='form-group row'>"+
	  	"    <label class='col-xs-2 col-form-label'>Company</label>"+
	  	"	<div class='col-xs-10' id='meeting_company'>"+
	    "	</div>"+
	  	"</div>"+
		"<div class='form-group row'>"+
		"	<label class='col-xs-2 col-form-label'>Meeting Summary</label>"+
	    "	<div class='col-xs-10'><textarea class='form-control' id='meeting_summary' rows='3'></textarea></div>"+
	  	"</div>"+
		"<div class='form-group row'>"+
		"	<label class='col-xs-2 col-form-label'>To Follow-up</label>"+
	    "	<div class='col-xs-10'><textarea class='form-control' id='meeting_followUp' rows='3'></textarea></div>"+
	  	"</div>"+
	  	"<div class='form-group row'>"+
	  	"    <label class='col-xs-2 col-form-label'>Location [specific address]</label>"+
	  	"	<div class='col-xs-10'>"+
	    "		<input class='form-control' value=''  id='meeting_address'>"+
	  	"    </div>"+
	  	"</div>	"+
	  	"<div class='form-group row'>"+
	  	"    <label class='col-xs-2 col-form-label'>Location [Country]</label>"+
	  	"	<div class='col-xs-10' id='meeting_country'>"+
	    "	</div>"+
	  	"</div>";
	$("#"+mainDiv+"").append(str);

}

function getData(divID,mainDiv){
	
	$(".modal").modal("hide");
	$(".modal-backdrop").hide();
	$('html, body').css({
	    'overflow': 'auto',
	    'height': 'auto'
	});

	if(mainDiv == "meeting_company"){
		getCompanyListMeet("meeting_company","meeting_Comp","ind_location",divID);
	}else{
		
		var totalAuthors = $(".connected_authors").length;
		var splitDiv = mainDiv.split("_");
		var count = parseInt(splitDiv[1]) - 1;
		console.log(totalAuthors);
		$("#"+mainDiv+"").remove();
		loadConnectedAuthors(count,divID,totalAuthors);
	}

} 


function NewMeeting(){
	
     $.ajax({
           url:"/market_observation/meeting/data",
          data: "eventDate="+document.getElementById("meeting_date").value+
                "&company=" + document.getElementById("meeting_Comp").value+
                "&summary="+document.getElementById("meeting_summary").value+
                "&followUp="+document.getElementById("meeting_followUp").value+
                "&locAddress="+document.getElementById("meeting_address").value+
                "&locCountry="+document.getElementById("meetingBase").value+
                "&individuals="+getConnectionAuthors(),
           type: "POST",
          success: function (result) {
        	  console.log(result);
                 if(result.ok){
                     location.reload();
                 }
                 else{
                    console.log("error in saving");
                  }
              },
              error: function (result) {
                  console.log("saving failed");
              }
      });

}

function getIndividualList(divName, dropDownName,newAuthorDiv,count,textValue,lastAuthorCount){
   console.log("d " + count);
	$.ajax({
	 	url: "/market_observation/marketSignal/individualList",
	    type: "GET",
        success: function (result) {
        	
        	var str = "";
        	var appendToDiv = "moreAuthors_"+count;
        	var str = "<div id='"+appendToDiv+"'>";
        	var indID = 0;
        	var divNum = 0;
        	
        	if(divName == 'update_attendees'){
        		var splitValue = textValue.split("_");
        		textValue = splitValue[0];
        		indID = splitValue[1];
        		divNum = 1;
        	}
        
        	if(divName == 'authors_connection' || divName == 'update_attendees'){
        		str += "<span class='countAuthors'>"+count+". </span>" + 
        			   "<input type='hidden' class='form-control attendees_id' value = '"+indID+"' id='attendees__id_"+count+"'>" +
        			   "<div id='"+ newAuthorDiv +"'></div>";
        	}
    		str += "<select class='connected_authors' id='"+dropDownName+"'>" +
    				"<option value='0'> </option>";
    		if(result.length != 0){	
	        	for (i=0;i<result.length;i++){
	        		if(result[i].id == textValue){
	        			str += "<option selected value='"+result[i].id+"'>"+result[i].firstName+ " " + result[i].middleInitial+ " " + result[i].lastName+"</option>";
	     		    }else{
	        		    str += "<option value='"+result[i].id+"'>"+result[i].firstName+ " " + result[i].middleInitial+ " " + result[i].lastName+"</option>";
	     		    }
	        	}
    		}	
	        str +="<option value='otherInd' class='OtherIndividual'>Input Other Individual</option>"+
	        	  "</select>";
	        
	        if(count > 0 && textValue == 0){
	        	str +="<div class='pull-right' id='buttons_authors_connection_"+count+"'>";
	        	str += "<input type='button' id='add_3_"+count+"' value='Add More Attendees' onclick='addConnection("+count+",3,"+divNum+")'>";
	        	
	        	if(count > 1){
	        		str += "<input type='button' id='cancel_3_"+count+"' value='Cancel' onclick='cancelConnection("+count+",3,"+divNum+")'>";
	        	}
	        	str +="</div>";
	        }
	        
	        if(count > 0 && textValue > 0){
	        	
	        	if(count == lastAuthorCount){
	        		str +="<div class='pull-right' id='buttons_authors_connection_"+count+"'>";
	        		str += "<input type='button' id='add_3_"+count+"' value='Add More Attendees' onclick='addConnection("+count+",3,"+divNum+")'>";
	        		str +="</div>";
	        	}
	        }
	        
	        str +="</div>";
	            		
    		$("#"+ divName +"").append(str);
    		$("#"+newAuthorDiv+"").hide();
    		
    		getInputOtherMeet(appendToDiv,dropDownName,newAuthorDiv,count);
    		
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function loadNewAuthorMeet(mainDiv,dropDown,count){
	var str = "<div class='modal fade' id='meetingsIndModal' role='dialog'>" + 
	          "<div class='modal-dialog'>"+
              "<div class='modal-content'>"+
              "<div class='modal-header'>"+
              "<h4 class='modal-title'>New Attendee</h4>"+
              "</div>"+
              "<div class='modal-body'>";
	str += "<div id='newAuthor'>"+
	 	"		    <div class='form-group row'>"+                       
  		"				<label class='col-xs-3 col-form-label'>First Name</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<input class='form-control' type='text' id='first_name'>"+
  		"				</div>"+
  		"			</div>"+
	 	"		    <div class='form-group row'>"+
  		"				<label class='col-xs-3 col-form-label'>Last Name</label>"+
  		"				<div class='col-xs-8'>"+
  		"					<input class='form-control' type='text' id='last_name'>"+
  		"				</div>"+
  		"			</div>"+
  		"		    <div class='form-group row'>"+                       
  		"				<label class='col-xs-3 col-form-label'>Middle Initial</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<input class='form-control'  maxlength='1' type='text' id='middle_initial'>"+
  		"				</div>"+
  		"			</div>"+
	 	"		   <div class='form-group row'>"+
  		"				<label class='col-xs-3 col-form-label'>Country/Location</label>"+
  		"				<div class='col-xs-8'>"+
    	"					<div id='individualLocation'></div>"+
  		"				</div>"+
  		"			</div>"+
	 	"		    <button onClick='newIndividual(\""+mainDiv+"\")' id='individualButton' class='btn'>Insert Individual</button>"+
//	 	"		 <button onClick='cancelNewAddition(\""+mainDiv+"\",\""+dropDown+"\",\"newAuthor\",\""+count+"\")' id='cancelButton' class='btn'>Cancel</button>"+
	    "		 </div>" +
	    "</div>"+
        "<div class='modal-footer'>"+
        "<button type='button' class='btn btn-default' onClick='cancelNewAddition(\""+mainDiv+"\",\""+dropDown+"\",\"newAuthor\",\""+count+"\")' data-dismiss='modal'>Close</button>"+
        "</div>"+
        "</div>"+
        "</div>"+
        "</div>";
	$("#"+mainDiv+"").append(str);
	$("#meetingsIndModal").modal();
}


function cancelNewAddition(divName,dropDown,forRemoval,count){
	//$("#"+forRemoval+"").remove(); 
	$("#"+dropDown+"").show()
	$("#"+dropDown+"").val(0);
	
	if(divName.indexOf("moreAuthors") !== -1){
		$("#author_span_"+count+"").show();
    	$("#connectedBy_"+count+"").show();
    	$("#buttons_authors_connection_" + count+"").show();
	}
}


function newIndividual(mainDiv){
	
	$.ajax({
	 	url:"/market_observation/individual/add",
	    data: "firstName="+document.getElementById(("first_name")).value+
    		  "&lastName="+document.getElementById(("last_name")).value+
    		  "&middleInitial="+document.getElementById(("middle_initial")).value+
    		  "&country.id="+document.getElementById(("authorBase")).value,
	    type: "POST",
	    success: function (result) {
	           if(result.ok){
	        	   console.log(result);
	        	   getData(result.id,mainDiv);
	           }
	           else{
        		  console.log("error in updating db");
	        	}
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
    });
}

function loadConnectedAuthors(authorAdd,authorValue,lastAuthorCount){
	var buttonID = authorAdd + 1;
	getIndividualList("authors_connection","authors_dropdownAuthors_"+ authorAdd,"IndConnectionsText_" + authorAdd, buttonID,authorValue,lastAuthorCount);
}

function getInputOtherMeet(divName,dropDown,divInput,count){
	
	$("#"+divName+"").on('click', "#"+ dropDown +"", function(){
	
	    if ((this.value) == "otherInd") {
	    	
	    	$("#"+dropDown+"").hide();
	    	$("#connectedBy_"+count+"").hide();
	    	$("#author_span_"+count+"").hide();
	    	$("#buttons_authors_connection_" + count+"").hide();
	    	loadNewAuthorMeet(divName,dropDown,count);
	    	getCountryListMeet("individualLocation","authorBase",0,0,0);
	   }
	   else if((this.value) == "OtherHolding" || (this.value) == "OtherComp" ){
	    	
	    	$("#"+dropDown+"").hide();
	    	$("#"+divInput+"").show();
	    	loadNewCompany(divName,dropDown,count);
	    	loadContactDetails("company_contact");
	    	loadContactNumbers("company_phoneNumbers",0);
	    	getCountryList("companyLocation","mainCompanyBase",0,0,0);
	    }
	});
	
}

function getCountryListMeet(divName, dropDownName,drpClass,textValue){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/countryList",
	    type: "GET",
        success: function (result) {
        	var str = "";

        	str += "<select class='"+drpClass+"' id='"+dropDownName+"'>";
        	for (i=0;i<result.length;i++){
				if(result[i].id == textValue){
        			str += "<option selected value='"+result[i].id+"'>"+result[i].name+"</option>";
        		}else{
        			str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
        		}
			 }
	        str +="</select>";
	        str +="</div>";
	        
    		$("#"+ divName +"").append(str);
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function getCompanyListMeet(divName, dropDownName, newCompDiv,textValue){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/companyList",
	    type: "GET",
        success: function (result) {
        	var str = "";
            
        	str +="<div id='"+ newCompDiv +"'></div>";
        	str += "<select class='form-control' id='"+dropDownName+"'>";
        	str +="<option value='0'> </option>";
    			for (i=0;i<result.length;i++){
					if(result[i].id == textValue){
	        			str += "<option selected value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}else{
	        			str += "<option value='"+result[i].id+"'>"+result[i].name+"</option>";
	        		}
    			 }
    	    str +="<option value='OtherComp' class='OtherComp'>Input Other Company</option>"+
	        	  "</select>";
	        str +="</div>";
	        
    		$("#"+ divName +"").append(str);
    		$("#"+newCompDiv+"").hide();
    		
    		getInputOtherMeet(divName,dropDownName,newCompDiv);
    	    
    	},
        error: function (result) {
        	console.log("error");
	     }
	  });
}

function getConnectionAuthors(){
	
	var totalAuthors = $(".connected_authors").length;
	var authors = new Array();
	
	for(i = 0; i < totalAuthors; i++){
		
		var compareDiv = "moreAuthors_"+ (i+1);
		var drpDown = "authors_dropdownAuthors_"+ i;	
		var id = document.getElementById("attendees__id_" + (i+1)).value;	

		authors[i] = compareDiv + "-" + document.getElementById(drpDown).value + "-" + id;
		
	}
	return authors;
}


function addConnection(count,type,divNum){
	var buttonId = type+"_"+ count;
	
	$("#add_"+buttonId+"").hide();
	$("#cancel_"+buttonId+"").hide(); 

	if(divNum == 1){
		loadAttendees(count,0 + "_" + 0,0);
	}else{
		loadConnectedAuthors(count,0,0,1);
	}
}


function cancelConnection(count,type){
	
	$("#moreAuthors_"+ count +"").remove(); 
	
	count--;
	var buttonId = type+"_"+ count;
	$("#add_"+ buttonId +"").show();
	$("#cancel_"+ buttonId +"").show(); 
}