function addMarquee(msg){
	$("#marquee").html("");
	msgs= [];
	msgs.push(msg);
	$("#marquee").append('<div class="marquee" id="mycrawler"> &nbsp;&nbsp;&nbsp; '+msgs.join(" &nbsp;&nbsp;&nbsp; ")+' &nbsp;&nbsp;&nbsp; </div>');
	marqueeInit({
		uniqueid: 'mycrawler',
		style: {
			'padding': '0px',
			'width' : '200%',
			'background': 'trasparent',
			'border': 'none'
		},
		inc: 1, //speed - pixel increment for each iteration of this marquee's movement
		mouse: 'pause', //mouseover behavior ('pause' 'cursor driven' or false)
		moveatleast: 2,
		neutral: 150,
		savedirection: true
	});
}

function convertKorM(num){
	if(num.indexOf("K") > -1 || num.indexOf("k") > -1){
		return parseInt(num.split("K")[0]) * 1000;
	}else if(num.indexOf("M") > -1 || num.indexOf("m") > -1){
		return parseInt(num.split("M")[0]) * 1000000;
	}else{
		return num;
	}
}

function shuffle(o) {
    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

function login(){
	$( "#modalHeader" ).html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" style='font-size: 12px'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");
	$( "#modalBody" ).load( baseUrl+"/login/" );
	$( "#modalFooter" ).html('<div id="signup-link" class="col-lg-12 col-md-12 center signup-link">'+
					'Not a member? <a href="#" onclick="signup()">Sign up</a> today.'+
				'</div>');
	$('#modal').modal('show');
}

function signup() {
	$( "#modalHeader" ).html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" style='font-size: 12px'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button><a href='#' onclick='back()'><i class='fa fa-arrow-left'></i></a>");
	$( "#modalFooter" ).html("");
	$( "#modalBody" ).load( baseUrl+"/researchemployee/new", function(){
		$( "#registrationBody" ).fadeOut(0);
		$( "#registrationBody" ).fadeIn(500);
	});
}

function signupShow() {
	$( "#modalHeader" ).html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" style='font-size: 12px'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button><a href='#' onclick='back()'><i class='fa fa-arrow-left'></i></a>");
	$( "#modalFooter" ).html("");
	$( "#modalBody" ).load( baseUrl+"/researchemployee/new", function(){
		$( "#registrationBody" ).fadeOut(0);
		$( "#registrationBody" ).fadeIn(500);
	});
	$('#modal').modal('show');
}

function forgot() {
	$( "#modalHeader" ).html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" style='font-size: 12px'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button><a href='#' onclick='back()'><i class='fa fa-arrow-left'></i></a>");
	$( "#modalFooter" ).html('<div id="signup-link" class="col-lg-12 col-md-12 center signup-link">'+
			'Not a member? <a href="#" onclick="signup()">Sign up</a> today.'+
		'</div>');
	$( "#modalBody" ).load( baseUrl+"/researchemployee/forgotPassword", function(){
		$( "#forgotPasswordBody" ).fadeOut(0);
		$( "#forgotPasswordBody" ).fadeIn(500);
	});
}

function forgotResend() {
	$("#errorMessage").html("Resending email");

	$.ajax({
	 	url: baseUrl+"/researchemployee/password/reset",
	    data: "&email="+document.getElementById(("email")).value,
	    type: "POST",
        success: function (result) {
        	if(result.ok){
        		var email = $("#email").val();
        		$( "#modalHeader" ).html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" style='font-size: 12px'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button><a href='#' onclick='back()'><i class='fa fa-arrow-left'></i></a>");
        		$( "#modalFooter" ).html('<div id="signup-link" class="col-lg-12 col-md-12 center signup-link">'+
        				'Not a member? <a href="#" onclick="signup()">Sign up</a> today.'+
        			'</div>');
        		$( "#modalBody" ).load( baseUrl+"/researchemployee/sentPassword", function(){
        			$( "#forgotPasswordBody" ).fadeOut(0);
        			$( "#forgotPasswordBody" ).fadeIn(500);
        			$( "#email" ).val(email);
        		});
        	}
        	else{
        		$("#errorMessage").html(result.message);
        	}
        },
        error: function (result) {
    		$("#errorMessage").html(result.message);
        }
    });
}

function forgotPassword() {
	$("#forgotPassword").attr("disabled", "disabled");
	$("#forgotPassword").text("Loading...");
	
	$.ajax({
	 	url: baseUrl+"/researchemployee/password/reset",
	    data: "&email="+document.getElementById(("forgotPswdEmail")).value,
	    type: "POST",
        success: function (result) {
        	if(result.ok){
        		var email = $("#forgotPswdEmail").val();
        		$( "#modalHeader" ).html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" style='font-size: 12px'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button><a href='#' onclick='back()'><i class='fa fa-arrow-left'></i></a>");
        		$( "#modalFooter" ).html('<div id="signup-link" class="col-lg-12 col-md-12 center signup-link">'+
        				'Not a member? <a href="#" onclick="signup()">Sign up</a> today.'+
        			'</div>');
        		$( "#modalBody" ).load( baseUrl+"/researchemployee/sentPassword", function(){
        			$( "#forgotPasswordBody" ).fadeOut(0);
        			$( "#forgotPasswordBody" ).fadeIn(500);
        			$( "#email" ).val(email);
        		});
        	}
        	else{
        		$("#errorMessage").html(result.message);
        		$("#forgotPassword").attr("disabled", false);
        		$("#forgotPassword").text("Submit");
        	}
        },
        error: function (result) {
    		$("#errorMessage").html(result.message);
    		$("#forgotPassword").attr("disabled", false);
    		$("#forgotPassword").text("Submit");
        }
    });
}

function back(){
	$( "#modalHeader" ).html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" style='font-size: 12px'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");
	$( "#modalBody" ).load( baseUrl+"/login", function(){
		$( "#registrationBody" ).fadeOut(0);
		$( "#registrationBody" ).fadeIn(500);
	});
	$( "#modalFooter" ).html('<div id="signup-link" class="col-lg-12 col-md-12 center signup-link">'+
					'Not a member? <a href="#" onclick="signup()">Sign up</a> today.'+
				'</div>');
}

function modal(head, body, footer, width){
	$( "#modalHeader" ).html(head+"<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" style='font-size: 12px'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");
	$( "#modalBody" ).load(body);
	$( "#modalFooter" ).html(footer);
	$('#modal').modal('show');
	if(width){
		$('.modal-content').css("width", width);
		$('.modal-dialog').css("width", width);
	}else{
		$('.modal-content').css("width", "600px");
		$('.modal-dialog').css("width", "600px");
	}
}

function reload(){
	location.reload(true);
}

function resetPassword() {
	if($("#password").val() == $("#repeatPassword").val()){
		$.ajax({
		 	url: baseUrl +"/researchemployee/edit/password",
		    data: "&Pswd="+window.btoa($("#password").val()),
		    type: "POST",
	        success: function (result) {
	    		$("#errorMessage").html(result.message);
	    		if(result.message == "Research Employee password successfully changed!"){
	    			$("#resetPassword").attr("disabled", "disabled");
	    			$("#resetPassword").text("Redirecting in 5...");
	    			setTimeout(function(){ 
		    			$("#resetPassword").text("Redirecting in 4...");
	    			}, 1000);
	    			setTimeout(function(){ 
		    			$("#resetPassword").text("Redirecting in 3...");
	    			}, 2000);
	    			setTimeout(function(){ 
		    			$("#resetPassword").text("Redirecting in 2...");
	    			}, 5000);
	    			setTimeout(function(){ 
		    			$("#resetPassword").text("Redirecting in 1...");
	    			}, 4000);
	    			setTimeout(function(){ 
	    				window.location.replace(baseUrl +"");
	    			}, 5000);
	    		}
	        },
	        error: function (result) {
	    		$("#errorMessage").html(result.message);
	        }
	    });
	}else{
		$("#errorMessage").html("Password does not match.");
	}
}

$(document).ready(function(){
    $('.js-activated').dropdownHover().dropdown();
	$("#modal").on("shown.bs.modal", function(){
		if($(this).find("input[type='text']").length > 0)
			$(this).find("input[type='text']")[0].focus();
	});
	msgs = [];
});