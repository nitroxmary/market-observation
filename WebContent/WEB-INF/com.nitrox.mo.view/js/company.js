/**
 * 
 */

$(document).ready(function(){
	$("#comp_details").hide();
	companyList();
});


function companyList(){
	
	$.ajax({
	 	url: "/market_observation/marketSignal/companyList",
	    type: "GET",
        success: function (result) {
        	
    		var str = "<table class='table table-bordered' id='company_table'>" +
    				  "<thead><tr><th>COMPANY NAME</th>" +
    				  "<th>TYPE</th>" +
    				  "<th>LOCATION</th>" +
    				  "<th></th></tr></thead><tbody>";
    		for (i=0;i<result.length;i++){
    			
    			str += "<tr id='displayRow_"+result[i].id+"'>"+ 
	        				"<td>"+result[i].name+"</td>" +
	    			        "<td>"+getType(result[i].type)+"</td>" +
	    			        "<td>"+result[i].country.name+"</td>" +
	    			        "<td><a id='view"+result[i].id+"' href='javascript:viewCompany("+result[i].id+")'>View</a>  &nbsp;|&nbsp; " +
	    			        "	 <a id='delete"+result[i].id+"' href='javascript:deleteCompDetails("+result[i].id+")'>Delete</a>  &nbsp;|&nbsp; " +
	    			        "    <a href='" + baseUrl + "/view/ConnCompXLS/"+result[i].id+"'>Export</a>" +
	    			        "</td>" +
	    			    "</tr>";
        	}
        	
    		$("#comp_list").append(str);
    		$("#company_table").DataTable({
        		 "pageLength": 25
        	});
    		
//    		result.forEach(function(e){
//	    		$("#view"+e.id).on("click", function(){
//	    			
//	    			viewCompany(e);
//	    		});
//	    		
//	    		$("#delete"+e.id).on("click", function(){
//	    			deleteCompDetails(e);
//	    		});
//    		});
    		
        },
        error: function (result) {
        	console.log("error");
	     }
	  });
	
}

function getType(type){
	
	switch(type){
	case 1:
		return "Bank";
	case 2:
		return "Central Bank";
	case 3:
		return "Insurance";
	case 4:
		return "Industry Association";
	case 5:
		return "Media";
	default:
		return "Not Specified";
	}
}

function updateCompDetails(type){
	
	if(type == 1){
		$("#view_comp").hide();
		$(".updateComp").show();
		$("#update1").hide();
	}else if(type == 2){
		$(".updateFiles").show();
		$("#update2").hide();
	}else if(type == 3){
		$("#view_shareHolders").hide();
		$(".updateSholders").show();
		$("#update3").hide();
	}else if(type == 4){
		$("#view_ContactDetails").hide();
		$(".updateConDetails").show();
		$("#update4").hide();
	}else if(type == 5){
		$("#display_Phones").hide();
		$(".updatePhones").show();
		$("#update5").hide();
	}else if(type == 21){
		$("#update21").hide();
		$(".updateSeller").show();
	}else if(type == 22){
		$(".updateprovider").show();
		$("#update22").hide();

	}
	
}

function cancelCompUpdate(type){
	if(type == 1){
		$(".updateComp").hide();
		$("#update1").show();
		$("#view_comp").show();
	}else if(type == 2){
		$(".updateFiles").hide();
		$("#update2").show();
		$("#view_files").show();
	}else if(type == 3){
		$("#view_shareHolders").show();
		$("#update3").show();
		$(".updateSholders").hide();
	}else if(type == 4){
		$("#view_ContactDetails").show();
		$("#update4").show();
		$(".updateConDetails").hide();
	}else if(type == 5){
		$("#display_Phones").show();
		$("#update5").show();
		$(".updatePhones").hide();
	}
	else if(type == 21){
		$("#view_ContactDetails").show();
		$("#update21").show();
		$(".updateprovider").hide();
	}else if(type == 22){
		$("#alm_seller").show();
		$("#update22").show();
		$(".updateprovider").hide();
	}
		
}

function viewCompany(id){
	
	$.ajax({
	 	url: "/market_observation/company/getCompany",
	    type: "GET",
	    data:"id="+id,
        success: function (res) {
        	
        	console.log("dsf " + res.inputDate);
        	console.log("contact id : " + res.id);
        	var user = document.getElementById("userType").value;
        	
        	$("#company_details2").empty();
        	$("#comp_details").show();
        	$("#comp_list").hide();
        	viewList();
        	
        	var contact = 0;
        	if(res.contact != null){
        		contact = res.contact.id;
        	}
        	var str =  " <div id='view_comp'></div><div class='row'>" +
                       "	<div class='col-md-9'></div>";
        	if(user == "admin")
        	{
        	str +=     "		<input class='form-control' type='hidden' id='inputBy' value='"+res.inputBy+"'>"+
        			   "		<input class='form-control' type='hidden' id='inputDate' value='"+res.inputDate+"'>"+
        			   "	<div class='col-md-3' id='update1'>" +
        			   "		<a href='#' onclick='updateCompDetails(1)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
        			   "	</div>";
        	}
        	str +=    " </div>";
        	str += "<div class='updateComp'><div id='update_comp'></div><div class='form-group row' id='cancelButton1'></div></div></div></div>";

        	$("#company_details2").append(str);
        	$(".updateComp").hide();
        	
        	 var cancel = "<div class='row'>" +
        		"	<div class='col-md-12'>" +
        		"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelCompUpdate(1)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
        		"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateCompanyDetails("+res.id+","+contact+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
        		"	</div>" +
        		"</div>";
        	 $("#cancelButton1").append(cancel); 
        	 
        	loadCompany("view_comp",res.name,res.type,res.website,res.isHolding,res.country.id,res.notes);
        	loadCompany("update_comp",res.name,res.type,res.website,res.isHolding,res.country.id,res.notes);
        	
        	$("#company_viewFiles").empty();
        	var str1 =  " <div id='view_files'></div><div class='row'>" +
        				"	<div class='col-md-9'></div>";
        	if(user ==  "admin")
        	{
        	str1 +=     "	<div class='col-md-3' id='update2'>" +
        				"		<a href='#' onclick='updateCompDetails(2)'><span class='glyphicon glyphicon-edit'></span> add files</a>" +
        				"	</div>";
        	}
        	str1 +=   " </div>";
        	str1 += "<div class='updateFiles'><div id='update_files'></div><div class='form-group row' id='cancelButton2'></div></div></div></div>";
        	
        	$("#company_viewFiles").append(str1);
        	$(".updateFiles").hide();
        	
        	var cancel1 = "<div class='row'>" +
        		"	<div class='col-md-12'>" +
        		"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelCompUpdate(2)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Cancel</button></div>" +
        		"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='insertCompanyFiles("+res.id+",1)'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
        		"	</div>" +
        		"</div>";
        	 $("#cancelButton2").append(cancel1); 
        	 
        	 $.ajax({
        	 	url:"/market_observation/company/viewCompFiles",
        	    type: "GET",
        	    data: "id="+res.id,
        	    success: function (result) {
        	    	
        	    	if(result.length == 0){		
        	    		loadFiles("update_files",0,res.id,0,"","","","","");
        	    	}else{
        	    		for (i=0;i<result.length;i++){
        			    	loadFiles("view_files",0,res.id,result[i].id,result[i].fileType,result[i].fileDesc,result[i].fileName,result[i].fileExt,result[i].fileYear);
        			    	//loadFiles("update_files",0,res.id,result[i].id,result[i].fileType,result[i].fileDesc,result[i].fileName,result[i].fileExt,result[i].fileYear);
        		    	}
        	    	}
        	    },
                error: function (result) {
                	console.log("failed");
                }
             });
        	 
        	//--------
        	 $("#alm_seller").empty();
        		var str21 =  " <div id='view_seller'></div><div class='row'>" +
        					"	<div class='col-md-9'></div>";
        		if(user ==  "admin")
        		{
        		str21 +=     "	<div class='col-md-3' id='update21'>" +
        					"		<a href='#' onclick='updateCompDetails(21)'><span class='glyphicon glyphicon-edit'></span> add seller</a>" +
        					"	</div>";
        		}
        		str21 +=   " </div>";
        		str21 += "<div class='updateSeller'><div id='update_seller'></div><div class='form-group row' id='cancelButton21'></div></div></div></div>";
        		
        		$("#alm_seller").append(str21);
        		$(".updateSeller").hide();
        		
        		var cancel21 = "<div class='row'>" +
        			"	<div class='col-md-12'>" +
        			"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelCompUpdate(21)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Cancel</button></div>" +
        			"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='insertALMSeller("+res.id+",21)'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
        			"	</div>" +
        			"</div>";
        		 $("#cancelButton21").append(cancel21); 
        		 
        		 $.ajax({
        			 	url:"/market_observation/company/viewALMSeller",
        			    type: "GET",
        			    data: "id="+res.id,
        			    success: function (result) {
        			    	
        			    	if(result.length == 0){		
        			    		loadCompDesc("update_seller",0,"","","");		
        			    	}else{
        			    		for (i=0;i<result.length;i++){
        			    			loadCompDesc("view_seller",i,result[i].appname,result[i].almDesc,"readOnly")
        				    	}
        			    	}		    	
        			    },
        		        error: function (result) {
        		        	console.log("failed");
        		        }
        		     });
        	//--------
        	 

        	//--------
        	 $("#alm_provider").empty();
        		var str22 =  " <div id='view_provider'></div><div class='row'>" +
        					"	<div class='col-md-9'></div>";
        		if(user ==  "admin")
        		{
        		str22 +=    "	<div class='col-md-3' id='update22'>";
        		var str23 =   	"		<a href='#' onclick='updateCompDetails(22)'><span class='glyphicon glyphicon-edit'></span> add provider</a>" ;
        		str22 +=   "	</div>";
        		}
        		str22 +=   " </div>";
        		str22 += "<div class='updateProvider'><div id='update_provider'></div><div class='form-group row' id='cancelButton22'></div></div></div></div>";
        		
        		$("#alm_provider").append(str22);
        		$(".updateprovider").hide();
        		
        		var cancel22 = "<div class='row'>" +
        			"	<div class='col-md-12'>" +
        			"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelCompUpdate(22)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Cancel</button></div>" +
        			"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='insertALMProvider("+res.id+",22)'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
        			"	</div>" +
        			"</div>";
        		 $("#cancelButton22").append(cancel22); 
        	
        		 $.ajax({
        			 	url:"/market_observation/company/viewALMProvider",
        			    type: "GET",
        			    data: "id="+res.id,
        			    success: function (result) {
        			    	
        			    	if(result.length == 0){
        			    		$("#update22").append(str23);
        			    		loadOffers("update_provider","",0,"");	
        			    		
        			    	}else{
        			    		loadOffers("view_provider",result.appName,result.almProvider.id,"readOnly")
        			    	}
        			    },
        		        error: function (result) {
        		        	console.log("failed");
        		        }
        		     });
        	//--------
        		 
        	
        	$("#company_shreholders").empty();
        	var str2 =  " <div id='view_shareHolders'></div><div class='row'>" +
        				"	<div class='col-md-9'></div>";
        	if(user ==  "admin")
        	{
        	str2 +=     "	<div class='col-md-3' id='update3'>" +
        				"		<a href='#' onclick='updateCompDetails(3)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
        				"	</div>";
        	}
        	str2 +=   " </div>";
        	str2 += "<div class='updateSholders'><div id='update_shareHolders'></div><div class='form-group row' id='cancelButton3'></div></div></div></div>";
        	
        	$("#company_shreholders").append(str2);
        	$(".updateSholders").hide();
        	
        	 var cancel2 = "<div class='row'>" +
        		"	<div class='col-md-12'>" +
        		"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelCompUpdate(3)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
        		"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='insertShareholders("+res.id+",1)'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
        		"	</div>" +
        		"</div>";
        	 $("#cancelButton3").append(cancel2); 
        	
        	$.ajax({
        	 	url:"/market_observation/company/viewCompShares",
        	    type: "GET",
        	    data: "id="+res.id,
        	    success: function (result) {
        	    	console.log(result);
        	    	for (i=0;i<result.length;i++){
        	    		var valueId = 0;
        	    		if(result[i].type == 1){
        	    			valueId =  result[i].csCompany.id;
        	    		}else if(result[i].type == 2){
        	    			valueId =  result[i].csInd.id;
        	    		}
        	    		console.log("type : " + result.length);
        	    		loadCompShareholders("view_shareHolders",i,0,result[i].type,valueId,result[i].percentage,result.length);
        	    		loadCompShareholders("update_shareHolders",i,result[i].id,result[i].type,valueId,result[i].percentage,result.length);
        	    	}
        	    	
        	    	if(result.length == 0){
        	    		loadCompShareholders("update_shareHolders",0,0,0,0,0,1);
        	    	}
        	    },
                error: function (result) {
                	console.log("failed");
                }
            });
        	
        	$("#company_contact_details").empty();
        	var str3 =  "<div id='view_ContactDetails'></div>" +
        			    "   <div class='row'>" +
        				"		<div class='col-md-9'></div>";
        	if(user ==  "admin")
        	{
        	str3 +=     "		<div class='col-md-3' id='update4'>" +
        				"			<a href='#' onclick='updateCompDetails(4)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
        				"		</div>";
        	}
        	str3 +=     "</div></div>";
        	str3 += 	"<div class='updateConDetails'>" +
        				"	<div id='update_contactDetails'></div>" +
        				"	<div class='form-group row' id='cancelButton4'></div>" +
        				"</div>";
        	
        	$("#company_contact_details").append(str3);
        	$(".updateConDetails").hide();
        	
        	var str4 = "<div id='display_Company_Contact'><div class='row'><div class='col-md-9'>Contact Address 1 : "+getContactsData(res.contact,1)+"</div></div>"+
        			    "<div class='row'><div class='col-md-9'>Contact Address 2 : "+getContactsData(res.contact,2)+"</div></div>" +
        			    "<div class='row'><div class='col-md-9'>Contact Address 3 : "+getContactsData(res.contact,3)+"</div></div>" +
        			    "<div class='row'><div class='col-md-6'>Email Address  : "+getContactsData(res.contact,4)+"</div>" +
        			    "<div class='col-md-6'>LinkedIn : "+getContactsData(res.contact,5)+"</div></div>" +
        			    "<div class='row'><div class='col-md-6'>Skype ID : "+getContactsData(res.contact,6)+"</div>" +
        			    "<div class='col-md-6'>Youtube : "+getContactsData(res.contact,8)+"</div></div>" +
        			    "<div class='row'><div class='col-md-6'>Twitter Account : "+getContactsData(res.contact,7)+"</div></div></div>";
        	 $("#view_ContactDetails").append(str4);
        	 
        	 var contactId = 0;
        	 if(res.contact != null){
        		 contactId = res.contact.id; 
        	 }
        	 var str5 =  "Contact Address :<br/>" +
        		         "<input class='form-control' type='hidden' id='comp_contactID' value='"+contactId+"'>"+
        		         "<div class='row'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1  <input type='text' class='address' value='"+getContactsData(res.contact,1)+"' id='addr1'></div>" +
        			     "<div class='row'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2  <input type='text' class='address'  value='"+getContactsData(res.contact,2)+"'id='addr2'></div>" +
        			     "<div class='row'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3  <input type='text' class='address'  value='"+getContactsData(res.contact,3)+"'id='addr3'></div><br/>" +
        			     "<div class='row'><div class='col-md-6'>Email &nbsp;&nbsp;: <input class='contact' type='text'  value='"+getContactsData(res.contact,4)+"'id='email'></input></div>" +
        			     "<div class='col-md-6'>LinkedIn &nbsp;: <input type='text' class='contact'  value='"+getContactsData(res.contact,5)+"'id='linkedIn'></input></div></div>" +
        			     "<div class='row'><div class='col-md-6'>Skype &nbsp; : <input type='text' class='contact'  value='"+getContactsData(res.contact,6)+"'id='skype'></input></div>" + 
        			     "<div class='col-md-6'>Youtube &nbsp;&nbsp;: <input type='text' class='contact'  value='"+getContactsData(res.contact,8)+"'id='youtube'></input></div></div>" +
        				 "<div class='row'><div class='col-md-6'>Twitter &nbsp;: <input type='text' class='contact' id='twitter' value='"+getContactsData(res.contact,7)+"'></input></div></div>";

            $("#update_contactDetails").append(str5);	
        	
        	 var cancel3 = "<div class='row'>" +
        		"	<div class='col-md-12'>" +
        		"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelCompUpdate(4)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
        		"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateCompContacts("+res.id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
        		"	</div>" +
        		"</div>";
        	 $("#cancelButton4").append(cancel3); 
        	 getCompanyPhones(res.id,user);
        },
        error: function (result) {
        	console.log("error");
	     }
	  });
	
	
}

function getCompanyPhones(id,user){
	$("#company_pnone_numbers").empty();
	$.ajax({
	 	url:"/market_observation/company/phones",
	    type: "POST",
	    data: "id="+id,
	    success: function (result) {
	    	console.log("contact :");
	    	var resLength = result.length;
	    	var str = "<div id='display_Phones'>";
	    	for (i=0;i<resLength;i++){
	    		str += " <div class='form-group'>" +
	    			   " <div class='row'>" +
	    			   "	<div class='col-md-6'>Phone Type: "+result[i].phone.phoneType+"</div>" +
	    		       "	<div class='col-md-6'>Country Code: "+result[i].phone.country.code+"</div>" +
	    		       " </div>" +
	    		       " <div class='row'>" +
	    		       "	<div class='col-md-6'>Area Code: "+result[i].phone.areaCode+"</div>" +
	    		       "	<div class='col-md-6'>Phone Number: "+result[i].phone.number+"</div>" +
	    		       " </div></div>" ;
	    	}
	    	    str += " <div class='row'>" +
	    		   	   "	<div class='col-md-9'></div>";
			if(user == "admin"){
			  str +=   "	<div class='col-md-3' id='update5'>" +
			           "		<a href='#' onclick='updateCompDetails(5)'><span class='glyphicon glyphicon-edit'></span> update</a>" +
			           "	</div>";
			}
			  str +=   " </div>" +
			           "</div>";
	    	str += "<div class='updatePhones'><div class='form-group row' id='update_phones'></div><div class='form-group row' id='cancelButton5'></div></div></div></div>";
        	
	       
	        $("#company_pnone_numbers").append(str);
	        $(".updatePhones").hide();
	        
	        if(resLength != 0){
	        	console.log( "----" +resLength);
		        for (i=0;i<resLength;i++){	
		        	var count = (i);
		       
		        	loadContactNumbers("update_phones",count,result[i].phone.country.id,resLength - 1);
		        	document.getElementById("ind_phone_id_" + count).value = result[i].phone.id;
		        	document.getElementById("ind_phone_num_" + count).value = result[i].phone.number;
	        		document.getElementById("ind_phone_area_" + count).value = result[i].phone.areaCode;
	        		document.getElementById("ind_phone_type_" + count).value = result[i].phone.phoneType;
	        	}
	        }else{
	        	loadContactNumbers("update_phones",0,0,resLength);
	        }
	        
	        var cancel = "<div class='row'>" +
			"	<div class='col-md-12'>" +
			"		<div class='col-md-6 pull-left'><button type='button' class='btn btn-default btn-sm' onclick='cancelCompUpdate(5)'><span class='glyphicon glyphicon-step-backward'></span>&nbsp;&nbsp;Back</button></div>" +
			"		<div class='col-md-6 pull-right'><button type='button' class='btn btn-default btn-sm' onclick='updateNumbers("+id+")'><span class='glyphicon glyphicon-ok'></span>&nbsp;&nbsp;Save</button></div>" +
			"	</div>" +
			"</div>";
       
            $("#cancelButton5").append(cancel); 
        
	     },
	     error: function (result) {
	        console.log("failed");
	     }
    });
}

function deleteCompDetails(id){
	$.ajax({
	 	url: "/market_observation/company/getCompany",
	    type: "GET",
	    data:"id="+id,
        success: function (res) {
        	
        	$("#comp_list").show();
        	$("#comp_removal").empty();
            
        	var str = "<div class='modal fade' id='removeModal' role='dialog'>" + 
        	   		"<div class='modal-dialog'>"+
        	   		"<div class='modal-content'>"+
        	   		"	<div class='modal-header'>"+
        	   		"		<h4 class='modal-title'>Remove Company?</h4>"+
        	   		"	</div>"+
        	   		"	<div class='modal-body'>" +
        	   		"			Do you really want to remove company : <br/> <strong>" + res.name+ "</strong> of <strong>" + res.country.name + "</strong>" + 
        			"	</div>"+
                   	"	<div class='modal-footer'>"+
                	"	 	<button onClick='removeCompany(\""+res.id+"\")' id='removeButton' class='btn btn-danger'>Delete Company</button>"+
                   	"		<button type='button' onClick='okButton()' class='btn btn-default' data-dismiss='modal' >Cancel</button>"+
                   	"	</div>"+
                   	"</div></div></div>";
        	
        	$("#comp_removal").append(str);
        	$("#removeModal").modal();
        }
	});
	
}


function additionalUpdate(count,type){
	var buttonId = type+"_"+ count;
	count++;
	
	if(type == 3){
		loadContactNumbers("update_phones",count,0,0);
	}
	
	$("#add_"+buttonId+"").hide();
	$("#cancel_"+buttonId+"").hide(); 
}

function cancelNumbers(count,type){

	$("#morePhones_"+ type +"_"+ count +"").remove(); 
	
	count--;
	
	$("#add_"+ type +"_"+ count +"").show();
	$("#cancel_"+ type +"_"+ count +"").show();  
}

function okButton(){
	
	 $(".modal").modal("hide");
	 $(".modal-backdrop").hide();
	 $('html, body').css({
		   'overflow': 'auto',
		   'height': 'auto'
	});
}

function createCompany(){
	 $.ajax({
		 	url:"/market_observation/company/add",
		    data: "&name="+document.getElementById(("name")).value+
	    		  "&region="+document.getElementById(("region")).value+
	    		  "&code="+document.getElementById(("code")).value,
		    type: "POST",
		    success: function (result) {
		           if(result.ok){
		        	   companyList();
		        	   $("#newCompanyBtn").show();
		        	   $("#CompanyList").show();
		        	 }
		           else{
	        		  console.log("error in updating db");
		        	}
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}

function editCompany(companyId){
	 $.ajax({
		 	url:"/market_observation/company/update",
		    data: "id="+companyId+
	    	"&name="+document.getElementById(("name_"+companyId)).value+
	    	"&region="+document.getElementById(("region_"+countryId)).value+
	    	"&code="+document.getElementById(("code_"+countryId)).value,
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   location.reload();
		        	   companyList();
		        	   $("#newCompanyBtn").show();
		        	   $("#CompanyList").show();
		        	 }
		           else{
	        		  console.log("error in updating dba");
		        	}
		    	console.log(result);
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}


function removeCompany(companyId){
	$.ajax({
	 	url:"/market_observation/company/delete",
	    type: "POST",
	    data: "id="+companyId,
	    success: function (result) {
           if(result.ok){
        	   location.reload();
        	   companyList();
           }
           else{
        		  console.log("error in updating dba");
	        	}
	    	console.log(result);
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
	});
}

function updateCompContacts(companyID){

	$.ajax({
	 	url:"/market_observation/company/updateCompanyContact",
	    data: "id="+document.getElementById(("comp_contactID")).value+
	    	  "&address1="+document.getElementById(("addr1")).value+
    		  "&address2="+document.getElementById(("addr2")).value+
    		  "&address3="+document.getElementById(("addr3")).value+
    		  "&email="+document.getElementById(("email")).value+
    		  "&linkedIn="+document.getElementById(("linkedIn")).value+
    		  "&skype="+document.getElementById(("skype")).value +
    		  "&youtube="+document.getElementById(("youtube")).value+
    		  "&twitter="+document.getElementById(("twitter")).value+
    		  "&companyID="+companyID,
	    type: "POST",
	    success: function (result) {
	           if(result.ok){
	        	   viewDetails(companyID);
	           }
	           else{
        		  console.log("error in updating db");
	        	}
	        },
	        error: function (result) {
	        	console.log("failed");
	        }
	    });
}

function updateCompanyDetails(companyID,contactID){
	
	 $.ajax({
		 	url:"/market_observation/company/updateCompanyDetails",
		 	data: "id="+companyID+
		 		  "&name="+document.getElementById("comp_name1").value+
		   		  "&type="+document.getElementById("comp_type_update_comp").value+
		   		  "&website="+document.getElementById("comp_wbsite").value+
		   		  "&notes="+document.getElementById("comp_note").value+
		   		  "&isActive="+true+
		   		  "&isHolding="+document.getElementById("comp_holding").value+
		   		  "&country.id="+document.getElementById("companyBase").value+
		   		  "&inputBy="+document.getElementById("inputBy").value+
		   		  "&inputdate="+document.getElementById("inputDate").value+
			      "&contact.id="+contactID,
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(companyID);
		        	 }
		           else{
	        		  console.log("error in updating db");
		        	}
		    	console.log(result);
		        },
		        error: function (result) {
		        	console.log("failed");
		        }
	    });
}

function updateNumbers(companyID){
	 $.ajax({
		 	url:"/market_observation/phone/updatePhone",
		    data: "numbers="+getPhoneNum()+
		          "&id="+parseInt(companyID),
		    type: "POST",
		    success: function (result) {
		    	if(result.ok){
		    		   viewDetails(companyID);
	        	}else{
	        		console.log("error in updating db");
	        	}
		     },
	         error: function (result) {
	        	console.log("failed");
	         }
	 });
}

function getPhoneNum(){
	var totalPhone = $(".ind_additionalPhones").length;
	var numbers = new Array();
	
	for(i = 0; i < totalPhone; i++){
		var mainDiv = "morePhones_3_"+i;
		var pType = document.getElementById("ind_phone_type_"+i).value;
		var cCode = document.getElementById("ind_phone_3_loc_"+i).value;
		var aCode = document.getElementById("ind_phone_area_"+i).value;
		var pNum = document.getElementById("ind_phone_num_"+i).value;
		var id = document.getElementById("ind_phone_id_"+i).value;
		
		numbers[i] = mainDiv + "-" + pType + "-" + cCode + "-" +aCode + "-" +pNum + "-" +id;
	}
	return numbers;
}

function viewDetails(companyID){

	viewCompany(companyID);
	
}

function viewList(){
	
	$("#comp_action").empty();
	
	var str = "<br/><div class='row'><div class='col-xs-4'><input type='button' value='Back to List' onclick='getList()'></div></div>";
	
	  $("#comp_action").append(str);
}

function getList(){
	location.reload();
	$("#comp_details").hide();
	$("#comp_list").show();
}

