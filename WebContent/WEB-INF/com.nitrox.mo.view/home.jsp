<div class="container" style="height:750px">
  	<img alt="" class="img-responsive center-block" src="${pageContext.request.contextPath}/images/logo.png">  
    <div class="row"> 
        <div class="col-xs-8 col-xs-offset-2">     	
		    <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    	<span id="search_concept">Filter by</span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#contains" style="color:#999999;">Individual</a></li>
                      <li><a href="#its_equal" style="color:#999999;">Company</a></li>
                      <li><a href="#greather_than" style="color:#999999;">All</a></li>
                    </ul>
                </div>
                <input type="hidden" name="search_param" value="all" id="search_param">         
                <input type="text" class="form-control" name="x" placeholder="Search ...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" style="height:34px"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>
	</div>
</div>
