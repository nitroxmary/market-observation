<div class="container" style="width:80%">	

  <div id="meeting_list"></div> 
  <div id="meeting_details">
       <input type="hidden" value="0" id="userType"></input>
       <div id="meeting_removal"></div>
       <div id="meeting_confirmed"></div>
 	   <div id="meeting_moreDetails"></div>
 	   <div id="meeting_individual"></div>
 	   <div id="meeting_action"></div>
 	   
     <script type="text/javascript">
    	var user = "${isAdmin}"
    		document.getElementById("userType").value = user;
    </script>
    
  </div> 
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"> </script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/meeting.js"> </script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/meeting_list.js"> </script> 