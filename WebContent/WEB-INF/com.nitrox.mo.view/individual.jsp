<div class="container" style="width:1000px">	
  <div class="form-group row">
        <div class="col-xs-12">
    		<strong>NEW INDIVIDUAL:<br/></strong><hr/>
  		</div>
    </div>
   <div class='form-group row'>                   
		<label class='col-xs-3 col-form-label'>First Name</label>
	   <div class='col-xs-8'>
			<input class='form-control' type='text' id='first_name'>
	   </div>
	</div>
	<div class='form-group row'>
		<label class='col-xs-3 col-form-label'>Last Name</label>
		<div class='col-xs-8'>
			<input class='form-control' type='text' id='last_name'>
	  </div>
	</div>
	   <div class='form-group row'>                   
		<label class='col-xs-3 col-form-label'>Middle Initial</label>
		<div class='col-xs-8'>
				<input class='form-control' maxlength="1" type='text' id='middle_initial'>
		</div>
	</div>
	 <div class='form-group row'>
		<label class='col-xs-3 col-form-label'>Country/Location</label>
		<div class='col-xs-8'>
				<div id='individualLocation'></div>
		</div>
	</div>
	<div>
	    COMPANY INVOLVEMENT <strong>:</strong><hr/>
		<div class='form-group row'>
			<div class='form-group row' id="compInvolvement"></div>
		</div>
    </div>
	<div>
		EDUCATIONAL BACKGROUND <strong>:</strong><hr/>
	   <div class='form-group row'>
			<div class='form-group row' id="education"></div>
		</div>
	</div>
	<div>
	    AWARDS <strong>:</strong><hr/>
		<div class='form-group row'>
			<div class='form-group row' id=awards></div>
		</div>
    </div>
	<div>
	    NATIONALITIES <strong>:</strong><hr/>
		<div class='form-group row'>
			<div class='form-group row' id="nationalities"></div>
		</div>
    </div>
    <div>
	    LANGUAGES <strong>:</strong><hr/>
		<div class='form-group row'>
			<div class='form-group row' id="languages"></div>
		</div>
    </div>
    <div>
	    INDIVIDUAL CONTACT DETAILS <strong>:</strong><hr/>
		<div class='form-group row'>
			<div class='form-group row' id="indContacts"></div>
		</div>
    </div>

	<br/><br/>
	<div><button onClick='newIndividual()' id='individualButton' class='btn'>Save</button></div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"> </script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/individual.js"> </script> 