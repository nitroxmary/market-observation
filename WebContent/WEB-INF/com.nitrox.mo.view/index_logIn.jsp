<%@include file="required/header_logIn.jsp" %>
<div class="jumbotron">
	<div id="page"></div>
	<div id="user"></div>
</div>
<%@include file="required/modal.jsp" %>
<%@include file="required/footer.jsp" %>


<script type="text/javascript">
$(document).ready(function(){
	var page = "${page}", modalValue = "${modal}";
	
	$( "#page" ).load( "${pageContext.request.contextPath}/"+page, function(){
	});
	
	if(modalValue == "logIn"){
		var str = "<div id='log-in'></div>"
		$("#user").append(str);
	}
	if(modalValue == "registerUser"){
		var str = "<div id='register'></div>"
		$("#user").append(str);
	}
	
});
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/log_in.js"> </script> 
