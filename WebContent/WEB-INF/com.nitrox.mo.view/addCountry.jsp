<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>More Country</title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/countries.js"> </script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.cookie.js"> </script>
    <script type="text/javascript">
        function doAjaxPost() {
        // get the form values
        var name = $('#name').val();
        var education = $('#region').val();
        var code = $('#code').val();

        $.ajax({
        type: "POST",
        url: "/market_observation/countries/add",
        data: "name=" + name + "&education=" + education,
        success: function(response){
        // we have the response
        $('#info').html(response);
        $('#name').val('');
        $('#region').val('');
        $('#code').val('');
        },
        error: function(e){
        alert('Error: ' + e);
        }
        });
        }
    </script>
</head>
<body>
<table>
      <tr><td>Country Name : </td><td> <input type="text" id="name"><br/></td></tr>
      <tr><td>Country Region : </td><td> <input type="text" id="region"><br/></td></tr>
      <tr><td>Country Code : </td><td> <input type="text" id="code"><br/></td></tr>
      <tr><td colspan="2"><input type="button" value="Add Country" onclick="doAjaxPost()"><br/></td></tr>
      <tr><td colspan="2"><div id="info" style="color: green;"></div></td></tr>
</table>
</body>
</html>