<%@include file="required/header.jsp" %>

<div class="jumbotron">
	<div id="page"></div>
</div>
<%@include file="required/modal.jsp" %>
<%@include file="required/footer.jsp" %>


<script type="text/javascript">
$(document).ready(function(){
	var page = "${page}";
	
	$( "#page" ).load( "${pageContext.request.contextPath}/"+page, function(){
	});
	
	
});
</script>